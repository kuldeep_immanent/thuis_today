<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('freelancer'))
{
    function freelancer($user_id)
    {
        $ci = & get_instance();
        $ci->load->model('user_model');
        //$this->load->model('Dashboard_model');
        $data = $ci->user_model->freelancer_detail($user_id);
        if(!empty($data))
        {
        	return $data->user_name;
        }
        else
        {
        	return "No Name";
        }
    }   
}