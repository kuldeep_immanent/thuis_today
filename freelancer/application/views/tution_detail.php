<script type="text/javascript"src="<?php echo base_url(); ?>assest/css/datatables/js/jquery.dataTables.min.js"></script>
<script>
      $(document).ready(function()
      {
          $('#myTable').dataTable();
      });
</script>    
    <head> 
      <link rel="stylesheet" href="<?php echo base_url(); ?>assest/css/datatables/css/jquery.dataTables.min.css"></style>
    </head>      
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
              <h2>Tution list</h2>
              <div class="clearfix"></div>
        </div>
          <div class="x_content">
            <?php 
                $lerror = $this->session->flashdata('error_msg');
                  if(isset($lerror))
                  {
                      echo '<div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.'</div>'; 
                  }
             ?>  
             
                 <p>Teacher Name: <?php echo $tution_detail->t_first.' '.$tution_detail->t_last; ?></p> 
                 <p>Student Name: <?php echo $tution_detail->u_first.' '.$tution_detail->u_last; ?></p> 
                 <p>Time: <?php echo $tution_detail->time; ?></p> 
                 <p>Date: <?php echo $tution_detail->date; ?></p> 
                 <p>Subject: <?php echo $tution_detail->subject; ?></p> 
                 <p>Budget: <?php echo $tution_detail->budget; ?></p> 
                 <p>Status: <?php 
                 switch ($tution_detail->notification_type)
                 {
                            case 0:
                                echo "Request";
                                break;
                            case 1:
                                echo "Acknowledge";
                                break;
                            case 2:
                                echo "Confirm";
                                break;
                            case 3:
                                echo "Success";
                                break;
                            case 4:
                                echo "Initiated";
                                break;
                            case 5:
                                echo "Decline";
                                break;
                            case 6:
                                echo "Payment Success";
                                break;
                            case 7:
                                echo "Start Tution";
                                break;
                            case 8:
                                echo "Stop Tution";
                                break;
                            case '-1':
                                echo "Reschedule";
                                break;
                            case 10:
                                echo "Cancelled";
                                break;                 
                 }  ?> </p> 
                 
            </div>
        </div>
    </div>
  </div>
