	<script type="text/javascript"src="<?php echo base_url(); ?>assest/css/datatables/js/jquery.dataTables.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assest/date/include/ui-1.10.0/ui-lightness/jquery-ui-1.10.0.custom.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assest/date/jquery.ui.timepicker.css?v=0.3.3" type="text/css" />
	<script type="text/javascript" src="<?php echo base_url(); ?>assest/date/include/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assest/date/include/ui-1.10.0/jquery.ui.core.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assest/date/include/ui-1.10.0/jquery.ui.widget.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assest/date/include/ui-1.10.0/jquery.ui.tabs.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assest/dateinclude/ui-1.10.0/jquery.ui.position.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assest/date/jquery.ui.timepicker.js?v=0.3.3"></script>
<div class="right_col" role="main">
	<div class="row">
	<div class="col-md-12">
	<?php  
        $lerror = $this->session->flashdata('error_msg');
          if(isset($lerror)){
                echo '<div class="alert alert-danger">
                   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.
                   '</div>';
                            }
      ?>
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			Add New Shop
		</h3>
		<!---<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="">Home</a> 
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
                <i class="fa fa-cutlery"></i>
                <a href="">Shop Name</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li><a href="#"><i class="fa fa-plus"></i>Add New Shop</a></li>
		</ul>--->
		<!-- END PAGE TITLE & BREADCRUMB-->
	</div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form action="<?php echo base_url(); ?>admin/Dashboard/add_newshop" class="form-horizontal" method="post" enctype="multipart/form-data">
            <div class="portlet solid light-grey">
                <div class="portlet-body">
                    <h3><i class="fa fa-chevron-circle-right" style="vertical-align: middle;"></i>Shop Info:</h3>
                    <hr/>
               
					<div class="form-group">
                        <label for="res_name" class="control-label col-md-2">*Shop Name:</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-cutlery"></i></span>
								<input class="form-control" type="text" name="res_name_ar" placeholder="Enter Shop title / name here in Dutch" required>
                            </div>
                        </div>
                    </div>
               
					<div class="form-group">
                        <label for="res_description" class="control-label col-md-2">Description:</label>
                        <div class="col-md-8">
						<input style="height: 100px;" class="form-control" type="text" name="res_description_ar" placeholder="Describe Shop specialities and any other information here in Dutch" required>
                         
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="res_name" class="control-label col-md-2">Contact Email:</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                    <input class="form-control" type="text" name="res_contact" placeholder="Enter contact email address here" required|is_unique>
                            </div>
                        </div>
                    </div>
                      <div class="form-group">
                        <label for="res_delivery_fees" class="control-label col-md-2">*Shop Category:</label>
                        <div class="col-md-2">
                            <div class="input-group"> 
                           <!--<input class="form-control" type="text" name="res_contact" placeholder="Enter contact email address here" required>-->
							<select name="category" class="form-control" required>
							<option value="-1">-select shop category-</option>
							<?php if (!empty($category_name)) {foreach ($category_name as $result){ ?>
							<option value="<?php echo $result->category_id; ?>"><?php echo $result->category_name; ?></option>
							<?php }}?>
							</select>
                             
                            </div>
                        </div>
                    </div>
                    
            <div class="portlet solid light-grey">
                <div class="portlet-body">
                    <h3><i class="fa fa-chevron-circle-right" style="vertical-align: middle;"></i>Shop Address:</h3>
                    <hr/>
                  <div class="form-group">
                        <label for="res_address" class="control-label col-md-2">*Address</label>
                        <div class="col-md-8">
						 <input id="address" style="height:100px;" class="form-control" type="textarea" name="res_address_ar" placeholder="Enter Shop full address except location and city in Dutch" required>
                        </div>
                    </div>
					<!--------------------------------
                 <div class="form-group">
                        <label for="res_location" class="control-label col-md-2">*Location / Region:</label>
                        <div class="col-md-10">
                             <div class="input-group" style="width:79%">
                                <input id="gmaps-input-address" class="form-control" type="text" name="mapsearch" placeholder="Start typing a place name...">   
                           <div id='gmaps-error'></div>
                              </div>
                              
                                <div id="gmaps-canvas" style="height: 400px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden;">
                               
                              </div>
                        </div>
                    </div>
					--------------->
                   </div>
           
            <div class="portlet solid light-grey">
                <h3><i class="fa fa-chevron-circle-right" style="vertical-align: middle;"></i> Shop Admin Account Details:</h3>
                <p>This account is used to manage the Items served by Shop and other settings.</p>
                <hr />
                <div class="form-group">
                    <label for="res_admin_name" class="control-label col-md-2">*Admin User Name:</label>
                    <div class="col-md-8">
					<input class="form-control" type="text" name="res_admin_name" placeholder="Enter admin full name">   
                     <span class="error"><?php echo form_error('res_admin_name'); ?></span>   
                    </div>
                </div>
                <div class="form-group">
                    <label for="res_admin_email" class="control-label col-md-2">*Email Address:</label>
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">@</span>
							<input class="form-control" type="text" name="res_admin_email" placeholder="Enter admin email address" required>   
                         
                        </div>
                        <!--<div class="help-block">Email address entered here will be verified, and login details will be dispatched after verification is successful.</div>-->
                    </div>
                </div>
                <div class="form-group">
                    <label for="res_admin_phone" class="control-label col-md-2">Contact Number:</label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
							<input class="form-control" type="text" name="res_admin_phone" placeholder="Multiple numbers must be separated by comma(,)" required>
                            
                        </div>
                    </div>
                </div>
				 <div class="form-group">
                    <label for="res_admin_phone" class="control-label col-md-2">Enterance Fee:</label>
                    <div class="col-md-8">
                        <div class="input-group">
							<input id="enterance_fee" class="form-control" type="number" name="res_admin_enterance" value="<?php echo $enterance_fee->entrancefee; ?>" min="<?php echo $enterance_fee->entrancefee; ?>">
                            <span id="enterance_errror"></span>
                        </div>
                    </div>
                </div>
                
            <h3><i class="fa fa-chevron-circle-right" style="vertical-align: middle;"></i>Password setting:</h3>
                    <hr />
                    <div class="form-group">
                        <label for="res_address" class="control-label col-md-2">*Enter password</label>
                        <div class="col-md-8">
						<input class="form-control" type="password" name="res_password" placeholder="Enter password" required>
                        </div>
                    </div>    
					<div class="form-group">          
					<label for="res_address" class="control-label col-md-2">*Enter confirm password</label>   
					<div class="col-md-8">  
                      <input class="form-control" type="password" name="res_con_pass" placeholder="Enter confirm password" required>					
					</div>            
					</div> 					
                
              </div>	
			  </div>
            <button type="submit" class="btn btn-lg green"><i class="fa fa-check"></i>Add this Shop</button>
        
    </div>
</div>
</form>
</div>
</div>
</div>
<script src="http://cdn.kendostatic.com/2015.1.429/js/kendo.all.min.js"></script>
<script>
 $(".timings").kendoTimePicker();
$('#res_delivery_time').timepicker();
$('#res_extra_time').timepicker();
$('#open_time').timepicker();
$('#close_time').timepicker();
</script>

