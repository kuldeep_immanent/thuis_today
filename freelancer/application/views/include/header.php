<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Freelancer-admin | </title>

  <!-- Bootstrap core CSS -->

  <link href="<?php echo base_url(); ?>assest/css/bootstrap.min.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>assest/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assest/css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assest/css/style.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="<?php echo base_url(); ?>assest/css/custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assest/css/maps/jquery-jvectormap-2.0.3.css" />
  <link href="<?php echo base_url(); ?>assest/css/icheck/flat/green.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assest/css/floatexamples.css" rel="stylesheet" type="text/css" />

  <script src="<?php echo base_url(); ?>assest/js/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assest/js/nprogress.js"></script>
 <script src="<?php echo base_url(); ?>assest/js/script.js"></script>
  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo base_url('admin/Dashboard'); ?>" class="site_title"><i class="fa fa-paw"></i> <span>Freelancer</span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
           <!--  <div class="profile_pic">
              <img src="<?php echo base_url(); ?>assest/images/img.jpg" alt="..." class="img-circle profile_img">
            </div> -->
            <div class="profile_info">
              <span>Welcome,</span>
              <h2><?php echo $freelancer_name = $_SESSION['user_data']['freelancer_name']; ?></h2>
            </div>
          </div>
          <!-- /menu prile quick info -->
          <br />
          <br />
          <br />
          <br />
