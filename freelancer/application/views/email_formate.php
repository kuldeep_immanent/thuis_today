<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>

<table border='0' cellspacing="0" cellpadding="0" style='width:632px; text-align:center; margin: 15px auto;'>
			<tr><td style=' background:url(<?php echo base_url(); ?>/form-images/head.png); margin-bottom: 0; text-align: left;'>
			<img src="<?php echo base_url(); ?>/assest/images/logo-thuis-today.png" style='margin:25px 0 13px 40px;max-height:67px;'></td></tr>
			<tr><td style=' background:url(<?php echo base_url(); ?>/form-images/bdr-hd.png); margin-bottom: 0;margin-top: 0;'>&nbsp;</td></tr> 
			<tr><td style='text-align:left; background:url(<?php echo base_url(); ?>/form-images/mid.png);padding: 10px 35px;font-size: 17px; margin-bottom: 0;margin-top: 0;'>

<?php echo $freelancer_name; ?> has updated your Shop Information. <br/>Your updated shop details are as follows: </td></tr>
<tr><td>
		<table border='0'  style='text-align:left; background:url(<?php echo base_url(); ?>/form-images/mid.png);padding: 0 35px; margin-bottom: 0;margin-top: 0; width:100%;'>
			<tr class="mail">
			<th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 100px;'>Shop Name:</th>
			<td style='text-align:left;font-size: 16px;padding: 8px 0;'>
			<?php echo $res_name_ar; ?>
			</td>
			</tr>
            <tr class="mail">
			<th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 100px;'>Description:</th>
			<td style='text-align:left;font-size: 16px;padding: 8px 0;'>
			<?php echo $res_description_ar; ?>
			</td>
			</tr>
            <tr class="mail">
			<th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 100px;'>Contact Email:</th>
			<td style='text-align:left;font-size: 16px;padding: 8px 0;'>
			<?php echo $res_contact; ?>
			</td>
			</tr>
            <tr class="mail">
			<th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 100px;'>Shop Category:</th>
			<td style='text-align:left;font-size: 16px;padding: 8px 0;'>
			<?php echo $category_namesingle; ?>
			</td>
			</tr>
            <tr class="mail">
			<th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 100px;'>Address:</th>
			<td style='text-align:left;font-size: 16px;padding: 8px 0;'>
			<?php echo $address_ar; ?>
			</td>
			</tr>
            <tr class="mail">
			<th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 100px;'>Admin User Name:</th>
			<td style='text-align:left;font-size: 16px;padding: 8px 0;'>
			<?php echo $res_admin_name; ?>
			</td>
			</tr>
            <tr class="mail">
			<th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 100px;'>Email Address:</th>
			<td style='text-align:left;font-size: 16px;padding: 8px 0;'>
			<?php echo $res_admin_email;?>
			</td>
			</tr>
            <tr class="mail">
			<th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 100px;'>Contact Number:</th>
			<td style='text-align:left;font-size: 16px;padding: 8px 0;'>
			<?php echo $res_admin_phone;?>
			</td>
			</tr>
            <?php if($user_type == 't'){?>
		   <tr class="mail">
			<th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 100px;'>Enterance Fee:</th>
			<td style='text-align:left;font-size: 16px;padding: 8px 0;'>
			<?php echo $entrancefee; ?>
			</td>
			</tr>
			 <tr class="mail">
			<th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 100px;'>Password:</th>
			<td style='text-align:left;font-size: 16px;padding: 8px 0;'>
			<?php echo $res_password; ?>
			</td>
			</tr>
			<?php } ?>
			
           
            </table>
            </td>
            </tr>
            
            <tr><td>
		<table border='0'  style='text-align:left; background:url(<?php echo base_url(); ?>/form-images/mid.png);padding: 0 35px; margin-bottom: 0;margin-top: 0; width:100%;'>
			<tr>
			<th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 310px;'> Please click this link to login your account:</th>
			<td style='text-align:left;font-size: 16px;padding: 8px 0;'>
			<a href="http://deliveryapp.demodemo.ga/restaurant/admin/login">http://deliveryapp.demodemo.ga/restaurant/admin/login</a>
			</td>
			</tr>  
            </table>
            </td>
            </tr>
            
            
            
            
				<tr><td style='text-align: center; width: 100%; padding: 1px 0 20px;background:url(<?php echo base_url(); ?>/form-images/bdr-mid.png) repeat scroll -9px 0 rgba(0, 0, 0, 0);margin-bottom: 0;margin-top: 0;'>
			<span style=' border-top:1px dashed #ccc; width:100%;'>&nbsp;</span>
			</td></tr>
			
			<tr><td style='width:100%;color:#000;text-decoration:none;text-align:center;background:url(<?php echo base_url(); ?>/form-images/foot.png) repeat scroll -9px 0 rgba(0, 0, 0, 0);padding: 5px 0;height: 77px;
    margin: 0;margin-bottom: 0;margin-top: 0;'>Copyright 2016 � HOME TODAY. All rights reserved.</td></tr>
		
		</table>
</body>
</html>
