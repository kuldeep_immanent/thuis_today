 <head>
<style>
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>
</head>
 <script type="text/javascript">
    function confirmDeletion(eid)
      {
        var result = confirm("Are you sure you want to Delete this user?");
        if(result)
          {  
            document.location.href="<?php echo base_url('admin/user/deactivation'); ?>/"+eid;
           return true;
          }
        else
          {
            return false;
          }
      } 
  </script>   
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
              <h2>User Detail</h2>
              <div class="clearfix"></div>
        </div>
          <div class="x_content">
            <?php  
                $lerror = $this->session->flashdata('error_msg');
                  if(isset($lerror))
                  {
                      echo '<div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.'</div>'; 
                  }
             ?>  
                 <p>Profile Pic:<div><?php echo $user_detail->profile_pic ? '<img src="'.base_url().'uploads/'.$user_detail->profile_pic.'" height="100px";  width="100px"; >' : '<img src="'.base_url().'assest/images/no-image.jpeg" height="100px"; width="100px"; >'; ?></div></p>
                 <p>Name: <?php echo $user_detail->first_name.' '.$user_detail->last_name; ?></p> 
                 <p>Email: <?php echo $user_detail->email; ?></p> 
                 <p>Age: <?php echo $user_detail->age; ?></p> 
                 <p>Address: <?php echo $user_detail->address; ?></p> 
                 <p>Gender: <?php if($user_detail->gender == 'm'): echo "Male"; else: echo "Female"; endif; ?></p>                   
                 <p>Tution Type: <?php switch ($user_detail->tution_type) { case 'pr_sc': echo "Primary"; break;
                 case 'sec_sc': echo "Secondary"; break; case 'clg': echo 'College'; break; } ?></p>
                 <p>Mobile No: <?php echo $user_detail->mobile_no; ?></p>
                 <?php if($user_detail->user_type == 't'){ ?>
                 <p>User Type: Teacher</p>
                 <p>Tution Rate: <?php echo $user_detail->tution_rate; ?></p>
                 <p>Experience: <?php echo $user_detail->experience; ?></p>
                 <?php } else {?> <p>User Type: Student</p> <?php } ?>
                 <p>Proof id:<div><?php echo $user_detail->proof_id ? '<img src="'.base_url().'uploads/'.$user_detail->proof_id.'" height="100px"; id="myImg" width="100px"; >' : '<img src="'.base_url().'assest/images/no-image.jpeg" height="100px"; width="100px"; >'; ?></div></p>
                 <p><a href="<?php echo base_url();?>admin/user/activation/<?php echo $user_detail->id;?>" class="btn btn-info">Activation</a><button onclick="confirmDeletion(<?php echo $user_detail->id; ?>)" class="btn btn-info">Deactivation</button></p>
            </div>
        </div>
    </div>
  </div>
<div id="myModal" class="modal">
  <span class="close">×</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script>