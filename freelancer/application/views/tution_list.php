<script type="text/javascript"src="<?php echo base_url(); ?>assest/css/datatables/js/jquery.dataTables.min.js"></script>
<script>
      $(document).ready(function()
      {
          $('#myTable').dataTable();
      });
</script>    
    <head> 
      <link rel="stylesheet" href="<?php echo base_url(); ?>assest/css/datatables/css/jquery.dataTables.min.css"></style>
    </head>      
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
              <h2>Tution list</h2>
              <div class="clearfix"></div>
        </div>
          <div class="x_content">
            <?php 
                $lerror = $this->session->flashdata('error_msg');
                  if(isset($lerror))
                  {
                      echo '<div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.'</div>'; 
                  }
             ?>  
                  <table id="myTable" class="table table-striped responsive-utilities jambo_table table table-bordered"> 
                    <thead>
                      <tr class="headings">
                        <th>Teacher Name</th>
                        <th>Student Name</th>
                        <!-- <th>Date</th> -->
                        <th>Status</th>
                        <th>Action</th>                        
                      </tr>
                    </thead>
                    <tbody>
              <?php if (!empty($tution_detail)) {foreach ($tution_detail as $result){ ?>
                <tr class="odd pointer">
                <td><?php echo $result->t_first.' '.$result->t_last;?></td>
                <td><?php echo $result->u_first.' '.$result->u_last; ?></td>
                <!-- <td><?php// echo $result->date; ?></td> -->
                <td><?php switch ($result->notification_type)
                 {
                            case 0:
                                echo "Request";
                                break;
                            case 1:
                                echo "Acknowledge";
                                break;
                            case 2:
                                echo "Confirm";
                                break;
                            case 3:
                                echo "Success";
                                break;
                            case 4:
                                echo "Initiated";
                                break;
                            case 5:
                                echo "Decline";
                                break;
                            case 6:
                                echo "Payment Success";
                                break;
                            case 7:
                                echo "Start Tution";
                                break;
                            case 8:
                                echo "Stop Tution";
                                break;
                            case '-1':
                                echo "Reschedule";
                                break;
                            case 10:
                                echo "Cancelled";
                                break;                 
                 }
                ?>
                </td>
                <td> <a class="btn btn-info" href="<?php echo base_url();?>admin/tution/tution_detail/<?php echo $result->id;?>">Detail</a></td>             
               </tr>
         <?php } }?>           
                    </tbody>
            </table>
            </div>
        </div>
    </div>
  </div>
