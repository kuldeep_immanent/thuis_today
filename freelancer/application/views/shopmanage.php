<script type="text/javascript"src="<?php echo base_url(); ?>assest/css/datatables/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    function confirmDeletion(eid)
      {
        var result = confirm("Are you sure you want to Delete this event?");
        if(result)
          {  
            document.location.href="<?php echo site_url('admin/dashboard/del_event'); ?>/"+eid;
           return true; 
          }
        else
          {
            return false;
          }
      } 
  </script>   
    <script>
      $(document).ready(function()
      {
          $('#myTable').dataTable();
      });
    </script>    
    <head> 
      <link rel="stylesheet" href="<?php echo base_url(); ?>assest/css/datatables/css/jquery.dataTables.min.css"></style>
    </head>              

<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
              <h2>Shop list</h2>
			  <a class="btn btn-info gh" style="float: right;" href="<?php echo base_url();?>admin/dashboard/add_shop">Add New Shop</a>
              <div class="clearfix"></div>
        </div>
          <div class="x_content">
            <?php  
                $lerror = $this->session->flashdata('error_msg');
                  if(isset($lerror))
                  {
                      echo '<div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.'</div>'; 
                  }
             ?>  
                  <table id="myTable" class="table table-striped responsive-utilities jambo_table table table-bordered" >  
                    <thead>
                      <tr class="headings">
                        <th>Shop Name</th>
						<th>Shop Category</th>
                        <th>Address</th>
                        <th>Status</th>
                        <th>Actions</th>   
                      </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($shop_list)) { foreach ($shop_list as $result){ ?>
                          <tr class="odd pointer">
                          <td><?php echo $result->restaurant_name_ar; ?></td>
                          <td><?php echo $result->category_name; ?></td>
						  <td><?php echo $result->address_ar; ?></td>
                          <td><?php if($result->status == 1){echo "Active";}else{echo "Inactive";}?></td>
                          <td> <a class="btn btn-info" href="<?php echo base_url();?>admin/dashboard/edit_newshop/<?php echo $result->restaurant_id;?>">Edit</a></td>
                          </tr>
                        <?php } } ?>  
                    </tbody>
              </table>
            </div>
        </div>
    </div>
  </div>
