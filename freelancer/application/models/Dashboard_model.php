<?php
class Dashboard_model extends CI_Model
{
  /**************************************************
   -----function to get all users details-----
 *******************************************/ 
 function userDetail()
   { 
     $where = array('is_verified' => 1);
     $query = $this->db->get_where('registration', $where); 
     return $query->num_rows() > 0 ? $query->result() : false;        
   }
   /***********************************************************
          --function for to get single user details--
  ***********************************************************/ 
 function singleUserData($id)
   { 
     $where = array('id' => $id);
     $query = $this->db->get_where('registration', $where);
     return $query->num_rows() > 0 ? $query->row() : false;      
   }
/***************************************************************
 ---Activate user account with update his account status---
 ********************************************************/ 
 function accountActivate($id)
   { 
     $data = array('status'=>1);  
     $this->db->where('id', $id);   
     $this->db->update('registration', $data);   
     return true;     
   }

/***************************************************************
      ---Delete user account with his unique id---
 ********************************************************/ 
 function accountDeactivate($id)
   { 
     $this->db->where('id', $id);   
     $this->db->delete('registration');   
     return true;     
   }
//  /****************************************************
//    -------function for Check Data Invoices -------
//  *******************************************/ 
//   public function checkData()
//     {
//       $this->db->select('*');
//       $query = $this->db->get('create_invoice');
//       return $query->row();       
//     }

//  /************************************************************
//    -----function for Check Data for Goods or Service ----
//  ***************************************************/ 

//   public function checkDataService()
//     {
//       $this->db->select('*');
//       $query = $this->db->get('goods_services');
//       return $query->row();       
//     }

//  /***********************************************************
//    -------function for Check Data for request payment ----
//  *******************************************/ 
//   public function checkDataPayment()
//     {
//       $this->db->select('*');
//       $query = $this->db->get('request_payment');
//       return $query->row();       
//     }
}
