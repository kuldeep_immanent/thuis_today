<?php
class Tution_model extends CI_Model
{
  /**************************************************
   -----function to get all tutions Detail----
 *******************************************/ 
 function tutionDetail()
   { 
    $query = $this->db->query("SELECT `notification`.*, `teacher`.`first_name` AS 
      t_first, `teacher`.`last_name` AS t_last, `user`.`first_name` AS 
      u_first, `user`.`last_name` AS u_last
      FROM notification
      INNER JOIN registration AS teacher ON `notification`.`teacher_id` = `teacher`.`id`
      INNER JOIN registration AS user ON `notification`.`user_id` = `user`.`id`");  
    return $query->num_rows() > 0 ? $query->result() : false; 
   }

  /**************************************************
   -----function to get single tution details -----
 *******************************************/ 
  function singleTutionDetail($id)
   { 
     $query = $this->db->query("SELECT `notification`.*, `teacher`.`first_name` AS 
      t_first, `teacher`.`last_name` AS t_last, `user`.`first_name` AS 
      u_first, `user`.`last_name` AS u_last
      FROM notification
      INNER JOIN registration AS teacher ON `notification`.`teacher_id` = `teacher`.`id`
      INNER JOIN registration AS user ON `notification`.`user_id` = `user`.`id` WHERE `notification`.`id` = '$id'");  
    return $query->num_rows() > 0 ? $query->row() : false; 
    }
  /**************************************************
   -----function for insert Invoice template-----
 *******************************************/ 
 function getStudentId()
   { 
    $data = array();
    $query = $this->db->get_where('notification');     
    if($query->num_rows() > 0)
     {  
       $result = $query->result();  
      foreach($result as $value)
       {       
        $data[] = $value->user_id;     
       } 
       return $data;
     }
     else
     {
      return false;
     }
   }
    /**************************************************
   -----function for insert Invoice template-----
 *******************************************/ 
 function getStudentDetail($id)
   { 
          $where = array('id' => $id); 
          $query = $this->db->get_where('registration', $where);  
          return $query->num_rows() > 0 ? $query->row() : false;     
   } 
}        
