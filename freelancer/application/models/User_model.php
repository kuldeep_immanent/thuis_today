<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model{
 
     public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }
	 //***************register_data function start*********************//
	 function register_data($register = array(),$subject)
	 {
		   $count=count($subject);
	       $this->db->insert("registration",$register);  
           $last_id=$this->db->insert_id();
		   //echo $last_id;die;
		   for($i=0;$i<$count;$i++)
		   {
		   $sub = array("user_id"=>$last_id,"subject_name"=>$subject[$i]);
		   $this->db->insert("subject",$sub);
		   }
			//return $this->db->insert_id();
			$this->db->select('*');
			$this->db->from('registration');
			$this->db->where('id',$last_id);
			$result = $this->db->get();
				if($result->num_rows() > 0)
				{
				$result = $result->result(); 
				return $result;
				//print_r($result);die;
				}
				else
				{
					return false;
				}
	 
	 }
	 //******************************************************************//	  
	public function verify_user($id)
	 {
		$data=array('is_verified'=>1);
	    $this->db->where('id', $id);
	    $this->db->update('registration', $data);
	     return true;
	 }
	  public function check_verify($email,$password)
	 {
		$this->db->select('*');
		$this->db->from('registration');
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$this->db->where('is_verified',1);
		$query = $this->db->get();
            if($query->num_rows()>0)
                { 
                   return $result = $query->result_array();
				  
				}
              else
                {
                  return false;
                }
	 }
	  public function check_verify_admin($email,$password)
	 {
	    $this->db->select('*');
		$this->db->from('registration');
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$this->db->where('status',1);
		$query = $this->db->get();
            if($query->num_rows()>0)
                { 
                   return $result = $query->result_array();
				  
				}
              else
                {
                  return false;
                }
	 }


//******************************************************************//
	  //***************register_data function end*********************//
	  /////////////////////////////////////////////////////////////////
	  //***************mail_exists function start*********************//
	 	   function mail_exists($key)
		{
		 $this->db->select('*');
		$this->db->from('registration');
		$this->db->where('email', $key);
		$result = $this->db->get();
		if($result->num_rows() > 0)
		{
		return $result->result();  //row
        }
		else
		{
			return false;
		}
		}
		//***************mail_exists function end*********************//
		///////////////////////////////////////////////////////////////
		//***************cheak_user_detail function start*********************//
    function cheak_user_detail($email, $password, $device_id, $device_type)
	{
		$this->db->select('*');
		$this->db->from('registration');
		$this->db->where('email', $email);
		$this->db->where('password', $password);
	
       $query = $this->db->get();
		if($query->num_rows() == 1)
		{
		     /////////////////////////////////////////
			    $data11 = array('device_id'=>$device_id, 'device_type' => $device_type);
				$this->db->where('email', $email);
				$this->db->where('password', $password);
			    $this->db->update('registration',$data11);			   
			///////////////////////////////////////////////
		     //$data['profile'] = $query->row();
			 /////////updae data////////////
			 $this->db->select('*');
		     $this->db->from('registration');
		     $this->db->where('email', $email);
		     $this->db->where('password', $password);
	         $queryd = $this->db->get();
			 $data['profile'] = $queryd->row();
			 /////////updae data////////////
		     $id = $data['profile']->id;
			 $rating = $this->rating($id);
			  if($rating->rating == "")
					  {
					  $data['rating'] = "0";
					  }
					  else
					  {
					  $data['rating'] = $rating->rating;
					  }
		     $data['subjects'] =$this->get_subjects($id);	
            return $data;   
		}
		else
		{
			return false;
		}
    }
	//***************cheak_user_detail function end*********************//
	//////////////////////////////////////////////////////////////////////
	//***************forget function start*********************//
	function forget($email)
	{
		$this->db->select('*');
		$this->db->from('registration');
		$this->db->where('email',$email);
		$query = $this->db->get();
		if($query -> num_rows() == 1)
		{
		   return $query->row();
		}
		else
		{
		   return false;
		}
	}
	//***************forget function end*********************//
	//////////////////////////////////////////////////////////
	//***************old_password function start*********************//
	function old_password($old_password,$user_id,$new_password)
	{
	    $this->db->select('*');
		$this->db->from('registration');
		$this->db->where('id', $user_id);
		$this->db->where('password', $old_password);
	    $query = $this->db->get();
		if($query->num_rows() == 1)
		{
		    return $query->row();
		}
		else
		{
			return false;
		}
	}
	//***************old_password function end*********************//
	//////////////////////////////////////////////////////////
	//***************update_password function start*********************//
	function update_password($user_id, $new_password)
	{
	$data=array('password'=>$new_password);
    $this->db->where('id',$user_id);
    $this->db->update('registration',$data);
     return TRUE;
	}
	//***************update_password function end*********************//
	//////////////////////////////////////////////////////////
	//***************edit_profiles function start*********************//
	 function edit_profiles($data,$subject,$id)
	  {
	  	  
		   $count=count($subject);
		   
		   //print_r($subject);die;
		   //echo $count;die;
		   $this->db->where('id',$id);
           $this->db->update('registration',$data);
		   $this->db->where('user_id', $id);
		   //if($count > 0)
		  // {
			   $this->db->delete('subject');
			   for($i=0;$i<$count;$i++)
			   {
			   $sub = array("user_id"=>$id,"subject_name"=>$subject[$i]);
			   $this->db->where('user_id',$id);
			   $this->db->insert("subject",$sub);
			   }
		  // }
		   return $data = $this->get_data($id);
			//return $this->db->insert_id();
	  }
	  function get_data($id)
	  {
		    $this->db->select('*');
		   $this->db->from('registration');
		   $this->db->where('id',$id);
		   $query = $this->db->get();
		   if($query->num_rows() == 1)
			{
			   //return $query->row();
			   $data['profile'] = $query->row();
			   $data['subjects'] =$this->get_subjects($id);	
			   return $data;
//print_r($data);
//die;			   
			}
			else
			{
				return false;
			}
	  }
	  //***************edit_profiles function end*********************//
	//////////////////////////////////////////////////////////
	//***************insert_otp function start*********************//
     public function insert_otp($user_id, $otp)
	  { 
	    //$date = date_default_timezone_set("Asia/Kuala_Lumpur");//for maleshiya//
	  	$currentTime = date('Y-m-d H:i:s');
	  	$query = $this->db->get_where('verify_otp', array('user_id' => $user_id))->row();
	  	if(!empty($query))
		  {
		  	$this->db->where('user_id', $user_id);
		  	$this->db->update('verify_otp', array('otp'=>$otp, 'time'=>$currentTime));
		  	return true;
		  }
	  	else
	  	  {
		  	$this->db->insert('verify_otp', array('user_id' =>$user_id , 'otp'=>$otp, 'time'=>$currentTime));
		  	return true;
	 	  }

	  }
	   //***************insert_otp function end*********************//
	//////////////////////////////////////////////////////////
	//***************get_otp function start*********************//

	  public function get_otp($user_id, $otp)
	  {
           $query = $this->db->get_where('verify_otp' , array('user_id' =>$user_id , 'otp'=>$otp));
            return $query->num_rows() > 0 ? $query->row() : false;

	  }
	   //***************get_otp function end*********************//
	//////////////////////////////////////////////////////////
	//***************delete_otp function start*********************//

	  public function delete_otp($user_id)
	  {

	  	$this->db->where('user_id', $user_id);
	  	$this->db->delete('verify_otp');
	  }
	  	   //***************delete_otp function end*********************//
	//////////////////////////////////////////////////////////
	//***************teacher_profile function start*********************//
	  function teacher_profile($teacher_id)
	 {
		$this->db->select('*');
		$this->db->from('registration');
		$this->db->where('id', $teacher_id);
		$query = $this->db->get();
        if($query ->num_rows() > 0)
			{
				$data['profile'] =$query ->result_array();
				$data['subject'] =$this->get_subjects($teacher_id);
				$rating =$this->rating($teacher_id);
				if($rating->rating == "")
					  {
					  $data['rating'] = "0";
					  }
					  else
					  {
					  $data['rating'] = $rating->rating;
					  }
				return $data;
			}
		else
			{
				return false;
			}
	 }
	 //***************teacher_profile function end*********************//
	//////////////////////////////////////////////////////////
	//***************get_subjects function start*********************//
	 function get_subjects($id)
	 {
		 $this->db->select('subject_name');
		 $this->db->from('subject');
		 $this->db->where('user_id',$id);
		 $result = $this->db->get()->result_array();
		 return $result;
	 
	 }
	  //***************get_subjects function end*********************//
	  /////////////////////////////////////////////////////////////////
	  //***************rating function start*********************//
	 function rating($id)
	 {
		 
		 /*$this->db->select('rating');
		 $this->db->from('teacher_review');
		 $this->db->where('teacher_id',$id);
		 $result = $this->db->get()->result_array();
		 return $result;*/
		 $query = $this->db->query("SELECT floor(avg(`rating`)) as `rating` FROM `teacher_review` WHERE `teacher_id`=$id");
		 return $result = $query->row();
	 
	 }
	  //***************rating function end*********************//
	//////////////////////////////////////////////////////////
	//***************send_review function start*********************//
	 function send_review($data)
	 {
		 $this->db->insert("teacher_review",$data);
		 return $this->db->insert_id();
	 }
	 //***************send_review function end*********************//
	//////////////////////////////////////////////////////////
	//***************teacher_review function start*********************//
	 function teacher_review($teacher_id,$offset)
	 {
		$this->db->select('*');
		$this->db->from('teacher_review');
		$this->db->where('teacher_id',$teacher_id);
		$this->db->limit($offset);
		$result = $this->db->get()->result_array();
		return $result;
	 }
	 //***************send_review function end*********************//
	//////////////////////////////////////////////////////////
	//***************user_pic function start*********************//
	function user_pic($user_id)
	{
		$this->db->select('*');
		$this->db->from('registration');
		$this->db->where('id',$user_id);
		$result = $this->db->get()->row();
		return $result;
	}
	//***************user_pic function end*********************//
	//////////////////////////////////////////////////////////
	//***************send_request function start*********************//
	 function send_request($data)
	 {
		 $this->db->insert('notification',$data);
		 return $this->db->insert_id();
	 }
	  //***************send_request function end*********************//
	//////////////////////////////////////////////////////////
	//***************notification function start*********************//
	 function notification($notifi_type, $id, $offset,$initiated_id,$is_started)
	 {
	
	     if($notifi_type == 5 || $notifi_type == 6)
		 {
			 $this->db->select('*');
			 $this->db->from('registration');
			 $this->db->where('id',$id);
			 $results = $this->db->get()->row();
			if($results->user_type == 't')
            {				
			 $this->db->select('*');
			 $this->db->from('notification');
			 $this->db->where('teacher_id', $id);
			 $this->db->where('notification_type', $notifi_type);
			 $this->db->limit(25,$offset);
             $result = $this->db->get()->result_array();
			 return $result;
			 }
			 else if($results->user_type == 's')
			 {
			 $this->db->select('*');
			 $this->db->from('notification');
			 $this->db->where('user_id', $id);
			 $this->db->where('notification_type', $notifi_type);
			 $this->db->limit($offset);
             $result = $this->db->get()->result_array();
			 return $result;
			 }
		 }
		 else if($notifi_type == 0)
		 {
			 $this->db->select('*');
			 $this->db->from('notification');
			 $this->db->where('teacher_id',$id);
			 $this->db->where('notification_type',$notifi_type);
			 $this->db->limit($data['offset']);
             $result = $this->db->get()->result_array();
			 return $result;
		 }
		  else if($notifi_type == 2)
		  {
		    $this->db->select('*');
			 $this->db->from('notification');
			 $this->db->where('teacher_id',$id);
			 $this->db->where('notification_type',$notifi_type);
			 $this->db->limit($data['offset']);
             $result = $this->db->get()->result_array();
			 return $result;
		  }
		  
		  else if($notifi_type == 1 && $initiated_id == 4 && $is_started == 7)
		  { 
		     $array = array(1,4,7);
			 $this->db->select('*');
			 $this->db->from('notification');
			 $this->db->where('user_id',$id);
			 $this->db->where_in('notification_type',$array);
			 $this->db->limit($data['offset']);
             $result = $this->db->get()->result_array();
			 return $result;
		  }
		  else if($notifi_type == -1)
		  { 
		     $array = array(-1);
			 $this->db->select('*');
			 $this->db->from('notification');
			 $this->db->where('user_id',$id);
			 $this->db->where_in('notification_type',$array);
			 $this->db->limit($data['offset']);
             $result = $this->db->get()->result_array();
			 return $result;
		  }
		  else if($notifi_type == 10)
		 {
			 $this->db->select('*');
			 $this->db->from('registration');
			 $this->db->where('id',$id);
			 $results = $this->db->get()->row();
			if($results->user_type == 't')
            {				
			 $this->db->select('*');
			 $this->db->from('notification');
			 $this->db->where('teacher_id', $id);
			 $this->db->where('notification_type', $notifi_type);
			 $this->db->limit(25,$offset);
             $result = $this->db->get()->result_array();
			 return $result;
			 }
			 else if($results->user_type == 's')
			 {
			 $this->db->select('*');
			 $this->db->from('notification');
			 $this->db->where('user_id', $id);
			 $this->db->where('notification_type', $notifi_type);
			 $this->db->limit($offset);
             $result = $this->db->get()->result_array();
			 return $result;
			 }
		 }
		 
	 }
	  //***************notification function end*********************//
	//////////////////////////////////////////////////////////
	//***************user function start*********************//
	 function user($data)
	 {
		 $this->db->select('*');
		 $this->db->from('registration');
		 $this->db->where('id',$data['user_id']);
		 return $result = $this->db->get()->row();
	 }
	 //***************user function end*********************//
	//////////////////////////////////////////////////////////
	//***************teacher function start*********************//
	 function teacher($data)
	 {
		 $this->db->select('*');
		 $this->db->from('registration');
		 $this->db->where('id',$data['teacher_id']);
		 return $result = $this->db->get()->row();
	 }
	 function teacher_name($teacher_id)
	 {
		 $this->db->select('*');
		 $this->db->from('registration');
		 $this->db->where('id',$teacher_id);
		 return $result = $this->db->get()->row();
	 }
	 function teacher_id($notification_id)
	 {
		 $this->db->select('*');
		 $this->db->from('notification');
		 $this->db->where('id',$notification_id);
		 return $result = $this->db->get()->row();
	 }
	 //***************teacher function end*********************//
	//////////////////////////////////////////////////////////
	//***************accept function start*********************//
	 function accept($data)
	 {
		//$date = date_default_timezone_set("Asia/Kuala_Lumpur");//for maleshiya//
		 $currentTime = date('H:i:s');
		 $update=array('status'=>1,'notification_type'=>1,'notification_message'=>$data['notification_message'],'time'=>$currentTime);
		 $this->db->where('user_id',$data['user_id']);
		 $this->db->where('teacher_id',$data['teacher_id']);
		 $this->db->where('id',$data['notification_id']);
		 $this->db->update('notification',$update);
		 return true;
	 }
	  //***************accept function end*********************//
	//////////////////////////////////////////////////////////
	//***************decline function start*********************//
	 function decline($data)
	 {
		//$date = date_default_timezone_set("Asia/Kuala_Lumpur");//for maleshiya//
	     $currentTime = date('H:i:s');
		 $update=array('status'=>5,'notification_type'=>5,'notification_message'=>$data['notification_message'],'time'=>$currentTime);
		 $this->db->where('user_id',$data['user_id']);
		 $this->db->where('teacher_id',$data['teacher_id']);
		 $this->db->where('id',$data['notification_id']);
		 $this->db->update('notification',$update);
		 return true;
	 }
	 //***************decline function end*********************//
	 //***************acknowledge function start*********************//
	 function acknowledge($data)
	 {
		//$date = date_default_timezone_set("Asia/Kuala_Lumpur");//for maleshiya//
	     $currentTime = date('H:i:s');
		 $update=array('status'=>2,'notification_type'=>2,'notification_message'=>$data['notification_message'],'time'=>$currentTime);
		 $this->db->where('user_id',$data['user_id']);
		 $this->db->where('teacher_id',$data['teacher_id']);
		 $this->db->where('id',$data['notification_id']);
		 $this->db->update('notification',$update);
		 return true;
	 }
	 //***************acknowledge function end*********************//
	 //***************confirm function start*********************//
	 function confirm($data)
	 {
		//$date = date_default_timezone_set("Asia/Kuala_Lumpur");//for maleshiya//
	     $currentTime = date('H:i:s');
		 $update=array('status'=>3,'notification_type'=>3,'notification_message'=>$data['notification_message'],'time'=>$currentTime);
		 $this->db->where('user_id',$data['user_id']);
		 $this->db->where('teacher_id',$data['teacher_id']);
		 $this->db->where('id',$data['notification_id']);
		 $this->db->update('notification',$update);
		 return true;
	 }
	 //***************confirm function end************************//
       //////////////////////////////////////////////////////////
	//***************search_teacher function start*****************//
	  function search_teacher($subject)
	 {
		 $this->db->select('user_id');
		 $this->db->from('subject');
		 $this->db->like('subject_name',$subject);
		 $result = $this->db->get()->result_array();
		 $str="";
		 foreach ($result as $arr) 
			 {
			   $str .= $arr["user_id"] . ",";
			 }
			 if(!empty($str))
				 {
				   $str = trim($str, ',');
				   $sql="select * from registration where id in ($str) AND user_type='t'";
				   $query=$this->db->query($sql);
				   return $results = $query->result_array();

			      }
		 return false;
	 }
	 //***************search_teacher function end*********************//
	 ////////////////////////////////////////////////////////////////////
	 //***************budget function start*********************//
	 function budget($data)
	 {
		 $this->db->select('*');
		 $this->db->from('notification');
		 $this->db->where('user_id',$data['user_id']);
		 $this->db->where('teacher_id',$data['teacher_id']);
		 return $result = $this->db->get()->row();
	 }
	 //***************budget function end*********************//
	  ////////////////////////////////////////////////////////////////////
	 //***************user_teacher function start*********************//
	 function user_teacher($teacher_id,$user_id,$subject)
	 {
		$this->db->select('*');
		$this->db->from('notification');
		$this->db->where('teacher_id', $teacher_id);
		$this->db->where('user_id', $user_id);
		$this->db->where('subject', $subject);
	    $query = $this->db->get();
		if($query->num_rows() > 0)
		{
		    $data = $query->row();
			if($data->notification_type == 0 || $data->notification_type == 1)
			{
			return $data;
			}
	    }
     }
	 //***************user_teacher function end*********************//
	  ////////////////////////////////////////////////////////////////////
	 //***************all_subjects function start*********************//
	 function all_subjects()
	 {
		$this->db->select('*');
		$this->db->from('all_subjects');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		    return $query->result();
	    }
		else
		{
			return false;
		}
	 }
	 //***************all_subjects function end*********************//
	  ////////////////////////////////////////////////////////////////////
	 //***************recent_tasks function start*********************//
	 function recent_tasks($teacher_id,$offset)
	 {
		$status =array(3,6,4);
		$this->db->select('*');
		$this->db->from('notification');
		$this->db->where('teacher_id',$teacher_id);
		//$where = '(status=1 or status =2 or status =3)';
        $this->db->where_in('notification_type',$status);
		$this->db->limit(25,$offset);
        $result = $this->db->get()->result_array();
	    return $result;
		//echo $this->db->last_query();
		//die;
	 }
	  //***************recent_tasks function end *********************//
	  ///////////////////////////////////////////////////////////////////
	  //***************notification_data function start *********************//
	/* function notification_data($user_id)
	 {
		$this->db->select('*');
		$this->db->from('notification');
		$this->db->where('user_id', $user_id);
	    $query = $this->db->get();
		if($query->num_rows() > 0)
		{
		    return $data = $query->row();
		}
     }*/
	 //***************notification_data function end*********************//
	 /////////////////////////////////////////////////////////////////////
	 //***************reschedule function start*********************//
	 function reschedule($notification_id,$date,$tution_time,$message)
	 {
		$currentTime = date('H:i:s');
		$data=array('notification_type'=>-1,'message'=>$message,'date'=>$date,'tution_time'=>$tution_time,'time'=>$currentTime);
		$this->db->where('id',$notification_id);
		$this->db->update('notification',$data);
		$query = $this->db->get_where('notification', array('notification_type' => -1, 'message' => $message, 'date'=>$date, 'tution_time'=>$tution_time));
		return $query->num_rows() > 0 ? $query->row() : false;
	 }
	 //***************notification_data function end*********************//
	 /////////////////////////////////////////////////////////////////////
	 //***************reschedule function start*********************//
	 function reschedule_update($notification_message,$notification_id)
	 {
		$currentTime = date('H:i:s');
		$data=array('notification_message'=>$notification_message, 'time'=>$currentTime);
		$this->db->where('id',$notification_id);
		$this->db->update('notification',$data);
		return true;
	 }
	 //***************notification_data function end*********************//
	 /////////////////////////////////////////////////////////////////////
	 //***************reschedule function start*********************//
	 function push_notification($user_id,$on_off)
	 {
		 $update=array('push_notification'=>0);
		 $this->db->where('id',$user_id);
		 $this->db->update('registration',$update);
		 return true;
	 }
	 //***************notification_data function end*********************//
	 /////////////////////////////////////////////////////////////////////
	 //***************reschedule function start*********************//
	 function initiate_tution($data)
	 {
	    $this->db->insert('initiate_tution',$data);
		return $this->db->insert_id();
	 }
	 //***************initiate_tution function end*********************//
	 /////////////////////////////////////////////////////////////////////
	 //***************update_notification function start*********************//
	 function update_notification($teacher_id,$user_id,$subject,$notification_id,$notification_message)
	 {
	     //$currentTime = date('H:i:s');
		 $update=array('status'=>4,'notification_type'=>4,'notification_message'=>$notification_message);
		 $this->db->where('user_id',$user_id);
		 $this->db->where('teacher_id',$teacher_id);
		 $this->db->where('id',$notification_id);
		 $this->db->update('notification',$update);
		 return true;
	 }
	 //***************update_notification function end*********************//
	 ///////////////////////////////////////////////////////////////
	 //***************transaction function start*********************//
	 function transaction($data,$notification_id,$notification_message)
	 {
		$this->db->insert('transaction',$data);
		//return $this->db->insert_id();
		 $update=array('status'=>1);
		 $this->db->where('notification_id',$notification_id);
		 $this->db->update('initiate_tution',$update);
		 //return true;
		 $array=array('notification_type'=>6, 'status'=>6, 'notification_message'=>$notification_message);
		 $this->db->where('id',$notification_id);
		 $this->db->update('notification',$array);
		 return true;
		
	 }
	 //***************transaction function end*********************//
	 ////////////////////////////////////////////////////////////////
	 //***************start_tution function start*********************//
	 function start_tution($notification_id,$notification_message,$start_tutiontime)
	 {
		 $update=array('status'=>2);
		 $this->db->where('notification_id',$notification_id);
		 $this->db->update('initiate_tution',$update);
		 $array=array('notification_type'=>7, 'status'=>7, 'notification_message'=>$notification_message, 'time'=>$start_tutiontime);
		 $this->db->where('id',$notification_id);
		 $this->db->update('notification',$array);
		 return true;
	 }
	 
	 //***************start_tution function end*********************//
	 //////////////////////////////////////////////////////////////////
	  //***************cancel_tution function start*********************//
	 function cancel_tution($notification_id,$notification_message)
	 {
		 //$update=array('status'=>3);
		 //$this->db->where('notification_id',$notification_id);
		 //$this->db->update('initiate_tution',$update);
		 $array=array('notification_type'=>10, 'status'=>10, 'notification_message'=>$notification_message);
		 $this->db->where('id',$notification_id);
		 $this->db->update('notification',$array);
		 return true;
	 }
	 //*******************cancel_tution function end*************************//
	 ///////////////////////////////////////////////////////////////////
	 //***************stop_tution function start*********************//
	 function stop_tution($notification_id,$notification_message)
	 {
		 $update=array('status'=>3);
		 $this->db->where('notification_id',$notification_id);
		 $this->db->update('initiate_tution',$update);
		 $array=array('notification_type'=>8, 'status'=>8, 'notification_message'=>$notification_message);
		 $this->db->where('id',$notification_id);
		 $this->db->update('notification',$array);
		 return true;
	 }
	 //*******************stop_tution function end*************************//
	 ///////////////////////////////////////////////////////////////////////
	 //***************transaction_history function start*********************//
	 function transaction_history($user_id,$offset)
	 {
		$this->db->select('*');
		$this->db->from('registration');
		$this->db->where('id',$user_id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		    $data = $query->row();
			$user_type = $data->user_type;
			if($user_type == 't')
			{
				$this->db->select('*');
				$this->db->from('transaction');
				$this->db->where('teacher_id',$user_id);
				$this->db->limit(25,$offset);
				$querys = $this->db->get();
				if($querys->num_rows() > 0)
				{
					$datas = $querys->result_array();
					$detail = array();
					foreach($datas as $user_id)
				    {
						$userdata['id'] = $user_id['id'];
						$userdata['teacher_id'] = $user_id['teacher_id'];
						$userdata['user_id'] = $user_id['user_id'];
						$userdata['transaction_id'] = $user_id['transaction_id'];
						$userdata['date'] = $user_id['date'];
						$userdata['time'] = $user_id['time'];
						$userdata['total_amount'] = $user_id['total_amount'];
						$userdata['notification_id'] = $user_id['notification_id'];
						$userdata['status'] = $user_id['status'];
						$array = $this->user_pic($user_id['user_id']);				
						$userdata['first_name'] = $array->first_name;
						$userdata['last_name'] = $array->last_name;
						$userdata['profile_pic'] = $array->profile_pic;
						$userdata['address'] = $array->address;
						$userdata['email'] = $array->email;
						$userdata['gender'] = $array->gender;
						$userdata['user_type'] = $array->user_type;
						$userdata['tution_type'] = $array->tution_type;
						$userdata['mobile_no'] = $array->mobile_no;
						$request = $this->teacher_id($user_id['notification_id']);
						$userdata['tution_time'] = $request->tution_time;
						$userdata['subject'] = $request->subject;
						$userdata['request_time'] = $request->time;
						$userdata['request_date'] = $request->date;
						$detail[] = $userdata;
						}
						return $detail;
				}
			}
			else if($user_type == 's')
			{
				$this->db->select('*');
				$this->db->from('transaction');
				$this->db->where('user_id',$user_id);
				$this->db->limit(25,$offset);
				$querys = $this->db->get();
				if($querys->num_rows() > 0)
				{
					$datas = $querys->result_array();
					$detail = array();
					foreach($datas as $user_id)
				    {
						$userdata['id'] = $user_id['id'];
						$userdata['teacher_id'] = $user_id['teacher_id'];
						$userdata['user_id'] = $user_id['user_id'];
						$userdata['transaction_id'] = $user_id['transaction_id'];
						$userdata['date'] = $user_id['date'];
						$userdata['time'] = $user_id['time'];
						$userdata['total_amount'] = $user_id['total_amount'];
						$userdata['notification_id'] = $user_id['notification_id'];
						$userdata['status'] = $user_id['status'];
						$array = $this->user_pic($user_id['teacher_id']);				
						$userdata['first_name'] = $array->first_name;
						$userdata['last_name'] = $array->last_name;
						$userdata['profile_pic'] = $array->profile_pic;
						$userdata['address'] = $array->address;
						$userdata['email'] = $array->email;
						$userdata['gender'] = $array->gender;
						$userdata['user_type'] = $array->user_type;
						$userdata['tution_type'] = $array->tution_type;
						$userdata['mobile_no'] = $array->mobile_no;
						$request = $this->teacher_id($user_id['notification_id']);
						$userdata['tution_time'] = $request->tution_time;
						$userdata['subject'] = $request->subject;
						$userdata['request_time'] = $request->time;
						$userdata['request_date'] = $request->date;
						$detail[] = $userdata;
						}
						return $detail;
				}
			}
		}
		
	 }
	 /////////////////////////////////////////////////////////////////////////////////
	  //***************function used for single user subjects*********************//
     /////////////////////////////////////////////////////////////////////////////////
	 function user_subjects($user_id)
	 {
		$this->db->select('subject_name');
		$this->db->from('subject');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		    return $query->result();
	    }
		else
		{
			return false;
		}
	 }
	 ///////////////////////////////////////////////////////////////////////////////////////////////
	 ///////////////////////////////////////////////////////////////////////////////////////////////
	 ///////////////////////////////////////////////////////////////////////////////////////////////
	 /////////////////////////////////////////////////////////////////////////////////
	  //***************function used for shop list*********************//
     /////////////////////////////////////////////////////////////////////////////////
	 function shop_list($user_id)
	 {
	        $this->db->select('a.*,b.address_ar,c.category_name,d.status');
            $this->db->from('fos_restaurants a'); 
            $this->db->join('fos_restaurant_address b', 'b.restaurant_id=a.restaurant_id', 'left');
            $this->db->join('fos_shop_categories c', 'b.category=c.category_id', 'left');
			$this->db->join('fos_restaurant_admin_users d', 'd.restaurant_id=a.restaurant_id', 'left');
            $this->db->where('a.reference',$user_id);        
            $query = $this->db->get(); 
            if($query->num_rows() != 0)
            {
                return $query->result();
				
            }
            else
            {
                return false;
            }
	
	 }
	 /////////////////////////////////////////////////////////////////////////////////
	  //***************function used for shop category_name  list*********************//
     /////////////////////////////////////////////////////////////////////////////////
	 function category_name()
	 {
		    $this->db->select('*');
            $this->db->from('fos_shop_categories');
			$query = $this->db->get(); 
            if($query->num_rows() != 0)
            {
                return $query->result();
				
            }
            else
            {
                return false;
            }
	 }
	  /////////////////////////////////////////////////////////////////////////////////
	  //***************function used for shop category_name  list*********************//
     /////////////////////////////////////////////////////////////////////////////////
	 function category_namesingle($category_id)
	 {
		    $this->db->select('*');
            $this->db->from('fos_shop_categories'); 
			$this->db->where('category_id',$category_id);
            $query = $this->db->get(); 
            if($query->num_rows() != 0)
            {
                return $query->row();
				
            }
            else
            {
                return false;
            }
	 }
	 
	 /////////////////////////////////////////////////////////////////////////////////
	  //***************function used for insert new shop *********************//
     /////////////////////////////////////////////////////////////////////////////////
	 function add_newshop($data)
	 {
		 $data1 = array('reference'=>$data['user_id'], 'restaurant_name_ar'=>$data['res_name_ar'], 'restaurant_description_ar' => $data['res_description_ar'], 'contact_email'=>$data['res_contact'], 'entrancefee'=>$data['entrancefee']);
		 $this->db->insert('fos_restaurants',$data1);
		 $last_id = $this->db->insert_id();
			////////////////////////////////////
		$data5 = array('res_id'=>$last_id) ;
		 $this->db->insert('fos_restaurant_setting',$data5);		 
			//////////////////////////////////////////
		 $data2 = array('restaurant_id'=>$last_id, 'category'=>$data['category'], 'address_ar' => $data['address_ar']);
		 $this->db->insert('fos_restaurant_address',$data2);
		 $data3 = array('restaurant_id'=>$last_id, 'user_name' => $data['res_admin_name'], 'user_email'=>$data['res_admin_email'], 'contact_number'=>$data['res_admin_phone'], 'is_active'=>1, 'account_password'=>$data['res_password']);
		 $this->db->insert('fos_restaurant_admin_users',$data3);
		 return $id = $this->db->insert_id();
		
	 }	  
	 /////////////////////////////////////////////////////////////////////////////////
	  //***************function used for insert new shop *********************//
     /////////////////////////////////////////////////////////////////////////////////
	 function update_newshop($data)
	 {
		 //print_r($data);die;
   		 $data1 = array('restaurant_name_ar'=>$data['res_name_ar'], 'restaurant_description_ar' => $data['res_description_ar'], 'contact_email'=>$data['res_contact'], 'entrancefee'=>$data['entrancefee']);
		 $this->db->where('restaurant_id',$data['restaurant_id']);
		 $this->db->update('fos_restaurants',$data1);
		 //echo $afftectedRows = $this->db->affected_rows();die;
		 $data2 = array('restaurant_id'=>$data['restaurant_id'], 'category'=>$data['category'], 'address_ar' => $data['address_ar']);
		 $this->db->where('restaurant_id',$data['restaurant_id']);
		 $this->db->update('fos_restaurant_address',$data2);
		// echo $afftectedRows = $this->db->affected_rows();die;
		 if(isset($data['res_password']))
		 {
		 $data3 = array('restaurant_id'=>$data['restaurant_id'], 'user_name' => $data['res_admin_name'], 'user_email'=>$data['res_admin_email'], 'contact_number'=>$data['res_admin_phone'], 'is_active'=>1, 'account_password'=>$data['res_password']);
		 }
		 else
		 {
			$data3 = array('restaurant_id'=>$data['restaurant_id'], 'user_name' => $data['res_admin_name'], 'user_email'=>$data['res_admin_email'], 'contact_number'=>$data['res_admin_phone'], 'is_active'=>1); 
		 }
		 $this->db->where('restaurant_id',$data['restaurant_id']);
		 $this->db->update('fos_restaurant_admin_users',$data3);
		 //echo $afftectedRows = $this->db->affected_rows();die;
		 //return $afftectedRows = $this->db->affected_rows();
		 return true;
		
	 }
	 /////////////////////////////////////////////////////////////////////////////////
	  //***************function used for check mail  *********************//
     /////////////////////////////////////////////////////////////////////////////////
	 function check_mail($restaurant_id)
	 {
		    $this->db->select('*');
            $this->db->from('fos_restaurant_admin_users'); 
			$this->db->where('restaurant_id',$restaurant_id);
            $query = $this->db->get(); 
            if($query->num_rows() != 0)
            {
               return $query->row();
			}
            else
            {
                return false;
            }
	 }
	 /////////////////////////////////////////////////////////////////////////////////
	  //***************function used for get enterance fees *********************//
     /////////////////////////////////////////////////////////////////////////////////
	 function enterance_fee()
	 {
		    $this->db->select('*');
            $this->db->from('fos_admin_users'); 
            $query = $this->db->get(); 
            if($query->num_rows() != 0)
            {
                return $query->row();
				
            }
            else
            {
                return false;
            }
	 }
    /////////////////////////////////////////////////////////////////////////////////
	  //***************function used for edit single shop *********************//
     /////////////////////////////////////////////////////////////////////////////////
	 function edit_newshop($restaurant_id)
	 {
	       // echo "hello";die;
		    $this->db->select('a.*,b.address_ar,c.*,d.user_name,d.user_email,d.contact_number');
            $this->db->from('fos_restaurants a'); 
            $this->db->join('fos_restaurant_address b', 'b.restaurant_id=a.restaurant_id', 'left');
            $this->db->join('fos_shop_categories c', 'b.category=c.category_id', 'left');
			$this->db->join('fos_restaurant_admin_users d', 'd.restaurant_id=a.restaurant_id', 'left');
            $this->db->where('a.restaurant_id',$restaurant_id);        
            $query = $this->db->get(); 
            if($query->num_rows() != 0)
            {
                //return $query->result();
				 return $result = $query->row();
            }
            else
            {
                return false;
				
            }
	
	 }
	/////////////////////////////////////////////////////////////////////////////////
	  //***************function used for freelancer information *********************//
     ////////////////////////////////////////////////////////////////////////////////
	 function freelancer_detail($user_id)
	 {
		    $this->db->select('*');
            $this->db->from('fos_restaurant_admin_users'); 
            $query = $this->db->get(); 
            if($query->num_rows() != 0)
            {
                return $query->row();
				
            }
            else
            {
                return false;
            }
	 }
 }

?>