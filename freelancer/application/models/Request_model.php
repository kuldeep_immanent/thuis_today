<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Request_model extends CI_Model
{
    public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }
//**********************************************************//
	//	function used for getting tution type of a user	//
//***********************************************************//
	function get_tutionType($user_id)
	 {
	 	   $this->db->select('tution_type');
	       $query = $this->db->get_where('registration', array('id' =>$user_id)); 
	       return $query->num_rows() > 0 ? $query->row() : false;
	 }
//**********************************************************//
	//	function used for getting Random teacher list //
//***********************************************************//
	/***************function get_teacherRandomList($tution_type, $offset)
	 {
	 	   $this->db->where("FIND_IN_SET('$tution_type',tution_type) !=", 0);
	 	   $this->db->limit(25, $offset);
	 	   $query = $this->db->get_where('registration', array('user_type' => 't')); 
	 	   return $query->num_rows() > 0 ? $query->result() : false;
	 }******************/
	 //**********************************************************//
	//	function used for getting Random teacher list //
//***********************************************************//
     function get_teacherRandomList($offset)
	 {
	 	  $this->db->select('*');
		  $this->db->from('registration');
		  $this->db->where('user_type','t');
		  $this->db->limit(25, $offset);
		  $query = $this->db->get();
		  return $query->num_rows() > 0 ? $query->result() : false;
	 }
 }
?>