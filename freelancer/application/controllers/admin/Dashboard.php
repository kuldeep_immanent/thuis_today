<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Dashboard extends CI_Controller 
{

   function __construct()
    {
      parent::__construct();
      $this->load->model('user_model');
      $this->load->library('form_validation');
      $this->load->library('pagination'); 
    }
 /*******************************************
   -------------load admin dashboard -------
 *******************************************/   
   
     function index()
       { 
       	$this->load->template('index');
       }

/***********************************************
   ------------- Invoice Template ----------
 *********************************************/

    function shopmanage()
      {
      
          $user_id = $_SESSION['user_data']['user_id'];
		  $data['shop_list'] = $this->user_model->shop_list($user_id);
		  $this->load->template('shopmanage', $data);
      }
/***********************************************
   ------------- function used for add shop  ----------
 *********************************************/	  
	  function add_shop()
	  {
		  $data['category_name'] = $this->user_model->category_name();
		  $data['enterance_fee'] = $this->user_model->enterance_fee();
		  $this->load->template('add_shop', $data);
	  }
/***********************************************
   ------------- function used for Add New shop ----------
 *********************************************/	  
	  function add_newshop()
	  {
		    //$result = $this->input->post();
			//print_r($result);die;
			$this->form_validation->set_rules('res_password', 'Password', 'required|min_length[3]|matches[res_con_pass]');
            $this->form_validation->set_rules('res_con_pass', 'Password Confirmation', 'required');
			if($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('error_msg','Your Password and Confirm Password do not match!');
				redirect('admin/dashboard/add_shop/');
			}
			else
			{
				$data['user_id'] = $_SESSION['user_data']['user_id'];
				$data['res_name_ar'] = $this->input->post('res_name_ar');
				$data['res_description_ar'] = $this->input->post('res_description_ar');
				$data['res_contact'] = $this->input->post('res_contact');
				$res_contact = $this->input->post('res_contact');
				$data['category'] = $this->input->post('category');
				$data['address_ar'] = $this->input->post('res_address_ar');
				$data['res_admin_name'] = $this->input->post('res_admin_name');
				$data['res_admin_email'] = $this->input->post('res_admin_email');
				$res_admin_email = $this->input->post('res_admin_email');
				if(!empty($res_admin_email))
				{
					$this->form_validation->set_rules('res_admin_email', 'Email', 'required|valid_email|is_unique[fos_restaurant_admin_users.user_email]');
				    if($this->form_validation->run() == FALSE)
					{
						$this->session->set_flashdata('error_msg','Email has been already taken!');
						redirect('admin/dashboard/add_shop/');
					}
				}
				$data['res_admin_phone'] = $this->input->post('res_admin_phone');
				$res_password = $this->input->post('res_password');
				$data['res_password'] = $this->input->post('res_password');
				$data['res_con_pass'] = $this->input->post('res_con_pass');
				$data['entrancefee'] = $this->input->post('res_admin_enterance');
				$data = $this->user_model->add_newshop($data);
				if(!empty($data))
				{
					//$message = "Your email and password given below:-<br/>Email: ".$res_contact."<br/>Password: ".$res_password.".";
					//$this->load->library('email');
					//$this->email->from('youexample@gmail.com', 'Testing');
					//$this->email->to($res_admin_email);
					//$this->email->subject('Verification');
					//$this->email->message($message);
					//$this->email->set_mailtype('html');
				  // if($this->email->send())
				  // {
					$this->session->set_flashdata('error_msg','Shop has been saved successfully!');  
					redirect('admin/dashboard/shopmanage/');
				  // }
				  // else
				  // {
				//	   $this->load->template('add_shop', $data);
				  // }
				}
			}
		
	  }
 /***********************************************
   ------------- function used for Edit shop ----------
 *********************************************/	  
	  function edit_newshop($restaurant_id)
	  {
		   $data['shop_list'] = $this->user_model->edit_newshop($restaurant_id);
		   $data['category_name'] = $this->user_model->category_name();
		   $data['enterance_fee'] = $this->user_model->enterance_fee();
		   $data = $this->load->template('edit_shop', $data);
	  }
	  /***********************************************
   ------------- function used for update New shop ----------
 *********************************************/	  
	  function update_newshop()
	  {
				$data['user_id'] = $_SESSION['user_data']['user_id'];
				$data['restaurant_id'] = $this->input->post('resturant_id');
				$data['res_name_ar'] = $this->input->post('res_name_ar');
				$data['res_description_ar'] = $this->input->post('res_description_ar');
				$data['res_contact'] = $this->input->post('res_contact');
				$res_contact = $this->input->post('res_contact');
				$data['category'] = $this->input->post('category');
				$data['address_ar'] = $this->input->post('res_address_ar');
				$data['res_admin_name'] = $this->input->post('res_admin_name');
				$res_admin_email = $this->input->post('res_admin_email');
				$check_mail = $this->user_model->check_mail($data['restaurant_id']);
				if($check_mail->user_email == $res_admin_email)
				{
					$data['res_admin_email'] = $this->input->post('res_admin_email');
				}
				else
				{
				 $this->form_validation->set_rules('res_admin_email', 'Email', 'required|valid_email|is_unique[fos_restaurant_admin_users.user_email]');
				 if($this->form_validation->run() == FALSE)
					{
						$this->session->set_flashdata('error_msg','Email has been already taken!');
						redirect('admin/dashboard/edit_newshop/'.$data['restaurant_id'].'');
					}
					else
					{
						$data['res_admin_email'] = $this->input->post('res_admin_email');
					}
				}
				$data['res_admin_phone'] = $this->input->post('res_admin_phone');
				$res_password = $this->input->post('res_password');
				$res_con_pass = $this->input->post('res_con_pass');
				if(!empty($res_password))
				{
					$this->form_validation->set_rules('res_password', 'Password', 'required|min_length[3]|matches[res_con_pass]');
					$this->form_validation->set_rules('res_con_pass', 'Password Confirmation', 'required');
					if($this->form_validation->run() == FALSE)
					{
						$this->session->set_flashdata('error_msg','Your Password and Confirm Password do not match!');
						redirect('admin/dashboard/edit_newshop/'.$data['restaurant_id'].'');
					}
					else
					{
						$data['res_password'] = $res_password;
						$data['res_con_pass'] = $res_con_pass;
					}
				}
				$data['entrancefee'] = $this->input->post('res_admin_enterance');
				$data1 = $this->user_model->update_newshop($data);
				if(!empty($data1))
				{
					$freelancer_detail = $this->user_model->freelancer_detail($data['user_id']);
					$data['freelancer_name'] = $freelancer_detail->user_name;
					$category_namesingle = $this->user_model->category_namesingle($data['category']);
					$data['category_namesingle'] = $category_namesingle->category_name;
					$message = $this->load->view('email_formate',$data, TRUE);
					$this->load->library('email');
					$this->email->from('youexample@gmail.com', 'Home Today');
					$this->email->to($res_admin_email);
					$this->email->subject('Account Information');
					$this->email->message($message);
					$this->email->set_mailtype('html');
				   if($this->email->send())
				  {
					$this->session->set_flashdata('error_msg','Shop has been updated successfully!');  
					redirect('admin/dashboard/shopmanage/');
				  }
				  //else
				   //{
					//   $this->load->template('add_shop', $data);
				   //}
				}
			//}
		
	  }

/***********************************************
   ------------- Services Template ----------
 *********************************************/

  function services()
      {  
          $check['service'] = $this->user_model->checkDataService();
          $this->load->template('services', $check);
      }
 


/***********************************************
   ------------- Payments Template ----------
 *********************************************/

    function payments()
      {
          $check['payment'] = $this->user_model->checkDataPayment();
          $this->load->template('payments', $check);
      }


/***********************************************
   ------- save invoices----------
 *********************************************/

  public function saveInvoice()
    {
         $data['subject'] = $this->input->post('subject');
         $data['above_content'] = $this->input->post('above');
         $data['below_content'] = $this->input->post('below');
        // $data['status'] = $this->input->post('status');
                    
         $insert = $this->user_model->insertinvoice($data);
         redirect('admin/dashboard/invoices/');   
    }


/***********************************************
   ------- save invoices----------
 *********************************************/

  public function saveGoods()
    {
         $data['subject'] = $this->input->post('subject');
         $data['content'] = $this->input->post('content');
       //  $data['status'] = $this->input->post('status');
                    
         $insert = $this->user_model->insertgoodService($data);
         redirect('admin/dashboard/services/');   
    }


/***********************************************
   ------- save invoices----------
 *********************************************/

  public function saveRequestPayment()
    {
         $data['subject'] = $this->input->post('subject');
         $data['content'] = $this->input->post('content');
        // $data['status'] = $this->input->post('status');
                    
         $insert = $this->user_model->insertpaymentRequest($data);
         redirect('admin/dashboard/payments/');   
    }
}
?>