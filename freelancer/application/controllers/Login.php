<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Login extends CI_Controller {

 function __construct()
  {
    parent::__construct();
  $this->load->library('form_validation');
  $this->load->model('a_login');


  }
 function index()
 {

 	$this->load->view('a_login');
 	
 }



/***********************************
login function 
***********************************/
function do_login()
{ 
 $this->form_validation->set_rules('username', 'username', 'trim|required|valid_email');
 $this->form_validation->set_rules('password', 'Password', 'trim|required');
    if($this->form_validation->run() == FALSE)
    {
    $this->load->template('a_login');
    }
    else
    {
    $data['username'] = $this->input->post('username');
    $data['password'] = $this->input->post('password');
     
    $result = $this->a_login->login($data);
     
          if(!empty($result))
             {             
              $session_data = array('username'=>$result->email, 'user_id'=>$result->id, 'freelancer_name'=>$result->name);
              $this->session->set_userdata('user_data', $session_data);
              redirect('admin/Dashboard');  
            }
    else
    {
   $this->session->set_flashdata('error_msg','Invalid username or password!');
            redirect('login','refresh');
    }
    }
 
 }




/***********************************
logout function 
***********************************/
function logout()
{

	redirect('login');
}


}
?>