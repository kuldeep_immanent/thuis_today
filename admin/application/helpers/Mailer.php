<?php

    class FOS_Action_Helpers_Mailer extends Zend_Controller_Action_Helper_Abstract{
        
        private $config;
        private $database;
        private $tp;
        
        public function init(){
            $bootstrap = $this->getFrontController()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->tp = $this->getFrontController()->getParam('TablePrefix');
            $this->config = $bootstrap->getResource('AppConfig');
        }
        
        public function sendMail($mail = null){
            if($mail === null) throw new Exception('No mail specified');
            if(!$mail instanceof Zend_Mail) throw new Exception('Invalid mail data.');
            
            $sender = (object)array(
                'name' => $this->config->mailsettings->sender->name,
                'email' => $this->config->mailsettings->sender->email 
            );
            $mail->setFrom($sender->email, $sender->name);
            
            try{
                switch($this->config->mailsettings->adapter){
                    case 'sendmail': {
                        $transport = new Zend_Mail_Transport_Sendmail();
                        $mail->send($transport);
                        break;
                    }
                    case 'smtp': {
                        $config = array(
                            'auth' => 'login',
                            'username' => $this->config->mailsettings->smtp->username,
                            'password' => $this->config->mailsettings->smtp->password,
                            'port' => $this->config->mailsettings->smtp->port
                        );
                        if(Zend_Validate::is($this->config->mailsettings->smtp->ssl, 'NotEmpty'))
                            $config['ssl'] = $this->config->mailsettings->smtp->ssl;
                        $transport = new Zend_Mail_Transport_Smtp($this->config->mailsettings->smtp->host, $config);
                        $mail->send($transport);
                        break;                            
                    }
                }
            }catch(Exception $e){
                throw new Exception('Failed to send mail.');
            }
        }
        
    }