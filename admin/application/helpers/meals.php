<?php
class  FOS_Action_Helpers_Meals extends Zend_Controller_Action_Helper_Abstract
{
	
	private $tp;
	private $database;
	private $config;
	
 public function init(){
            $bootstrap = $this->getFrontController()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->tp = $this->getFrontController()->getParam('TablePrefix');
            $this->config = $bootstrap->getResource('AppConfig');
        }

        public function meals_info()
        {
        	$meals = new stdclass;
        	$meals->error = false;
        	$meals->errormessage = null;
        	
        	try
        	{
            $timestamp = date('Y-m-d');
        	$lastweekdate = $this->fetching_date($timestamp);	
        	$meal = "SELECT item_id
                FROM fos_restaurant_per_item_order_report
                WHERE order_date
                BETWEEN '{$lastweekdate }'
                AND '$timestamp' "
                ;
		   
        	$meal = $this->database->fetchAll($meal);
        	$dummyarray = array();
        	//print_r($meal);
        	
        	$result = array();
        	foreach($meal as $meal_id):
        	
        	
        	if(in_array($meal_id->item_id,$dummyarray)):
        	
        	 $result[$meal_id->item_id] = $result[$meal_id->item_id] + 1; 
        	
        	  else:
        		
        	  $dummyarray[] =$meal_id->item_id;
        	  
        	  $result[$meal_id->item_id] = 1;
        	  endif;
        	 endforeach;
        	
        	for($start=0;$start<10;$start++):
            foreach($result as $key => $res)
            {
            	
            	foreach($result as $main):
            	if($res > $main || $res == $main)
            	{	
            	$flag = true;
            	}
            	else 
            	{	
            	$flag = false;
            	break;		
            	}
            	endforeach;
            	
            	if($flag == true):
            	$original[$key] = $res;
            	unset($result[$key]);
            	endif;
            	
            	
            }
            
        	if(count($result) == " "  )
            {
            	
            	break;
            	
            	
            }
            
            endfor;
            $initial = 0;
            foreach($original as $key =>$value):   
            $data =  "SELECT meal_name,meal_description,meal_price
                      FROM {$this->tp}restaurant_meals
                      WHERE meal_id = {$key}";
            $info[$initial] = $this->database->fetchrow($data);
            $initial++;
            endforeach;
        	
            $meals->data =$info;
            
            //unsetting the values
             unset($info);
             unset($original);
             unset($dummyarray);
             unset($result);
            
        	}
        	catch(Exception $e)
        	{
        		
        	$meals->error = true;
        	$meals->errormessage = $e->getMessage();	
        		
        		
        		
        	}
        	
        	
        	
        	
        	
        	return  $meals;
        	
        	
        	
        }
        
        
        
		public function  fetching_date($new_time)
			{
		
				if($new_time !=null)
				{
					$timestamp =$new_time;	
				}
				else {	
					$timestamp = date('Y-m-d');		
				}
				$previous_week = strtotime("-1 week $timestamp ");
				$start_week = date("Y-m-d",$previous_week);
				//$end_week = date("Y-m-d",$end_week)
				unset($previous_week);
		
				return $start_week;
		
		
		
		
			}
			
			// popular restaurant detail list
			public function popular_rest_detail()
			{
			   $res_det = new stdclass;
			   $res_det->error =false;
			   $res_det->errormessage =null;
			   
			   try
			   {
			   	
			   	$data =  "SELECT restaurant_name,restaurant_description,logo
                FROM {$this->tp}restaurants
                WHERE  	is_active = 1";
			   	
			   	$res_det->data = $this->database->fetchAll($data);
			   	
			   	
			   }
			   catch(Exception $e)
			   {
			   	
			   $res_det->error = true;
			   $res_det->errormessage = $e->getMessage();
				
			   }
				return $res_det;
				
			}
			
			
			
	
	
	
	
}




?>