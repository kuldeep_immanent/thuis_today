<?php
/* Class: admin message controller 
 * Author:Gagandeep Singh
 * Company:Boom Infotech Mohali
 * */
include('CommonController.php');
class Restaurant_MessageController extends Zend_Controller_Action 
{
	
	    public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
	
	// main method for getting all parent properties of /zend fron Controller
	function init()
	{
		 //initializing resources
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig = $bootstrap->getResource('AppConfig');
            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test=commonnotify::Notification();
            $this->count=commonnotify::total_count();
            
            
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            $this->view->headTitle('Admin Panel | Message Center');
	
		
	}   
	    /*  preDispacth  method for chechking pre defined  funtion */
         public function preDispatch(){
            //checking session
            if(!$this->session->admin->loggedin)
              $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
            //adding few variables to view
	            $this->view->request = $this->getRequest();
	            $this->view->session = $this->session;
	            $this->view->config = $this->config;
	            $this->view->appConfig = $this->appConfig;
        }
	
	       /* message action  for receiving the messages */
			
        public  function indexAction()
			{
				
				$detail=new stdclass;
				$detail->error=false;
				$detail->success=false;
				
				
				$detail->errormessage=null;
				/* check for adding the main concept */
				try {
					
					
				// quering the database  for fething extract data //
				  $id= $this->session->admin->id;
				 
				  
				  
				  try
				  {
				  if(isset($_GET['user_id'])):
				  	 
				  	 $user_id=$_GET['user_id'];
				  	
				  	 
				    $this->database->update($this->tp.'restaurant_messages',array('status'=>0),'res_admin_id = '.$id.' AND super_admin_id = '.$user_id);
                   
				  endif;
				  }
				  
				  catch (Exception $e)
				  {
				  	
				  	$detail->error=true;
				  	$detail->errormessage=$this->translate('query exception occured'.$e);
		
				  }
				   $query=$this->database->select()->from( $this->tp.'restaurant_messages')->where('res_admin_id ='.$id);	
                    
					$result=$this->database->fetchall($query);
					
					 $start=0;
					 
					 $sendby=null;
					 
					foreach($result as $res)
					{
						$detail->user->data['message'][$start]=$res->message;
						$detail->user->data['time'][$start]=$res->time;
						$detail->user->data['status'][$start]=$res->status;
					    $detail->user->data['super_admin_id'][$start]=$res->super_admin_id;
						
						$sendby=$res->send_by;
		
						if($sendby == 'super_admin')
						{

							try
							{
								
                             $id=$res->super_admin_id;
								
							$info=$this->database->select()->from($this->tp.'admin_users')->where('user_id = ?',$id);
								
						    $super_admin_array=$this->database->fetchrow($info);
						    
                            $detail->user->data['username'][$start]=$super_admin_array->user_name;
                            
                           
							}
							
							catch(Exception $e)
							{
								$detail->error=true;
								
								$detail->errormessage=$this->translate('error found'.$e);
								
								
							}
	
						}
						else 
						{
							
						 $id=$res->res_admin_id;
						 
						 $info=$this->database->select()->from( $this->tp.'restaurant_admin_users')->where('restaurant_id = ?', $id);	
					     
						 $result=$this->database->fetchrow($info);
					     
					      $detail->user->data['username'][$start]=$result->user_name;	
							
							
							
						}
						
						
						$start++;
						
					}
							
					
				}
				
				catch (Exception $e)
				
				{
					$detail->error=false;
					
					$datail->errormessage=$this->translate('query exception occured');
	
					
				}
				  
				  $detail->test=$this->test;
				  $detail->count=$this->count;
				  $detail->user_id= $user_id;
				  
				  
				  
				  
				   
				     $this->view->assign((array) $detail);
				   
		            $this->view->sidebar_menu_item = 'Messages list';
		            $this->view->headTitle($this->translate('Dashboard').' | ', 'PREPEND');

				
			}
			/* submitting messages to main action  */
			
			 public function ajaxmessageAction(){
			 	
	            $response = new stdclass;
	            $response->error = false;
	            $response->success = false;
	           
            
            try{
                if(!Zend_Validate::is($this->getRequest()->message, 'NotEmpty'))
                    throw new Exception($this->translate('Fields marked(*) are mandatory.'));
              
                //saving the meal details
                    try{
                    	
                    	 
                        $this->database->insert(
                            $this->tp.'restaurant_messages',
                            array(
                                'super_admin_id' => trim($this->getRequest()->to),
                                'res_admin_id' =>trim($this->getRequest()->admin),
                                'message' => trim($this->getRequest()->message),
                                'send_by' =>'admin',	
                                'status' => 1 
                            )
                           
                        );
                        $response->success = true;
                    }
                    catch(Exception $e){
                    	
                        throw new Exception($this->translate('Failed to save. Please re-try again.'));
                        $response->error="true";
                    }
	            }
	            catch(Exception $e){
	                $response->error = true;
	                $response->errorMessage = $e->getMessage();
	            }
	           
	               if($response->success=true)
	               
	                commonnotify::response();
	            
	            
	                $this->_helper->layout()->disableLayout();
                    $this->_helper->viewRenderer->setNoRender(true);
	              
        }
				
				
				
				
				
				
		
	
			/* function to translate the language */
			
			public  function translate()
			{
				
				
				  return $this->translator->translate($message);
				
				
				
			}
			
	
	
	
	
	
	
	
	
	
}





?>