 <?php
   include('CommonController.php');
   

    class Restaurant_DeliveryController extends Zend_Controller_Action{
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_helper;
        public $model;
        
        public function init(){
            //initializing resources
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig = $bootstrap->getResource('AppConfig');
            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test=commonnotify::Notification();
            $this->count=commonnotify::total_count();
            $this->_C_helper  = $this->getHelper('Custom');
           
            
              
           
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/restaurant/views/layouts',
                'layout' => 'restaurantadmin'
            ));
            //adding default page title
            $response = new stdclass;  
            $response->test=$this->test;
            $response->count=$this->count;
            $this->view->headTitle($this->translate('Shop').' |  Delivery Area');
        }
        
        public function preDispatch(){
            
            
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
        }
        
        public function postDispatch(){
            
        }
        
       
        private function checkLogin(){
            if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);
        }
        
     public function translate($message){
            return $this->translator->translate($message);
        }

    public function indexAction()
     {       
           $response = new stdClass();
           $respose->error = false;
           $response->message = null;
           $resid=$this->session->admin->resid;        
        try
           {  
             $select = $this->database->query("select * from fos_area where shop_id = '$resid'");
             $response->area =  $select->fetchall();
             $response->count  = count($response->area);     
           } 
        catch (Exception $ex) 
          {            
                $response->message =$ex->getMessage();               
          }           
            $this->view->assign((array)$response);                       
     }


     public function getcityAction()
     {  
          
           $response         = new stdClass();
           $respose->error   = false;
           $response->message = null;
           try
           {

            $country_id = $this->getRequest()->country;
            $city =$this->_C_helper->G_Get_Data("select * from fos_locations where location_city = $country_id");
                $response->city = array('all' => '--- '.$this->translate('Select City').' ---');
                 echo '<option value="" > Select City </option>';
                foreach($city as $city)
                 {
                     echo '<option value="'.$city->location_id.'" >'.$city->location_name.'</option>';

                 }
               
                 die;
                
           } catch (Exception $ex) {
            
                $response->message =$ex->getMessage();
               
           }
           
     }


      public function getareaAction()
     {  
          
           $response         = new stdClass();
           $respose->error   = false;
           $response->message = null;
           try
           {

            $country_id = $this->getRequest()->country;
            $city = $this->getRequest()->city;
            $area =$this->_C_helper->G_Get_Data("select * from fos_area_relation where country_id = $country_id and city_id=$city"); 
               foreach ($area as $key => $value) {
                   echo $value->area_id;
               }
                 die;
                
           } catch (Exception $ex) {
            
                $response->message =$ex->getMessage();
               
           }
           
     }


     /***********************************

      this function is use for add edit delivery area 

      ****************************************/
      public function addeditAction()
        { 
            
           $response         = new stdClass();
           $respose->error   = false;
           $response->message = null;
           $resid=$this->session->admin->resid;
            try
              {
                    
           
              // $country =$this->_C_helper->G_Get_Data("select * from fos_countries");
              //       $response->country = array('' => '--- '.$this->translate('Select Country').' ---');
              //       foreach($country as $entry)
              //       {
              //           $response->country[$entry->id] = stripslashes($entry->country_name);
              //       }

              // $city =$this->_C_helper->G_Get_Data("select * from fos_locations");
              // $response->city = array('' => '--- '.$this->translate('Select City').' ---');
              //       foreach($city as $entry)
              //       {
              //           $response->city[$entry->location_id] = stripslashes($entry->location_name);
              //       }  

              //    $area =$this->_C_helper->G_Get_Data("select * from fos_area"); 
              //    $response->area = array('' => '--- '.$this->translate('Select area').' ---');
              //       foreach($area as $entry)
              //       {
              //           $response->area[$entry->area_id] = stripslashes($entry->area_name);
              //       }  

                    // $id = $this->getRequest()->getParam('id');
                    // if(!empty($id))
                    //    {
                    //     $result =$this->_C_helper->G_Get_Data("select * from fos_area_relation where id=$id"); 
                    //     $response->country_id =  $result[0]->country_id;  
                    //     $response->city_id =  $result[0]->city_id; 
                    //     $response->area_id =  $result[0]->area_id; 
                    //    }

                if($this->getRequest()->area_name)
                  {                     
                    $area_name = $this->getRequest()->area_name;
                    $post_code = $this->getRequest()->post_code;
                    $resid = $this->session->admin->resid;  

                    $save = array('area_name' => $area_name,
                                  'post_code' => $post_code,
                                   'shop_id' => $resid);
                    $this->database->insert('fos_area', $save);
               
                    $response->success= true;

                  }
            } catch (Exception $ex) {
            
                $response->message =$ex->getMessage();
               
           }

         $this->view->assign((array)$response); 


        }


       public function deleteAction()
           {
            
            $id = $this->getRequest()->getParam('id');
            try {
                $this->database->delete('fos_area', array(
                    'area_id = ?' => $id
                    
                ));
                
            } catch (Exception $ex) {
                
            }
             $this->_redirect("/restaurant/admin/delivery");  
           

            
         }
    

}



?>