<?php
include('CommonController.php');
    class Restaurant_PromotionsController extends Zend_Controller_Action{
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        
        public function init(){
            //initializing resources
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig = $bootstrap->getResource('AppConfig');
            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test=commonnotify::Notification();
            $this->count=commonnotify::total_count();
            
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/restaurant/views/layouts',
                'layout' => 'restaurantadmin'
            ));
            //adding default page title
            $this->view->headTitle($this->translate('Shop Admin Panel').' | Kebapiste');
        }
        
        public function preDispatch(){
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
        }
        
        public function postDispatch(){
            
        }
        
        private function translate($message){
            return $this->translator->translate($message);
        }
        
        private function checkLogin(){
            if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);
        }
        
        public function managepromotionsAction(){
            $response = new stdclass;
            $response->error = false;
            
            if(isset($this->getRequest()->doAction)){
                try{
                    switch($this->getRequest()->doAction){
                        case 'activatepromotion': {
                            $this->database->update(
                                $this->tp."restaurant_promotions",
                                array('is_active' => 1),
                                'promotion_id = '.$this->getRequest()->promotion_id
                            );
                            break;
                        }
                        case 'deactivatepromotion': {
                            $this->database->update(
                                $this->tp."restaurant_promotions",
                                array('is_active' => 0),
                                'promotion_id = '.$this->getRequest()->promotion_id
                            );
                            break;
                        }
                        case 'moveuppromotion': {
                            //finding promotion greater than current promotion
                            $currentPromotion = $this->database->fetchRow("select * from {$this->tp}restaurant_promotions where promotion_id = ?", $this->getRequest()->promotion_id);
                            $higherPromotion = $this->database->fetchRow("select * from {$this->tp}restaurant_promotions where promotion_order > ".$currentPromotion->promotion_order." order by promotion_order asc LIMIT 1");
                            if(!$higherPromotion) throw new Exception($this->translate('This promotion is already at the top.'));
                            //getting current promotion
                            
                            $newOrder = $higherPromotion->promotion_order;
                            //updating promotions
                            $this->database->update($this->tp."restaurant_promotions", array('promotion_order' => $currentPromotion->promotion_order), 'promotion_id = '.$higherPromotion->promotion_id);
                            $this->database->update($this->tp."restaurant_promotions", array('promotion_order' => $newOrder), 'promotion_id = '.$this->getRequest()->promotion_id);
                            break;                                
                        } 
                        case 'movedownpromotion': {
                            //finding promotion lower than current promotion
                            $currentPromotion = $this->database->fetchRow("select * from {$this->tp}restaurant_promotions where promotion_id = ?", $this->getRequest()->promotion_id);
                            $lowerPromotion = $this->database->fetchRow("select * from {$this->tp}restaurant_promotions where promotion_order < ".$currentPromotion->promotion_order." order by promotion_order desc LIMIT 1");
                            if(!$lowerPromotion) throw new Exception($this->translate('This promotion is already at the bottom.'));
                            //getting current promotion
                            $newOrder = $lowerPromotion->promotion_order;
                            //updating promotions
                            $this->database->update($this->tp."restaurant_promotions", array('promotion_order' => $currentPromotion->promotion_order), 'promotion_id = '.$lowerPromotion->promotion_id);
                            $this->database->update($this->tp."restaurant_promotions", array('promotion_order' => $newOrder), 'promotion_id = '.$this->getRequest()->promotion_id);
                            break;
                        }
                        case 'deletepromotion': {
                            $this->database->delete(
                                $this->tp."restaurant_promotions",
                                'promotion_id = '.$this->getRequest()->promotion_id
                            );
                            break;
                        }
                    }
                }catch(Exception $e){
                    $response->error = true;
                    $response->errorMessage = $e->getMessage();
                }
            }
            
            //loading promotions
                try{
                    $response->promotions = $this->database->fetchAll("select * from {$this->tp}restaurant_promotions where restaurant_id = ? order by promotion_order desc", $this->session->admin->resid);
                }catch(Exception $e){
                    $response->error = true;
                    $response->errorMessage = $e->getMessage();
                }
                   $response->test=$this->test;
				  $response->count=$this->count;
            $this->view->headTitle($this->translate('Manage Promotions').' | ', 'PREPEND');
            $this->view->assign((array)$response);
        }   
        
        public function ajaxpromotionsAction(){
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
            
            if(isset($this->getRequest()->doAction)){
                try{
                    switch($this->getRequest()->doAction){
                        case 'addpromotion': {
                            if(!Zend_Validate::is($this->getRequest()->promotion_title, 'NotEmpty'))
                                throw new Exception($this->translate('Fields marked(*) are mandatory.'));
                            //adding promotion to database
                                try{
                                    $maxPromotionOrder = $this->database->fetchOne("select max(promotion_order) from {$this->tp}restaurant_promotions");
                                    $promotionOrder = ($maxPromotionOrder ? ($maxPromotionOrder + 1) : 1);
                                    $this->database->insert(
                                        $this->tp.'restaurant_promotions',
                                        array(
                                            'restaurant_id' => $this->session->admin->resid,
                                            'promotion_title' => trim($this->getRequest()->promotion_title),
                                            'promotion_details' => trim($this->getRequest()->promotion_details),
                                            'promotion_order' => $promotionOrder,
                                            'is_active' => $this->getRequest()->promotion_active
                                        )
                                    );
                                    $response->success = true;
                                }catch(Exception $e){
                                    throw new Exception($this->translate('Failed to add promotion. Please re-try again.'));
                                }
                            break;
                        }
                        case 'savepromotion': {
                            if(!Zend_Validate::is($this->getRequest()->promotion_title, 'NotEmpty'))
                                throw new Exception($this->translate('Fields marked(*) are mandatory.'));
                            //saving promotion to database
                                try{
                                    $this->database->update(
                                        $this->tp.'restaurant_promotions',
                                        array(
                                            'promotion_title' => trim($this->getRequest()->promotion_title),
                                            'promotion_details' => trim($this->getRequest()->promotion_details)
                                        ),
                                        'promotion_id = '.$this->getRequest()->promotion_id
                                    );
                                    $response->success = true;
                                }catch(Exception $e){
                                    throw new Exception($this->translate('Failed to save promotion. Please re-try again.').$e->getMessage());
                                }
                            break;
                        }
                    }
                }catch(Exception $e){
                    $response->error = false;
                    $response->errorMessage = $e->getMessage();
                }
            }
            
            $this->getHelper('Json')->sendJson($response);
        }
        
        public function editpromotionAction(){
            $response = new stdclass;
            $response->error = false;
            
            try{
                $response->promotion = $this->database->fetchRow("select * from {$this->tp}restaurant_promotions where promotion_id = ?", $this->getRequest()->promotion_id);
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $this->translate('Failed to load promotion details for editing');
            }
            
            $this->getHelper('Layout')->disableLayout();
            $this->view->assign((array)$response);
        }     
    }