<?php
include('CommonController.php');
    class Restaurant_LocationController extends Zend_Controller_Action{
        
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_C_Helper;
        public $_table;
        
        public function init(){
            //initializing resources
            $bootstrap        = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database   = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp         = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig  = $bootstrap->getResource('AppConfig');
            $this->config     = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test       = commonnotify::Notification();
            $this->count      = commonnotify::total_count();
            $this->_C_Helper  = $this->getHelper('Myreuse');
            $this->table      = 'fos_customer_order_tracker';
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/restaurant/views/layouts',
                'layout' => 'restaurantadmin'
            ));
            //adding default page title
            
        	
            $this->view->headTitle($this->translate('Shop Location Panel').' | Delivery');
        }
        
        public function preDispatch(){
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
           
        }
        
        public function postDispatch(){
            
        }
        
        private function translate($message){
            return $this->translator->translate($message);
        }
        
        private function checkLogin(){
            if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);
        }
        
        /****
         * function to list the all deliver boys
         ****/
        public  function indexAction(){
            $response = new stdclass;
            $response->error = false;
            $response->messaage = null;
            
            
            try 
            {
            	$get_rest_id=$_SESSION['fosressession'];
            	foreach ($get_rest_id as $res_id) {
            		$restaurant_id =  $res_id->resid;
            	}
                //echo "select * from fos_delivery_boy_login where res_id=$restaurant_id and availabe_status=1";
                $deliver_boy = "select * from fos_delivery_boy_login where res_id=$restaurant_id";
                $response->data =  $this->_C_Helper->G_Get_Data($deliver_boy,'result');
                $get_del_boy_id = $response->data;

                foreach ($get_del_boy_id as $del_boy) {
                	 $del_boy_id[] = $del_boy->id;
                	
                }
                $all_del_boy_id =implode(',',$del_boy_id);
               

                // "select * from fos_delivery_order_rel_boy where delivery_id IN($all_del_boy_id) and status=1";
                $all_orders = "select * from fos_delivery_order_rel_boy where delivery_id IN($all_del_boy_id)";
                $response->data =  $this->_C_Helper->G_Get_Data($all_orders,'result');
                $get_del_order = $response->data;
                foreach ($get_del_order as $orders) {
                	$all_id[] = $orders->order_id;
                }
                $all_id  = implode(',',$all_id);
        
                

                $query = "select * from fos_customer_order_tracker where order_id IN($all_id)";
                $response->data =  $this->_C_Helper->G_Get_Data($query,'result'); 
                
               
            } 
            catch (Exception $ex) 
            {
            $response->error = true;
            $response->messaage = $ex->getMessage();
            }
            
             $paginator = Zend_Paginator::factory($response->data);
             $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
             $paginator->setDefaultItemCountPerPage(10000);
             $this->view->paginator = $paginator;
             $this->view->sidebar_menu_item = 'location';
             $this->view->assign((array)$response);
             $this->view->headTitle($this->translate('Delivery Boy Location ').' | ', 'PREPEND');
            
        }
        
         
        
           
    }
    
    
    ?>