<?php

include('CommonController.php');

    class Restaurant_SettingsController extends Zend_Controller_Action{

        

        public $database;

        public $tp; //Table prefix

        public $appConfig;

        public $config;

        public $session;

        public $translator;

        public $test;

        public $count;

        

        public function init(){

            //initializing resources

            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');

            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();

            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);

            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');

            $this->appConfig = $bootstrap->getResource('AppConfig');

            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');

            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');

            $this->translator = Zend_Registry::get('Zend_Translate');

            $this->test=commonnotify::Notification();

            $this->count=commonnotify::total_count();
             $this->_C_Helper = $this->getHelper('Custom');

            //initializing layout

            Zend_Layout::startMvc(array(

                'layoutPath' => APPLICATION_PATH . '/modules/restaurant/views/layouts',

                'layout' => 'restaurantadmin'

            ));

            //adding default page title

            

        	

            $this->view->headTitle($this->translate('Shop Admin Panel').' | Delivery');

        }

        

        public function preDispatch(){

            $this->view->request = $this->getRequest();

            $this->view->session = $this->session;

            $this->view->config = $this->config;

            $this->view->appConfig = $this->appConfig;

           

        }

        

        public function postDispatch(){

            

        }

        

        private function translate($message){

            return $this->translator->translate($message);

        }

        

        private function checkLogin(){

            if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);

        }

        

        private function indexAction(){

             

            

        }

         /*****
         * function the setting the restaurant
         */
        public function settingAction()
        {
			
            $response = new stdclass;
            $response->error = false;
            $response->errormessage = null;
            $response->success = false;
            
            // "Select * from {$this->tp}restaurant_setting where res_id={$this->session->admin->resid}";
            try 
            {

              
              $testquery = "Select * from {$this->tp}restaurant_setting where res_id = {$this->session->admin->resid}";
              $datatest = $this->_C_Helper->G_Get_Data($testquery); 
              if(!empty($datatest))
              {
                $query = "Select * from {$this->tp}restaurant_setting left join {$this->tp}restaurants on {$this->tp}restaurant_setting.res_id = {$this->tp}restaurants.restaurant_id left join {$this->tp}restaurant_address on {$this->tp}restaurant_setting.res_id = {$this->tp}restaurant_address.restaurant_id left join {$this->tp}restaurant_admin_users on {$this->tp}restaurant_setting.res_id = {$this->tp}restaurant_admin_users.restaurant_id where {$this->tp}restaurant_setting.res_id = {$this->session->admin->resid}";
                 $response->data  =  $this->_C_Helper->G_Get_Data($query);
              }
              else
              {
                $query = "Select * from {$this->tp}restaurants left join {$this->tp}restaurant_address on {$this->tp}restaurants.restaurant_id = {$this->tp}restaurant_address.restaurant_id left join {$this->tp}restaurant_admin_users on {$this->tp}restaurants.restaurant_id = {$this->tp}restaurant_admin_users.restaurant_id where {$this->tp}restaurants.restaurant_id = {$this->session->admin->resid}";
                $response->data  =  $this->_C_Helper->G_Get_Data($query);   
              }
              $cate = $this->database->fetchAll("select * from {$this->tp}shop_categories ");
                $response->category[$entry->category_id] = array('' => $this->translate('-Select Shop Category-'));
                foreach($cate as $entry)
                    {
                $response->category[$entry->category_id] = stripslashes($entry->category_name);
                    }          
              
              /***** 
               * saving and updating the values of setting
               */
                if($this->getRequest()->isPost())
                {
                    if(count($response->data)>0):
                        
                        $this->SaveEditSetting('update');
                         
                        else:
                        $this->SaveEditSetting('insert');
                    endif;

                }
             
               
            } catch (Exception $ex) {
                
              $response->error = true; 
             
              $response->errormessage = $ex->getMessage();
            }
            if($this->getRequest()->isPost())
                {
                   if(!$response->error):
                     $response->success = true;
                   endif;
                   $this->_redirect(BASEURLAPP.'setting/setting?cache=1');
                }  
               
               // $response->getHeaders();  
          //  $response->setHeader("Cache-Control: no-cache, must-revalidate");    
          //  $response->setHeader("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
          //  $response->setHeader("Content-Type: application/xml; charset=utf-8"); 
            $this->view->sidebar_menu_item = 'setting';
            $this->view->assign((array)$response);
              
            
        }

public function header()
{
    // $response = $this->getResponse();
    // $request = $this->getRequest();
    
        $this->getResponse()->setHeader('Cache-Control','no-store, no-cache, must-revalidate, post-check=0, pre-check=0',1);
        $this->getResponse()->setHeader('Expires','Thu, 19 Nov 1981 08:52:00 GMT',1);
        $this->getResponse()->setHeader('Pragma','no-cache',1);
      //  $response->setHeader("Cache-Control: no-cache, must-revalidate");    
        //   $response->setHeader("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
   // $response->setHeader("Content-Type: application/xml; charset=utf-8");
   // $response->setHeader('Last_Modified', gmdate('D, d M Y H:i:s', time()).' GMT', true);


}
 /*******
         * function  to save and edit the setttings
         */
        private function SaveEditSetting($type)
        {           
             // $cache = Zend_Cache::factory();
             // Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
             // $cache->clean(Zend_Cache::CLEANING_MODE_ALL); 
			/* echo "<pre>";
			print_r($this->session->admin);
			die; */
			
			
           // if(!Zend_Validate::is($this->getRequest()->res_delivery_time, 'NotEmpty')||
                    //!Zend_Validate::is($this->getRequest()->res_extra_time, 'NotEmpty'))
                   
                     //   throw new Exception($this->translate('All fields are mandatory.'));
            
           
                 if($type=='insert'):		
                         if(Zend_Validate::is($this->getRequest()->res_contact, 'NotEmpty') and !Zend_Validate::is($this->getRequest()->res_contact, 'EmailAddress', array(Zend_Validate_Hostname::ALLOW_DNS)))
                        throw new Exception($this->translate('Invalid contact email address.'));

                    if($this->getRequest()->res_password !=$this->getRequest()->res_con_pass)
                    throw new Exception($this->translate('confirm password does not match with password'));
                              $data = array(
                                   // 'deliver_time' => $this->getRequest()->res_delivery_time,
                                   // 'extra_time' => $this->getRequest()->res_extra_time,
                                   // 'approval_check' => $this->getRequest()->autoapprove,
                                    'open_time' =>  $this->getRequest()->open_time,
                                    'close_time'=>$this->getRequest()->close_time,
                                   // 'auto_open_close'=>$this->getRequest()->auto_open_close,
                                    'delivery_charges'=> str_replace('.', ',', $this->getRequest()->delivery_charges),
                                    'delivery_charges_opening_hour' => str_replace('.', ',', $this->getRequest()->delivery_charges_opening_hour),
                                    'taxpayable' =>str_replace('.', ',', $this->getRequest()->taxpayable),
                                    'payment_method'=>$this->getRequest()->paymentmethod[0].$this->getRequest()->paymentmethod[1],
                                    'during_opening'=>$this->getRequest()->slotDeliveryCheckbox,
                                    'duringweekDays'=>$this->getRequest()->duringweekDays,
                                    'mon'=>$this->getRequest()->otime2.'-'.$this->getRequest()->ctime2,
                                    'tue'=>$this->getRequest()->otime3.'-'.$this->getRequest()->ctime3,
                                    'wed'=>$this->getRequest()->otime4.'-'.$this->getRequest()->ctime4,
                                    'thr'=>$this->getRequest()->otime5.'-'.$this->getRequest()->ctime5,
                                    'fri'=>$this->getRequest()->otime6.'-'.$this->getRequest()->ctime6,
                                    'sat'=>$this->getRequest()->otime7.'-'.$this->getRequest()->ctime7,
                                    'sun'=>$this->getRequest()->otime1.'-'.$this->getRequest()->ctime1,
                                    'mo'=>$this->getRequest()->sotime2.'-'.$this->getRequest()->sctime2,
                                    'tu'=>$this->getRequest()->sotime3.'-'.$this->getRequest()->sctime3,
                                    'we'=>$this->getRequest()->sotime4.'-'.$this->getRequest()->sctime4,
                                    'th'=>$this->getRequest()->sotime5.'-'.$this->getRequest()->sctime5,
                                    'fr'=>$this->getRequest()->sotime6.'-'.$this->getRequest()->sctime6,
                                    'sa'=>$this->getRequest()->sotime7.'-'.$this->getRequest()->sctime7,
                                    'su'=>$this->getRequest()->sotime1.'-'.$this->getRequest()->sctime1,
                                    'pick_at_location'=>$this->getRequest()->pickupatlocation,
                                    'select_time'=>$this->getRequest()->select_time, 
                                    // 'shop_holiday'=>$this->getRequest()->shop_holiday,
                                    'res_id' => $this->session->admin->resid
                                );
                                
                                $this->database->insert(
                                $this->tp.'restaurant_setting',
                                $data
                                );

                                if( isset($_FILES['res_icon']) && $_FILES['res_icon'] !='' )
                                     {                                       
                                        $datalogo['logo']   = $this->G_Upload_file();      
                                     }
                                     else 
                                     {                                         
                                         $datalogo['logo']   = 'photo_not_available.png';    
                                     }
                              $this->database->insert(                          
                              $this->tp.'restaurants',
                              $datalogo
                            );
                    /////////////////////////////////////////////////////////////////////////////////////
                                $data1 = array(
                                    'restaurant_name_ar' => $this->getRequest()->res_name_ar,
                                    'restaurant_description_ar' =>$this->getRequest()->res_description_ar,
                                    'contact_email' => $this->getRequest()->res_contact
                                );
                                $this->database->insert(
                                $this->tp.'restaurants',
                                $data1
                                );
                                /////////////////////
                                $data2 = array(
                                    'address_ar'=> $this->getRequest()->res_address_ar,
                                    'postal_code' =>$this->getRequest()->res_postal_code                                    
                                );
                                $this->database->insert(
                                $this->tp.'restaurant_address',
                                $data2
                                );
                                ////////////////////////////
                                $data3 = array(
                                    'user_name'=> $this->getRequest()->res_admin_name,
                                    // 'user_email' =>$this->getRequest()->res_admin_email,                                    
                                    'account_password' =>$this->getRequest()->res_password, 
                                    'contact_number' => $this->session->admin->res_admin_contact_number                                    
                                );
                                $this->database->insert(
                                $this->tp.'restaurant_admin_users',
                                $data3
                                );
                  ///////////////////////////////////////////////////////////////////////////////////////////////////
                        /*  $this->database->update(
						  
                                $this->tp.'restaurant_setting',
								
                                array(
                                    'deliver_time' => $this->getRequest()->res_delivery_time,
                                    'extra_time' => $this->getRequest()->res_extra_time,
									'minimum_order' => $this->getRequest()->minimum_order,
                                    'open_time' =>  $this->getRequest()->open_time,
                                    'close_time'=>$this->getRequest()->close_time,
                                    'approval_check' => $this->getRequest()->autoapprove,
                                    'delivery_charges'=> $this->getRequest()->delivery_charges,
                                    'taxpayable'      =>$this->getRequest()->taxpayable
                                ),
                                "res_id = ".$this->session->admin->resid
                            );
                        */ 
 // $this->header();
 // $form->reset();
                 endif;
            

			 if($type=='update'):
                        /* $this->database->insert(
                                $this->tp.'restaurant_setting',
                                array(
                                    'deliver_time' => $this->getRequest()->res_delivery_time,
                                    'extra_time' => $this->getRequest()->res_extra_time,
                                    'approval_check' => $this->getRequest()->autoapprove,
                                    'open_time' =>  $this->getRequest()->open_time,
                                    'close_time'=>$this->getRequest()->close_time,
                                    'delivery_charges'=> $this->getRequest()->delivery_charges,
                                    'taxpayable'      =>$this->getRequest()->taxpayable,

                                    'res_id' => $this->session->admin->resid
                                )
                     );*/
if(Zend_Validate::is($this->getRequest()->res_contact, 'NotEmpty') and !Zend_Validate::is($this->getRequest()->res_contact, 'EmailAddress', array(Zend_Validate_Hostname::ALLOW_DNS)))
                        throw new Exception($this->translate('Invalid contact email address.'));

                   if($this->getRequest()->res_password !=$this->getRequest()->res_con_pass)
                   {

                     throw new Exception($this->translate('confirm password does not match with password'));
                   }
                   

								
                               $data = array(
                                   // 'deliver_time' => $this->getRequest()->res_delivery_time,
                                  //  'extra_time' => $this->getRequest()->res_extra_time,
									'minimum_order' => $this->getRequest()->minimum_order,
                                    'open_time' =>  $this->getRequest()->open_time,
                                    'close_time'=>$this->getRequest()->close_time,
                                  //  'auto_open_close' => $this->getRequest()->auto_open_close,
                                   // 'approval_check' => $this->getRequest()->autoapprove,
                                    'delivery_charges'=> str_replace('.', ',', $this->getRequest()->delivery_charges),
                                    'delivery_charges_opening_hour' => str_replace('.', ',', $this->getRequest()->delivery_charges_opening_hour),
                                     'payment_method'=>$this->getRequest()->paymentmethod[0].$this->getRequest()->paymentmethod[1],
                                    // 'shop_holiday'=>$this->getRequest()->shop_holiday,
                                    'during_opening'=>$this->getRequest()->slotDeliveryCheckbox,
                                    'duringweekDays'=>$this->getRequest()->duringweekDays,                                    
                                    'mon'=>$this->getRequest()->otime2.'-'.$this->getRequest()->ctime2,
                                    'tue'=>$this->getRequest()->otime3.'-'.$this->getRequest()->ctime3,
                                    'wed'=>$this->getRequest()->otime4.'-'.$this->getRequest()->ctime4,
                                    'thr'=>$this->getRequest()->otime5.'-'.$this->getRequest()->ctime5,
                                    'fri'=>$this->getRequest()->otime6.'-'.$this->getRequest()->ctime6,
                                    'sat'=>$this->getRequest()->otime7.'-'.$this->getRequest()->ctime7,
                                    'sun'=>$this->getRequest()->otime1.'-'.$this->getRequest()->ctime1,
                                    'mo'=>$this->getRequest()->sotime2.'-'.$this->getRequest()->sctime2,
                                    'tu'=>$this->getRequest()->sotime3.'-'.$this->getRequest()->sctime3,
                                    'we'=>$this->getRequest()->sotime4.'-'.$this->getRequest()->sctime4,
                                    'th'=>$this->getRequest()->sotime5.'-'.$this->getRequest()->sctime5,
                                    'fr'=>$this->getRequest()->sotime6.'-'.$this->getRequest()->sctime6,
                                    'sa'=>$this->getRequest()->sotime7.'-'.$this->getRequest()->sctime7,
                                    'su'=>$this->getRequest()->sotime1.'-'.$this->getRequest()->sctime1,
                                    'pick_at_location'=>$this->getRequest()->pickupatlocation,
                                    'select_time'=>$this->getRequest()->select_time,                                    
                                    'taxpayable'      =>str_replace('.', ',', $this->getRequest()->taxpayable)
                                );
                                    
                              $this->database->update(                          
                              $this->tp.'restaurant_setting',
                              $data,
                                "res_id = ".$this->session->admin->resid
                            );

                              
                               //       {                                       
                               //          $datalogo['logo']   = $this->G_Upload_file();      
                               //       }
                               //       else 
                               //       {                                         
                               //           $datalogo['logo']   = 'photo_not_available.png';    
                               //       }
                                ////////////////////////////
                               //    $siz = json_encode(array('status'=>0, 'message'=>'Large File Size'));
                               // if ($_FILES['approval_site_image']['size'] > 5242880 )
                               //    {die ("$siz");}
                                /////////////////////////
                              
                               if(isset($_FILES['res_icon']['name']) AND !empty($_FILES['res_icon']['name'])){
                                      $target_dir = "./uploads/";
                                      // $pdf2 = basename($_FILES["res_icon"]["name"]);
                                      $temp = explode(".", $_FILES["res_icon"]["name"]);
                                      $iconimages = uniqid().implode(".", $temp);
                                    move_uploaded_file($_FILES["res_icon"]["tmp_name"], $target_dir . $iconimages);  
                                    $iconimage['logo'] = $iconimages;
                              $this->database->update(                          
                              $this->tp.'restaurants',
                              $iconimage,
                                "restaurant_id = ".$this->session->admin->resid
                            );
                          }
                                                  /////////////////////////////////////////////////////////////////////////////////////
                                $data1 = array(
                                    'restaurant_name_ar' => $this->getRequest()->res_name_ar,
                                    'restaurant_description_ar' =>$this->getRequest()->res_description_ar,
                                    'contact_email' => $this->getRequest()->res_contact
                                );
                                $this->database->update(
                                $this->tp.'restaurants',
                                $data1,
                                "restaurant_id = ".$this->session->admin->resid
                                );
                                /////////////////////
                                $data2 = array(
                                    'address_ar' => $this->getRequest()->res_address_ar,
                                    'category' => $this->getRequest()->category,
                                    'postal_code' => $this->getRequest()->res_postal_code                                    
                                );
                                $this->database->update(
                                $this->tp.'restaurant_address',
                                $data2,
                                "restaurant_id = ".$this->session->admin->resid
                                );
                                ////////////////////////////
                                if(isset($this->getRequest()->res_password) AND !empty($this->getRequest()->res_password)){
                                $data3 = array(
                                    'user_name'=> $this->getRequest()->res_admin_name,
                                    'contact_number' => $this->getRequest()->res_admin_contact_number,
                                    'account_password' =>$this->getRequest()->res_password
                                );
                              }
                              else{
                                 $data3 = array(
                                    'user_name'=> $this->getRequest()->res_admin_name,
                                    'contact_number' => $this->getRequest()->res_admin_contact_number
                                );
                              }
                                $this->database->update(
                                $this->tp.'restaurant_admin_users',
                                $data3,
                                "restaurant_id = ".$this->session->admin->resid
                                );
                  ///////////////////////////////////////////////////////////////////////////////////////////////////
                          // $this->header();
                          // $form->reset();
                 endif;
            
            
            
        }

        public function G_Upload_file()
        {
            
         $adapter = new Zend_File_Transfer_Adapter_Http();
         $adapter->addFilter('Rename', array('target' => './uploads/'.time().'.jpg',
                 'overwrite' => true));
            
         if (!$adapter->receive()) 
                {
                
                  return $messages = array();
                 
                
                }
            return $adapter->getFileName(null,false); 
        }

        
        public function managedeliveryareasAction(){

            $this->checkLogin();

            

            $response = new stdclass;

            $response->error = false;

            $response->success = false;

            

            if(isset($this->getRequest()->xaction)){

                try{

                    switch($this->getRequest()->xaction){

                        case 'delete': {

                            $this->database->delete(

                                $this->tp . "restaurant_delivery_areas",

                                "restaurant_id = ".$this->getRequest()->restaurant_id." and location_id = ".$this->getRequest()->location_id

                            );

                            break;

                        }

                    }

                }catch(Exception $e){

                    $response->error = true;

                    $response->errorMessage = $e->getMessage();

                }

            }

            

            try{

                $response->deliveryAreas = $this->database->fetchAll("select * from {$this->tp}restaurant_delivery_areas left join {$this->tp}locations on {$this->tp}restaurant_delivery_areas.location_id = {$this->tp}locations.location_id where restaurant_id = ?", $this->session->admin->resid); 

            }catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $this->translate('Failed to load delivery areas. Please re-try again.');

            }

            

            try{

                $response->cities = array('' => $this->translate('-- Select A City / Town --'), 'Istanbul' => 'Istanbul');

                $response->locations = array('' => $this->translate('-- Select A Location / Region --'));

            }catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $this->translate('Failed to load location details.');

            }

            $response->test=$this->test;

		    $response->count=$this->count;

            $this->view->sidebar_menu_item = 'settings';

            $this->view->assign((array)$response);

            $this->view->headTitle($this->translate('Delivery Areas').' | ', 'PREPEND');

        }

        

        public function ajaxadddeliveryareaAction(){

            $response = new stdclass;

            $response->error = false;

            $response->success = false;

            

            try{

                //validating fields

                    if(!Zend_Validate::is($this->getRequest()->res_location, 'NotEmpty') || !Zend_Validate::is($this->getRequest()->res_delivery_fees, 'NotEmpty'))

                        throw new Exception($this->translate('All fields are mandatory.'));

                    if(!is_numeric($this->getRequest()->res_delivery_fees) || $this->getRequest()->res_delivery_fees < 0)

                        throw new Exception($this->translate('Delivery amount must be numeric and greater than or equal to 0.'));

                //checking whether delivery area already added or not

                    $delivery_area = $this->database->fetchOne("select count(*) from {$this->tp}restaurant_delivery_areas where restaurant_id = ? and location_id = ?", array($this->session->admin->resid, $this->getRequest()->res_location));

                    if($delivery_area) throw new Exception($this->translate('Delivery area already exists in the list, cannot add twice.'));

                //adding the delivery area

                    try{

                        $this->database->insert(

                            $this->tp."restaurant_delivery_areas",

                            array(

                                'restaurant_id' => $this->session->admin->resid,

                                'location_id' => $this->getRequest()->res_location,

                                'minimum_delivery_amount' => $this->getRequest()->res_delivery_fees

                            )

                        );

                        $response->success = true;

                    }catch(Exception $e){

                        throw new Exception($this->translate('Failed to add. Please re-try again.'));

                    }

            }catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $e->getMessage();

            }

            

            $this->getHelper('Json')->sendJson($response);

        }

        

        

        public function ajaxsaveamountAction(){

            $response = new stdclass;

            $response->error = false;

            $response->success = false;

            

            try{

                //validating fields

                    if(!Zend_Validate::is($this->getRequest()->delivery_amount, 'NotEmpty'))

                        throw new Exception($this->translate('All fields are mandatory.'));

                    if(!is_numeric($this->getRequest()->delivery_amount) || $this->getRequest()->delivery_amount < 0)

                        throw new Exception($this->translate('Delivery amount must be numeric and greater than or equal to 0.'));

                //updating delivery amount

                    try{

                        $this->database->update(

                            $this->tp."restaurant_delivery_areas",

                            array(

                                'minimum_delivery_amount' => $this->getRequest()->delivery_amount

                            ),

                            'restaurant_id = ' . $this->getRequest()->restaurant_id . ' and location_id = ' . $this->getRequest()->location_id

                        );

                        $response->success = true;

                    }catch(Exception $e){

                        throw new Exception($this->translate('Failed to add. Please re-try again.'));

                    }

            }catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $e->getMessage();

            }

            

            $this->getHelper('Json')->sendJson($response);

        }

        

        public function manageworkinghoursAction(){

            $this->checkLogin();

            

            $response = new stdclass;

            $response->error = false;

            $response->success = false;

            

            try{

                $workingHours = $this->database->fetchAll("select * from {$this->tp}restaurant_working_hours where restaurant_id = ?", $this->session->admin->resid);

                $response->workingHours = array(

                    'sun' => (object)array('is_closed' => true, 'start_time' => '0:00', 'end_time' => '0.00'),

                    'mon' => (object)array('is_closed' => true, 'start_time' => '0:00', 'end_time' => '0.00'),

                    'tue' => (object)array('is_closed' => true, 'start_time' => '0:00', 'end_time' => '0.00'),

                    'wed' => (object)array('is_closed' => true, 'start_time' => '0:00', 'end_time' => '0.00'),

                    'thu' => (object)array('is_closed' => true, 'start_time' => '0:00', 'end_time' => '0.00'),

                    'fri' => (object)array('is_closed' => true, 'start_time' => '0:00', 'end_time' => '0.00'),

                    'sat' => (object)array('is_closed' => true, 'start_time' => '0:00', 'end_time' => '0.00'),

                );

                foreach($workingHours as $entry){

                    $response->workingHours[$entry->working_day] = $entry;

                }

            }catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $this->translate('Failed to load working hours details.');

            }

            $response->test=$this->test;

		    $response->count=$this->count;

            $this->view->sidebar_menu_item = 'settings';

            $this->view->assign((array)$response);

            $this->view->headTitle($this->translate('Modify Working Hours').' | ', 'PREPEND');

        }

        

        public function ajaxmodifyworkinghourAction(){

            $response = new stdclass;

            $response->error = false;

            $response->success = false;

            

            try{

                $start_time = date_parse($this->getRequest()->start_time . ':00');

                $end_time = date_parse($this->getRequest()->end_time . ':00');

                if((bool)$this->getRequest()->is_closed and $start_time['hour'] > $end_time['hour'])

                    throw new Exception($this->translate('Start Time should be less than end time'));

                //saving data

                    $isAvailable = (bool)$this->database->fetchOne("select count(*) from {$this->tp}restaurant_working_hours where restaurant_id = ? and working_day = ?", array($this->session->admin->resid, $this->getRequest()->working_day));

                    try{

                        if($isAvailable){

                            $this->database->update(

                                $this->tp.'restaurant_working_hours',

                                array(

                                    'is_closed' => $this->getRequest()->is_closed,

                                    'start_time' => $this->getRequest()->start_time,

                                    'end_time' => $this->getRequest()->end_time

                                ),

                                "restaurant_id = ".$this->session->admin->resid." and working_day = '".$this->getRequest()->working_day."'"

                            );

                        }else{

                            $this->database->insert(

                                $this->tp.'restaurant_working_hours',

                                array(

                                    'restaurant_id' => $this->session->admin->resid,

                                    'working_day' => $this->getRequest()->working_day,

                                    'is_closed' => $this->getRequest()->is_closed,

                                    'start_time' => $this->getRequest()->start_time,

                                    'end_time' => $this->getRequest()->end_time

                                )

                            );

                        }

                        $response->success = true;

                    }catch(Exception $e){

                        throw new Exception($this->translate('Failed to update working hour. Please re-try again.'));   

                    }

            }catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $e->getMessage();

            }

            

            $this->getHelper('Json')->sendJson($response);

        }

    }