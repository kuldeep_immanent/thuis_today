<?php
    include('CommonController.php');
    class Restaurant_CouponController extends Zend_Controller_Action
	{
        
        public $database;
        public $tp; //Table prefix
        public $config;
        public $appConfig;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_C_helper;
        public $checkhelper;
        public $table;
        public function init(){
            //initializing resources
            $bootstrap        = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database   = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp         = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig  = $bootstrap->getResource('AppConfig');
            $this->config     = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->table      = 'for_coupons';     
            $this->_C_helper  = $this->getHelper('Custom');
          
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/restaurant/views/layouts',
                'layout' => 'restaurantadmin'
            ));
            //adding default page title
            $this->view->headTitle('restaurant Panel | Coupon Management System | Delivery');
        }
        
        public function preDispatch(){
			if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
             $this->view->sidebar_menu_item = 'coupons';
        }
        
        public function translate($message){
            return $this->translator->translate($message);
        }
        
        public function postDispatch(){
            //adding few variables to view
            
        }
        
        /*********
         * function  to get the user lists
         */
        public function indexAction()
         {
            
           $response         = new stdClass();
           $respose->error   = false;
           $response->message = null;
           $resid=$this->session->admin->resid;
           try
           {
               
             $current_date  =strtotime(date('Y-m-d'));  
             $response->data  = $this->_C_helper->G_Get_Data("Select * from {$this->table} where expiry_date>$current_date and restaurant_id=$resid");
             $response->count = count($response->data);
               
               
           } catch (Exception $ex) {
            
                $response->message =$ex->getMessage();
               
           }
           
            
            $paginator = Zend_Paginator::factory($response->data);
            $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
            $paginator->setDefaultItemCountPerPage(10000);
            $this->view->paginator = $paginator;
            $this->view->assign((array)$response);       
            $this->view->headTitle($this->translate('Coupon').' | ', 'PREPEND');        
            
        }
        /********
         * 
         * Function to add Users
         */
        public function addeditAction()
           {
            $response               = new stdClass;
            $response->error        = false;
            $response->errormessage = null;
            $response->success      = false;
            
            
             if($this->getRequest()->isPost())
             {
                  
                 
                 $saved = $this->Save_Edit_User();
                 
                 if($saved->error)
                 {
                     
                    $response->error        = true;
                    $response->errormessage = $saved->errormessage;   
                     
                     
                 }
                 else
                 {
                     
                    $response->success      = true;   
                     
                 }
               
             }
            else
             {
           
                        try
                        {
                            
                              if(isset($_GET['id'])):
                                  $id               = $this->getRequest()->getParam('id');

                                 $response->data   = $this->_C_helper->G_Get_Data("Select * from {$this->table} where  id={$id}");              
            
                              endif;
                             
                        }catch(Exception $e)
                        {
                             $response->error        = true;
                             $response->errormessage = $e->getMessage();

                        }

            
            }
            $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Add Or Edit  Coupon').' | ', 'PREPEND');
          }
          
          
        /*********
         * function to edit user
         */
        public function editAction()
        {
           
            
            $response               = new stdClass;
            $response->error        = false;
            $response->errormessage = null;
            $response->success      = false;
            
            
             if($this->getRequest()->isPost())
             {
                  
                 
                 $saved = $this->Save_Edit_User();
                 
                 if($saved->error)
                 {
                     
                  $response->error        = true;
                  $response->errormessage = $saved->errormessage;   
                     
                     
                 }
                 else
                 {
                     
                   $response->success      = true;   
                     
                 }
               
             } 
          try
              {
              
                    $id               = $this->getRequest()->getParam('id');
                     
                    $response->data   = $this->_C_helper->G_Get_Data("Select * from {$this->table}users where  id={$id}");           
                  
                   

                }
                catch(Exception $e)
                {
                     $response->error        = true;
                     $response->errormessage = false;

                }
            
             $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Edit User').' | ', 'PREPEND');
        }
        
        
        
        /********
         * 
         * Function to save the data 
         * 
         */
        
        private Function Save_Edit_User()
        {
             $postresult               = new stdClass;
             $postresult->error        = false;
             $postresult->errormessage = null;
             $postresult->success      = false;
		     $resid=$this->session->admin->resid;
			
                 
                try{   
                       //  ||!Zend_Validate::is($this->getRequest()->ages_range, 'NotEmpty')
					   
                       if( !Zend_Validate::is($this->getRequest()->code, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->expiry_date, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->discount_type, 'NotEmpty')||
                               !Zend_Validate::is($this->getRequest()->discount, 'NotEmpty')  
                               )
                                throw new Exception($this->translate('Mandatory fields are found empty.'));
                       if($this->getRequest()->discount_type=='offer' && is_numeric($this->getRequest()->discount) )
                              throw new Exception($this->translate('Discount Offer should be an item'));
                   $data = $this->_C_helper->_Post_Filtered_Data($this->_request->getPost());        
                   $data['restaurant_id'] = $resid;
                      
                       if(isset($_GET['id']))
                       {
                               
                            $this->database->update(
                                    $this->table,
                                    $data, 
                                    'id ='.$this->getRequest()->getParam('id'));    
                           $postresult->success       = true;
                       }
                       else
                       {
                           
                            $this->database->insert(
                                   $this->table,
                                    $data
                             ); 
                       $postresult->success       = true;
                       }
                       
                       
                       
                }
                catch (Exception $e)
                {

                   $postresult->error         = true;
                   $postresult->success       = false;
                   $postresult->errormessage  = $e->getMessage(); 


                }
                
                
                return $postresult;
             
          
        }
        
        /*******
         * 
         * function to delete the user
         */
        public function deleteAction()
        {
            
            $id = $this->getRequest()->getParam('id');
            try {
                $this->database->delete($this->table, array(
                    'ID = ?' => $id
                    
                ));
                
            } catch (Exception $ex) {
                
            }
             $this->_redirect("/restaurant/admin/Coupon");  
           

            
        }
        
     
       
        
    }