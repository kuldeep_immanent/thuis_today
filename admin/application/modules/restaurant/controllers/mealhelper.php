<?php


class mealhelper extends Zend_Controller_Action{
	
	    public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        
        
  /* initialising the  all parent  vallue and database */
        
        public  function  init()
        {
        	
        	 //initializing resources by singleton instance
        	 $bootstrap=Zend_Controller_Front::getInstance()->getParam('bootstrap');
        	 $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
        	 $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
        	 $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
             $this->appConfig = $bootstrap->getResource('AppConfig');
             $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
             $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
             $this->translator = Zend_Registry::get('Zend_Translate');
         
        	
        }
        
         // Setting the preprocess function before all action be performed
        public function preDispatch()
        {
        	
        	//checking if session is created //
        	  if(!$this->session->admin->loggedin)
               $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
	            //adding few variables to view
	        	$this->view->request = $this->getRequest();
	            $this->view->session = $this->session;
	            $this->view->config = $this->config;
	            $this->view->appConfig = $this->appConfig;	
        	
        }
 
        // Getting all meals of particular restaurant
        public function get_all_restaurant_meals($res_id)
        {
        	
        	$r_meals= new stdclass();
        	
        	$r_meals->error=false;
        	$r_meals->errormessage=null;
        	  
        	
        	
         
        	
        	try
        	{
        		
        		$meals_data=$this->database->select()->from($this->tp.'restaurant_meals',array('meal_id','meal_name'))->where('restaurant_id = ?',$res_id);
        		
        		
        		$r_meals->data = $this->database->fetchAll($meals_data);
        		
        		
        		
        		
        	}
        	catch (Exception $e)
        	{
        		$r_meals->error=true;
        		$r_meals->errormessage=$e->getMessage();
        		
        		
        	}
        	
        		
        	
        	return $r_meals;
        	
        	
        	
        	
        }
        
        
        
        public function  particular_meal_orders($array ,$restaurant_id,$start_date=null)
        {
         $orders=new stdclass;
         $orders->error=false;
         $orders->errormessage=null;
         
         if($array->error == true)
         {
         	
         $orders->error = true;
         $orders->errormessage = $array->errormessage;	
         	
         	
         }
         else 
         {
         	$today_date = date("Y-m-d");
         	
             if(!is_null($start_date))
             {
             	$cluase = " order_date BETWEEN '{$start_date}' AND '{$today_date}'";
             
             }
             else {
             	
             $cluase = "order_date  = '{$today_date }'";	
            	
             }
         	
	
         try
         {
 	
         	//print_r($orders->data[0]->order_date);
         
         	$product_report=array();
         	$initial=0;
         	
         	foreach($array->data as $arr):
         	
         	 $id=$arr->meal_id;
         	 
         	$order = "SELECT sum(total) as total, sum(quantity) as quantity FROM {$this->tp}restaurant_per_item_order_report" . "  WHERE res_id={$restaurant_id} " . " AND  $cluase  AND item_id ={$id} " ; 
         	
            echo $order;
            echo die;
         		
         	$product_order = $this->database->fetchAll($order);
         
         	
         	 if($product_order[0]->total == "")
         	 {
         	 	
         	 	$product_order[0]->total = 0;
         	 	$product_order[0]->quantity = 0;
         	 	
         	 }
         
         	
         	$product_report['name'][$initial] = $arr->meal_name;
         	$product_report['total'][$initial] = $product_order[0]->total;
         	$product_report['quantity'][$initial] = $product_order[0]->quantity;
         	
         
         	
            $initial++;
             
         	endforeach;
         
         	$orders->data=$product_report;
         
         		

         	
         }
         catch(Exception $e)
         {
         	
         	
         $orders->error=true;
         $orders->errormessage=$e->getMessage();	
         	
         	
         }
         
         }
        
         return $orders;
         
         }
        	
        	
        	
       
	
        
   /* function to translate the language */
			
			public  function translate()
			{
				
				
				  return $this->translator->translate($message);
				
				
				
			}
			
	// Get Order Report  of every particular  Meal of particular Restaurant

	
	
	
	
	
	
	
	
}







?>