<?php
include('CommonController.php');
    class Restaurant_FeedbackController extends Zend_Controller_Action{
        
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_C_Helper;
        public $_table;
        
        public function init(){
            //initializing resources
            $bootstrap        = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database   = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp         = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig  = $bootstrap->getResource('AppConfig');
            $this->config     = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test       = commonnotify::Notification();
            $this->count      = commonnotify::total_count();
            $this->_C_Helper  = $this->getHelper('Myreuse');
            $this->table      = 'fos_feedbacks';
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/restaurant/views/layouts',
                'layout' => 'restaurantadmin'
            ));
            //adding default page title
            
        	
            $this->view->headTitle($this->translate('Shop Feedback Panel').' | Delivery');
        }
        
        public function preDispatch(){
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
           
        }
        
        public function postDispatch(){
            
        }
        
        private function translate($message){
            return $this->translator->translate($message);
        }
        
        private function checkLogin(){
            if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);
        }
        
        /****
         * function to list the all deliver boys
         */
        public  function indexAction(){
            $response = new stdclass;
            $response->error = false;
            $response->messaage = null;
            
            
            try 
            {
                
                $query = "Select * from {$this->table} inner join fos_users on {$this->table}.user_id =fos_users.ID where res_id={$this->session->admin->resid}";
                
                $response->data =  $this->rebuitArray($this->_C_Helper->G_Get_Data($query,'result')); 
				 
                
               
            } 
            catch (Exception $ex) 
            {
            $response->error = true;
            $response->messaage = $ex->getMessage();
            }
            
             $paginator = Zend_Paginator::factory($response->data);
             $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
             $paginator->setDefaultItemCountPerPage(10000);
             $this->view->paginator = $paginator;
             $this->view->sidebar_menu_item = 'feedback';
             $this->view->assign((array)$response);
             $this->view->headTitle($this->translate('Feedback List').' | ', 'PREPEND');
            
        }
		
		
		/*******
* function to rebuit Array for specific use
*****/
         private function rebuitArray($data)
         {
         	
             $dummyArray =array();
            
             foreach ($data as $key => $value) {
                  
                  $maindata['id'] =$value->id;
                  $maindata['order_id'] =$value->order_id;
                  $maindata['order_id'] =$value->order_id;
                  $maindata['res_id'] =$value->res_id;
                  $maindata['comment'] =$value->comment;
                  $maindata['FirstName'] =$value->FirstName;  
                  $maindata['LastName'] =$value->LastName;                
                  $maindata['delivery_boy']= $this->Getdeliverboy($value->order_id);
             
             	$dummyArray[] =$maindata;
             }

          
           return $dummyArray;

         }

  /*******
  * Adding Delivery Boy Name in list
  *********/
         private function Getdeliverboy($order_id)
         {

         	$query  = "select delivery_id from {$this->tp}delivery_order_rel_boy where order_id=$order_id";
         	

         	$data   = $this->_C_Helper->G_Get_Data($query,'result');
         	
            if(count($data)>0)
            {
              $deliveryboyid=$data[0]->delivery_id;
              $query  = "select name from {$this->tp}delivery_boy_login where id=$deliveryboyid";
         	  $data   = $this->_C_Helper->G_Get_Data($query,'result');
              
              if(count($data)>0)
              {
                   return $data[0]->name;

              }
              else
              {
                 return null;

              }

            }
            else
             {
               return null;
             }	
 

         }

        
         /***
          * funtion to addedit the delivery boy status
          */
         public function detailAction()
         {
             
             /***** 
               * saving and updating the values of setting
               */
            $response = new stdclass;
            $response->error = false;
            $response->errormessage = null;
            $response->success = false;
            
            
            try 
            {
                 if(isset($_GET['id'])):
                       $id    = $_GET['id']; 
                       $query = "Select * from {$this->table} inner join fos_users on {$this->table}.user_id =fos_users.ID where res_id={$this->session->admin->resid} and  {$this->table}.id=$id";

                        $response->data  =  $this->_C_Helper->G_Get_Data($query);
                 endif;
                 $response->restaurants->count = 0;
                 $queryStr = "SELECT * " .
                "FROM {$this->tp}restaurant_customer_order"."  
                WHERE id={$response->data[0]->order_id}";
                $response->restaurants->list = $this->_C_Helper->G_Get_Data($queryStr);
                $response->restaurants->count = count($response->restaurants->list);  
            }
            catch (Exception $e)
                       {
   
                            $response->error = true;
                            $response->errormessage = $e->getMessage();

                       }
                       
                       
             $this->view->sidebar_menu_item = 'feedback';
             $this->view->assign((array)$response);
             $this->view->headTitle($this->translate('Detail').' | ', 'PREPEND');        
             
             
         }
         /***
          * function to delete Action to delete the delivery boys
          */
         public function deleteAction()
         {
             try {
                 
                 $id = $this->getRequest()->id;
                 $this->database->delete($this->table, array(
                    'ID = ?' => $id
                    
                ));
                 
                 
             } catch (Exception $ex) {
                 
             }
             
             $this->_redirect("/restaurant/admin/feedback");  
             
         }
       
       
        
        /*******
         * function  to save and edit the setttings
         */
        private function SaveEditdelivery($type)
        {
            if(!Zend_Validate::is($this->getRequest()->email, 'NotEmpty')||
                    !Zend_Validate::is($this->getRequest()->name, 'NotEmpty'))
                   
                        throw new Exception($this->translate('All fields are mandatory.'));
            
             if(!isset($_GET['id']))
             {
                 if(!Zend_Validate::is($this->getRequest()->password, 'NotEmpty') || $this->getRequest()->password != $this->getRequest()->cpassword)
                 {
                     throw new Exception($this->translate('password does not Match With Conform password'));
                     
                 }      
                 
                 
             }    
                  $emailtest   = $this->getRequest()->email;
                  $query       = "Select * from {$this->table} where email='$emailtest'";
                   
                  $Emailcheck  =  $this->_C_Helper->G_Get_Data($query,'count');
                  
                  if($Emailcheck>0)
                  {
                      
                      throw new Exception($this->translate('Email is already existing choose new one'));
                       
                  }
                  
            
                 if($type=='insert'):
                         $this->database->insert(
                                $this->table,
                                array(
                                    'email' => $this->getRequest()->email,
                                    'name' => $this->getRequest()->name,
                                    'password' => md5($this->getRequest()->password),
                                    'availabe_status' => 0,
                                    'res_id' => $this->session->admin->resid,
                                    'is_loggined'=>1
                                     )
                     );
                     else:
                         
        if(isset($_POST['password']) && $_POST['password'] !=''):
                $data = array(
                                        'email' => $this->getRequest()->email,
                                        'name' => $this->getRequest()->name,
                                        'password' => $this->getRequest()->password,

                              );
                else:

                    $data = array(
                                        'email' => $this->getRequest()->email,
                                        'name' => $this->getRequest()->name



                                    );
            
           endif;
                          $this->database->update(
                               $this->table,
                                $data,
                                "id = ".$this->getRequest()->id
                            );
                         
                 endif;
            
            
        }
        
       
    }
    
    
    ?>