<?php



 include('CommonController.php');

    class Restaurant_OrdersController extends Zend_Controller_Action{

        

        public $database;

        public $tp; //Table prefix

        public $appConfig;

        public $config;

        public $session;

        public $translator;

        public $test;

        public $count;

        

        

        

        public function init(){

        

        	 

        	

            //initializing resources

            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');

            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();

            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);

            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');

            $this->appConfig = $bootstrap->getResource('AppConfig');

            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');

            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');

            $this->translator = Zend_Registry::get('Zend_Translate');

            $this->test=commonnotify::Notification();

            $this->count=commonnotify::total_count();

            //initializing layout

            Zend_Layout::startMvc(array(

                'layoutPath' => APPLICATION_PATH . '/modules/restaurant/views/layouts',

                'layout' => 'restaurantadmin'

            ));

            //adding default page title

            $this->view->headTitle($this->translate('Admin Panel').' | Delivery');

        }

        

        public function preDispatch(){

            //checking session

            if(!$this->session->admin->loggedin)

                $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);

            //adding few variables to view

            $this->view->request = $this->getRequest();

            $this->view->session = $this->session;

            $this->view->config = $this->config;

            $this->view->appConfig = $this->appConfig;

            $this->view->sidebar_menu_item = "orders";  

        }

		

		public function translate($message){

            return $this->translator->translate($message);

        }

        

      

       

    // Function for editing the  orders of particular user //    

        public function editAction()

		{

        	

        	 

			 $detail=new stdclass;

			 $detail->error=false;

			 $detail->success=false;

			 

			

			  try{

			  	

                 $detail->restaurants->count = 0;

                // fetching  the restaurant particular order data from database //

                

                $queryStr = "SELECT id, restaurant_name, firstname, lastname,address,postcode,city, order_date, delivery_date, total_amount,cart,deleted"." ".

                   

                "FROM {$this->tp}restaurant_customer_order"." "."INNER JOIN "." "."{$this->tp}restaurants ON {$this->tp}restaurants.restaurant_id = {$this->tp}restaurant_customer_order.restuarant_service_id "." "." WHERE

                {$this->tp}restaurant_customer_order.id= {$_GET['id']}";

                

				

                $detail->restaurants->list = $this->database->fetchAll($queryStr);

                

               

                

                $detail->restaurants->count = count( $detail->restaurants->list); 

               

			  }

            catch(Exception $e){

                 $detail->error = true;

                 $detail->errorMessage = $this->translate('Failed to load restaurant list. Please re-try again.');

            }

            

			/* if getting the request to edit the value */

			

			 if($this->getRequest()->isPost()){

			 	

			   

			  

			  try{

                    //validating fields by exception handling 

                     

                    if( !Zend_Validate::is($this->getRequest()->res_firstname, 'NotEmpty') ||!Zend_Validate::is($this->getRequest()->res_lastname , 'NotEmpty')

                       

                    ||!Zend_Validate::is($this->getRequest()->res_total_amount , 'NotEmpty')    ||!Zend_Validate::is($this->getRequest()->res_address , 'NotEmpty')

                     ||!Zend_Validate::is($this->getRequest()->res_city , 'NotEmpty')) 

                     

                        throw new Exception($this->translate('Mandatory fields are found empty.'));

                        

                        

                        



                   /* posted data */  

                        	   

                     

						 try{

						 	

						// updating data according to  request // 	

                     $this->database->update(

                                $this->tp.'restaurant_customer_order',

                                

                                array(

							               'firstname'      => $this->getRequest()->res_firstname,

          							     'lastname'      => $this->getRequest()->res_lastname,

	                            'address'      => $this->getRequest()->res_address,

	                            'postcode'     => $this->getRequest()->res_postcode,

	                            'city'         => $this->getRequest()->res_city,

	                            'total_amount' => $this->getRequest()->res_total_amount 

						    ),

                                'id = '.$this->getRequest()->res_id

                            );

			          }

			          

			          catch(Exception $e)

			          {

                        throw new Exception($this->translate('Failed to save. Please re-try again.'));

                       }

                      

                            

                            $detail->success=true;

                            

	

			     }

			     //caching exception

				 catch(Exception $e){

                 $detail->error = true;

                 $detail->errorMessage = $this->translate('Failed to update particular order restaurant name. Please re-try again.');

                }

				

				}

			      $detail->test=$this->test;

				  $detail->count=$this->count;

             $this->view->assign((array) $detail);

            $this->view->sidebar_menu_item = 'Order detail';

            $this->view->headTitle($this->translate('Dashboard').' | ', 'PREPEND');

        }

		

        	

            

        

        

        /* calling the list of order of */

        

     public function ordersAction(){

     	

                          

               $response          = new stdclass;

               $response->error   = false;

               $response->success = false;

               

                /* Filter Requests */

              

                $where_clause = null;

                

                $order_date   = null;

                

                $deliver_date = null;	

                

                $sort_by='asc';

                $id=$this->session->admin->resid;

               
               

                // post requests //

		  if($this->getRequest()->isPost()){

		  	 

             try{

                switch($this->getRequest()->frmaction){

                	

                	

                  case 'deleted': {

                  	

                  	

                    		

                        	  if(!empty($this->getRequest()->selectedcheckbox)):

                        	  

                            foreach($this->getRequest()->selectedcheckbox as $resid){

                         

                            	

                                try{

                                	

                               $this->database->update(

                                        'customer_order',

                                        array('status' => '5'),

                                        'id = '.$resid

                                        

                                    );

                                    $response->success=true;

                                    

                                }		

                                catch(Exception $e){

                                    //do nothing

                                }

                            }

                            endif; 

                            break;

                        }

                        
///////////////////////////////////////////////////
                case 'fordelivery': {

                           if(!empty($this->getRequest()->selectedcheckbox)):

                           

                            foreach($this->getRequest()->selectedcheckbox as $resid){

                                try{

                                  

                                    $this->database->update(

                                        'customer_order',

                                        array('status' => 7),

                                        'id = '.$resid

                                        

                                    );

                                    

                                   $response->success=true; 

                                    

                                }catch(Exception $e){

                                    //do nothing

                                }

                            }

                            endif;

                            break;

                           }
////////////////////////////////
                case 'publish': {

                        	 if(!empty($this->getRequest()->selectedcheckbox)):

                        	 

                            foreach($this->getRequest()->selectedcheckbox as $resid){

                                try{

                                	

                                    $this->database->update(

                                        'customer_order',

                                        array('status' => 0),

                                        'id = '.$resid

                                        

                                    );

                                    

                                   $response->success=true; 

                                    

                                }catch(Exception $e){

                                    //do nothing

                                }

                            }

                            endif;

                            break;

                           }
               case 'Unapprove': {

                           if(!empty($this->getRequest()->selectedcheckbox)):

                           

                            foreach($this->getRequest()->selectedcheckbox as $resid){

                                try{

                                  

                                    $this->database->update(

                                        'customer_order',

                                        array('status' => 4),

                                        'id = '.$resid

                                        

                                    );

                                    

                                   $response->success=true; 

                                    

                                }catch(Exception $e){

                                    //do nothing

                                }

                            }

                            endif;

                            break;

                           }

                case 'Approve': {

                           if(!empty($this->getRequest()->selectedcheckbox)):

                           

                            foreach($this->getRequest()->selectedcheckbox as $resid){

                                try{

                                  

                                    $this->database->update(

                                        'customer_order',

                                        array('status' => 3),

                                        'id = '.$resid

                                        

                                    );

                                    

                                   $response->success=true; 

                                    

                                }catch(Exception $e){

                                    //do nothing

                                }

                            }

                            endif;

                            break;

                           }
                           


                      }

                      $response->success = true; 

                     }

                     

				catch(Exception $e){

						

							$response->error = true;

							$response->errorMessage = $e->getMessage();

						 }

						 

						 // sorting by different Status

						 try{

						 	  if(!empty($this->getRequest()->filterbystatus)) 

						 	  {

						 	

                               if($this->getRequest()->filterbystatus=="Canceled")

                               {

                               try{

	     

						 			                  $where_clause='AND status = 5';	

						 				

						 			               }

						 			catch (Exception $e)

						 			{

						 				$response->error = true;

						 				

							             $response->errorMessage = $e->getMessage();

						 				

						 			}

						 					 				

						 			}

						 			

						 			else 

						 			{

						 			 $where_clause='AND status =0  ';	

						 				

						 			}

						 	

						 	}

						 

						 }

						 catch(Exception $e)

						 {

						 	

						 	$response->error = true;

							$response->errorMessage = $e->getMessage();

						 	

						 	

						 }

						 

						 //order by  data

						 if(!empty($this->getRequest()->sortorder))

						 {

						 	

						 	switch ($this->getRequest()->sortorder)

						 	{

						 		

						 	case 'desc':

						 			{

						 				

						 			$sort_order='order by total_payment'.$this->getRequest()->sortorder;	

				

						 				

						 			}

						 			

						 	default :

						 			

						 			{

						 				

						 			$sort_order='order by total_payment asc';		

						 				

						 				

						 			}

						 			

						 		

						 		

						 		

						 		

						 	}

						 	

						 	

						 	

						 	

						 }

						 // filter data by order_date //

				if(!empty($this->getRequest()->Order_from) && !empty($this->getRequest()->Order_from) )

				{		 

						 

					try{

						

						  	

						  $Order_from = new DateTime($this->getRequest()->Order_from);

						  $order_date= "'".$Order_from->format("Y-m-d H:i:s")."'";

						  $Order_to = new DateTime($this->getRequest()->Order_to);

						  $orders_to= "'".$Order_to->format("Y-m-d H:i:s")."'";

						  $order_date=' AND  date BETWEEN '. $order_date.' '.'AND'.' '.$orders_to;

   

						

						 }

						 catch (Exception $e)

						 {

						 	

						 $response->error = true;

						 $response->errorMessage = $e->getMessage();	

					 	

					 }

				}

				

				 

				

		  }

	

     	  try{

     	  	

                $response->restaurants->count = 0;

       

                //Main query for fetching the restaurant orders //

             $queryStr = "select customer_order.*, {$this->tp}restaurants.restaurant_name_ar, {$this->tp}users.name, {$this->tp}users.zipcode, {$this->tp}users.address, {$this->tp}users.city from customer_order left join {$this->tp}restaurants ON {$this->tp}restaurants.restaurant_id = customer_order.shopid left join {$this->tp}users ON {$this->tp}users.id = customer_order.user_id where customer_order.shopid={$id}"." ".(!empty($where_clause) ? $where_clause : '')."  ".(!empty( $order_date) ? $order_date : '')."  ".(!empty($sort_order) ? $sort_order : '');
            
                $response->restaurants->list = $this->database->fetchAll($queryStr);
                
				// print_r($response->restaurants->list);die;
 

                $response->restaurants->count = count($response->restaurants->list);

  

            }

                catch(Exception $e){

                	

                $response->error = true;

                

                $response->errorMessage = $this->translate('Failed to load restaurant list. Please re-try again.');

            }

            

              $response->test  = $this->test;

	            $response->count = $this->count;

              

              $paginator = Zend_Paginator::factory($response->restaurants->list);  

              $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
               $paginator->setDefaultItemCountPerPage(10000);
              $this->view->paginator = $paginator; 

             $this->view->assign((array)$response);

             

            

            $this->view->headTitle($this->translate('Dashboard').' | ', 'PREPEND');

        }

        ////////////////////////////////////////////////////////////////////

     public function invoiceAction()
        {     
               $response          = new stdclass;
               $response->error   = false;
               $response->success = false;    
                /* Filter Requests */   
                $where_clause = null;   
                $order_date   = null;
                $deliver_date = null;    
                $sort_by='asc';
                $id=$this->session->admin->resid;      
                // post requests //
          try
           {
                $response->restaurants->count = 0;      
                $response->data2  = $this->database->fetchAll("Select * from {$this->tp}invoice where shopid = $id and status = 0");
                 ////////////////////
                 $response->data3  = $this->database->fetchAll("Select * from {$this->tp}invoice where shopid = $id and status = 1");                   
            }
         catch(Exception $e)
            {    
                $response->error = true;
                $response->errorMessage = $this->translate('Failed to load invoice list. Please re-try again.');
            }                
              $paginator = Zend_Paginator::factory($response->data2);  
              $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
              $paginator->setDefaultItemCountPerPage(10000);
              $this->view->paginator = $paginator; 
              $this->view->assign((array)$response);
              $this->view->sidebar_menu_item = 'invoice';
              $this->view->headTitle($this->translate('Dashboard').' | ', 'PREPEND');
        }
        /////////////////////////////////////////////////////////////////////////

         // order detail

	    public function detailAction(){

		      

     	       $id= $_GET['id'];


     	  try{
              
               if(isset($this->getRequest()->statusupdate))
               {
                 $this->UpdateStatus($this->getRequest()->statusupdate,$id);

               }

     	  	    $response->restaurants->count = 0;

                

                $queryStr = "Select customer_order.*, {$this->tp}users.name, {$this->tp}users.zipcode, {$this->tp}users.phone, {$this->tp}users.address, {$this->tp}users.city from  customer_order left join {$this->tp}users ON {$this->tp}users.id = customer_order.user_id  where customer_order.id={$id}";


                $response->restaurants->list = $this->database->fetchAll($queryStr);
                
                $querydicount ="Select customer_order.*, {$this->tp}users.name, {$this->tp}users.zipcode, {$this->tp}users.phone, {$this->tp}users.address, {$this->tp}users.city from  customer_order left join {$this->tp}users ON {$this->tp}users.id = customer_order.user_id  where customer_order.id={$id}";

                $response->discountdetail     = $this->database->fetchAll($querydicount);

              

                $response->restaurants->count = count($response->restaurants->list);

  

            }

            catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $this->translate('Failed to load restaurant list. Please re-try again.');

            }

             $response->test=$this->test;

             $response->count=$this->count;

              

             $this->view->assign((array)$response);

             $this->view->sidebar_menu_item = 'orders';

             $this->view->headTitle($this->translate('Dashboard').' | ', 'PREPEND');

        }
 
        

        public function barcoderenderAction()
        {
            


                    
                  header('Content-type: image/gif');
                  $barcodeOptions = array(
                      'text' =>  'ZEND-FRAMEWORK', 
                      'barHeight'=> 74, 
                      'factor'=>3.98,
                  );


                  $rendererOptions = array();
                  $renderer = Zend_Barcode::factory(
                      'code128', 'image', $barcodeOptions, $rendererOptions
                  )->render();
                 die;








              }

 
 /*******
 * function to update the status of particular order
 *******/

    public function UpdateStatus($status,$id)
    {

          
          $this->database->update(
          $this->tp.'restaurant_customer_order',
          array('status' => $status),
          'id = '.$id
          );


    }

     

    } 

    

     



?>

