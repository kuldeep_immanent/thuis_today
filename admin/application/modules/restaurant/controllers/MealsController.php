 <?php
   include('CommonController.php');
   
   
    use  admin\model ;
    class Restaurant_MealsController extends Zend_Controller_Action{
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_helper;
        public $model;
        
        public function init(){
            //initializing resources
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig = $bootstrap->getResource('AppConfig');
            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test=commonnotify::Notification();
            $this->count=commonnotify::total_count();
            $this->_C_helper  = $this->getHelper('Custom');
           
            
              
           
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/restaurant/views/layouts',
                'layout' => 'restaurantadmin'
            ));
            //adding default page title
            $response = new stdclass;  
            $response->test=$this->test;
            $response->count=$this->count;
            $this->view->headTitle($this->translate('Shop Admin Panel').' |  Delivery');
        }
        
        public function preDispatch(){
            
            
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
        }
        
        public function postDispatch(){
            
        }
        
       
        private function checkLogin(){
            if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);
        }
        
        public function managemealsAction(){

            $this->checkLogin();
			/* echo "<pre>";
			print_r($this->session->admin);
			die; */
            $resid=$this->session->admin->resid;
            $response = new stdclass;
            $response->error = false;
            $response->success = false;

            if(isset($this->getRequest()->category)):
                
                $where ='where '.$this->tp.'restaurant_meals.category_id='.$this->getRequest()->category .' and '.$this->tp.'restaurant_meals.restaurant_id='.$resid;
                else:
                $where ='where '.$this->tp.'restaurant_meals.restaurant_id='.$resid;    
               
            endif;
            
            if(isset($this->getRequest()->doAction)){
                try{
                    switch($this->getRequest()->doAction){
                        case 'activatemeal': {
                            $this->database->update(
                                $this->tp.'restaurant_meals',
                                array(
                                    'is_active' => 1
                                ),
                                'meal_id = '. $this->getRequest()->meal_id
                            );
                            break;
                        }
                        case 'deactivatemeal': {
                            
                            $this->database->update(
                                $this->tp.'restaurant_meals',
                                array(
                                    'is_active' => 0
                                ),
                                'meal_id = '. $this->getRequest()->meal_id
                            );
                            break;
                        }
                        case 'deletemeal': 
                         {
                            //TODO: add other records to delete
                            $this->database->delete(
                                $this->tp.'restaurant_meals',
                                'meal_id = '. $this->getRequest()->meal_id
                            );
                            break;
                        }
                       
                    }
                }catch(Exception $e){
                    $response->error = true;
                    $response->errorMessage = $this->translate('Your action failed. Please re-try again.'.$e->getMessage());
                }
            }
            
            //loading meals category
                try
                  {
					 //  where restaurant_id = $resid
					 //$resid = $this->session->admin->resid;
					 
                    $categories = $this->database->fetchAll("select * from {$this->tp}restaurant_meals_category  where restaurant_id = $resid order by category_name");
                    $response->mealscategories = array('' => '--- '.$this->translate('Select All').' ---');
                    foreach($categories as $entry)
                    {
                        $response->mealscategories[$entry->category_id] = stripslashes($entry->category_name_ar);
                    }

                     $vats = $this->database->fetchAll("select * from {$this->tp}vat");
                    $response->vat = array('all' => '--- '.$this->translate('Select vat').' ---');
                    foreach($vats as $entry)
                    {
                        $response->vat[$entry->id] = stripslashes($entry->vat.'% '.$entry->type);
                    }
                }
                catch(Exception $e)
                {
                    $response->error = true;
                    $response->errorMessage = $this->translate('Failed to load products categories. Please re-try again.');
                }
            //loading all meals
                try
                {

                    $response->meals = $this->database->fetchAll(
                        "select *, (select count(*) from {$this->tp}restaurant_meals_selections where {$this->tp}restaurant_meals_selections.meal_id = {$this->tp}restaurant_meals.meal_id) as total_selections, ".
                        "(select count(*) from {$this->tp}restaurant_meals_addons where {$this->tp}restaurant_meals_addons.meal_id = {$this->tp}restaurant_meals.meal_id) as total_addons" .
                        " from {$this->tp}restaurant_meals left join {$this->tp}restaurant_meals_category on {$this->tp}restaurant_meals.category_id = {$this->tp}restaurant_meals_category.category_id $where order by {$this->tp}restaurant_meals.meal_name");
                
                        
                            
                        }
                catch(Exception $e)
                {
                    $response->error = true;
                    $response->errorMessage = $e->getMessage();    
                }
                
                
                /********
                 * 
                 * getting helper  data to get the exact restaurant list
                 */
                
                $restaurant = $this->_C_helper->Get_Data_List();
                if($restaurant->error)
                  {
                            
                     $response->error = true;
                     $response->errorMessage = $e->getMessage();      
                  }
                  else
                  {
                     $response->restaurant   = $restaurant->restaurant;  
                      
                      
                  }
            
            $response->test   = $this->test;
            $response->count  = $this->count;
            $this->view->sidebar_menu_item = "managemeals";
            $this->view->headTitle($this->translate('Manage Products').' | ', 'PREPEND');
            $paginator = Zend_Paginator::factory($response->meals);
            $paginator->setDefaultItemCountPerPage(10000);
            $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
             // set the current page to what has been passed in query string, or to 1 if none set
             
           
            // set the number of items per page to 10
                       
             
            $this->view->paginator = $paginator;
            $this->view->assign((array)$response);
        }
        
        public function ajaxaddcategoryAction(){
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
            
            try{

                if(!Zend_Validate::is($this->getRequest()->category_name, 'NotEmpty'))
                     throw new Exception($this->translate('Category Name  cant be empty'));
                $this->database->insert(
                    $this->tp.'restaurant_meals_category',
                    array(
                        'category_name' => $this->getRequest()->category_name,
                        'is_enabled' => true
                    )
                );
                $response->success = true;
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Json')->sendJson($response);
        }
        
        public function ajaxaddmealAction(){
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
            // print_r($this->getRequest()->mealcategory); die;
                    
            try{
                if(!Zend_Validate::is($this->getRequest()->mealname_ar, 'NotEmpty')||
                   !Zend_validate::is($this->getRequest()->mealprice, 'NotEmpty'))
                    throw new Exception($this->translate('Fields marked(*) are mandatory.'));
                if($this->getRequest()->mealprice < 0)
                    throw new Exception($this->translate('Product price must be numeric, and not less than 0'));
                ////////////////////////////////////////                
                // if(empty($this->getRequest()->mealcategory))
                    if(!Zend_Validate::is($this->getRequest()->mealcategory, 'NotEmpty'))
                    throw new Exception($this->translate('Please select product category.'));
                ////////////////////////////////////
                //saving the meal
                try{
                    
                   
                    /********
                     * checking if image is uploaded or not
                     * 
                     */
            if(isset($_FILES['mealpic']['name'])&& $_FILES['mealpic']['name']!='')
            {
            $adapter = new Zend_File_Transfer_Adapter_Http();
            
            $adapter->addFilter('Rename', array('target' => './uploads/'.time().'.jpg',
                     'overwrite' => true)); 
                                          
            
            if (!$adapter->receive()) 
                {
                
                  $messages = $adapter->getMessages();
                  $response->error = false;
                
                }
                
               $file = $adapter->getFileName(null,false);
               $image_add = './uploads/'.$file;
            
            $images = $image_add;
            $new_images = $images;
            $width=200; // define width
            $size=GetimageSize($images);
            $height=round($width*$size[1]/$size[0]);
            $images_orig = ImageCreateFromJPEG($images);
            $photoX = ImagesX($images_orig);
            $photoY = ImagesY($images_orig);
            $images_fin = ImageCreateTrueColor($width, $height);

            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
            ImageJPEG($images_fin,$new_images);
            ImageDestroy($images_orig);
            ImageDestroy($images_fin);    

               $data = array(
                                'category_id' => $this->getRequest()->mealcategory,
                                'meal_name' => trim($this->getRequest()->mealname),
                                'meal_name_ar'=>trim($this->getRequest()->mealname_ar),
                                'meal_description' => trim($this->getRequest()->mealdescription),
                                'meal_description_ar' => trim($this->getRequest()->mealdescription_ar),
                                'meal_price' => str_replace('.',',',$this->getRequest()->mealprice),
                                'is_active' => $this->getRequest()->mealactive,
                               // 'crust_on' => $this->getRequest()->crust_on,
                               // 'custompizza'=>$this->getRequest()->custompizza,
                               // 'souce_on' => $this->getRequest()->souce_on,
								'vat' => $this->getRequest()->vat,
                                'product_unit' => $this->getRequest()->product_unit,
                                'meal_image'=>$file,
                                'restaurant_id'=> $this->session->admin->resid   
                            );
                            
            }
            else  
            {
                
               $data  = array(
                                'category_id' => $this->getRequest()->mealcategory,
                                'meal_name' => trim($this->getRequest()->mealname),
                                'meal_name_ar'=>trim($this->getRequest()->mealname_ar),
                                'meal_description' => trim($this->getRequest()->mealdescription),
                                'meal_description_ar' => trim($this->getRequest()->mealdescription_ar),
                                'meal_price' => str_replace('.',',',$this->getRequest()->mealprice),
                                'product_unit' => $this->getRequest()->product_unit,
                                'vat' => $this->getRequest()->vat,
                                'is_active' => $this->getRequest()->mealactive,
                              //  'crust_on' => $this->getRequest()->crust_on,
                               // 'custompizza'=>$this->getRequest()->custompizza,
                              //  'souce_on' => $this->getRequest()->souce_on,
								'vat' => $this->getRequest()->vat,
                                'product_unit' => $this->getRequest()->product_unit,
                                'restaurant_id'=> $this->session->admin->resid 
                            ); 
                
                
            }
            
              if($this->getRequest()->pizzaselect=='yes'):
                $data['pizza_prices'] = $this->_C_helper->To_Get_Pizza_Price($this->getRequest()->size);
                
                        endif;
                    
                     
                    
                    $this->database->insert(
                        $this->tp.'restaurant_meals',
                        $data
                    );
                    $response->success = true;
                }catch(Exception $e){
                    throw new Exception($this->translate('Failed to add product. Please re-try again.').$e->getMessage());
                }
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Json')->sendJson($response);
        }
        
        public function editmealAction(){
            $response = new stdclass;
            $response->error = false;
            
            
            
            try{
                if(!isset($this->getRequest()->meal_id))
                    throw new Exception($this->translate('No product has been specified to edit.'));
                //loading the meal 
                    try{
                        $response->meal = $this->database->fetchRow("select * from {$this->tp}restaurant_meals left join {$this->tp}restaurant_meals_category on {$this->tp}restaurant_meals.category_id = {$this->tp}restaurant_meals_category.category_id where {$this->tp}restaurant_meals.meal_id = ?", $this->getRequest()->meal_id); 
                    }catch(Exception $e){
                        throw new Exception($this->translate('Failed to load product. Please re-try again.'));
                    }
                //loading meals category
                    try{
						 //$resid=$this->session->admin->resid;
                        $categories = $this->database->fetchAll("select * from {$this->tp}restaurant_meals_category order by category_name");
                        $response->mealscategories = array('' => '--- '.$this->translate('Select All').' ---');
                        foreach($categories as $entry){
                            $response->mealscategories[$entry->category_id] = stripslashes($entry->category_name_ar);
                        }
                    }catch(Exception $e){
                        $response->error = true;
                        $response->errorMessage = $this->translate('Failed to load products categories. Please re-try again.');
                    }
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            
             
            $this->view->assign((array)$response);
            $this->getHelper('Layout')->disableLayout();
        }
        
        public function ajaxsavemealAction(){
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
            
            
            // if(isset($_FILES['mealpic']['name'])&& $_FILES['mealpic']['name']!='')
            // {
            // $adapter = new Zend_File_Transfer_Adapter_Http();
            // $adapter->addFilter('Rename', array('target' => './uploads/'.time().'.jpg',
            //          'overwrite' => true));           
            
            // if (!$adapter->receive()) 
            //     {
                
            //       $messages = $adapter->getMessages();
            //       $response->error = false;
                
            //     }
            if(isset($_FILES['mealpic']['name']) AND !empty($_FILES['mealpic']['name']))
                            {
                                      $target_dir = "./uploads/";
                                      // $pdf2 = basename($_FILES["res_icon"]["name"]);
                                      $temp = explode(".", $_FILES["mealpic"]["name"]);
                                      $iconimages = uniqid().implode(".", $temp);
                                    move_uploaded_file($_FILES["mealpic"]["tmp_name"], $target_dir . $iconimages);  
                                    $file = $iconimages;                
                            
               $data = array(
                                'category_id' => $this->getRequest()->emealcategory,
                                'meal_name' => trim($this->getRequest()->emealname),
                                'meal_name_ar'=>trim($this->getRequest()->mealname_ar),
                                'meal_description' => trim($this->getRequest()->emealdescription),
                                'meal_description_ar' => trim($this->getRequest()->mealdescription_ar),
                                'meal_price' => $this->getRequest()->emealprice,
                                'is_active' => $this->getRequest()->emealactive,
                                'crust_on' => $this->getRequest()->crust_on,
                                'custompizza'=>$this->getRequest()->custompizza,
                                'souce_on' => $this->getRequest()->souce_on,
                                'meal_image'=>$file    
                            );
            }
            else
            {
                
               $data  = array(
                                'category_id' => $this->getRequest()->emealcategory,
                                'meal_name' => trim($this->getRequest()->emealname),
                                'meal_name_ar'=>trim($this->getRequest()->mealname_ar),
                                'meal_description' => trim($this->getRequest()->emealdescription),
                                'meal_description_ar' => trim($this->getRequest()->mealdescription_ar),
                                'meal_price' => $this->getRequest()->emealprice,
                                'is_active' => $this->getRequest()->emealactive,
                                'crust_on' => $this->getRequest()->crust_on,
                                'custompizza'=>$this->getRequest()->custompizza,
                                'souce_on' => $this->getRequest()->souce_on,
                                 
                            ); 
                
                
            }
            
            
          
             if($this->getRequest()->pizzaselect=='yes'):
                $data['pizza_prices'] = $this->_C_helper->To_Get_Pizza_Price($this->getRequest()->size);
             
             else:
                $data['pizza_prices'] = '';
            endif;
            
            try{
                if(!Zend_Validate::is($this->getRequest()->emealcategory, 'NotEmpty') ||
                   !Zend_Validate::is($this->getRequest()->emealname, 'NotEmpty') || 
                   !Zend_Validate::is($this->getRequest()->emealprice, 'NotEmpty'))
                    throw new Exception($this->translate('Fields marked(*) are mandatory.'));
                if(!is_numeric($this->getRequest()->emealprice) || $this->getRequest()->emealprice < 0)
                    throw new Exception($this->translate('Product price must be numeric, and must not be less than 0.'));
                //saving the meal details
                    try{
                        $this->database->update(
                            $this->tp."restaurant_meals",
                             $data,
                            'meal_id = '.$this->getRequest()->meal_id
                        );
                        $response->success = true;
                    }catch(Exception $e){
                        throw new Exception($this->translate('Failed to save. Please re-try again.'));
                    }
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Json')->sendJson($response);
        }
        
        public function manageselectionsAction(){
            $response = new stdclass;
            $response->error = false;
            
            if(isset($this->getRequest()->doAction)){
                try{
                    switch($this->getRequest()->doAction){
                        case 'delete': {
                            $this->database->delete(
                                $this->tp.'restaurant_meals_selections',
                                'meal_selection_id = '.$this->meal_selection_id
                            );
                            break;
                        }
                    }
                }catch(Exception $e){
                    $response->error = true;
                    $response->errorMessage = $this->translate('Your action failed. Please re-try again.');
                }
            }
            
            try{
                //loading all the selections for the meals
                $response->selections = $this->database->fetchAll("select * from {$this->tp}restaurant_meals_selections where meal_id = ?", $this->getRequest()->meal_id);
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Layout')->disableLayout();
            $this->view->assign((array)$response);
        }
        
        public function ajaxaddselectionAction(){
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
            
            try{
                if(!Zend_Validate::is($this->getRequest()->selection_name, 'NotEmpty') ||
                   !Zend_Validate::is($this->getRequest()->selection_choices, 'NotEmpty'))
                    throw new Exception($this->translate('All fields are mandatory.'));
                //checking all choices
                    $choiceDirty = false;
                    foreach($this->getRequest()->selection_choices as $entry){
                        $data = explode(';', $entry);
                        if(!is_numeric($data[1])){
                            $choiceDirty = true;
                            break;
                        }
                    }
                    if($choiceDirty) throw new Exception($this->translate('Some of choices seems to be invalid.'));
                //adding selection to database
                    try{
                        $this->database->insert(
                            $this->tp."restaurant_meals_selections",
                            array(
                                'meal_id' => $this->getRequest()->meal_id,
                                'selection_name' => trim($this->getRequest()->selection_name),
                                'selection_type' => $this->getRequest()->selection_type,
                                'selection_data' => serialize($this->getRequest()->selection_choices)
                            )
                        );
                        $response->success = true;
                    }catch(Exception $e){
                        throw new Exception($this->translate('Failed to add product selection. Please re-try again.'));
                    }
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Json')->sendJson($response);
        }
        
        public function ajaxdeletemealselectionAction(){
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
            
            try{
                $this->database->delete(
                    $this->tp."restaurant_meals_selections",
                    'meal_selection_id = '.$this->getRequest()->meal_selection_id
                );
                $response->success = true;
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Json')->sendJson($response);
        } 
        
        public function editmealselectionAction(){
            $response = new stdclass;
            $response->error = false;
            
            try{
                $response->selection = $this->database->fetchRow("select * from {$this->tp}restaurant_meals_selections where meal_selection_id = ?", $this->getRequest()->meal_selection_id);
            }catch(Exception $e){
                $response->error = false;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->view->assign((array)$response);
            $this->getHelper('Layout')->disableLayout();
        }
        
        public function ajaxsaveselectionAction(){
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
            
            try{
                if(!Zend_Validate::is($this->getRequest()->selection_name, 'NotEmpty') ||
                   !Zend_Validate::is($this->getRequest()->selection_choices, 'NotEmpty'))
                    throw new Exception($this->translate('All fields are mandatory.'));
                //checking all choices
                    $choiceDirty = false;
                    foreach($this->getRequest()->selection_choices as $entry){
                        $data = explode(';', $entry);
                        if(!is_numeric($data[1])){
                            $choiceDirty = true;
                            break;
                        }
                    }
                    if($choiceDirty) throw new Exception($this->translate('Some of choices seems to be invalid.'));
                //adding selection to database
                    try{
                        $this->database->update(
                            $this->tp."restaurant_meals_selections",
                            array(
                                'selection_name' => trim($this->getRequest()->selection_name),
                                'selection_type' => $this->getRequest()->selection_type,
                                'selection_data' => serialize($this->getRequest()->selection_choices)
                            ),
                            'meal_selection_id = '.$this->getRequest()->meal_selection_id
                        );
                        $response->success = true;
                    }catch(Exception $e){
                        throw new Exception($this->translate('Failed to add product selection. Please re-try again.'));
                    }
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Json')->sendJson($response);
        }
        
        public function manageaddonsAction(){
            $response = new stdclass;
            $response->error = false;
            
            try{
                $response->addons = $this->database->fetchAll("select * from {$this->tp}restaurant_meals_addons where meal_id = ?", $this->getRequest()->meal_id);
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Layout')->disableLayout();
            $this->view->assign((array)$response);
        }
        
        public function ajaxmealaddonsAction(){
            $response = new stdclass;
            $response->error = false;
            $response->errorMessage = false;
            
            try{
                switch($this->getRequest()->doAction){
                    case 'addaddon': {
                        if(!Zend_Validate::is($this->getRequest()->addon_name, 'NotEmpty') ||
                           !Zend_Validate::is($this->getRequest()->addon_choices, 'NotEmpty'))
                            throw new Exception($this->translate('All fields are mandatory'));
                        //verifying the addon choices
                            $choicesDirty = false;
                            foreach($this->getRequest()->addon_choices as $entry){
                                $data = explode(';', $entry);
                                if(!is_numeric($data[1])){
                                    throw new Exception($this->translate('Some of choices seems to be invalid.'));
                                }
                            }
                        //saving the addon
                            try{
                                $this->database->insert(
                                    $this->tp."restaurant_meals_addons",
                                    array(
                                        'meal_id' => $this->getRequest()->meal_id,
                                        'addon_name' => trim($this->getRequest()->addon_name),
                                        'addon_type' => $this->getRequest()->addon_type,
                                        'addon_data' => serialize($this->getRequest()->addon_choices),
                                        'is_active' => $this->getRequest()->addon_active
                                    )
                                );
                                $response->success = true;
                            }catch(Exception $e){
                                throw new Exception($this->translate('Failed to add product addon. Please re-try again.').$e->getMessage());
                            }
                        break;
                    }
                    case 'saveaddon': {
                        if(!Zend_Validate::is($this->getRequest()->eaddon_name, 'NotEmpty') ||
                           !Zend_Validate::is($this->getRequest()->eaddon_choices, 'NotEmpty'))
                            throw new Exception($this->translate('All fields are mandatory'));
                        //verifying the addon choices
                            $choicesDirty = false;
                            foreach($this->getRequest()->eaddon_choices as $entry){
                                $data = explode(';', $entry);
                                if(!is_numeric($data[1])){
                                    throw new Exception($this->translate('Some of choices seems to be invalid.'));
                                }
                            }
                        //saving the addon
                            try{
                                $this->database->update(
                                    $this->tp."restaurant_meals_addons",
                                    array(
                                        'addon_name' => trim($this->getRequest()->eaddon_name),
                                        'addon_type' => $this->getRequest()->eaddon_type,
                                        'addon_data' => serialize($this->getRequest()->eaddon_choices)
                                    ),
                                    'addon_id = '.$this->getRequest()->addon_id
                                );
                                $response->success = true;
                            }catch(Exception $e){
                               
                    
                                throw new Exception($this->translate('Failed to add product addon. Please re-try again.'));
                            }
                        break;
                    }
                    case 'deleteaddon': {
                        $this->database->delete(
                            $this->tp."restaurant_meals_addons",
                            'addon_id = '.$this->getRequest()->addon_id
                        );
                        $response->success = true;
                        break;
                    }
                    case 'activateaddon': {
                        $this->database->update(
                            $this->tp."restaurant_meals_addons",
                            array('is_active' => 1),
                            'addon_id = '.$this->getRequest()->addon_id
                        );
                        $response->success = true;
                        break;
                    }
                    case 'deactivateaddon': {
                        $this->database->update(
                            $this->tp."restaurant_meals_addons",
                            array('is_active' => 0),
                            'addon_id = '.$this->getRequest()->addon_id
                        );
                        $response->success = true;
                        break;
                    }
                }
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Json')->sendJson($response);
        }
        
        public function editaddonAction(){
            $response = new stdclass;
            $response->error = false;
            
            try{
                $response->addon = $this->database->fetchRow("select * from {$this->tp}restaurant_meals_addons where addon_id = ?", $this->getRequest()->addon_id);
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Layout')->disableLayout();
            $this->view->assign((array)$response);
        }
        
        
         public function translate($message)
                 {
            return $this->translator->translate($message);
        }
       
    }