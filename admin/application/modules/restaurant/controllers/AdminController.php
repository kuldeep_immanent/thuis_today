<?php

    include("upload.php");

    class Restaurant_AdminController extends Zend_Controller_Action{

        

        public $database;

        public $tp; //Table prefix

        public $appConfig;

        public $config;

        public $session;

        public $translator;

        public $test;

        public $count;

        public $model;

        

        public function init(){

            //initializing resources

            $bootstrap        = Zend_Controller_Front::getInstance()->getParam('bootstrap');

            $this->database   = $bootstrap->getPluginResource('db')->getDbAdapter();

            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);

            $this->tp         = Zend_Controller_Front::getInstance()->getParam('TablePrefix');

            $this->appConfig  = $bootstrap->getResource('AppConfig');

            $this->config     = Zend_Controller_Front::getInstance()->getParam('Config');

            $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');

            $this->translator = Zend_Registry::get('Zend_Translate');

            $this->model      = new Restaurant_Model_Restusers();

            $this->helper     = $this->getHelper('Myreuse');

            

            //initializing layout

            Zend_Layout::startMvc(array(

                'layoutPath' => APPLICATION_PATH . '/modules/restaurant/views/layouts',

                'layout' => 'restaurantadmin'

            ));

            //adding default page title

            $this->view->headTitle($this->translate('Shop Admin Panel').' | ');

        }

        

        public function preDispatch(){

            

            if($this->helper->Credentials_Check() && !isset($_GET['switch_id']))

            {

                

                if($this->getRequest()->action !='login'):

               $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);

                endif;

            }

            if(!$this->session->admin->loggedin and $this->getRequest()->getCookie('rememberme')){

                //auto login the user

                try{

                    $user = $this->database->fetchRow("select * from {$this->tp}admin_users where rememberme = ?", $this->getRequest()->getCookie('rememberme'));

                    if($user){

                        $this->session->admin = (object)array(

                            'loggedin' => true,

                            'id' => $user->user_id,

                            'name' => $user->user_name,

                            'privileges' => unserialize($user->user_privileges),

                            'is_super_admin' => (bool)$user->is_super_admin 

                        );

                        //updating last login date time

                        $this->database->update(

                            $this->tp."admin_users",

                            array("last_login_on" => new Zend_Db_Expr('now()')),

                            'user_id = '.$user->user_id

                        );

                        //forwarding to dashboard

                        $this->getHelper('Redirector')->gotoRoute(array(), 'admin-dashboard', true);

                    }

                }catch(Exception $e){

                    echo $e->getMessage(); exit;

                }

            }

            $this->view->request = $this->getRequest();

            $this->view->session = $this->session;

            $this->view->config = $this->config;

            $this->view->appConfig = $this->appConfig;

        }

        

        public function postDispatch(){

           

            

           

            

        }

        

        private function translate($message){

            return $this->translator->translate($message);

        }

        

        private function checkLogin(){

            if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);

        }

        

        public function indexAction(){

            $this->_forward('dashboard');

        }

        

        public function loginAction(){

            $this->getHelper('Layout')->disableLayout();

            

            $response = new stdclass;

            $response->error = false;

            $response->rememberme = false;

            

            if($this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-dashboard', true);

            if($this->getRequest()->isPost()){

                try{

                    if(!Zend_Validate::is($this->getRequest()->username, 'NotEmpty') || !Zend_Validate::is($this->getRequest()->password, 'NotEmpty'))

                        throw new Exception($this->translate('All fields are mandatory.'));

                    //loading matching admin account

                    $account = $this->database->fetchRow("select * from {$this->tp}restaurant_admin_users where user_email = ? and account_password = ? and status = '1'", array($this->getRequest()->username, $this->getRequest()->password));

                    if(!$account) throw new Exception($this->translate('Invalid login details.'));

                    //checking whether admin account is active or not

                    if(!$account->status) throw new Exception($this->translate('Your account has been deactivated. Please consult our support.'));

                    if(isset($this->getRequest()->remember)){

                        $rememberme_code = $account->user_id.time();

                        setcookie('rememberme', $rememberme_code, time() + (30*24*60*60));

                        //updating remember me code

                        $this->database->update(

                            $this->tp.'restaurant_admin_users',

                            array('rememberme' => $rememberme_code),

                            'user_id = '.$account->user_id

                        );

                    }else{

                        if($this->getRequest()->getCookie('rememberme')){

                            setcookie('rememberme', '', (time() - 3600));

                        }

                    }

                    //logging in the admin

                    $this->session->admin = (object)array(

                        'id' => $account->user_id,

                        'name' => stripslashes($account->user_name),

                        'email' => $account->user_email,

                        'resid' => $account->restaurant_id,

                        'loggedin' => true  

                    );

                    $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-dashboard', true);

                }catch(Exception $e){

                    $response->error = true;

                    $response->errorMessage = $e->getMessage();   

                }

            }

            

            $this->view->assign((array)$response);

            $this->view->headTitle($this->translate('Login').' | ', 'PREPEND');

        }

        

        public function verifyemailAction(){

            $this->getHelper('Layout')->disableLayout();

            

            $response = new stdclass;

            $response->error = false;

            $response->success = false;

            

            try{

                if(!isset($this->getRequest()->vercode)) throw new Exception($this->translate('Invalid email verification link.'));

                $vercode = $this->getRequest()->vercode;

                //loading admin user account related to verification code

                $account = $this->database->fetchRow("select * from {$this->tp}restaurant_admin_users where md5(concat(restaurant_id, user_email)) = ?", $this->getRequest()->vercode);

                if(!$account) throw new Exception($this->translate('No account found matching verification code.'));

                if($account->email_verified) throw new Exception($this->translate('Your account is already verified. No need to re-verify.'));

                //generating temp password and activating account

                $tempPassword = strtoupper(substr(uniqid(), 0, 10));

                $this->database->update(

                    $this->tp.'restaurant_admin_users',

                    array('email_verified' => 1, 'is_active' => 1, 'account_password' => md5($tempPassword)),

                    'user_id = '.$account->user_id

                );

                $response->success = true; 

                //sending email with account login details

                $mail = new Zend_Mail('utf-8');

                $mail->addTo($account->user_email, $account->user_name);

                $mail->setSubject($this->translate('FOS: Your restaurant management panel login details'));

                $this->view->subject = $mail->getSubject();

                $this->view->username = $account->user_email;

                $this->view->password = $tempPassword;

                $this->getHelper('ViewRenderer')->setNoRender();

                $mailBody = $this->view->render('mails/account_details.phtml');

                $this->getHelper('ViewRenderer')->setNoRender(false);

                $mail->setBodyHtml($mailBody);

                $this->getHelper('Mailer')->sendMail($mail);

                $response->success = true;

            }catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $e->getMessage();

            }

            

            $this->view->assign((array)$response);

            $this->view->headTitle($this->translate('Verify Email').' | ', 'PREPEND');

        }

        

        public function dashboardAction()

        {

              $this->checkLogin();

              $CustomerOrder   = new Restaurant_Model_CustomerOrder();
                
              $response->myid  = $this->session->admin->resid;
             

                    try

                    {



                            $query                        = "select * from {$this->tp}restaurant_customer_order where  restuarant_service_id=".$this->session->admin->resid;



                            $response->totalorders        = $this->helper->G_Get_Data($query,'count');

                            $status                       = "select* from {$this->tp}restaurants where restaurant_id=".$this->session->admin->resid;

                            

                            $status                       = $this->helper->G_Get_Data($status ,'');

                          

                            $response->status             = $status[0]->is_open;

                            $income                       = "SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE restuarant_service_id=".$this->session->admin->resid." and status=0 ";

                           

                            $income                       = $this->helper->G_Get_Data($income ,'');

                            

                            $response->income             = $income[0]->sum;
                            

                            $recentorders                 = "Select * from {$this->tp}restaurant_customer_order where  restuarant_service_id=".$this->session->admin->resid.'  order by id Desc Limit 0,50 ';

                            

                            $response->recentorders       = $this->helper->G_Get_Data($recentorders ,'');

                            

                            $processionorder              = "Select * from {$this->tp}restaurant_customer_order where  restuarant_service_id=".$this->session->admin->resid.' and status=2 order by id Desc Limit 0,50 ';

                            $delivedorders                = "Select * from {$this->tp}restaurant_customer_order where  restuarant_service_id=".$this->session->admin->resid.' and status=0  order by id Desc Limit 0,50 ';
                            
                            $delivedorders                = $this->helper->G_Get_Data($delivedorders,'');


                            $processionorder              = $this->helper->G_Get_Data($processionorder ,'');

                             

                            $highalerted                  = $this->helper->Remake_Array($processionorder);

                            

                            $response->processionorder    = $processionorder;
                            $response->delivedorders      = $delivedorders;

                            $response->highalerted        = $highalerted;

                            $response->data               = $this->helper->Weekly_Report($this->session->admin->resid);
                             



                    }

                    catch (Exception $e)

                    {



                             $error =   $e->getMessage();



                    }

                    

            $this->view->assign((array)$response);

            $this->view->sidebar_menu_item = 'dashboard';

            $this->view->headTitle($this->translate('Dashboard').' | ', 'PREPEND');

        }


         /*******
        * function to to switch user by subadmin
        *****/

          public function switchloginAction()
        {
            
            $response = new stdclass;
            $response->error = false;
            
            if(isset($_GET['switch_id']) && isset($_GET['superadmin']))
            {
                $id = $this->getRequest()->getParam('switch_id');
                
                
            }
            else
            {
                
              $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);  
                
            }
            try 
            {
               
                 $account = $this->database->fetchRow("select * from {$this->tp}restaurant_admin_users where restaurant_id = ".(int)$id);
                 $count   =  $this->helper->G_Get_Data("select * from {$this->tp}restaurant_admin_users where restaurant_id = ".(int)$id);
                 
                 if(count($count)<1)
                 {
                   
                   $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-error', true);  
                     
                 }
               
            }
            catch (Exception $ex) 
            {
               $response->error = true; 
            }
            
            
            
              $this->session->admin = (object)array(
                        'id' => $account->user_id,
                        'name' => stripslashes($account->user_name),
                        'email' => $account->user_email,
                        'resid' => $account->restaurant_id,
                        'loggedin' => true  
                    );
                  $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-dashboard', true);  
                
                
            
            
            
        }
        

         /*****
         * function to check error  function 
         */
        public function errorAction()
        {
            
            echo"404 error no user is available ";
            die;
            
            
        }

        public function ajaxrecoverpasswordAction(){

            $response = new stdclass;

            $response->error = false;

            $response->success = false;

            

            try{

                if(!Zend_Validate::is($this->getRequest()->email, 'NotEmpty')) throw new Exception($this->translate('You must enter your account email address.'));

                //finding any account matching email 

                $account = $this->database->fetchRow("select * from {$this->tp}restaurant_admin_users where user_email = ?", $this->getRequest()->email);

                if(!$account) throw new Exception($this->translate('No account found matching specified email address.'));

                $temppassword = strtoupper(substr(uniqid(), 0, 10));

                //updating the password and sending email

                $this->database->update(

                    $this->tp.'restaurant_admin_users',

                    array('user_password' => $temppassword),

                    'user_id = '.$account->user_id

                );

                $mail = new Zend_Mail('utf-8');

                $mail->setSubject('kebapiste.com : '.$this->translate('Your account new password'));

                $mail->addTo($account->user_email, $account->user_name);

                $this->getHelper('ViewRenderer')->setNoRender();

                $this->getHelper('Layout')->disableLayout();

                $this->view->mail_subject = $mail->getSubject();

                $this->view->user_name = stripslashes($account->user_name);

                $this->view->temppassword = $temppassword;

                $mail->setBodyHtml($this->view->render('mails/recover_password.phtml'));

                $this->getHelper('Layout')->enableLayout();

                $this->getHelper('ViewRenderer')->setNoRender(false);

                $this->getHelper('Mailer')->sendMail($mail);

                $response->success = true;

            }catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $e->getMessage();

            }

            

            $this->getHelper('Json')->sendJson($response);

        }

        

        public function signoutAction(){

            //removing rememberme cookie, if set

            if($this->getRequest()->getCookie('rememberme')) setcookie('rememberme', '', (time() - 3600));

            unset($this->session->admin);

            $this->session->admin->loggedin = false;

            $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);

        }

        

        public function profileAction(){

            $this->checkLogin();

            $response = new stdclass;

            $response->error = false;

            $response->success = false;   

            if($this->getRequest()->isPost()){

                try{

                    //validating the fields

                    if(!Zend_Validate::is($this->getRequest()->user_name, 'NotEmpty'))

                        throw new Exception($this->translate('Fields marked (*) are mandatory.'));

                    if(Zend_Validate::is($this->getRequest()->user_password, 'NotEmpty') and iconv_strlen($this->getRequest()->user_password) < 8)

                        throw new Exception($this->translate('Password must be at least 8 characters long.'));

                    if(Zend_Validate::is($this->getRequest()->user_password, 'NotEmpty') and strcmp($this->getRequest()->user_password, $this->getRequest()->user_re_password))

                        throw new Exception($this->translate('Password do not match.'));

                    //saving the details

                    try{

                        if(Zend_Validate::is($this->getRequest()->user_password, 'NotEmpty')){

      

                             $this->model->update(

                               

                                array(

                                    'user_name' => trim($this->getRequest()->user_name),

                                    'contact_number' => trim($this->getRequest()->contact_number),

                                    'account_password' => $this->getRequest()->user_password

                                ),

                                'user_id = '.$this->session->admin->id

                            );  

                        }else{

                             $data = array('user_name' => trim($this->getRequest()->user_name),

                                    'contact_number' => trim($this->getRequest()->contact_number));

                             

                             $this->model->updateUser($data,$this->session->admin->id);

                        }

                        $response->success = true;

                    }catch(Exception $e){

                        throw new Exception($this->translate($e->getMessage()));

                    }

                }catch(Exception $e){

                    $response->error = true;

                    $response->errorMessage = $e->getMessage();

                }

            }else{

                try{

                    //loading profile details

                    $account = $this->database->fetchRow("select * from {$this->tp}restaurant_admin_users where user_id = ?", $this->session->admin->id);

                    $this->getRequest()->setParam('user_name', stripslashes($account->user_name));

                    $this->getRequest()->setParam('user_email', $account->user_email);

                    $this->getRequest()->setParam('contact_number', $account->contact_number);

                }catch(Exception $e){

                    $response->error = true;

                    $response->errorMessage = $this->translate('Failed to load profile details. Please re-try again.');

                }

            }

           

            $this->view->assign((array)$response);

            $this->view->headTitle($this->translate('My Profile').' | ', 'PREPEND');

        }

        

        public function settingsAction(){

            $this->checkLogin();

            $response = new stdclass;

            $response->error = false;

            $response->success = false;

         

            if($this->getRequest()->isPost()){

                try{

                    //validating fields

                    if( !Zend_Validate::is($this->getRequest()->res_name, 'NotEmpty') ||

                        !Zend_Validate::is($this->getRequest()->res_delivery_fees, 'NotEmpty') ||

                        !Zend_Validate::is($this->getRequest()->res_avg_delivery_time, 'NotEmpty') ||

                        !Zend_Validate::is($this->getRequest()->res_address, 'NotEmpty') ||

                        !Zend_Validate::is($this->getRequest()->res_city, 'NotEmpty') ||

                        !Zend_Validate::is($this->getRequest()->res_location, 'NotEmpty') 

                    )

                        throw new Exception($this->translate('Mandatory fields are found empty.'));

                    if(Zend_Validate::is($this->getRequest()->res_contact, 'NotEmpty') and !Zend_Validate::is($this->getRequest()->res_contact, 'EmailAddress', array(Zend_Validate_Hostname::ALLOW_DNS)))

                        throw new Exception($this->translate('Invalid contact email address.'));

                    if(empty($this->getRequest()->res_payment_modes))

                        throw new Exception($this->translate('You must select at least one payment mode.'));

                    if(!is_numeric($this->getRequest()->res_delivery_fees) || $this->getRequest()->res_delivery_fees < 0)

                        throw new Exception($this->translate('Invalid delivery fees, must be numeric and not less than 0.'));

                    if(!is_numeric($this->getRequest()->res_avg_delivery_time) || $this->getRequest()->res_avg_delivery_time <= 0)

                        throw new Exception($this->translate('Average delivery time must be numeric, and greater than 0.'));

                        

                      

                    //saving the settings

                    try{

                        $this->database->update(

                            $this->tp."restaurants",

                            array(

                                'restaurant_name' => trim($this->getRequest()->res_name),

                                'restaurant_description' => trim($this->getRequest()->res_description),

                                'contact_email' => $this->getRequest()->res_contact,

                                'payment_modes' => (count($this->getRequest()->res_payment_modes) == 2 ? 'cash,cc': (in_array('cash', $this->getRequest()->res_payment_modes) ? 'cash' : 'cc')),

                                'delivery_fees' => $this->getRequest()->res_delivery_fees,

                                'average_delivery_time' => $this->getRequest()->res_avg_delivery_time

                             

                               

                            ),

                            'restaurant_id = '.$this->session->admin->resid

                        );

                        $this->database->update(

                            $this->tp."restaurant_address",

                            array(

                                'location_id' => $this->getRequest()->res_location,

                                'address' => $this->getRequest()->res_address,

                                

                            ),

                            'restaurant_id = '.$this->session->admin->resid

                        );

                        $response->success = true;

                    }catch(Exception $e){

                        throw new Exception($this->translate('Failed to save settings. Please re-try again.').$e->getMessage());

                    }

                }catch(Exception $e){

                    $response->error = true;

                    $response->errorMessage = $e->getMessage();

                }

            }else{

                try{

                    $settings = $this->database->fetchRow("select * from {$this->tp}restaurants where restaurant_id = ?", $this->session->admin->resid);

                    if(!$settings) throw new Exception();

                    $address = $this->database->fetchRow("SELECT * FROM {$this->tp}restaurant_address left join {$this->tp}locations on {$this->tp}restaurant_address.location_id = {$this->tp}locations.location_id where {$this->tp}restaurant_address.restaurant_id = ?", $this->session->admin->resid);

                    $this->getRequest()->setParams(array(

                        'res_name' => stripslashes($settings->restaurant_name),

                        'res_description' => stripslashes($settings->restaurant_description),

                        'res_contact' => stripslashes($settings->contact_email),

                        'res_payment_modes' => explode(',', $settings->payment_modes),

                        'res_delivery_fees' => number_format($settings->delivery_fees, 2, '.', ''),

                        'res_avg_delivery_time' => $settings->average_delivery_time,

                        'res_address' => $address->address,

                        'res_city' => $address->location_city,

                        'res_location' => $address->location_name  

                    ));

                }catch(Exception $e){

                    $response->error = true;

                    $response->errorMessage = $this->translate('Failed to load settings data. Please re-try again.');

                }

            }

            

            try{

                $response->cities = array('' => $this->translate('-- Select A City / Town --'), 'Istanbul' => 'Istanbul');

                $response->locations = array('' => $this->translate('-- Select A Location / Region --'));

                $selectedCity = ((isset($this->getRequest()->res_city) and Zend_Validate::is($this->getRequest()->res_city, 'NotEmpty')) ? $this->getRequest()->res_city : null);

                if($selectedCity != null){

                    $locations = $this->database->fetchAll("select * from {$this->tp}locations where location_city = ? order by location_name asc", $selectedCity);

                    $response->locations = array();

                    foreach($locations as $entry){

                        $response->locations[$entry->location_id] = stripslashes($entry->location_name);

                    }

                }

            }catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $this->translate('Failed to load location details.');

            }

            

            $this->view->sidebar_menu_item = 'settings';

            $this->view->assign((array)$response);

            $this->view->headTitle($this->translate('Shop Settings'). ' | ', 'PREPEND');

        }

        

      private function getFileExtension($filename)

		{

			$fext_tmp = explode('.',$filename);

			return $fext_tmp[(count($fext_tmp) - 1)];

		}

        

         

       

		

		public function uploadAction()

		{

			$dest_dir = "assets/uploads/";

			

			/* Uploading Document File on Server */

			$upload = new Zend_File_Transfer_Adapter_Http();

			$upload->setDestination($dest_dir)

						 ->addValidator('Count', false, 1)

						 ->addValidator('Size', false, 1048576)

						 ->addValidator('Extension', false, 'jpg,png,gif,pdf');

			$files = $upload->getFileInfo();

			

			// debug mode [start]

			echo '<hr />

							<pre>';

			print_r($files);

			

			echo '	</pre>

						<hr />';

			// debug mode [end]

			

			try 

			{

				// upload received file(s)

				$upload->receive();

			} 

			catch (Zend_File_Transfer_Exception $e) 

			{

				$e->getMessage();

				exit;

			}

			

			$mime_type = $upload->getMimeType('doc_path');

			$fname = $upload->getFileName('doc_path');

			$size = $upload->getFileSize('doc_path');

			$file_ext = $this->getFileExtension($fname);			

			$new_file = $dest_dir.md5(mktime()).'.'.$file_ext;

			

			$filterFileRename = new Zend_Filter_File_Rename(

				array(

					'target' => $new_file, 'overwrite' => true

			));

			

			$filterFileRename->filter($fname);

			

			if (file_exists($new_file))

			{

				$request = $this->getRequest();

				$caption = $request->getParam('caption');

				

				$html = 'Orig Filename: '.$fname.'<br />';

				$html .= 'New Filename: '.$new_file.'<br />';

				$html .= 'File Size: '.$size.'<br />';

				$html .= 'Mime Type: '.$mime_type.'<br />';

				$html .= 'Caption: '.$caption.'<br />';

			}

			else

			{

				$html = 'Unable to upload the file!';

			}

			

			$this->view->assign('up_result',$html);

		}

		

		

            public function mainAction()

            {

            

            $form = new Application_Form_Upload();

            $this->view->assign('form',$form);

            

            }        

        public function ajaxloadlocationAction(){

            $response = new stdclass;

            $response->error = false;

            

            try{

                $response->locations = $this->database->fetchAll("select * from {$this->tp}locations where location_city = ?", $this->getRequest()->city);

            }catch(Exception $e){

                $response->error = true;

            }

            

            $this->getHelper('Layout')->disableLayout();

            $this->view->assign((array)$response);

        }

        public function changestatusAction()
        {
              $status = $this->getRequest()->val;
                           $this->database->update(
                                        $this->tp.'restaurants',
                                        array('is_open' => $status),
                                        'restaurant_id = '.$this->session->admin->resid
                                    );
            $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-dashboard', true);  
          
        }


      public function getstatusAction()
         {   
            $response = new stdclass;
            $response->error = false;

            try{

             $status= $this->database->fetchRow("select * from {$this->tp}restaurant_setting where res_id =".$this->session->admin->resid);
             echo $status->auto_open_close;
             die;
            } 
            catch(Exception $xe){
                   
              $response->error = true;
            }
            
         }

    }