<?php



    



    require_once dirname(__FILE__) . '/plugins/restaurant_module_boostrap_plugin.php';



    require_once dirname(__FILE__) . '/plugins/restaurant_module_custom_plugin.php';



    class Restaurant_Bootstrap extends Zend_Application_Module_Bootstrap{



        



        public function __construct($application){



            parent::__construct($application);



        }



        



        protected function _initPlugins(){



            $this->bootstrap('FrontController');



            $front = Zend_Controller_Front::getInstance();



            $front->registerPlugin(new Restaurant_Module_Bootstrap_Plugin());



            



        }



        



        protected function _initRoutes(){



            $this->bootstrap('FrontController');



            $fc = Zend_Controller_Front::getInstance();



            $router = $fc->getRouter();



            



            $router->addRoute('restaurant-admin-email-verification', new Zend_Controller_Router_Route('/winkel/admin/verifyemail/:vercode', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'verifyemail')));



            $router->addRoute('restaurant-admin-login', new Zend_Controller_Router_Route('/winkel/admin/login', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'login')));



            $router->addRoute('restaurant-admin-reset-password', new Zend_Controller_Router_Route('/winkel/admin/resetpassword', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'resetpassword')));



            $router->addRoute('restaurant-admin-dashboard', new Zend_Controller_Router_Route('/winkel/admin/dashboard', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'dashboard')));

            $router->addRoute('restaurant-admin-changestatus', new Zend_Controller_Router_Route('/winkel/admin/changestatus', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'changestatus')));



            $router->addRoute('restaurant-admin-signout', new Zend_Controller_Router_Route('/winkel/admin/signout', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'signout')));



            $router->addRoute('restaurant-admin-profile', new Zend_Controller_Router_Route('/winkel/admin/profile', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'profile')));



            $router->addRoute('restaurant-admin-settings', new Zend_Controller_Router_Route('/winkel/admin/settings', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'settings')));



           $router->addRoute('restaurant-admin-delivery', new Zend_Controller_Router_Route('/winkel/admin/delivery/', array('module' => 'restaurant', 'controller' => 'Delivery', 'action' => 'index')));



           $router->addRoute('restaurant-admin-delivery-addedit', new Zend_Controller_Router_Route('/winkel/admin/delivery/addedit', array('module' => 'restaurant', 'controller' => 'Delivery', 'action' => 'addedit')));



            $router->addRoute('restaurant-admin-delivery-delete', new Zend_Controller_Router_Route('winkel/admin/delivery/delete', array('module' => 'restaurant', 'controller' => 'delivery', 'action' => 'delete'))); 







            $router->addRoute('restaurant-admin-managedeliveryareas', new Zend_Controller_Router_Route('/winkel/admin/deliveryareas', array('module' => 'restaurant', 'controller' => 'settings', 'action' => 'managedeliveryareas')));



            $router->addRoute('restaurant-admin-manageworkinghours', new Zend_Controller_Router_Route('/winkel/admin/manageworkinghours', array('module' => 'restaurant', 'controller' => 'settings', 'action' => 'manageworkinghours')));



            //$router->addRoute('restaurant-admin-managemeals', new Zend_Controller_Router_Route('/restaurant/admin/managemeals', array('module' => 'restaurant', 'controller' => 'meals', 'action' => 'managemeals')));

            $router->addRoute('restaurant-admin-managemeals', new Zend_Controller_Router_Route('/winkel/admin/managemeals', array('module' => 'restaurant', 'controller' => 'meals', 'action' => 'managemeals')));

            $router->addRoute('restaurant-admin-managecustommeals', new Zend_Controller_Router_Route('/winkel/admin/managecustommeals', array('module' => 'restaurant', 'controller' => 'meals', 'action' => 'managecustommeals')));

            $router->addRoute('admin-mealcategory-addedit', new Zend_Controller_Router_Route('/winkel/admin/mealc/addedit', array('module' => 'restaurant', 'controller' => 'mealc', 'action' => 'addedit')));
			
            $router->addRoute('restaurant-mealcategory-addedit', new Zend_Controller_Router_Route('/winkel/admin/mealc/addedit', array('module' => 'restaurant', 'controller' => 'mealc', 'action' => 'addedit')));

            $router->addRoute('restaurant-admin-managepromotions', new Zend_Controller_Router_Route('/winkel/admin/managepromotions', array('module' => 'restaurant', 'controller' => 'promotions', 'action' => 'managepromotions')));

            $router->addRoute('restaurant-admin-coupons', new Zend_Controller_Router_Route('winkel/admin/Coupon/', array('module' => 'restaurant', 'controller' => 'Coupon', 'action' => 'index'))); 

            $router->addRoute('restaurant-admin-coupons-addedit', new Zend_Controller_Router_Route('winkel/admin/Coupon/addedit', array('module' => 'restaurant', 'controller' => 'Coupon', 'action' => 'addedit'))); 

            $router->addRoute('restaurant-admin-coupons-delete', new Zend_Controller_Router_Route('winkel/admin/Coupon/delete', array('module' => 'restaurant', 'controller' => 'Coupon', 'action' => 'delete'))); 

             $router->addRoute('restaurant-admin-bulkoffer', new Zend_Controller_Router_Route('/winkel/admin/bulkoffer', array('module' => 'restaurant', 'controller' => 'Bulkoffer', 'action' => 'index')));  

            $router->addRoute('restaurant-admin-Orders', new Zend_Controller_Router_Route('/winkel/admin/Orders', array('module' => 'restaurant', 'controller' => 'Orders', 'action' => 'orders')));



            $router->addRoute('restaurant-admin-Orders-edit', new Zend_Controller_Router_Route('/winkel/admin/Orders/edit', array('module' => 'restaurant', 'controller' => 'Orders', 'action' => 'edit')));



            $router->addRoute('restaurant-admin-Orders-detail', new Zend_Controller_Router_Route('/winkel/admin/Orders/detail', array('module' => 'restaurant', 'controller' => 'Orders', 'action' => 'detail')));

            $router->addRoute('restaurant-admin-mealcategory', new Zend_Controller_Router_Route('/winkel/admin/mealc', array('module' => 'restaurant', 'controller' => 'mealc', 'action' => 'index')));

            $router->addRoute('restaurant-admin-mealcategory-addedit', new Zend_Controller_Router_Route('/winkel/admin/mealc/addedit', array('module' => 'restaurant', 'controller' => 'mealc', 'action' => 'addedit')));

            $router->addRoute('restaurant-admin-mealcategory-delete', new Zend_Controller_Router_Route('/winkel/admin/mealc/deletedata', array('module' => 'restaurant', 'controller' => 'mealc', 'action' => 'deletedata')));

            $router->addRoute('restaurant-admin-CrustSouce-crusts', new Zend_Controller_Router_Route('/winkel/admin/Crust/crusts', array('module' => 'restaurant', 'controller' => 'Crust', 'action' => 'crusts')));

            $router->addRoute('restaurant-admin-CrustSouce-addedit', new Zend_Controller_Router_Route('winkel/admin/Crust/addedit', array('module' => 'restaurant', 'controller' => 'Crust', 'action' => 'addedit')));

            $router->addRoute('restaurant-admin-CrustSouce-delete', new Zend_Controller_Router_Route('winkel/admin/Crust/delete', array('module' => 'restaurant', 'controller' => 'Crust', 'action' => 'delete')));

            $router->addRoute('restaurant-Message', new Zend_Controller_Router_Route('/winkel/Messages', array('module' => 'restaurant', 'controller' => 'Message', 'action' => 'index')));



            $router->addRoute('restaurant-Report', new Zend_Controller_Router_Route('/winkel/Report', array('module' => 'restaurant', 'controller' => 'Report', 'action' => 'index')));



            $router->addRoute('restaurant-Report-mealreport', new Zend_Controller_Router_Route('/winkel/Report/mealreport', array('module' => 'restaurant', 'controller' => 'Report', 'action' => 'mealreport')));

            $router->addRoute('restaurant-Report-reportper', new Zend_Controller_Router_Route('/winkel/Report/reportper', array('module' => 'restaurant', 'controller' => 'Report', 'action' => 'reportper')));

            $router->addRoute('restaurant-Report-deliverreport', new Zend_Controller_Router_Route('/winkel/Report/deliverreport', array('module' => 'restaurant', 'controller' => 'Report', 'action' => 'deliverreport')));

            $router->addRoute('restaurant-Report-Customerhistory', new Zend_Controller_Router_Route('/winkel/Report/customerhistory', array('module' => 'restaurant', 'controller' => 'Report', 'action' => 'Customerhistory')));

			$router->addRoute('restaurant-admin-main', new Zend_Controller_Router_Route('/winkel/admin/main', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'main')));



            $router->addRoute('restaurant-admin-upload', new Zend_Controller_Router_Route('/winkel/admin/upload', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'upload')));

            

            $router->addRoute('restaurant-admin-setting', new Zend_Controller_Router_Route('/winkel/setting/setting', array('module' => 'restaurant', 'controller' => 'Settings', 'action' => 'setting')));

            

            $router->addRoute('restaurant-admin-switchlogin', new Zend_Controller_Router_Route('/winkel/admin/switchlogin', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'switchlogin')));

            

            $router->addRoute('restaurant-admin-error', new Zend_Controller_Router_Route('/winkel/admin/error', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'error'))); 

            $router->addRoute('restaurant-admin-deliveryboy', new Zend_Controller_Router_Route('/winkel/admin/Driver', array('module' => 'restaurant', 'controller' => 'Driver', 'action' => 'index'))); 

            

            $router->addRoute('restaurant-admin-deliveryboy-addedit', new Zend_Controller_Router_Route('/winkel/admin/Driver/addedit', array('module' => 'restaurant', 'controller' => 'Driver', 'action' => 'addedit'))); 

           

            $router->addRoute('restaurant-admin-deliveryboy-delete', new Zend_Controller_Router_Route('/winkel/admin/Driver/delete', array('module' => 'restaurant', 'controller' => 'Driver', 'action' => 'delete'))); 

           

            $router->addRoute('restaurant-admin-orders-BarcodeRender', new Zend_Controller_Router_Route('/winkel/admin/Orders/barcoderender', array('module' => 'restaurant', 'controller' => 'Orders', 'action' => 'barcoderender'))); 

            

            $router->addRoute('restaurant-admin-feedback', new Zend_Controller_Router_Route('/winkel/admin/feedback', array('module' => 'restaurant', 'controller' => 'Feedback', 'action' => 'index'))); 

            

            $router->addRoute('restaurant-admin-feedback-delete', new Zend_Controller_Router_Route('/winkel/admin/feedback/delete', array('module' => 'restaurant', 'controller' => 'Feedback', 'action' => 'delete'))); 

            

            $router->addRoute('restaurant-admin-feedback-detail', new Zend_Controller_Router_Route('/winkel/admin/feedback/detail', array('module' => 'restaurant', 'controller' => 'Feedback', 'action' => 'detail'))); 

           

            //$router->addRoute('restaurant-admin-location', new Zend_Controller_Router_Route('/restaurant/admin/location', array('module' => 'restaurant', 'controller' => 'Location', 'action' => 'detail'))); 

           

           $router->addRoute('restaurant-admin-location', new Zend_Controller_Router_Route('/winkel/admin/location', array('module' => 'restaurant', 'controller' => 'Location', 'action' => 'index'))); 

           $router->addRoute('restaurant-admin-invoice', new Zend_Controller_Router_Route('/winkel/admin/invoice', array('module' => 'restaurant', 'controller' => 'Orders', 'action' => 'invoice')));

        }



        



    }