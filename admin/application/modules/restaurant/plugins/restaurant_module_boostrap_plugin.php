<?php

    class Restaurant_Module_Bootstrap_Plugin extends Zend_Controller_Plugin_Abstract{
        
        public $_actionController;
        
        public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request){
            if('restaurant' != $request->getModuleName()) return;
            
            $frontController = Zend_Controller_Front::getInstance();
            
            //registering action helpers
            Zend_Controller_Action_HelperBroker::addPath( APPLICATION_PATH . '/modules/restaurant/helpers', 'FOS_Res_Helpers_' );
            
            //registering view helpers
            $viewRenderer = Zend_Controller_Action_HelperBroker::getHelper('ViewRenderer');
            $view = $viewRenderer->view;
            $view->doctype('HTML5');
            $view->addHelperPath(APPLICATION_PATH . '/modules/restaurant/views/helpers', 'FOS_Res_View_Helpers_');
            
            //loading config
            $config = new Zend_Config_Xml(
                APPLICATION_PATH . '/modules/restaurant/configs/config.xml',
                null,
                array(
                    'allowModifications' => true
                )
            );
            $frontController->setParam('Config', $config);
            
            //loading session
            $session = new Zend_Session_Namespace('fosressession');
            $session->setExpirationSeconds((int)$config->session->expirationtime);
            if(!isset($session->admin->loggedin)) $session->admin->loggedin = false;
            $frontController->setParam('Session', $session);
        }
        
    }