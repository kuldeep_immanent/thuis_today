<?php

/* 
 * Classs to attach Custom Function to  our main plugin file with bootstrap
 * Author:Gagandeep Singh
 */


class Restaurant_Module_Custom_Plugin extends Zend_Controller_Plugin_Abstract
{
    
    
    
    protected $session;
    public function __construct() {
        
         $this->session     = new Zend_Session_Namespace('admin');
         
      
    }
    
    /********
     * function to process before the action  class is loaded 
     */
    public function preDispatch(\Zend_Controller_Request_Abstract $request) {
       
       
        if($request->getActionName()!='login'):
            
        
            if(!$this->session->admin->loggedin) 
            {
               
                
            }
            
           //$this->checkLogin(); 
        endif;
         
           
    }
    
    /********
     * function to process after  the action  class is loaded 
     */
    public function postDispatch(\Zend_Controller_Request_Abstract $request) 
    {
     
        
        
        
    }
    
    
  /*****
   * function  to  find the user is login or  not 
   * 
   */
     private function checkLogin(){
        
                    
            
                
               // header('location:/Resturant/restaurant/admin/login');
                
                
        }
    
    
    
    
    
}
