<?php

/* 
 * class: Customer Order
 * Author:Gagandeep Singh
 * Description:buid for customer orderdetail get
 */
  
 class Restaurant_Model_CustomerOrder extends Zend_Db_Table_Abstract{
        /* The Model Class Will be associated with register table*/

        protected $_name='fos_restaurant_customer_order';

        
        
        
        
        /********
         * function to update to user info
         */
        public function UpdateOrder($data=array(),$userid)
        {
            
            $this->update($data,'user_id='.$userid);
          
        }
        
        /*******
         * 
         * function to save the  user info
         */
        public function SaveOrder($data)
        {
            
            $this->insert($data);
            
            
        }
        
        /*****
         * 
         * function to Delete the user info
         */
        public function DeleteOrder($where)
        {
            
            
            $this->delete($where);
            
            
        }
        
        
       
        
        

    }
?>  




