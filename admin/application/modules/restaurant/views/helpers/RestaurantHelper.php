<?php



    class FOS_Res_View_Helpers_RestaurantHelper extends Zend_View_Helper_Abstract{

        

        private $database;

        private $tp;

        private $config;

        private $session;

        

        public function __construct(){

            $front = Zend_Controller_Front::getInstance();

            $bootstrap = $front->getParam('bootstrap');

            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();

            $this->tp = $front->getParam('TablePrefix');

            $this->config = $front->getParam('Config');

            $this->session = $front->getParam('Session');

        }

        

        public function isAdminAccountActive($resID){

            try{

                $query = $this->database->fetchRow("select is_active, email_verified from {$this->tp}restaurant_admin_users where restaurant_id = ?", $resID);

                if(!$query->email_verified) return false;

                return (bool)$query->is_active;

            }catch(Exception $e){

                return false;

            }

        }

        

        /*

         * Function to check the helper workin or not

         */

        public function Currency_Helper($value)

        {

           

             $currency = new Zend_Currency();

             echo $currency->toCurrency($value);

             

        }

        

        

        

        public function To_make_Graph_Json($data)

        {

           

            $weekdays  = array('First week','Second week','Third Week','Last Week');

                                                         $start = 0;

            foreach($data as $week):

                                                                                       

                     if(empty($week->sum))

                       {



                         $sum =0 ;    

                       }

                      else

                       {

         

                         $sum = $week->sum;    



                       }

                         $week_graph[]= $sum;





               $start++;

            endforeach;

            

            return $week_graph;

            

            

        }

        

         /*****

       * 

       * Function to return the whole data by fetching query

       */

        

        public function G_Get_Data($query)

        {

            

           $result = $this->database->fetchAll($query);

           

          

           return $result;

        }

        

        

       /****

       * 

       * Function Meal Editing Check for particular restaurant

       *  

       */         

      public function Meal_Editing_Check()

      {

               $res    = $this->session->admin->resid;

               $query  = "Select permitme from {$this->tp}restaurants where restaurant_id={$res}";

               $result = $this->G_Get_Data($query);  

               return $result[0]->permitme;  

      }

      /***

       * 

       * function to get the meal activation status

       */

      

      function Is_Active($res)

      {

          

               $query  = "Select * from meal_res_relation where meal_id={$res} and res_id={$this->session->admin->resid}";
               
               $result = $this->G_Get_Data($query);  

             

               if(count($result)):

                        echo'<span class="label label-sm label-class ">

                             Inactive <i class="fa fa-share"></i>

                              </span>';

                   else:

                        echo'<span class="label label-sm  label-success ">

                            Active <i class="fa fa-share"></i>

                             </span>';   

               endif;

          

          

      }


    
      /******
      * Function to   get the  Restaurant name 
      ********/

      public function The_Res_Name($resid)
      {
             $query   = "select restaurant_name from {$this->tp}restaurants where restaurant_id=$resid ";
            
             $result  = $this->database->fetchAll($query);

             return $result[0]->restaurant_name;
      }

   /******
   * Function to make the  proper report section  json report done 
   *******/
      public function Making_report_proper($jsonformate)
      {
        

       $jsonarray = json_decode($jsonformate);
      
       return $jsonarray;
      
      

      }


      /*****
      *function to generate the barcode   from order number
      *****/
      function To_Generate_Barcode($checksum)
      {
        
           $image = file_get_contents('http://food.vervetelecom.com/barcodelibrary/html/BCGcode39.php?checksum='.$checksum);
           echo $image;
      }


	     /*
   * function to make json formatter for high charts
   */
     public function chartsJsonFormatter($data)
     {
         
       
         $start =0;
        foreach($data as $dat):
             extract($dat);
              if($rate==0)
              {
                  $rate=0;
                  
              }
            if($start==0):
               
                $dummy ="{ name: '$restaurant_display_id',
                y: $rate,
                drilldown:$restaurant_id }";
                else:
                 $dummy .=",{ name: '$restaurant_display_id',
                y: $rate,
                drilldown: $restaurant_id }";
            endif;
            
            $start++;
         
        endforeach; 
         
       
        return $dummy;
         
     }
     
	 /*****
      * function to get  the sub list of particular  restaurant detail
      */
     public function chartJsonSubList($data)
     {
         $start =0;
         foreach($data as $dat):
             extract($dat);
             if($start==0):
               $dummy = " {
                                id: $restaurant_id,
                                data: [
                                    ['Total Cash orders', $cashorders],
                                    ['Total Credit Orders', $creditorders],
                                    ['Total Approved orders',$approved],
                                    ['Total Delivered', $delivered],
                                    ['Total Canceled Orders', $canceled]
                                ]
                            } ";
                 else:
                 $dummy .= ", {
                                id: $restaurant_id,
                                data: [
                                    ['Total Cash orders', $cashorders],
                                    ['Total Credit Orders', $creditorders],
                                    ['Total Approved orders',$approved],
                                    ['Total Delivered', $delivered],
                                    ['Total Canceled Orders', $canceled]
                                ]
                            } ";
             endif;
             
             $start++;
         endforeach;
         
       return $dummy;  
         
     }
	 
	 
	        /*
   * function to make json formatter for high charts
   */
     public function chartsJsonFormatterDelivery($data)
     {
         
       
         $start =0;
        
        foreach($data as $dat):
             extract($dat);
              if($rate==0)
              {
                  $rate=10;
                  
              }
            if($start==0):
               
                $dummy ="{ name: '$name',
                y: $totalorders,
                drilldown:$delivery_id }";
                else:
                 $dummy .=",{ name: '$name',
                y: $rate,
                drilldown: $delivery_id }";
            endif;
            
            $start++;
         
        endforeach; 
         
       
        return $dummy;
         
     }
	 
        

        

        

       

    }