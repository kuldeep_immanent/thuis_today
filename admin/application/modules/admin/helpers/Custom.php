<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FOS_Action_Helpers_Custom extends Zend_Controller_Action_Helper_Abstract
{
    
    
  
        private $config;
        private $database;
        private $tp;
    /*********
     * 
     * Function to init the files 
     */
    public function init()
    {
            $bootstrap        = $this->getFrontController()->getParam('bootstrap');
            $this->database   = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp         = $this->getFrontController()->getParam('TablePrefix');
            $this->config     = $bootstrap->getResource('AppConfig'); 
            $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate'); 
    }
    
    /**********
     * function to get the the date according to the requirement
     */
    public function  fetching_date($new_time=null)
	 {

		if($new_time !=null)
		{
			$timestamp =$new_time;
				
		}


		else 
    {
				
				
			$timestamp = date('Y-m-d');
				
				
		}
		$previous_week = strtotime("-1 week $timestamp ");



		$start_week = date("Y-m-d",$previous_week);
		//$end_week = date("Y-m-d",$end_week);

		unset($previous_week);
               
		return $start_week;




	 }
      /*******************************
        *
        * Function is used for to show the cart vat 
        *
        **********/
         public function gethighlowvat($vat)
          {        
             $array = array();  
             $original = json_decode($vat);            
              foreach ($original as  $value) 
              {     
                $data = $value->originalprice*$value->qty;                   
                $array[$value->vat]+=$data;// array('price'=>$value->originalprice, 'vat'=> $value->vat);             
              }            
               return $array;
          }
       /**********
        * 
        * Function To Make The Weekly Report
        * 
        ********/ 
       public function  Weekly_Report()
       {
         
           $week               = new stdClass;
           $week->error        = false;
	         $week->errormessage = null;
           $week->data         = array();
           
           try
           {
            $first_week[0]= $this->fetching_date($new_time=null);
				//Preserves old date for getting next last date
				$next = $first_week[0];

				$current = date('Y-m-d H:i:s');

				$week->current_week_date = $this->time_stamp_converter($current);

				$first_week[0]          = $this->date_converter($first_week[0]);


				$sql="SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE status=0   AND order_date BETWEEN  '{$first_week[0]}' " . "AND '{$current}' ORDER BY order_date ";

				$data[]=$this->database->fetchrow($sql);

				//next week data //
				$first_week[1]=$this->fetching_date($next);
				//Preserves old date for getting next last date
				$next    = $first_week[1];
				$current =$first_week[0];

				$first_week[1]=$this->date_converter($first_week[1]);

				$week2="SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE status=0   AND order_date BETWEEN  '{$first_week[1]}' " . "AND '{$current}' ORDER BY order_date ";


				$data[]=$this->database->fetchrow($week2);


				$first_week[2]=$this->fetching_date($next);
				//Preserves old date for getting next last date
				$next=$first_week[2];
					
				 
				$current=$first_week[1];

				$first_week[2]=$this->date_converter($first_week[2]);
				 
				$week2="SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE status=0  AND order_date BETWEEN  '{$first_week[2]}' " . "AND '{$current}' ORDER BY order_date ";

				$data[]=$this->database->fetchrow($week2);

				$first_week[3]=$this->fetching_date($next);
				 
				//Preserves old date for getting next last date
				$next = $first_week[3];
				 
				$current=$first_week[2];

				$first_week[3]=$this->date_converter($first_week[3]);
				 
				$week2="SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE status=0    AND order_date BETWEEN  '{$first_week[3]}' " . "AND '{$current}' ORDER BY order_date ";

				$data[]= $this->database->fetchrow($week2);

				$week->last_week_date = $this->time_stamp_converter($first_week[3]);  
                                $week->data = $data;
           }
           catch(Exception $e)
           {
               $week->error = true;
	       $week->errormessage = $e->getMessage(); 
           }
           
           
           return $week;
           
       }
       
       
       /*
        * Function  for Converting the date with  in time stamp formate
        */
	public function date_converter($date)
	{

		$time = new DateTime($date);

		$date = $time->format('Y-m-d H:i:s');

		return $date;


	}
		
	/****
         * Function for converting the time stamp into date
         */
		
	public function time_stamp_converter($timestamp)
	{


		$time = new DateTime($timestamp);

		$date = $time->format('Y-m-d');

		return $date;


	}
       /*****
        * function to translate the message 
        */
        public function translate($message)
        {
            return $this->translator->translate($message);
        }
        
      /*****
       * 
       * Function to return the whole data by fetching query
       */
        
        public function G_Get_Data($query)
        {
            
           $result = $this->database->fetchAll($query);
           
          
           return $result;
        }
        
         public function G_result($query)
        {
           
           $result = $this->database->fetchAll($query);
           
          
           return $result;
        }

         /************
         * Function to convert the age into date  
         ************/
        
        public function G_Date_Convert($age)
        {
            
            
            return strtotime(date("Y-m-d",strtotime("-$age year"))) ;
           
            
        }
        
        
        /******
         * 
         * Function to send the Email to Particular Person
         * 
         */
         public function send_notification($registatoin_ids, $message,$googlekey) {
        // include config
       // include_once './config.php';
         

        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message
        );
        
		
        $headers = array(
            'Authorization: key=' .$googlekey,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
		
        if ($result === FALSE) {

            throw new Exception("Push Noyification is not possible due to ".curl_error($ch));
            
            //die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
       
        return json_decode($result);
        
    }
        
        
        
     /*****
      * 
      * Function to get the result and error handling with proper result
      */
        
        public function Get_Data_List()
        {
            $response               = new stdClass;
            $response->error        = false;
            $response->errormessage = null;
           //loading meals category
                try
                  {
                    $restaurant = $this->database->fetchAll("select * from {$this->tp}restaurants");
                    $response->restaurant = array('' => '--- '.$this->translate('Select One').' ---');
                    
                    foreach($restaurant as $entry)
                    {
                        $response->restaurant[$entry->restaurant_id] = stripslashes($entry->restaurant_name);
                    }
                }
                catch(Exception $e)
                {
                    $response->error = true;
                    $response->errorMessage = $e->getMessage();
                }
            
                
             return $response;
               
            
        }
        
        
      public function _Post_Filtered_Data($post)
      {
         
          $freearray = array();
          foreach($post as $key=>$po):
             
              if($po==null ||  $key=='userid' || $key=='cpassword'):
                
                  else:
                  if($key=='Password'):
                     $po =  md5($po); 
                  endif;
                  if($key=='expiry_date'):
                     $po =  strtotime($po); 
                    endif;
                  $freearray[$key]=$po;
              endif;
              
              
              
          endforeach;
          
        
          return $freearray;
          
          
      }
        
		/******
       * functin to get the pizza prices list
       */
      function To_Get_Pizza_Price($pizza)
      {
          
          $pizzas   = array('small','medium','large');
          $newarray = array();
          foreach($pizza as $key=>$pizz):
          $newarray[] = $pizzas[$key].';'.$pizz;    
          endforeach;
          
         
          return implode('~',$newarray);
          
      }
       /***
   * function to make json response to bulk  restaurant
   *****/
      public function Message_Json($list,$message)
      {

         $dummy = array();
         foreach ($list as $key => $value) 
         {
          $data['id']              = $value->id;
          $data['code']            = $value->code;
          $data['discount']        = $value->discount;
          $data['discount_type']   = $value->discount_type;
          $data['ages_range']      = $value->ages_range;
          $data['money_range']     = $value->money_range;
          $data['no_of_uses']      = $value->no_of_uses;
          $data['restaurant_id']   = $value->restaurant_id;
          $data['message']         = $message;
          $data['Expirydate']      = date('Y-m-d',$value->expiry_date);
          $data['status']          = 1;
		  $data['tag']          = "coupon";
          
         }

       return $data;
      } 

      /****************
       * 
       * Function to make the backup of database  file 
       * 
       ***************/
      
     public function backup_tables($host,$user,$pass,$name,$tables = '*')
     {
  
          $link = mysql_connect($host,$user,$pass);
          mysql_select_db($name,$link);
          
          //get all of the tables
          if($tables == '*')
          {
              $tables = array();
              $result = mysql_query('SHOW TABLES');
              while($row = mysql_fetch_row($result))
              {
                $tables[] = $row[0];
              }
          }
          else
          {
              $tables = is_array($tables) ? $tables : explode(',',$tables);
          }
		  
		 
          
          //cycle through
          foreach($tables as $table)
          {
              $result = mysql_query('SELECT * FROM '.$table);
              $num_fields = mysql_num_fields($result);
              
              //$return.= 'DROP TABLE '.$table.';';
              $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
              $return.= "\n\n".$row2[1].";\n\n";
              
              for ($i = 0; $i < $num_fields; $i++) 
              {
                  while($row = mysql_fetch_row($result))
                  {
                      $return.= 'INSERT INTO '.$table.' VALUES(';
                      for($j=0; $j<$num_fields; $j++) 
                      {
                          $row[$j] = addslashes($row[$j]);
                          $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                          if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                          if ($j<($num_fields-1)) { $return.= ','; }
                      }
                      $return.= ");\n";
                  }
              }
              $return.="\n\n\n";
          }
          
          //save file
          $file ='db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql';
          $handle = fopen( $file,'w+');
          fwrite($handle,$return); 
          fclose($handle);
                sleep(2);
                return $file;
}
        
        
        
    
}