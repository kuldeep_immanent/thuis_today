<?php

    require_once 'plugins/admin_module_boostrap_plugin.php';
    require_once 'plugins//admin_module_custom_plugin.php';
    class Admin_Bootstrap extends Zend_Application_Module_Bootstrap{
        
        public function __construct($application){
            parent::__construct($application);
        }
        
        protected function _initPlugins(){
            $this->bootstrap('FrontController');
            $front = Zend_Controller_Front::getInstance();
            $front->registerPlugin(new Admin_Module_Bootstrap_Plugin());
            $front->registerPlugin(new Admin_Module_Custom_Plugin());
        }
        
        protected function _initRoutes(){
            
            $this->bootstrap('FrontController');
            $fc = Zend_Controller_Front::getInstance();
            $router = $fc->getRouter();
            $router->addRoute('admin-login', new Zend_Controller_Router_Route('/admin/login', array('module' => 'admin', 'controller' => 'main', 'action' => 'login')));
            $router->addRoute('admin-logout', new Zend_Controller_Router_Route('/admin/logout', array('module' => 'admin', 'controller' => 'main', 'action' => 'logout')));
            $router->addRoute('admin-dashboard', new Zend_Controller_Router_Route('/admin/dashboard', array('module' => 'admin', 'controller' => 'main', 'action' => 'dashboard')));
            $router->addRoute('admin-Backup', new Zend_Controller_Router_Route('/admin/Backup', array('module' => 'admin', 'controller' => 'main', 'action' => 'Backup'))); 
            $router->addRoute('admin-restaurant-add', new Zend_Controller_Router_Route('/admin/restaurant/add', array('module' => 'admin', 'controller' => 'restaurant', 'action' => 'add')));
            $router->addRoute('admin-restaurant-edit', new Zend_Controller_Router_Route('/admin/restaurant/edit)', array('module' => 'admin', 'controller' => 'restaurant', 'action' => 'edit')));
            $router->addRoute('admin-restaurant-order', new Zend_Controller_Router_Route('/admin/restaurant/order', array('module' => 'admin', 'controller' => 'restaurant', 'action' => 'order')));
            $router->addRoute('admin-restaurant', new Zend_Controller_Router_Route('/admin/restaurant', array('module' => 'admin', 'controller' => 'restaurant', 'action' => 'index')));
            $router->addRoute('admin-settings-locations', new Zend_Controller_Router_Route('/admin/settings/locations', array('module' => 'admin', 'controller' => 'settings', 'action' => 'locations')));
            $router->addRoute('admin-myprofile', new Zend_Controller_Router_Route('/admin/myprofile', array('module' => 'admin', 'controller' => 'main', 'action' => 'myprofile')));
            $router->addRoute('admin-Orders', new Zend_Controller_Router_Route('/admin/Orders', array('module' => 'admin', 'controller' => 'Orders', 'action' => 'orders')));
            $router->addRoute('admin-Orders-edit', new Zend_Controller_Router_Route('/admin/Orders/edit', array('module' => 'admin', 'controller' => 'Orders', 'action' => 'edit')));
            $router->addRoute('admin-Message', new Zend_Controller_Router_Route('/admin/Messages', array('module' => 'admin', 'controller' => 'Message', 'action' => 'index')));
            $router->addRoute('admin-Report', new Zend_Controller_Router_Route('/admin/Report', array('module' => 'admin', 'controller' => 'Report', 'action' => 'index')));
            $router->addRoute('admin-Report-mealreport', new Zend_Controller_Router_Route('/admin/Report/mealreport', array('module' => 'admin', 'controller' => 'Report', 'action' => 'mealreport')));
            $router->addRoute('admin-Report-invoiceperres', new Zend_Controller_Router_Route('/admin/Report/invoiceperres', array('module' => 'admin', 'controller' => 'Report', 'action' => 'invoiceperres')));
            $router->addRoute('admin-Report-editinvoiceperres', new Zend_Controller_Router_Route('/admin/Report/editinvoiceperres', array('module' => 'admin', 'controller' => 'Report', 'action' => 'editinvoiceperres')));
             $router->addRoute('admin-Report-invoicedetail', new Zend_Controller_Router_Route('/admin/Report/invoicedetail', array('module' => 'admin', 'controller' => 'Report', 'action' => 'invoicedetail')));
            $router->addRoute('admin-Report-reportper', new Zend_Controller_Router_Route('/admin/Report/reportper', array('module' => 'admin', 'controller' => 'Report', 'action' => 'reportper')));
            $router->addRoute('admin-Report-deliverreport', new Zend_Controller_Router_Route('/admin/Report/deliverreport', array('module' => 'admin', 'controller' => 'Report', 'action' => 'deliverreport')));
			$router->addRoute('admin-Report-Customerhistory', new Zend_Controller_Router_Route('/admin/Report/customerhistory', array('module' => 'admin', 'controller' => 'Report', 'action' => 'Customerhistory'))); 
			//$router->addRoute('admin-Orders-edit', new Zend_Controller_Router_Route('/admin/Orders', array('module' => 'admin', 'controller' => 'Orders', 'action' => 'delete')));
            $router->addRoute('admin-managemeals', new Zend_Controller_Router_Route('/admin/managemeals', array('module' => 'admin', 'controller' => 'meals', 'action' => 'managemeals')));
            $router->addRoute('admin-managecustommeals', new Zend_Controller_Router_Route('/admin/managecustommeals', array('module' => 'admin', 'controller' => 'meals', 'action' => 'managecustommeals','')));
            $router->addRoute('admin-managepromotions', new Zend_Controller_Router_Route('/admin/managepromotions', array('module' => 'admin', 'controller' => 'promotions', 'action' => 'managepromotions')));
            $router->addRoute('admin-editor-edit', new Zend_Controller_Router_Route('/admin/Editor', array('module' => 'admin', 'controller' => 'Editor', 'action' => 'Editor')));
            $router->addRoute('admin-user', new Zend_Controller_Router_Route('/admin/User', array('module' => 'admin', 'controller' => 'User', 'action' => 'index')));            			
            $router->addRoute('admin-user-add', new Zend_Controller_Router_Route('/admin/User/add', array('module' => 'admin', 'controller' => 'User', 'action' => 'add')));            			$router->addRoute('admin-user-edit', new Zend_Controller_Router_Route('/admin/User/edit', array('module' => 'admin', 'controller' => 'User', 'action' => 'edit')));            			$router->addRoute('admin-user-delete', new Zend_Controller_Router_Route('/admin/User/delete', array('module' => 'admin', 'controller' => 'User', 'action' => 'delete'))); 
            $router->addRoute('admin-coupons', new Zend_Controller_Router_Route('/admin/Coupon/', array('module' => 'admin', 'controller' => 'Coupon', 'action' => 'index'))); 
            $router->addRoute('admin-coupons-addedit', new Zend_Controller_Router_Route('/admin/Coupon/addedit', array('module' => 'admin', 'controller' => 'Coupon', 'action' => 'addedit'))); 
            $router->addRoute('admin-coupons-delete', new Zend_Controller_Router_Route('/admin/Coupon/delete', array('module' => 'admin', 'controller' => 'Coupon', 'action' => 'delete'))); 
            $router->addRoute('admin-bulkoffer', new Zend_Controller_Router_Route('/admin/bulkoffer', array('module' => 'admin', 'controller' => 'Bulkoffer', 'action' => 'index'))); 
            $router->addRoute('restaurant-admin-switchlogin', new Zend_Controller_Router_Route('/restaurant/admin/switchlogin', array('module' => 'restaurant', 'controller' => 'admin', 'action' => 'switchlogin')));
            $router->addRoute('admin-Report-combinereport', new Zend_Controller_Router_Route('/admin/Report/combinereport', array('module' => 'admin', 'controller' => 'Report', 'action' => 'combineReport')));
            $router->addRoute('admin-CrustSouce-crusts', new Zend_Controller_Router_Route('/admin/Crust/crusts', array('module' => 'admin', 'controller' => 'Crust', 'action' => 'crusts')));
            $router->addRoute('admin-CrustSouce-addedit', new Zend_Controller_Router_Route('/admin/Crust/addedit', array('module' => 'admin', 'controller' => 'Crust', 'action' => 'addedit')));
            $router->addRoute('admin-CrustSouce-delete', new Zend_Controller_Router_Route('/admin/Crust/delete', array('module' => 'admin', 'controller' => 'Crust', 'action' => 'delete')));
            $router->addRoute('admin-mealcategory', new Zend_Controller_Router_Route('/admin/mealc', array('module' => 'admin', 'controller' => 'mealc', 'action' => 'index')));
            $router->addRoute('admin-mealcategory-addedit', new Zend_Controller_Router_Route('/admin/mealc/addedit', array('module' => 'admin', 'controller' => 'mealc', 'action' => 'addedit')));
            $router->addRoute('admin-mealcategory-delete', new Zend_Controller_Router_Route('/admin/mealc/deletedata', array('module' => 'admin', 'controller' => 'mealc', 'action' => 'deletedata')));
            $router->addRoute('admin-notify-setting', new Zend_Controller_Router_Route('/admin/notify', array('module' => 'admin', 'controller' => 'Notify', 'action' => 'addeditNotify')));  
			$router->addRoute('admin-feedback-feedback', new Zend_Controller_Router_Route('/admin/feedback', array('module' => 'admin', 'controller' => 'Feedback', 'action' => 'feedback')));
			$router->addRoute('admin-feedback-delete', new Zend_Controller_Router_Route('/admin/feedback/delete', array('module' => 'admin', 'controller' => 'Feedback', 'action' => 'delete'))); 
            $router->addRoute('admin-feedback-detail', new Zend_Controller_Router_Route('/admin/feedback/detail', array('module' => 'admin', 'controller' => 'Feedback', 'action' => 'detail'))); 
			$router->addRoute('admin-statusupdat-updatestatus', new Zend_Controller_Router_Route('/admin/statusupdat/updatestatus', array('module' => 'admin', 'controller' => 'statusupdat', 'action' => 'updatestatus')));
            $router->addRoute('admin-settings-locationsarea', new Zend_Controller_Router_Route('/admin/settings/locationsarea', array('module' => 'admin', 'controller' => 'settings', 'action' => 'locationsarea')));
            $router->addRoute('admin-settings-delarea', new Zend_Controller_Router_Route('/admin/settings/delarea', array('module' => 'admin', 'controller' => 'settings', 'action' => 'delarea')));
            $router->addRoute('admin-restaurant-category', new Zend_Controller_Router_Route('/admin/restaurant/category', array('module' => 'admin', 'controller' => 'restaurant', 'action' => 'category')));
            $router->addRoute('admin-restaurant-updatestatus', new Zend_Controller_Router_Route('/admin/restaurant/updatestatus', array('module' => 'admin', 'controller' => 'restaurant', 'action' => 'updatestatus')));
            $router->addRoute('admin-freelancer', new Zend_Controller_Router_Route('/admin/freelancer/', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'index')));
            $router->addRoute('admin-freelancer-newrequest', new Zend_Controller_Router_Route('/admin/freelancer/newrequest', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'newrequest')));
            $router->addRoute('admin-freelancer-pendingrequest', new Zend_Controller_Router_Route('/admin/freelancer/pendingrequest', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'pendingrequest')));
            $router->addRoute('admin-freelancer-approval', new Zend_Controller_Router_Route('/admin/freelancer/approval', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'approval')));
            $router->addRoute('admin-freelancer-delete', new Zend_Controller_Router_Route('/admin/freelancer/delete', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'delete')));
            $router->addRoute('admin-freelancer-deleterequest', new Zend_Controller_Router_Route('/admin/freelancer/deleterequest', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'deleterequest')));
            $router->addRoute('admin-freelancer-verify', new Zend_Controller_Router_Route('/admin/freelancer/verify', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'verify')));
            $router->addRoute('admin-freelancer-entrancefee', new Zend_Controller_Router_Route('/admin/freelancer/entrancefee', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'entrancefee')));
            $router->addRoute('admin-freelancer-deletevat', new Zend_Controller_Router_Route('/admin/freelancer/deletevat', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'deletevat')));
            $router->addRoute('admin-freelancer-addvat', new Zend_Controller_Router_Route('/admin/freelancer/addvat', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'addvat')));
            $router->addRoute('admin-restaurant-delete', new Zend_Controller_Router_Route('/admin/restaurant/delete', array('module' => 'admin', 'controller' => 'restaurant', 'action' => 'delete')));
            $router->addRoute('admin-restaurant-verify', new Zend_Controller_Router_Route('/admin/restaurant/verify', array('module' => 'admin', 'controller' => 'restaurant', 'action' => 'verify')));
            $router->addRoute('admin-freelancer-edit', new Zend_Controller_Router_Route('/admin/freelancer/edit', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'edit')));
            $router->addRoute('admin-freelancer-orderlist', new Zend_Controller_Router_Route('/admin/freelancer/orderlist', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'orderlist')));
            $router->addRoute('admin-freelancer-updatestatus', new Zend_Controller_Router_Route('/admin/freelancer/updatestatus', array('module' => 'admin', 'controller' => 'freelancer', 'action' => 'updatestatus')));
            
			
        }
        
    }