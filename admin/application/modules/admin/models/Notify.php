<?php

/***
* crusts souces class
*******/


class Admin_Model_Notify extends Zend_Db_Table
{
    
    
    protected $_name ='fos_notify';
    
    /****
     * 
     * function  to get the user detail
     */
    function fetchdata($where=null)
    {
        
        $data = $this->fetchAll();
          
        return $data->toArray();
        
    }


    /***
    function to get the row data
    ****/
    function fetchrowdata($where)
    {
       $data = $this->fetchAll($where);
          
        return $data->toArray();


    }
    /******
    function to edit the values of data
    ******/
    function edit($data,$id)
    {

      $this->update($data,$id);

    }
    /***
    function to add the data
    *******/
    function add($data)
    {

       $this->insert($data);

    }
   /****
   * function to delet  the data from here
   ********/
    function Delete($where,$id)
    {
      $where = $this->getAdapter()->quoteInto($where, $id);
      $this->delete($where);

    }
    
    
    
    
    
}

?>