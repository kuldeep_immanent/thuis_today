<?php

    class FOS_Admin_View_Helpers_RestaurantHelper extends Zend_View_Helper_Abstract{
        
        private $database;
        private $tp;
        private $config;
        private $session;
        
        public function __construct(){
            $front = Zend_Controller_Front::getInstance();
            $bootstrap = $front->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->tp = $front->getParam('TablePrefix');
            $this->config = $front->getParam('Config');
            $this->session = $front->getParam('Session');
        }
        
        public function isAdminAccountActive($resID){
            try{
                $query = $this->database->fetchRow("select is_active, email_verified from {$this->tp}restaurant_admin_users where restaurant_id = ?", $resID);
                if(!$query->email_verified) return false;
                return (bool)$query->is_active;
            }catch(Exception $e){
                return false;
            }
        }
        
        /*
         * Function to check the helper workin or not
         */
        public function Currency_Helper($value)
        {
           
             $currency = new Zend_Currency('US');
             echo $currency->toCurrency($value);
             
        }
          /*********         *          * function to get the  list of  pizza sizes and their  prizes         */                function Get_Pizza_Prices($sizes)        {                      if($sizes=='')            {                             return array('small'=>'','large'=>'','medium'=>'');            }                                   $sizes     = explode('~',$sizes);                       $pizzas    = array('small','large','medium');            $dummy     = array();            $start     = 0;                       foreach($sizes as $size):                               $prize = explode(';',$size);             $dummy[$pizzas[$start]] = $prize[1];             $start++;            endforeach;                                          return $dummy;                                  }          
        
        
          /*
   * function to make json formatter for high charts
   */
     public function chartsJsonFormatter($data)
     {
         
       
         $start =0;
        
        foreach($data as $dat):
             extract($dat);
              if($rate==0)
              {
                  $rate=10;
                  
              }
            if($start==0):
               
                $dummy ="{ name: '$restaurant_display_id',
                y: $rate,
                drilldown:$restaurant_id }";
                else:
                 $dummy .=",{ name: '$restaurant_display_id',
                y: $rate,
                drilldown: $restaurant_id }";
            endif;
            
            $start++;
         
        endforeach; 
         
       
        return $dummy;
         
     }
     
     
     /*****
      * function to get  the sub list of particular  restaurant detail
      */
     public function chartJsonSubList($data)
     {
         $start =0;
         foreach($data as $dat):
             extract($dat);
             if($start==0):
               $dummy = " {
                                id: $restaurant_id,
                                data: [
                                    ['Total Cash orders', $cashorders],
                                    ['Total Credit Orders', $creditorders],
                                    ['Total Approved orders',$approved],
                                    ['Total Delivered', $delivered],
                                    ['Total Canceled Orders', $canceled]
                                ]
                            } ";
                 else:
                 $dummy .= ", {
                                id: $restaurant_id,
                                data: [
                                    ['Total Cash orders', $cashorders],
                                    ['Total Credit Orders', $creditorders],
                                    ['Total Approved orders',$approved],
                                    ['Total Delivered', $delivered],
                                    ['Total Canceled Orders', $canceled]
                                ]
                            } ";
             endif;
             
             $start++;
         endforeach;
         
       return $dummy;  
         
     }
	 
	 
	        /*
   * function to make json formatter for high charts
   */
     public function chartsJsonFormatterDelivery($data)
     {
         
       
         $start =0;
        
        foreach($data as $dat):
             extract($dat);
              if($rate==0)
              {
                  $rate=10;
                  
              }
            if($start==0):
               
                $dummy ="{ name: '$name',
                y: $totalorders,
                drilldown:$delivery_id }";
                else:
                 $dummy .=",{ name: '$name',
                y: $rate,
                drilldown: $delivery_id }";
            endif;
            
            $start++;
         
        endforeach; 
         
       
        return $dummy;
         
     }
 /******
   * Function to make the  proper report section  json report done 
   *******/
      public function Making_report_proper($jsonformate)
      {
        

       $jsonarray = json_decode($jsonformate);
      
       return $jsonarray;
      
      

      }	 
        
    }