<?php

    class Admin_Module_Bootstrap_Plugin extends Zend_Controller_Plugin_Abstract{
        
        public $_actionController;
        
        public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request){
            if('admin' != $request->getModuleName()) return;
            
            $frontController = Zend_Controller_Front::getInstance();
            
            //registering action helpers
            Zend_Controller_Action_HelperBroker::addPath( APPLICATION_PATH . '/modules/admin/helpers', 'FOS_Admin_Helpers_' );
            Zend_Controller_Action_HelperBroker::addPath( APPLICATION_PATH . '/modules/admin/models', 'FOS_Admin_models_' );
            //registering view helpers
            $viewRenderer = Zend_Controller_Action_HelperBroker::getHelper('ViewRenderer');
            $view = $viewRenderer->view;
            $view->doctype('HTML5');
            $view->addHelperPath(APPLICATION_PATH . '/modules/admin/views/helpers', 'FOS_Admin_View_Helpers_');
            
            //loading config
            $config = new Zend_Config_Xml(
                APPLICATION_PATH . '/modules/admin/configs/config.xml',
                null,
                array(
                    'allowModifications' => true
                )
            );
            $frontController->setParam('Config', $config);
            
            //loading session
            $session = new Zend_Session_Namespace('fosadminsession');
            $session->setExpirationSeconds((int)$config->session->expirationtime);
            if(!isset($session->admin->loggedin)) $session->admin->loggedin = false;
            $frontController->setParam('Session', $session);
        }
        
    }