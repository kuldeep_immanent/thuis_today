 <?php

    class Admin_CrustController extends Zend_Controller_Action
    {
      
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_helper;
        public $model;
        
        public function init(){
            //initializing resources
            $bootstrap       = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database   = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig  = $bootstrap->getResource('AppConfig');
            $this->config     = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->_C_helper  = $this->getHelper('Custom');
            $this->model      = new Admin_Model_Crusts();
        
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            $response = new stdclass;  
        	$response->test=$this->test;
		    $response->count=$this->count;
            $this->view->headTitle($this->translate('Shop Admin Panel').' |');
        }
        
        public function preDispatch(){
        	
        	if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
             $this->view->sidebar_menu_item = "crust";
        }
        
        public function postDispatch(){
            
        }
        
       
        private function checkLogin(){
            if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
        }
        
       
       
               
         public function translate($message)
                 {
            return $this->translator->translate($message);
        }

      /****
      function to  get the Crusts 
      *****/
        public function crustsAction()
        {
            $response        = new stdclass;
            $response->error = false;
            $response->errormessage = null;
            try
            {
              $response->crustsmeals = $this->model->fetchdata();

            }
            catch(Exception $e)
            {
               $response->error = true;
                $response->errormessage =$e->getMessage();


            } 

           $this->view->assign((array)$response);


        }

        /***
        *Function to add the data in da 
        ****/

        public function addeditAction()
        {
             $reponse          = new stdclass;
             $response->error  = false;
             $response->errormessage = null;
             $response->success = false;     
             

             try 

             {
             if($this->getRequest()->isPost())
             {
                // $target_dir = "http://food.vervetelecom.com/images/";
                // $target_file = $target_dir . basename($_FILES["mealpic"]["name"]);
               
                // $uploadOk = 1;
                // $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

                // // Check if image file is a actual image or fake image

                //     $check = getimagesize($_FILES["mealpic"]["tmp_name"]);
                   

                //     if($check !== false) {
                //         echo "File is an image - " . $check["mime"] . ".";
                //         $uploadOk = 1;

                //     } else {
                //         echo "File is not an image.";
                //         $uploadOk = 0;
                //     }

                    $uploaddir = './uploads/';
                    $uploadfile = $uploaddir . basename($_FILES['mealpic']['name']);
                    $var = basename($_FILES['mealpic']['name']);
                    
                    if (move_uploaded_file($_FILES['mealpic']['tmp_name'], $uploadfile)) {
                      echo "File is valid, and was successfully uploaded.\n";
                    } else {
                       echo "Upload failed";
                    }


     $data = array('name'=>$this->getRequest()->name,
                                    'type'=>$this->getRequest()->type,
                                    'itemstatus'=>$this->getRequest()->paymentstatus,
                                    'price'=>$this->getRequest()->price,
									'name_ar'=>trim($this->getRequest()->name_ar),
									'custom_pizza_extra'=>$this->getRequest()->custom_pizza_extra,
                                    'crust_pic'=>$var
                                    );            

            // if(isset($_FILES['mealpic']['name'])&& $_FILES['mealpic']['name']!='')
            // {
            // $adapter = new Zend_File_Transfer_Adapter_Http();
            // $adapter->addFilter('Rename', array('target' => './uploads/'.time().'.jpg',
            //          'overwrite' => true)); 
          
            //     $file = $adapter->getFileName(null,false);

            //     $image_add = './uploads/'.$file;

            //     $images = $image_add;
            //     $new_images = $images;
            //     $width=200; // define width
            //     $size=GetimageSize($images);
            //     $height=round($width*$size[1]/$size[0]);
            //     $images_orig = ImageCreateFromJPEG($images);
            //     $photoX = ImagesX($images_orig);
            //     $photoY = ImagesY($images_orig);
            //     $images_fin = ImageCreateTrueColor($width, $height);

            //     ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
            //     ImageJPEG($images_fin,$new_images);
            //     ImageDestroy($images_orig);
            //     ImageDestroy($images_fin); 

            //     $data = array('name'=>$this->getRequest()->name,
            //                         'type'=>$this->getRequest()->type,
            //                         'itemstatus'=>$this->getRequest()->paymentstatus,
            //                         'price'=>$this->getRequest()->price,
            //                         'crust_pic'=>$file
            //                         );
            //     }

                    if(isset($this->getRequest()->id))
                    {
                        
                          $this->model->edit($data,'id='.$this->getRequest()->id);
                          $response->success = true;     
                    }
                    else
                    {
                     
                             $this->model->add($data);
                             $response->success = true;   
                    }


             }
  
             if(isset($_GET['edit']))
             { 

                $response->data = $this->model->fetchrowdata('id='.$this->getRequest()->edit);
                


             }
           }
           catch(Exception $e)
           {
               $response->error  = true;
               $response->success = false;   
               $response->errormessage = $e->getMessage();


           }

          
           
            $this->view->assign((array)$response);

        }
      
      
       /***
       * function to delete the data 
       *****/
         public function deleteAction()
         {
           if(isset($_GET['delete']))
           {

             try 
             {

              $this->database->delete(
                                $this->tp.'crustssouces',
                                'id = '.$this->getRequest()->delete
                            );
          
                
              } 
              catch (Exception $e) 
              {
                $error = $e->getMessage();
              } 
             
           }
              
                 $this->_redirect('admin/Crust/crusts');
         }
       

       
}



    ?>