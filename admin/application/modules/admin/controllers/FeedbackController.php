<?php

/******

*Controller:List Feedbacks

*Author:Gagandeep Singh

*Description:This controller handle  Restaurant Feedbacks

*******/

 class Admin_FeedbackController extends Zend_Controller_Action

 {

	

        public $tp; //Table prefix

        public $appConfig;

        public $config;

        public $session;

        public $translator;

        public $_helper;

        public $model;

        public function init()

              {

				$bootstrap        = Zend_Controller_Front::getInstance()->getParam('bootstrap');  
                $this->database   = $bootstrap->getPluginResource('db')->getDbAdapter();
                $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
                $this->tp         = Zend_Controller_Front::getInstance()->getParam('TablePrefix');

                $this->appConfig  = $bootstrap->getResource('AppConfig');

                $this->config     = Zend_Controller_Front::getInstance()->getParam('Config');

                $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');

                $this->translator = Zend_Registry::get('Zend_Translate');

                $this->_C_helper  = $this->getHelper('Custom'); 

				$this->model      =  new Admin_Model_Common('fos_feedbacks');

  

                $this->view->sidebar_menu_item ='feedback';	

                //initializing layout

            Zend_Layout::startMvc(array(

                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',

                'layout' => 'main'

            ));

            //adding default page title

            $this->view->headTitle('Admin Panel | Report Management System |');				

			  }


        public function preDispatch(){
			if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
           
        }
	   /******
       Translating the text Domain
       *******/
         private function translate($message){
            return $this->translator->translate($message);
        }
  

  /******

  * Generating Report by filtering data 

  *******/  

   public function feedbackAction()

   {
        $data               = new stdclass;
        $data->error        = false;
        $data->errormessage = null;

	    if($this->getRequest()->isPost())

		{

			$Restaurant_id= $this->getRequest()->res_id;
            $where  ="where fos_feedbacks.res_id=$Restaurant_id";
			

		}
		else 
		 {
		     $where  ="";
		 }	
		
		try{
				
               $query  = "SELECT * FROM  `fos_feedbacks`  left join fos_users on fos_feedbacks.user_id=fos_users.ID  left JOIN fos_restaurants
 
               ON fos_feedbacks.res_id = fos_restaurants.restaurant_id $where order by fos_users.FirstName ";
               

   			    $data->data  = $this->rebuitArray($this->_C_helper->G_Get_Data($query));
                $deliverpoints = $this->database->fetchAll("select * from {$this->tp}restaurants");
                $data->resids[''] = array('' => $this->translate('-- Select A Delivery Point --'));
                foreach($deliverpoints as $entry)
                    {
                $data->resids[$entry->restaurant_id] = stripslashes($entry->restaurant_name);
                    }

				}
				catch (Exception $e) 
				{
                      $data->error        = true;
                      $data->errormessage = $e->getMessage();
					
				}


	    $this->view->assign((array)$data);					

	  

	  

   }

   

    /***
    * function to delete Action to delete the delivery boys
     */
  public function deleteAction()
         {


             try {
                 
                 $id = $this->getRequest()->id;
                 $this->database->delete('fos_feedbacks', array(
                    'ID = ?' => $id
                    
                ));
                 
                 
             } catch (Exception $ex) {
                 
             }
             
             $this->_redirect("/admin/feedback");  
             
         }

	 
         /***
          * Feedback detail Action 
          */
         public function detailAction()
         {
             
             
            $response = new stdclass;
            $response->error = false;
            $response->errormessage = null;
            $response->success = false;
            
            
            try 
            {
                 if(isset($_GET['id'])):

                 $id    = $this->getRequest()->id;  
                 $query = "Select * from  fos_feedbacks inner join fos_users on fos_feedbacks.user_id =fos_users.ID where  fos_feedbacks.id=$id";
                 $response->data  =  $this->_C_helper->G_Get_Data($query);
                 endif;

                $response->restaurants->count = 0;
                $queryStr = "SELECT * " .
                "FROM {$this->tp}restaurant_customer_order"."  
                WHERE id={$response->data[0]->order_id}";
                $response->restaurants->list = $this->_C_helper->G_Get_Data($queryStr);
                $response->restaurants->count = count($response->restaurants->list);
               



                   }
            catch (Exception $e)
                       {
                            $response->error = true;
                            $response->errormessage = $e->getMessage();
                       }
             $this->view->assign((array)$response);
         }

/*******
* function to rebuit Array for specific use
*****/
         public function rebuitArray($data)
         {
         	
             $dummyArray =array();
            
             foreach ($data as $key => $value) {
                  
                  $maindata['id'] =$value->id;
                  $maindata['order_id'] =$value->order_id;
                  $maindata['order_id'] =$value->order_id;
                  $maindata['res_id'] =$value->res_id;
                  $maindata['comment'] =$value->comment;
                  $maindata['FirstName'] =$value->FirstName;  
                  $maindata['LastName'] =$value->LastName;                
                  $maindata['delivery_boy']= $this->Getdeliverboy($value->order_id);
             
             	$dummyArray[] =$maindata;
             }

          
           return $dummyArray;

         }

  /*******
  * Adding Delivery Boy Name in list
  *********/
         public function Getdeliverboy($order_id)
         {

         	$query  = "select delivery_id from {$this->tp}delivery_order_rel_boy where order_id=$order_id";
         	

         	$data   = $this->_C_helper->G_Get_Data($query);
         	
            if(count($data)>0)
            {
              $deliveryboyid=$data[0]->delivery_id;
              $query  = "select name from {$this->tp}delivery_boy_login where id=$deliveryboyid";
         	  $data   = $this->_C_helper->G_Get_Data($query);
              
              if(count($data)>0)
              {
                   return $data[0]->name;

              }
              else
              {
                 return null;

              }

            }
            else
             {
               return null;
             }	
 

         }



	 

	 

 }









?>