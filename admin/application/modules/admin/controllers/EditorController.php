<?php

/* Class: Editor Orders controller 
 * Author:Gagandeep Singh
 * Company:Boom Infotech Mohali
 * */

include('CommonController.php');
class Admin_EditorController extends Zend_Controller_Action
{
  
    
     public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
       
        
        public function init(){
           //initializing resources
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig = $bootstrap->getResource('AppConfig');
            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test=commonnotify::Notification();
            $this->count=commonnotify::total_count();
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            $this->view->headTitle($this->translate('Admin Panel').' | ');
        }
        
        /******
         * 
         * function for translating  the
         */
        public function translate($message){
            return $this->translator->translate($message);
        }
        
        /******
         * 
         * Action to go to editor
         */
        
        public function editorAction()
        {
            $editor          = new stdClass;
            $editor->success = false; 
            $editor->error = false;
            $editor->errormessage = null;
            if(isset($this->getRequest()->language_text))
            {
                try
                {
                    
                     file_put_contents($file, $this->getRequest()->language_text);
                
                     
                } 
                catch (Exception $ex) {
                  $editor->error = true;
                  $editor->errormessage = $ex->getMessage();
                }
                
                
            }
            
            
            
            try
            {
                
              $file = APPLICATION_PATH.'/languages/en.po';
              $editor->textval =   file_get_contents($file);  
            } catch (Exception $ex) {
               $editor->error = true;
                  $editor->errormessage = $ex->getMessage();
            }
            
              
           $this->view->assign((array) $editor);
           $this->view->sidebar_menu_item = 'LanguageEdit';
           $this->view->headTitle($this->translate('Dashboard').' | ', 'PREPEND'); 
            
            
            
        }
    
    
    
}





?>

