<?php
include('CommonController.php');
    class Admin_StatusupdatController extends Zend_Controller_Action{
        
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_C_Helper;
        public $_m_table ='for_coupons';
        public $_r_table ='fos_users';
        public function init(){
            //initializing resources
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig = $bootstrap->getResource('AppConfig');
            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->_C_Helper = $this->getHelper('Custom');
            $this->_m_table ='for_coupons';
             $this->_r_table ='fos_users';
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            
        	
            $this->view->headTitle( 'DeliveryAPP');
        }
        
        public function preDispatch(){
			if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
           
        }
        
        public function postDispatch(){
            
        }
        
      
        
      
       
   
        /********
         * Function to Find The People To Get The Offer 
         ********/
        public function updatestatusAction()
        {
          
            try { 
			               $user_lists = $this->_C_Helper->G_result("Select * from fos_users ");         
              

                            $coupondetail  = $this->_C_Helper->G_result("Select * from for_coupons where id=11"); 
			
			                 foreach($user_lists as $money):
                            
                        if($coupondetail[0]->discount_type=='cash'):
                            $message ="Use this Coupon  to get   " .$coupondetail[0]->discount." SAR cash discount on total cart price <br/> ";
                            $message .="coupon Code:".$coupondetail[0]->code." <br/>";
                            $message .="Expirary Date:".date('Y-m-d',$coupondetail[0]->expiry_date)." <br/>";
                           
                            else:
                            $message  ="Use this Coupon  to get free ".$coupondetail[0]->discount."  on order<br/>";
                            $message .="coupon Code:".$coupondetail[0]->code."<br/>";
                            $message .="Expirary Date:".date('Y-m-d',$coupondetail[0]->expiry_date)." <br/>";
                        endif;
                        
                        
                       
                        if($money->device_type=='android'):
                            $registatoin_ids = array($money->device_id);
                            
                             $pushmess = $this->_C_Helper->Message_Json($coupondetail,$message);

                              $message = array("message" => $pushmess);
                            

                              $result = $this->_C_Helper->send_notification($registatoin_ids,$message,GOOGLE_API_KEY);
                            
                              else:

                        endif;
                
                
            endforeach;
                           
                            
                          
                
                
          
               
               
               
            } catch (Exception $ex) {
                $error = $ex->getMessage();
            }
     
            return $result;
              die; 
        }
     
       
    }
    
    ?>