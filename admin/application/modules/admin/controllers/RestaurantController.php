<?php
  include('CommonController.php');
    class Admin_RestaurantController extends Zend_Controller_Action{
        
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        
        public function init(){
            //initializing resources
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig = $bootstrap->getResource('AppConfig');
            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test       = commonnotify::Notification();
            $this->count      = commonnotify::total_count();
            $this->C_Helper   = $this->getHelper('Custom');
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            $this->view->headTitle($this->translate('Admin Panel').' | ');
        }
        
        public function preDispatch(){
            //checking session
            if(!$this->session->admin->loggedin)
                $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
            //adding few variables to view
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
        }
        
        //for translating the language //
        
    public function translate($message){
            return $this->translator->translate($message);
              }
        //index function for fetching the indes page values //
        public function indexAction(){
            
        
            $response = new stdclass;
            $response->error = false;
            
            if($this->getRequest()->isPost()){
                try{
                    switch($this->getRequest()->frmaction){
                        case 'activate_selected': {
                            foreach($this->getRequest()->selectedcheckbox as $resid){
                                try{
                                    $this->database->update(
                                        $this->tp.'restaurants',
                                        array('is_active' => 1),
                                        'restaurant_id = '.$resid
                                    );
                                }catch(Exception $e){
                                    //do nothing
                                }
                            }
                            break;
                        }
                        case 'deactivate_selected': {
                            foreach($this->getRequest()->selectedcheckbox as $resid){
                                try{
                                    $this->database->update(
                                        $this->tp.'restaurants',
                                        array('is_active' => 0),
                                        'restaurant_id = '.$resid
                                    );
                                }catch(Exception $e){
                                    //do nothing
                                }
                            }
                            break;
                        }
                        case 'activate': {
                            $this->database->update(
                                $this->tp.'restaurants',
                                array('is_active' => 1),
                                'restaurant_id = '.$this->getRequest()->resid
                            );
                            break;
                        }
                        case 'deactivate': {
                            $this->database->update(
                                $this->tp.'restaurants',
                                array('is_active' => 0),
                                'restaurant_id = '.$this->getRequest()->resid
                            );
                            break;
                        }
                        case 'activate_admin_account': {
                            //verifying whether admin email is verified or not
                            $isEmailVerified = $this->database->fetchOne("select email_verified from {$this->tp}restaurant_admin_users where restaurant_id = ?", $this->getRequest()->resid);
                            if(!$isEmailVerified) throw new Exception($this->translate('Shop admin account can only be activated when admin email is verified.'));
                            $this->database->update(
                                $this->tp.'restaurant_admin_users',
                                array('is_active' => 1),
                                'restaurant_id = '.$this->getRequest()->resid                                
                            );
                            break;
                        }
                        case 'deactivate_admin_account': {
                            $this->database->update(
                                $this->tp.'restaurant_admin_users',
                                array('is_active' => 1),
                                'restaurant_id = '.$this->getRequest()->resid                                
                            );
                            break;
                        
                        
                       
                        }
                         case 'is_open':
                        {
                         
                       
                         $this->database->update(
                                $this->tp.'restaurants',
                                array('is_open' => 1),
                                'restaurant_id = '.$this->getRequest()->resid);   
                            
                        
                         
                         break;  
                        }
                        
                        
                         case 'is_close':
                        {
                         
                       
                         $this->database->update(
                                $this->tp.'restaurants',
                                array('is_open' => 0),
                                'restaurant_id = '.$this->getRequest()->resid);   
                          
                        
                           break;  
                        }
                       
                         case 'allow': {
                             
                        
                            //TODO: add other records to delete
                              foreach($this->getRequest()->selectedcheckbox as $resid)
                                  {
                                
                             $this->database->update(
                                $this->tp.'restaurants',
                                array('permitme' => 1),
                                'restaurant_id = '.$resid
                            );
                                }
                            break;
                        }
                        case 'denied': {
                            //TODO: add other records to delete
                            foreach($this->getRequest()->selectedcheckbox as $resid)
                                  {
                             $this->database->update(
                                $this->tp.'restaurants',
                                array('permitme' => 0),
                                'restaurant_id = '.$resid
                            );
                                  }
                            break;
                        }

                         case 'delete': {
                            //TODO: add other records to delete
                            foreach($this->getRequest()->selectedcheckbox as $resid)
                                  {
                             $this->database->delete(
                                $this->tp.'restaurants',
                                'restaurant_id = '.$resid
                            );
                                  }
                            break;
                        }
                        
                      
                    }
                }
                catch(Exception $e){
                    $response->error = true;
                    $response->errorMessage = $e->getMessage();
                }
            }
            
            $criteria = null;
            $sortcriteria = ' order by restaurant_id';
            $sortorder = ' asc';
            
            if($this->getRequest()->isPost() and (bool)$this->getRequest()->filterset){
                
                $criteria .= ' is_verified = '.(($this->getRequest()->filterbyapprovedstatus == 'approved') ? 1 : 0);
                
                $criteria .= ' and is_active = '.(($this->getRequest()->filterbyactivestatus == 'active') ? 1 : 0);
                $criteria .= ' and is_open = '.(($this->getRequest()->filterbyopenstatus == 'opened') ? 1 : 0);
                
                switch($this->getRequest()->sortby){
                    case 'name': {
                        $sortcriteria = ' order by restaurant_name';
                        break;
                    }
                    case 'location': {
                        $sortcriteria = ' order by location';
                        break;
                    }
                    case 'amount': {
                        $sortcriteria = ' order by amount';
                        break;
                    }
                    case 'orders': {
                        $sortcriteria = ' order by totalorders';
                        break;
                    }
                    case 'active-orders': {
                        $sortcriteria = ' order by activeorders';
                        break;
                    }
                }
                $sortorder = ' '.$this->getRequest()->sortorder;
            }
            
            try{
                $response->restaurants->count = 0;
                $queryStr = "select a.*, b.*, c.*, d.name from {$this->tp}restaurants as a LEFT JOIN {$this->tp}restaurant_address as b ON a.restaurant_id = b.restaurant_id LEFT JOIN {$this->tp}shop_categories as c ON b.category = c.category_id LEFT JOIN {$this->tp}freelancer as d on a.reference = d.id LEFT JOIN {$this->tp}restaurant_admin_users as e on a.restaurant_id = e.restaurant_id where e.status = 1";
                $response->restaurants->list = $this->database->fetchAll($queryStr);
                $response->restaurants->count = count($response->restaurants->list); 
                // $queryStr = "select * from {$this->tp}restaurant_address";
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $this->translate('Failed to load restaurant list. Please re-try again.');
            }
            $response->test=$this->test;
            $response->count=$this->count;
            $this->view->assign((array)$response);
            $this->view->sidebar_menu_item = 'restaurants';
            
            $paginator = Zend_Paginator::factory($response->restaurants->list);
            $paginator->setDefaultItemCountPerPage(10000);
            $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));  
            $this->view->paginator = $paginator;  
           
            $this->view->headTitle($this->translate('Manage Shops').' | ', 'PREPEND');
        
            }
public function updatestatusAction()
    {
      if($this->getRequest()->isPost())
        {
            $category_id = $this->getRequest()->category_id;
            $status = $this->getRequest()->changestatus;
             $this->database->update(
                                    $this->tp.'shop_categories',
                                    array('status' => $status),
                                    'category_id = '.$category_id
                                );           
        }
        $this->_redirect("/admin/restaurant/category");
    }
        public function categoryAction(){     
            $response = new stdclass;
            $response->error = false;                  
            try{                  
                $response->category->count = 0;
                $queryStr = "select * from {$this->tp}shop_categories" ;
                $response->category->list = $this->database->fetchAll($queryStr);

                $response->category->count = count($response->category->list); 
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $this->translate('Failed to load category list. Please re-try again.');
            }
             $response->test=$this->test;
            $response->count=$this->count;
            $this->view->assign((array)$response);
            $this->view->sidebar_menu_item = 'category';
            
            $paginator = Zend_Paginator::factory($response->category->list);
            $paginator->setDefaultItemCountPerPage(10000);
            $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));  
            $this->view->paginator = $paginator;  
             $this->view->headTitle($this->translate('Manage Categories').' | ', 'PREPEND');
        }

 /////////////////////////////////////////////////////////////
   public function pendingrequestAction(){
           $response         = new stdClass();
           $respose->error   = false;
           $response->message = null;           
           try
           {   
             $response->data  = $this->C_Helper->G_Get_Data("select a.*, b.*, c.* from {$this->tp}restaurants as a LEFT JOIN {$this->tp}restaurant_address as b ON a.restaurant_id = b.restaurant_id LEFT JOIN {$this->tp}restaurant_admin_users as c on a.restaurant_id = c.restaurant_id where c.status = 0");
             $response->count = count($response->data);    
           } catch (Exception $ex) {
            
                $response->message =$ex->getMessage();
               
           }
                       
            $paginator = Zend_Paginator::factory($response->data);
            $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
             $paginator->setDefaultItemCountPerPage(10000);
            $this->view->paginator = $paginator;
            $this->view->assign((array)$response);       
            $this->view->headTitle($this->translate('PendingRequest').' | ', 'PREPEND');    
        }
////////////////////////////////////////////////////////////
         public function addcategoryAction(){

             $response = new stdclass;
            $response->error = false;
            $response->success = false;
           
            if($this->getRequest()->isPost()){
                // print_r($this->getRequest()->cateImage);
                try{ 
                    if(!Zend_Validate::is($this->getRequest()->res_category_name, 'NotEmpty'))
                    throw new Exception($this->translate('Mandatory fields are found empty.'));                 
                    try{                       
                         $data = array( 
                                'category_name' => $this->getRequest()->res_category_name,
                                'image_id' => $this->getRequest()->cateImage,
                                 'status' => $this->getRequest()->status
                            );   

                        $this->database->insert(
                            $this->tp.'shop_categories',
                            $data
                        );

                          $response->success = true;                        
                    }catch(Exception $e){
                        throw new Exception($this->translate('Failed to add new Category. Please re-try again.'));
                    }
                }catch(Exception $e){
                    $response->error = true;
                    $response->errorMessage = $e->getMessage();
                }
            // res_category_name
         }
          $queryStr = "select * from {$this->tp}category_image";
                $response->categoyimage = $this->database->fetchAll($queryStr);

            $response->test=$this->test;
            $response->count=$this->count;
            $this->view->sidebar_menu_item = 'category';
            $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Add New Category').' | ', 'PREPEND');
     }

        public function addAction(){
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
           
            if($this->getRequest()->isPost()){
                try{ 
                    //validating fields
                    if( //!Zend_Validate::is($this->getRequest()->res_name, 'NotEmpty') ||
                        !Zend_Validate::is($this->getRequest()->res_name_ar, 'NotEmpty') ||
                       // !Zend_Validate::is($this->getRequest()->res_delivery_fees, 'NotEmpty') ||
                       // !Zend_Validate::is($this->getRequest()->res_avg_delivery_time, 'NotEmpty') ||
                       // !Zend_Validate::is($this->getRequest()->res_address, 'NotEmpty') ||
                       // !Zend_Validate::is($this->getRequest()->res_city, 'NotEmpty') ||
                        //!Zend_Validate::is($this->getRequest()->paymentmethod, 'NotEmpty') ||
                        !Zend_Validate::is($this->getRequest()->res_admin_name, 'NotEmpty')|| 
                        !Zend_Validate::is($this->getRequest()->res_password, 'NotEmpty')
                       // || !Zend_Validate::is($this->getRequest()->open_time, 'NotEmpty')
                       // || !Zend_Validate::is($this->getRequest()->close_time, 'NotEmpty')
                    )
                    throw new Exception($this->translate('Mandatory fields are found empty.'));
                    
                    
                    if(Zend_Validate::is($this->getRequest()->res_contact, 'NotEmpty') and !Zend_Validate::is($this->getRequest()->res_contact, 'EmailAddress', array(Zend_Validate_Hostname::ALLOW_DNS)))
                        throw new Exception($this->translate('Invalid contact email address.'));
                    if(Zend_Validate::is($this->getRequest()->res_admin_email, 'NotEmpty') and !Zend_Validate::is($this->getRequest()->res_contact, 'EmailAddress', array(Zend_Validate_Hostname::ALLOW_DNS)))
                        throw new Exception($this->translate('Invalid contact email address.'));
                   
                  /*  if(!is_numeric($this->getRequest()->res_delivery_fees) || $this->getRequest()->res_delivery_fees < 0)
                        throw new Exception($this->translate('Invalid delivery fees, must be numeric and not less than 0.')); */
                /*     if(!is_numeric($this->getRequest()->res_avg_delivery_time) || $this->getRequest()->res_avg_delivery_time <= 0)
                        throw new Exception($this->translate('Average delivery time must be numeric, and greater than 0.')); */
                   if($this->getRequest()->res_password !=$this->getRequest()->res_con_pass)
                    throw new Exception($this->translate('confirm password does not match with password'));
                // validation for unique email address
                    $email_user = $this->getRequest()->res_admin_email;
                    $verifi_email = $this->database->fetchAll("select * from {$this->tp}restaurant_admin_users  where user_email = '$email_user'");
                     if(!empty($verifi_email))
                    throw new Exception($this->translate('This email id is already registered. Please register with a different email id.'));           
                     
                    try{
                        //adding restaurant
                         $data = array( 
                                'restaurant_display_id' => $this->getHelper('RestaurantSlug')->generateSlug($this->getRequest()->res_name_ar),
                               // 'restaurant_name' => trim($this->getRequest()->res_name),
                                'restaurant_name_ar' => trim($this->getRequest()->res_name_ar),
                                //'restaurant_description' => trim($this->getRequest()->res_description),
                                 'restaurant_description_ar' => trim($this->getRequest()->res_description_ar),
                                'contact_email' => $this->getRequest()->res_contact,
                                'added_on' => new Zend_Db_Expr('now()'),
                                'is_verified' => true,
                                'is_active' => true, 
                                'reference' => 'A'                                                         
                               // 'payment_method'=>$this->getRequest()->paymentmethod,
                               // 'shop_holiday'=>$this->getRequest()->shop_holiday,
                               // 'delivery_fees' => $this->getRequest()->res_delivery_fees,
                               // 'average_delivery_time' => $this->getRequest()->res_avg_delivery_time
                                
                            );
                      /*    echo "<pre>";
                         print_r($data);
                         die; */
                         if( isset($_FILES['res_icon']) && $_FILES['res_icon'] !='' )
                         {
                           
                         $data['logo']   = $this->G_Upload_file();      
                         }
                         else 
                         {
                             
                         $data['logo']   = 'photo_not_available.png';    
                         }
                        $this->database->insert(
                            $this->tp.'restaurants',
                            $data
                        );
                        $resID = $this->database->lastInsertId();
                        
                        //adding restaurant address
                        $data = array(
                                'restaurant_id' => $resID,
                                //'location_id' => $this->getRequest()->res_location,
                                //'area_id' => $this->getRequest()->res_area,
                               // 'address' => $this->getRequest()->res_address,
                                'address_ar' => $this->getRequest()->res_address_ar,
                                'category'=>$this->getRequest()->category
                               // 'latitude'=> $this->getRequest()->res_latitude,
                                //'longitude'=>$this->getRequest()->res_longitude,
                               // 'open_time'=>$this->getRequest()->open_time,
                               // 'close_time'=>$this->getRequest()->close_time,
                                //'country_id'=>$this->getRequest()->res_city
                            );
                        /*  echo "<pre>";
                         print_r($data);
                         die; */
                        $this->database->insert(
                            $this->tp.'restaurant_address',
                            $data   
                        );
                        
                          
                        $this->database->insert(
                            $this->tp.'restaurant_setting',
                            array('res_id'=>$resID,
                               // 'deliver_time'=>$this->getRequest()->res_avg_delivery_time,
                               // 'open_time'=>$this->getRequest()->open_time,
                              //  'close_time'=>$this->getRequest()->close_time,
                            //  'minimum_order'=>$this->getRequest()->minimum_order,
                               // 'delivery_charges'=>$this->getRequest()->res_delivery_fees
                               )    
                        );
                        $this->database->insert(
                            $this->tp.'restaurant_admin_users',
                            array(
                                'restaurant_id' => $resID,
                                'user_name' => $this->getRequest()->res_admin_name,
                                'user_email' => $this->getRequest()->res_admin_email,
                                'contact_number'=> $this->getRequest()->res_admin_phone,
                                'account_password'=>$this->getRequest()->res_password,
                                'is_active'  =>1,
                                'status' => 1 
                            )
                        );
                                  $email1 = $this->getRequest()->res_admin_email;
                                  $pwed = $this->getRequest()->res_password;
                                  $link = "http://deliveryapp.demodemo.ga/restaurant/admin/login";
                                    $config = array(
                                    'auth' => 'login',
                                    'username' => 'mail@deliveryapp.demodemo.ga',
                                    'password' => 'netone@108',
                                    'port'     => 25);     
                                                      
                                    $transport = new Zend_Mail_Transport_Smtp('localhost', $config);                                     
                                    $mail = new Zend_Mail();
                                    $mail->setBodyHtml('<p>Your Shop account is activated successfully.</p><p>You can login with this credentials:</p><p>Email: '.$email1.'</p><p>Password: '.$pwed.'</p>'.$link);
                                    $mail->setFrom('mail@deliveryapp.demodemo.ga', 'Home Today');
                                    $mail->addTo($email1, 'Some Recipient');
                                    $mail->setSubject('Activation Successfully!');
                                    $mail->send($transport);  
                        //adding restaurant admin details
            
                        //sending email verification mail
                      /*  $mail = new Zend_Mail('utf-8');
                        $mail->setSubject('FOS : Verify your email address');
                        $mail->addTo($this->getRequest()->res_admin_email, $this->getRequest()->res_admin_name);
                        $this->getHelper('Layout')->disableLayout();
                        $this->getHelper('ViewRenderer')->setNoRender();
                        $this->view->admin_name = trim($this->getRequest()->res_admin_name);
                        $this->view->mail_subject = $mail->getSubject();
                        $this->view->ver_code =  md5($resID . $this->getRequest()->res_admin_email);
                        $mail->setBodyHtml($this->view->render('mails/restaurant_admin_verify_email.phtml'));
                        $this->getHelper('ViewRenderer')->setNoRender(false);
                        $this->getHelper('Layout')->enableLayout();
                        $this->getHelper('Mailer')->sendMail($mail); */
                        $response->success = true;                        
                    }catch(Exception $e){
                        throw new Exception($this->translate('Failed to add new restaurant. Please re-try again.'));
                    }
                }catch(Exception $e){
                    $response->error = true;
                    $response->errorMessage = $e->getMessage();
                }
            }
            
            
            try{
                
                 $cate = $this->database->fetchAll("select * from {$this->tp}shop_categories ");
                $response->category[$entry->category_id] = array('' => $this->translate('-Select Shop Category-'));
                foreach($cate as $entry)
                    {
                $response->category[$entry->category_id] = stripslashes($entry->category_name);
                    }

                // $cities = $this->database->fetchAll("select * from {$this->tp}countries  order by country_name asc");
                // $response->cities[$entry->location_id] = array('' => $this->translate('-- Select A Country / Town --'));
                // foreach($cities as $entry)
                //     {
                // $response->cities[$entry->id] = stripslashes($entry->country_name);
                //     }
                // $response->locations = array('' => $this->translate('-- Select City/Location --'));
                // $selectedCity = ((isset($this->getRequest()->res_city) and Zend_Validate::is($this->getRequest()->res_city, 'NotEmpty')) ? $this->getRequest()->res_city : null);
                // if($selectedCity != null)
                // { 
                //     $locations = $this->database->fetchAll("select * from {$this->tp}locations where location_city = ? order by location_name asc", $selectedCity);
                //     $response->locations = array();
                //     foreach($locations as $entry)
                //     {
                //         $response->locations[$entry->location_id] = stripslashes($entry->location_name);
                //     }
                // }
                // else
                // {
                    
                //  $locations = $this->database->fetchAll("select * from {$this->tp}locations  where location_city = ?   order by location_name asc",5656);
                //     $response->locations = array();
                //     foreach($locations as $entry)
                //     {
                //         $response->locations[$entry->location_id] = stripslashes($entry->location_name);
                //     }   
                    
                    
                // }
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $this->translate('Failed to load location details.');
            }

            $response->test=$this->test;
            $response->count=$this->count;
            $this->view->sidebar_menu_item = 'restaurants';
            $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Add New Shop').' | ', 'PREPEND');
        }
        
        /*******
         * Editing The Restaurant Profile
         ******/
        public function editAction()
        {
            
          $response             =    new stdClass;
          $response->data       =    null;
          $response->error      =    false;
          $respose->errormessage=    null;
          $id                   =   (int)$this->getRequest()->edit_id;
         
          
          
          
           if($this->getRequest()->isPost()){
           {
               
           try
           {
              if( //!Zend_Validate::is($this->getRequest()->res_name, 'NotEmpty') ||
                   !Zend_Validate::is($this->getRequest()->res_name_ar, 'NotEmpty') ||
                      //  !Zend_Validate::is($this->getRequest()->res_delivery_fees, 'NotEmpty') ||
                       // !Zend_Validate::is($this->getRequest()->res_avg_delivery_time, 'NotEmpty') ||
                      //  !Zend_Validate::is($this->getRequest()->res_address, 'NotEmpty') ||
                        //!Zend_Validate::is($this->getRequest()->res_city, 'NotEmpty') ||
                        !Zend_Validate::is($this->getRequest()->res_admin_name, 'NotEmpty')
                       // || !Zend_Validate::is($this->getRequest()->open_time, 'NotEmpty')
                       // || !Zend_Validate::is($this->getRequest()->close_time, 'NotEmpty') 
                    )
                    throw new Exception($this->translate('Mandatory fields are found empty.'));
                    if(Zend_Validate::is($this->getRequest()->res_contact, 'NotEmpty') and !Zend_Validate::is($this->getRequest()->res_contact, 'EmailAddress', array(Zend_Validate_Hostname::ALLOW_DNS)))
                        throw new Exception($this->translate('Invalid contact email address.'));
                   if(Zend_Validate::is($this->getRequest()->res_admin_email, 'NotEmpty') and !Zend_Validate::is($this->getRequest()->res_contact, 'EmailAddress', array(Zend_Validate_Hostname::ALLOW_DNS)))
                        throw new Exception($this->translate('Invalid contact email address.'));
                  /*   if(!is_numeric($this->getRequest()->res_delivery_fees) || $this->getRequest()->res_delivery_fees < 0)
                        throw new Exception($this->translate('Invalid delivery fees, must be numeric and not less than 0.')); */
                   /*  if(!is_numeric($this->getRequest()->res_avg_delivery_time) || $this->getRequest()->res_avg_delivery_time <= 0)
                        throw new Exception($this->translate('Average delivery time must be numeric, and greater than 0.'));  */
                     if($this->getRequest()->res_password !=$this->getRequest()->res_con_pass)
                    throw new Exception($this->translate('confirm password does not match with password'));
                    
                    
                    
                    /****
                     * updating delivery point  data
                     */
                    // $data = array(
                    //             'restaurant_display_id' => $this->getHelper('RestaurantSlug')->generateSlug($this->getRequest()->res_name),
                    //             'restaurant_name' => trim($this->getRequest()->res_name),
                    //             'restaurant_description' => trim($this->getRequest()->res_description),
                    //             'contact_email' => $this->getRequest()->res_contact,
                    //             'added_on'      => new Zend_Db_Expr('now()'),
                    //             'is_verified'   => true,
                    //             'is_active'     => true,
                    //             'close_time'=>$this->getRequest()->close_time,
                    //             'delivery_fees' => $this->getRequest()->res_delivery_fees,
                    //             'average_delivery_time' => $this->getRequest()->res_avg_delivery_time
                                
                    //         );
                   
                           
                   $queryStr1 = "select * from {$this->tp}restaurants  where restaurant_id=$id";
                $response1->restaurants->list = $this->database->fetchAll($queryStr1);
           
            $array =array($response1->restaurants->list);
      
            foreach($array as $array1){
                $var = $array1;
                
                foreach($var as $arr_var){
                        $closing_time = $arr_var->close_time;

                }
            
            }
                    $dt = new DateTime();
                    $current_time = date('H:i:s');
                    $check_open_time = 1;
                    $check_open_time1 = 1;

                    if($closing_time <$current_time){

                    $data = array(
                                'restaurant_display_id' => $this->getHelper('RestaurantSlug')->generateSlug($this->getRequest()->res_name),
                               // 'restaurant_name' => trim($this->getRequest()->res_name),
                                'restaurant_name_ar' => trim($this->getRequest()->res_name_ar),
                               // 'restaurant_description' => trim($this->getRequest()->res_description),
                                'restaurant_description_ar' => trim($this->getRequest()->res_description_ar),
                                'contact_email' => $this->getRequest()->res_contact,
                                'added_on'      => new Zend_Db_Expr('now()'),
                                'is_verified'   => true,
                                'is_active'     => true,
                              //  'payment_method'=>$this->getRequest()->paymentmethod,
                               // 'shop_holiday'=>$this->getRequest()->shop_holiday,
                               // 'close_time'    =>$this->getRequest()->close_time,
                               // 'delivery_fees' => $this->getRequest()->res_delivery_fees,
                                //'average_delivery_time' => $this->getRequest()->res_avg_delivery_time
                               
                                
                            );
                   }
                   else{
                                $data = array(
                                'restaurant_display_id' => $this->getHelper('RestaurantSlug')->generateSlug($this->getRequest()->res_name),
                               // 'restaurant_name' => trim($this->getRequest()->res_name),
                                'restaurant_name_ar' => trim($this->getRequest()->res_name_ar),
                                //'restaurant_description' => trim($this->getRequest()->res_description),
                                'restaurant_description_ar' => trim($this->getRequest()->res_description_ar),
                                'contact_email' => $this->getRequest()->res_contact,
                                'added_on'      => new Zend_Db_Expr('now()'),
                                'is_verified'   => true,
                                'is_active'     => true,
                                
                               // 'close_time'    =>$this->getRequest()->close_time,
                               // 'delivery_fees' => $this->getRequest()->res_delivery_fees,
                               // 'average_delivery_time' => $this->getRequest()->res_avg_delivery_time
                               
                                
                            );


                   }
               
            
                      if($_FILES['res_icon']['name'] !='' )
                         {
                           
                            $data['logo']   = $this->G_Upload_file();      
                         
                         
                         
                         }
                         
                         
       
                    $this->database->update($this->tp.'restaurants', $data, 'restaurant_id ='.$id);
                    
                    /****
                     * 
                     * updating delivery point adress
                     */
                     $data = array(
                                
                                //'location_id' => $this->getRequest()->res_location,
                               // 'area_id' => $this->getRequest()->res_area,
                               // 'country_id'=>$this->getRequest()->res_city,
                               // 'address' => $this->getRequest()->res_address,
                                'address_ar' => $this->getRequest()->res_address_ar, 
                                'category' => $this->getRequest()->category, 
                                //'latitude'=> $this->getRequest()->res_latitude,
                               // 'open_time'=>$this->getRequest()->open_time,
                              //  'close_time'=>$this->getRequest()->close_time,
                               // 'longitude'=>$this->getRequest()->res_longitude
                            ); 
                            
                            $this->database->update($this->tp.'restaurant_address', $data, 'restaurant_id ='.$id);
                    


                     $this->database->update(
                            $this->tp.'restaurant_setting',
                            array(
                                'deliver_time'=>$this->getRequest()->res_avg_delivery_time,
                               // 'open_time'=>$this->getRequest()->open_time,
                                //'close_time'=>$this->getRequest()->close_time,
                                
                                'delivery_charges'=>$this->getRequest()->res_delivery_fees),
                                'res_id ='.$id    
                        );
                    

                    if($this->getRequest()->res_password !=''):
                    
                    $values = array(
                                
                                'user_name' => $this->getRequest()->res_admin_name,
                                'user_email' => $this->getRequest()->res_admin_email,
                                'contact_number'=> $this->getRequest()->res_admin_phone,
                                'account_password'=>$this->getRequest()->res_password
                                
                            );
                    else:
                    $values = array(
                               
                                'user_name' => $this->getRequest()->res_admin_name,
                                'user_email' => $this->getRequest()->res_admin_email,
                                'contact_number'=> $this->getRequest()->res_admin_phone
        
                            );
                    endif;
                    
                    $this->database->update($this->tp.'restaurant_admin_users', $values, 'restaurant_id ='.$id);
                    $response->success = true;                        
                    }
                    catch(Exception $e)
                    {
                        throw new Exception($this->translate($e->getMessage()));
                    }
                
           }
                    
                    
                    
           }
          
          
          try
          {
              $cate = $this->database->fetchAll("select * from {$this->tp}shop_categories ");
                $response->category[$entry->category_id] = array('' => $this->translate('-Select Shop Category-'));
                foreach($cate as $entry)
                    {
                $response->category[$entry->category_id] = stripslashes($entry->category_name);
                    }
  

           $cities = $this->database->fetchAll("select * from {$this->tp}countries  order by country_name asc");
           $response->cities[$entry->location_id] = array('' => $this->translate('-- Select A Country / Town --'));
            foreach($cities as $entry)
             {
            $response->cities[$entry->id] = stripslashes($entry->country_name);
             }  
           
           $response->data    = $this->C_Helper->G_Get_Data('select * from '.$this->tp.'restaurants where restaurant_id='.$id);          
          
           $response->address = $this->C_Helper->G_Get_Data('select * from '.$this->tp.'restaurant_address where restaurant_id='.$response->data[0]->restaurant_id);
           
           $response->admindata = $this->C_Helper->G_Get_Data('select * from '.$this->tp.'restaurant_admin_users where restaurant_id='.$response->data[0]->restaurant_id);
           

         
           $selectedCity = ((isset($this->getRequest()->res_city) and Zend_Validate::is($this->getRequest()->res_city, 'NotEmpty')) ? $this->getRequest()->res_city : $response->address[0]->location_id);
           
           $locations = $this->database->fetchAll("select * from {$this->tp}locations  order by location_name asc");
           
           $response->locations = array();
           foreach($locations as $entry)
           {
            $response->locations[$entry->location_id] = stripslashes($entry->location_name);
           }




            $selectedarea = ((isset($this->getRequest()->res_location) and Zend_Validate::is($this->getRequest()->res_location, 'NotEmpty')) ? $this->getRequest()->res_location : $response->address[0]->area_id);
           
           $area = $this->database->fetchAll("select * from {$this->tp}area  order by area_name asc");
           
           $response->area = array();
           foreach($area as $entry)
           {
            $response->area[$entry->area_id] = stripslashes($entry->area_name);
           }

       

            
           }
          catch (Exception $e)
          {
              $response->errormessage = $e->getMessage();      
              $response->error        = true;
                           
          }
         
            $response->test   = $this->test;
            $response->count  = $this->count;
            $this->view->sidebar_menu_item = 'restaurants';
            
            $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Edit Shop').' | ', 'PREPEND');
             
            
        }
        
        /****
         * 
         * function to load the loction by setting it 
         */
         public function ajaxloadlocationAction(){
            $response = new stdclass;
            $response->error = false;
            
            
            
            try{
                
                $response->locations = $this->C_Helper->G_Get_Data("select * from {$this->tp}locations where location_city =".$this->getRequest()->city);
                    
                }
                catch(Exception $e)
                {
                $response->error = true;
            }
            $this->view->assign((array)$response);        
            $this->getHelper('Layout')->disableLayout();
            
        }


         public function ajaxloadareaAction(){
            $response = new stdclass;
            $response->error = false;
            
            
            
            try{
                
                $response->city = $this->C_Helper->G_Get_Data("select * from {$this->tp}area where location_id =".$this->getRequest()->city);
                    
                }
                catch(Exception $e)
                {
                $response->error = true;
            }
            $this->view->assign((array)$response);        
            $this->getHelper('Layout')->disableLayout();
            
        }
        
         /********
         * 
         * commonfunction to upload the file
         */
        
        public function G_Upload_file()
        {
            
         $adapter = new Zend_File_Transfer_Adapter_Http();
         $adapter->addFilter('Rename', array('target' => './uploads/'.time().'.jpg',
                 'overwrite' => true));
            
         if (!$adapter->receive()) 
                {
                
                  return $messages = array();
                 
                
                }
            return $adapter->getFileName(null,false); 
        }



        public function updatetimingAction()
         {  

            $response = new stdclass;
            $response->error = false;
            $response->time = date('H:i:s');
            $status = 0;
            try{
                
                $setting = $this->C_Helper->G_Get_Data("select * from {$this->tp}restaurants");


                if($setting)
                   {
                        foreach ($setting as $key => $resturant) {

                            $queryStr1 = "select * from {$this->tp}restaurant_setting where res_id = $resturant->restaurant_id";
                            $res= $this->database->fetchRow($queryStr1);
                             if($this->check_date_is_within_range($res->open_time, $res->close_time, $response->time)){
                             
                             if($resturant->is_open == 0 && $res->auto_open_close==1)
                                 {
                               $this->database->update(
                                        $this->tp.'restaurants',
                                        array('is_open' => 1),
                                        'restaurant_id = '.$resturant->restaurant_id
                                    );
                                  $status =1;
                                    }
                                 }   
                                 else {

                                   if($resturant->is_open == 1 && $res->auto_open_close==1)
                                     {   
                                   $this->database->update(
                                        $this->tp.'restaurants',
                                        array('is_open' => 0),
                                        'restaurant_id = '.$resturant->restaurant_id
                                    );
                                    $status = 1; 
                                   }
                              }                      
                        }
                  }
                    
                }
                catch(Exception $e)
                {
                $response->error = true;
                }

                echo $status;
                die;
    } 


    public function check_date_is_within_range($start_date, $end_date, $todays_date)
    {

      $start_timestamp = strtotime($start_date);
      $end_timestamp = strtotime($end_date);
      $today_timestamp = strtotime($todays_date);
   
  return (($today_timestamp >= $start_timestamp) && ($today_timestamp <= $end_timestamp));

    }
 /////////////////////////////////////////////////////////////
        public function verifyAction()
        {
           
                                    $link = "http://deliveryapp.demodemo.ga/restaurant/admin/login";
                                    $id = $this->getRequest()->getParam('user_id');                                       
                                    $this->database->update(
                                    $this->tp.'restaurant_admin_users',
                                    array('status' => '1'),
                                    'restaurant_id ='.$id);   

                                    $email = $this->getRequest()->getParam('email');  
                                    $data  = $this->C_Helper->G_Get_Data("Select `account_password` from {$this->tp}restaurant_admin_users where restaurant_id = $id");
                                  //  print_r($data); die;
                                    $pswd = $data[0]->account_password;
                                    
                            try 
                              {                                
                                    $config = array(
                                    'auth' => 'login',
                                    'username' => 'mail@deliveryapp.demodemo.ga',
                                    'password' => 'netone@108',
                                    'port'     => 25);   

                                    $transport = new Zend_Mail_Transport_Smtp('localhost', $config);                                     
                                    $mail = new Zend_Mail();
                                    $mail->setBodyHtml('<p>Your Shop account is activated successfully.</p><p>You can login with this credentials:</p><p>Username: '.$email.'</p><p>Password: '.$pswd.'</p>'.$link);
                                    $mail->setFrom('mail@deliveryapp.demodemo.ga', 'Home Today');
                                    $mail->addTo($email, 'Some Recipient');
                                    $mail->setSubject('Activation Successfully');
                                    $mail->send($transport);
                               }
                                  catch (Exception $ex)
                                   {                
                                   }
                       
               $this->_redirect("/admin/restaurant/pendingrequest");           
        }
     /////////////////////////////////////////////////////////////////////
     public function deleteAction()
        {            
            $id = $this->getRequest()->getParam('user_id');
             $email = $this->getRequest()->getParam('email');
          try 
            {
                $this->database->delete($this->tp.'restaurants', array(
                    'restaurant_id = ?' => $id                    
                ));   
                  $this->database->delete($this->tp.'restaurant_address', array(
                    'restaurant_id = ?' => $id                    
                )); 
                    $this->database->delete($this->tp.'restaurant_admin_users', array(
                    'restaurant_id = ?' => $id                    
                )); 
                      $this->database->delete($this->tp.'restaurant_setting', array(
                    'res_id = ?' => $id                    
                )); 

                                    $config = array(
                                    'auth' => 'login',
                                    'username' => 'mail@deliveryapp.demodemo.ga',
                                    'password' => 'netone@108',
                                    'port'     => 25);    

                                    $transport = new Zend_Mail_Transport_Smtp('localhost', $config);                                     
                                    $mail = new Zend_Mail();
                                    $mail->setBodyHtml('<p>Your request has been deleted by the Administration. Please contact the administration. </p><p>Thanks.</p>');
                                    $mail->setFrom('mail@deliveryapp.demodemo.ga', 'Home Today');
                                    $mail->addTo($email, 'Some Recipient');
                                    $mail->setSubject('Shop Request Deleted');
                                    $mail->send($transport);                                                    
            }
          catch (Exception $ex)
           {                
           }
             $this->_redirect("/admin/restaurant/pendingrequest");              
        } 
    ///////////////////////////////////////////////////////////////////////////

    }
    
    ?>