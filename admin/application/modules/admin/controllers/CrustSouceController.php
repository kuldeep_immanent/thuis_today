 <?php

   
  
    class Admin_CrustController extends Zend_Controller_Action
    {
      
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_helper;
        public $model;
        
        public function init(){
            //initializing resources
            $bootstrap       = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database   = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig  = $bootstrap->getResource('AppConfig');
            $this->config     = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->_C_helper  = $this->getHelper('Custom');
            //$this->model      = new Admin_Model_Crusts();
        
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            $response = new stdclass;  
        	$response->test=$this->test;
		    $response->count=$this->count;
            $this->view->headTitle($this->translate('Shop Admin Panel').' | Kebapiste');
        }
        
        public function preDispatch(){
        	
         if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);	
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
        }
        
        public function postDispatch(){
            
        }
        
       
        private function checkLogin(){
            if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
        }
        
       
       
               
         public function translate($message)
                 {
            return $this->translator->translate($message);
        }

      /****
      function to  get the Crusts 
      *****/
        public function crusts()
        {




        }
        /*****
        * function to get the souces  
        *****/
        public function souces()
        {



        }
      /****
      * ajax function to addedit souces
      *****/
         public function ajaxaddeditsouce()
         {



         }
        /*****
        * function to addedit  crusts
        ******/
         public function ajaxaddeditcrusts()
         {




         }
       

       
}



    ?>