 <?php

    class Admin_NotifyController extends Zend_Controller_Action
    {
      
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_helper;
        public $model;
        
        public function init(){
			
			
            //initializing resources
			$bootstrap        = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->appConfig  = $bootstrap->getResource('AppConfig');
            $this->config     = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->model      = new Admin_Model_Notify();
        
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            $response = new stdclass;  
        	$response->test=$this->test;
		    $response->count=$this->count;
            $this->view->headTitle($this->translate('Shop Admin Panel').' |');
        }
        
        public function preDispatch(){
        	
           $this->checkLogin();
        	
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
            $this->view->sidebar_menu_item = "notify";
        }
        
        public function postDispatch(){
            
        }
        
       
        private function checkLogin(){
            if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
        }
               
         public function translate($message)
              {
            return $this->translator->translate($message);
        }

     

        /***
        *Function to add the data in da 
        ****/

        public function addeditnotifyAction()
        {
			
             $reponse          = new stdclass;
             $response->error  = false;
             $response->errormessage = null;
             $response->success = false;       
             try
			 {
             if($this->getRequest()->isPost())
             {

                    $data = array('notifytext'=>$this->getRequest()->res_notify_text);

                    if(isset($this->getRequest()->id))
                    {
                        
                          $this->model->edit($data,'id='.$this->getRequest()->id);
                          $response->success = true;     
                    }
                    else
                    {
                     
                             $this->model->add($data);
                             $response->success = true;   
                    }


             }
  
              

                $response->data = $this->model->fetchdata();
                
               

            
           }
           catch(Exception $e)
           {
               $response->error  = true;
               $response->success = false;   
               $response->errormessage = $e->getMessage();


           }

          
           
            $this->view->assign((array)$response);

        }     

       
}



    ?>