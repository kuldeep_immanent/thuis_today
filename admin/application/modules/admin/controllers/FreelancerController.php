<?php print_r($this->data[0]);
    include('CommonController.php');
    class Admin_FreelancerController extends Zend_Controller_Action
	{
        
        public $database;
        public $tp; //Table prefix
        public $config;
        public $appConfig;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_C_helper;
        public $checkhelper;
        public function init(){
            //initializing resources
            $bootstrap        = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database   = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp         = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig  = $bootstrap->getResource('AppConfig');
            $this->config     = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test       = commonnotify::Notification();
            $this->count      = commonnotify::total_count();
            $this->_C_helper  = $this->getHelper('Custom');
          
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            $this->view->headTitle('Admin Panel | User Management System');
        }
        
        public function preDispatch(){
			if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
            $this->view->request            = $this->getRequest();
            $this->view->session            = $this->session;
            $this->view->config             = $this->config;
            $this->view->appConfig          = $this->appConfig;
			$this->view->sidebar_menu_item  = 'User';
        }
        
        public function translate($message){
            return $this->translator->translate($message);
        }
        
        public function postDispatch(){
            //adding few variables to view
            
        }

        /*********
         * function  to get the user lists
         */
        public function indexAction()
         {
            
           $response         = new stdClass();
           $respose->error   = false;
           $response->message = null;
           
           try
           {
               
               
             $response->data  = $this->_C_helper->G_Get_Data("Select * from {$this->tp}freelancer where status = 1");
             $response->count = count($response->data);
               
               
           } catch (Exception $ex) {
            
                $response->message =$ex->getMessage();
               
           }
           
            
            $paginator = Zend_Paginator::factory($response->data);
            $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
             $paginator->setDefaultItemCountPerPage(10000);
            $this->view->paginator = $paginator;
            $this->view->assign((array)$response);       
            $this->view->headTitle($this->translate('FreelancersList').' | ', 'PREPEND');        
            
        }

   /********** Display the order list*******/
       public function orderlistAction()
        {
           $response         = new stdClass();
           $respose->error   = false;
           $response->message = null;
           
           try
           {
                $shopid = $this->getRequest()->getParam('shopid');    
               
             $response->data  = $this->_C_helper->G_Get_Data("Select customer_order.*, b.*, c.name as name from customer_order LEFT JOIN {$this->tp}restaurants as b on customer_order.shopid = b.restaurant_id LEFT JOIN {$this->tp}users as c on customer_order.user_id = c.id where customer_order.shopid = $shopid and customer_order.admin_status = 0");
             //////////
             $response->data2  = $this->_C_helper->G_Get_Data("Select * from {$this->tp}invoice where shopid = $shopid and status = 0");
             ////////////////////

             $response->data3  = $this->_C_helper->G_Get_Data("Select * from {$this->tp}invoice where shopid = $shopid and status = 1");
             //$this->gethighlowvat($response->data);
            
             $response->count = count($response->data);
               $response->data1  = $this->_C_helper->G_Get_Data("Select * from {$this->tp}restaurants as a LEFT JOIN {$this->tp}restaurant_admin_users as b on a.restaurant_id = b.restaurant_id LEFT JOIN {$this->tp}restaurant_address as c on a.restaurant_id = c.restaurant_id where a.restaurant_id = $shopid");                
           } catch (Exception $ex) {
            
                $response->message =$ex->getMessage();
               
           }
           
            
            $paginator = Zend_Paginator::factory($response->data);
            $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
             $paginator->setDefaultItemCountPerPage(10000);
            $this->view->paginator = $paginator;
            $this->view->assign((array)$response);       
            $this->view->headTitle($this->translate('Orderslist').' | ', 'PREPEND');    
        }
     

        // public function newrequestAction(){

        //    $response         = new stdClass();
        //    $respose->error   = false;
        //    $response->message = null;
           
        //    try
        //    {
               
               
        //     $data = $response->data  = $this->_C_helper->G_Get_Data("Select * from {$this->tp}freelancer_request where status = 0");
        //      $response->count = count($response->data);
               
               
        //    } catch (Exception $ex) {            
        //         $response->message =$ex->getMessage();
               
        //    }
           
            
        //     $paginator = Zend_Paginator::factory($response->data);
        //     $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
        //      $paginator->setDefaultItemCountPerPage(10000);
        //     $this->view->paginator = $paginator;
        //     $this->view->assign((array)$response);       
        //     $this->view->headTitle($this->translate('NewRequest').' | ', 'PREPEND');    
        // }
         public function pendingrequestAction(){
           $response         = new stdClass();
           $respose->error   = false;
           $response->message = null;           
           try
           {   
             $response->data  = $this->_C_helper->G_Get_Data("Select * from {$this->tp}freelancer where status = 2");
             $response->count = count($response->data);    
           } catch (Exception $ex) {
            
                $response->message =$ex->getMessage();
               
           }
                       
            $paginator = Zend_Paginator::factory($response->data);
            $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
             $paginator->setDefaultItemCountPerPage(10000);
            $this->view->paginator = $paginator;
            $this->view->assign((array)$response);       
            $this->view->headTitle($this->translate('PendingRequest').' | ', 'PREPEND');    
        }
        public function updatestatusAction(){
           $response         = new stdClass();
           $respose->error   = false;
           $response->message = null;   
           $orderid = $this->getRequest()->order_id;    
           $shopid = $this->getRequest()->shopid;     
           try
           {   
         $response->data  = $this->_C_helper->G_Get_Data("UPDATE `fos_invoice` SET  `status` =  '1' WHERE `id` = $orderid");
             $response->count = count($response->data);    
           } catch (Exception $ex) {
            
                $response->message =$ex->getMessage();
               
           }
            $this->_redirect("/admin/freelancer/orderlist?shopid=".$shopid);   
        }
        // public function approvalAction()
        // {
        //     $id = $this->getRequest()->getParam('user_id');
        //     $data = $this->_C_helper->G_Get_Data("Select * from {$this->tp}freelancer_request where id = '$id'");
        //     $email = $data[0]->email;
        //     $user_token =  $this->random_string(50);
        //     $link = 'http://deliveryapp.demodemo.ga/web/index.php/Freelancerregistration?token='.$user_token.'&id='.$id;

        //     $mail = new Zend_Mail();
        //     $mail->setBodyHtml('<p>Here is a your registration link. Please click to registered as a freelancer. </p><p>Thanks.</p>'.$link);
        //     $mail->setFrom('HomeToday@testing.com', 'Home Today');
        //     $mail->addTo($email, 'Some Recipient');
        //     $mail->setSubject('Registration Link');
        //     $mail->send();   

        //            $this->database->update(
        //               $this->tp.'freelancer_request',
        //               array('token' => $user_token, 'status' => '1'),
        //               'id ='.$id);

        //     $data1 = array('email'=> $email, 'status'=> '2' ); 
        //      $this->database->insert(
        //                     $this->tp.'freelancer',
        //                     $data1
        //                 );           
        //        $this->_redirect("/admin/Freelancer/newrequest");           
        // }
////////////////////////////////////////////////////////////
       public function vatAction()
          {                    
           $response         = new stdClass();
           $respose->error   = false;
           $response->message = null;           
           try
           {   
             $response->data  = $this->_C_helper->G_Get_Data("Select * from {$this->tp}vat");
             $response->count = count($response->data);    
           } catch (Exception $ex) {
            
                $response->message =$ex->getMessage();
               
           }
                       
            $paginator = Zend_Paginator::factory($response->data);
            $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
             $paginator->setDefaultItemCountPerPage(10000);
            $this->view->paginator = $paginator;
            $this->view->assign((array)$response);       
            $this->view->headTitle($this->translate('Vat list').' | ', 'PREPEND');    
        
        }

           public function addvatAction(){
                if($this->getRequest()->isPost())
                {               
                   try
                   {
                        $data['vat'] = str_replace('.',',',$this->getRequest()->vat);  
                        $data['type'] = $this->getRequest()->type;     
                        $this->database->insert(
                                    $this->tp.'vat',
                                    $data
                             ); 
                        $this->_redirect("/admin/Freelancer/vat");  
                   }
                   catch(Exception $e)
                    {
                        throw new Exception($this->translate($e->getMessage()));
                    }
               }

        }
               public function editAction(){
                   $id = $this->getRequest()->getParam('id');   
                if($this->getRequest()->isPost())
                {                           
                   try
                   {
                        $data['vat'] = str_replace('.',',',$this->getRequest()->vat);  
                        $data['type'] = $this->getRequest()->type;     
                        $this->database->update(
                                    $this->tp.'vat',
                                    $data,
                                    'id ='.$id
                             ); 
                        $this->_redirect("/admin/Freelancer/vat");  
                   }
                   catch(Exception $e)
                    {
                        throw new Exception($this->translate($e->getMessage()));
                    }
               }
                try
                   {   
                     $response->data  = $this->_C_helper->G_Get_Data("Select * from {$this->tp}vat where id = '$id'");
                     $response->count = count($response->data);    
                   } catch (Exception $ex) {
                    
                        $response->message =$ex->getMessage();
                       
                   }
              $paginator = Zend_Paginator::factory($response->data);
            $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
             $paginator->setDefaultItemCountPerPage(10000);
            $this->view->paginator = $paginator;
            $this->view->assign((array)$response);       
            $this->view->headTitle($this->translate('Vat Edit').' | ', 'PREPEND');    

        }
        /////////////////////////////////////////////////////////////
        public function verifyAction()
        {
            $link = "http://deliveryapp.demodemo.ga/freelancer/";
                            $id = $this->getRequest()->getParam('user_id');           
                            $this->database->update(
                                    $this->tp.'freelancer',
                                    array('status' => '1'),
                                    'id ='.$id);
                                    $email = $this->getRequest()->getParam('email');  

                                    $data  = $this->_C_helper->G_Get_Data("Select `password` from {$this->tp}freelancer where id = $id");
                                    $pswd = $data[0]->password;
                                   try 
                                    {  
                                      $config = array(
                                    'auth' => 'login',
                                    'username' => 'mail@deliveryapp.demodemo.ga',
                                    'password' => 'netone@108',
                                    'port'     => 25);    

                                    $transport = new Zend_Mail_Transport_Smtp('localhost', $config);                                      
                                    $mail = new Zend_Mail();
                                    $mail->setBodyHtml('<p>Your Freelancer account is activated successfully.</p><p>You can login with this credentials:</p><p>Username: '.$email.'</p><p>Password: '.$pswd.'</p>'.$link);
                                    $mail->setFrom('immanentinfo@demodemo.ga', 'Home Today');
                                    $mail->addTo($email, 'Some Recipient');
                                    $mail->setSubject('Activation Successfully!');
                                    $mail->send($transport);   
                                       }
                                  catch (Exception $ex)
                                   {                
                                   }                         
                       
               $this->_redirect("/admin/Freelancer/pendingrequest");           
        }
        /********
         * 
         * Function to add Users
         */
        public function addAction()
           {
            $response               = new stdClass;
            $response->error        = false;
            $response->errormessage = null;
            $response->success      = false;
            
            
             if($this->getRequest()->isPost())
             {
                  
                 
                 $saved = $this->Save_Edit_User();
                 
                 if($saved->error)
                 {
                     
                  $response->error        = true;
                  $response->errormessage = $saved->errormessage;   
                     
                     
                 }
                 else
                 {
                     
                   $response->success      = true;   
                     
                 }
               
             }
            else
             {
           
                try
                {
                    $cities = $this->database->fetchAll("select * from {$this->tp}locations   order by location_city asc");
                    $response->cities[$entry->location_id] = array('' => $this->translate('-- Select A City / Town --'));
                    foreach($cities as $entry)
                    {
                      $response->cities[$entry->location_id] = stripslashes($entry->location_name);
                    }

                }catch(Exception $e)
                {
                     $response->error        = true;
                     $response->errormessage = false;

                }

            
            }
            $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Add User').' | ', 'PREPEND');
          }
          
          
        /**********************************
         * function to manage entrance fees
         ******************/
        public function entrancefeeAction()
        {
            $admin_id = $this->session->admin->id;
              $response             =    new stdClass;
              $response->data       =    null;
              $response->error      =    false;
              $respose->errormessage=    null;

              if($this->getRequest()->isPost())
                {               
                   try
                   {
                        $entrance = str_replace(".",",",$this->getRequest()->entrance_fee);                       
                        $this->database->update(
                        $this->tp.'admin_users',
                        array('entrancefee' => $entrance),
                        'user_id ='.$admin_id);
                   }
                   catch(Exception $e)
                    {
                        throw new Exception($this->translate($e->getMessage()));
                    }
               }
               try
                   {
                         $response->data = $this->_C_helper->G_Get_Data("Select * from {$this->tp}admin_users where user_id = $admin_id");                    
                   }
                   catch(Exception $e)
                    {
                        throw new Exception($this->translate($e->getMessage()));
                    }
                        $paginator = Zend_Paginator::factory($response->data);
                        $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
                        $paginator->setDefaultItemCountPerPage(10000);
                        $this->view->paginator = $paginator;
                        $this->view->assign((array)$response);       
                        $this->view->headTitle($this->translate('Entrance Fee').' | ', 'PREPEND');  
        }
        
        
        
        /********
         * 
         * Function to save the data 
         * 
         */
        
        private Function Save_Edit_User()
        {
             $postresult               = new stdClass;
             $postresult->error        = false;
             $postresult->errormessage = null;
            
                 
                try{   
                       if(isset($_GET['user_id'])){
                       if( !Zend_Validate::is($this->getRequest()->FirstName, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->LastName, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->Email, 'NotEmpty') 
                                
                               )
                                throw new Exception($this->translate('Mandatory fields are found empty.'));
                       }
                       else
                        {
                          if( !Zend_Validate::is($this->getRequest()->FirstName, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->LastName, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->Email, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->Password, 'NotEmpty')||

                                !Zend_Validate::is($this->getRequest()->cpassword, 'NotEmpty')
                               )
                                throw new Exception($this->translate('Mandatory fields are found empty.'));  
                       }                      
                       if(isset($_GET['user_id']))
                       if($this->getRequest()->Password !=$this->getRequest()->cpassword)  
                                throw new Exception($this->translate('Confirm password is not matching '));
                
                 
                   $data = $this->_C_helper->_Post_Filtered_Data($this->_request->getPost());        
                       
                      
                       if(isset($_GET['user_id']))
                       {
                               
                            $this->database->update(
                                    $this->tp.'users',
                                    $data, 
                                    'ID ='.$this->getRequest()->getParam('user_id'));    
                           
                       }
                       else
                       {
                           
                            $this->database->insert(
                                    $this->tp.'users',
                                    $data
                             ); 
                       
                       }
                       
                       
                       
                }
                catch (Exception $e)
                {

                   $postresult->error        = true;
                   $postresult->errormessage = $e->getMessage(); 


                }
                
                
                return $postresult;
             
          
        }
        
        /*******
         * 
         * function to delete the user
         */
        public function deleteAction()
        {            
            $id = $this->getRequest()->getParam('user_id');
             $email = $this->getRequest()->getParam('email');
          try 
            {
                $this->database->delete($this->tp.'freelancer', array(
                    'id = ?' => $id                    
                ));   
                                      $config = array(
                                    'auth' => 'login',
                                    'username' => 'mail@deliveryapp.demodemo.ga',
                                    'password' => 'netone@108',
                                    'port'     => 25);    

                                    $transport = new Zend_Mail_Transport_Smtp('localhost', $config);                                        
                                    $mail = new Zend_Mail();
                                    $mail->setBodyHtml('<p>Your request has been deleted by the Administration. Please try to register again or contact the administration. </p><p>Thanks.</p>');
                                    $mail->setFrom('immanentinfo@demodemo.ga', 'Home Today');
                                    $mail->addTo($email, 'Some Recipient');
                                    $mail->setSubject('Request Deleted');
                                    $mail->send($transport);                                                   
            }
          catch (Exception $ex)
           {                
           }
             $this->_redirect("/admin/freelancer/pendingrequest");              
        }  


        /*******
         * 
         * function to delete the user
         */
        public function deletevatAction()
        {            
            $id = $this->getRequest()->getParam('id');
          try 
            {
                $this->database->delete($this->tp.'vat', array(
                    'id = ?' => $id                    
                ));  
            }
          catch (Exception $ex)
           {                
           }
             $this->_redirect("/admin/freelancer/vat");              
        } 

     // public function deleterequestAction()
     //    {            
     //        $id = $this->getRequest()->getParam('user_id');
     //      try 
     //        {
     //            $this->database->delete($this->tp.'freelancer_request', array(
     //                'id = ?' => $id                    
     //            ));                
     //        }
     //      catch (Exception $ex)
     //       {                
     //       }
     //         $this->_redirect("/admin/freelancer/newrequest");              
     //    }

     // public function random_string($length) 
     //   {
     //        $key = '';
     //        $keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
     //        for ($i = 0; $i < $length; $i++) 
     //        { $key .= $keys[array_rand($keys)]; }  
     //        return $key;
     //   }  
    }