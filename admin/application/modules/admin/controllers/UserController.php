<?php
    include('CommonController.php');
    class Admin_UserController extends Zend_Controller_Action
	{
        
        public $database;
        public $tp; //Table prefix
        public $config;
        public $appConfig;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_C_helper;
        public $checkhelper;
        public function init(){
            //initializing resources
            $bootstrap        = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database   = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp         = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig  = $bootstrap->getResource('AppConfig');
            $this->config     = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test       = commonnotify::Notification();
            $this->count      = commonnotify::total_count();
            $this->_C_helper  = $this->getHelper('Custom');
          
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            $this->view->headTitle('Admin Panel | User Management System');
        }
        
        public function preDispatch(){
			if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
            $this->view->request            = $this->getRequest();
            $this->view->session            = $this->session;
            $this->view->config             = $this->config;
            $this->view->appConfig          = $this->appConfig;
			$this->view->sidebar_menu_item  = 'User';
        }
        
        public function translate($message){
            return $this->translator->translate($message);
        }
        
        public function postDispatch(){
            //adding few variables to view
            
        }
        
        /*********
         * function  to get the user lists
         */
        public function indexAction()
         {
            
           $response         = new stdClass();
           $respose->error   = false;
           $response->message = null;
           
           try
           {
               
               
             $response->data  = $this->_C_helper->G_Get_Data("Select * from {$this->tp}users ");
             $response->count = count($response->data);
               
               
           } catch (Exception $ex) {
            
                $response->message =$ex->getMessage();
               
           }
           
            
            $paginator = Zend_Paginator::factory($response->data);
            $paginator->setCurrentPageNumber((isset($this->getRequest()->page)?$this->getRequest()->page:1));
             $paginator->setDefaultItemCountPerPage(10000);
            $this->view->paginator = $paginator;
            $this->view->assign((array)$response);       
            $this->view->headTitle($this->translate('UsersList').' | ', 'PREPEND');        
            
        }
        /********
         * 
         * Function to add Users
         */
        public function addAction()
           {
            $response               = new stdClass;
            $response->error        = false;
            $response->errormessage = null;
            $response->success      = false;
            
            
             if($this->getRequest()->isPost())
             {
                  
                 
                 $saved = $this->Save_Edit_User();
                 
                 if($saved->error)
                 {
                     
                  $response->error        = true;
                  $response->errormessage = $saved->errormessage;   
                     
                     
                 }
                 else
                 {
                     
                   $response->success      = true;   
                     
                 }
               
             }
            else
             {
           
                try
                {
                    $cities = $this->database->fetchAll("select * from {$this->tp}locations   order by location_city asc");
                    $response->cities[$entry->location_id] = array('' => $this->translate('-- Select A City / Town --'));
                    foreach($cities as $entry)
                    {
                      $response->cities[$entry->location_id] = stripslashes($entry->location_name);
                    }

                }catch(Exception $e)
                {
                     $response->error        = true;
                     $response->errormessage = false;

                }

            
            }
            $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Add User').' | ', 'PREPEND');
          }
          
          
        /*********
         * function to edit user
         */
        public function editAction()
        {
           
            
            $response               = new stdClass;
            $response->error        = false;
            $response->errormessage = null;
            $response->success      = false;
            
            
             if($this->getRequest()->isPost())
             {
                  
                 
                 $saved = $this->Save_Edit_User();
                 
                 if($saved->error)
                 {
                     
                  $response->error        = true;
                  $response->errormessage = $saved->errormessage;   
                     
                     
                 }
                 else
                 {
                     
                   $response->success      = true;   
                     
                 }
               
             } 
          try
              {
              
                    $id               = $this->getRequest()->getParam('user_id');
                     
                    $response->data   = $this->_C_helper->G_Get_Data("Select * from {$this->tp}users where  ID={$id}");           
                  
                    $cities = $this->database->fetchAll("select * from {$this->tp}locations   order by location_city asc");
                    $response->cities[$entry->location_id] = array('' => $this->translate('-- Select A City / Town --'));
                    foreach($cities as $entry)
                    {
                      $response->cities[$entry->location_id] = stripslashes($entry->location_name);
                    }

                }
                catch(Exception $e)
                {
                     $response->error        = true;
                     $response->errormessage = false;

                }
            
             $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Edit User').' | ', 'PREPEND');
        }
        
        
        
        /********
         * 
         * Function to save the data 
         * 
         */
        
        private Function Save_Edit_User()
        {
             $postresult               = new stdClass;
             $postresult->error        = false;
             $postresult->errormessage = null;
            
                 
                try{   
                       if(isset($_GET['user_id'])){
                       if( !Zend_Validate::is($this->getRequest()->FirstName, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->LastName, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->Email, 'NotEmpty') 
                                
                               )
                                throw new Exception($this->translate('Mandatory fields are found empty.'));
                       }
                       else
                        {
                          if( !Zend_Validate::is($this->getRequest()->FirstName, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->LastName, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->Email, 'NotEmpty') ||
                                !Zend_Validate::is($this->getRequest()->Password, 'NotEmpty')||

                                !Zend_Validate::is($this->getRequest()->cpassword, 'NotEmpty')
                               )
                                throw new Exception($this->translate('Mandatory fields are found empty.'));  
                       }                      
                       if(isset($_GET['user_id']))
                       if($this->getRequest()->Password !=$this->getRequest()->cpassword)  
                                throw new Exception($this->translate('Confirm password is not matching '));
                
                 
                   $data = $this->_C_helper->_Post_Filtered_Data($this->_request->getPost());        
                       
                      
                       if(isset($_GET['user_id']))
                       {
                               
                            $this->database->update(
                                    $this->tp.'users',
                                    $data, 
                                    'ID ='.$this->getRequest()->getParam('user_id'));    
                           
                       }
                       else
                       {
                           
                            $this->database->insert(
                                    $this->tp.'users',
                                    $data
                             ); 
                       
                       }
                       
                       
                       
                }
                catch (Exception $e)
                {

                   $postresult->error        = true;
                   $postresult->errormessage = $e->getMessage(); 


                }
                
                
                return $postresult;
             
          
        }
        
        /*******
         * 
         * function to delete the user
         */
        public function deleteAction()
        {
            
            $id = $this->getRequest()->getParam('user_id');
            try {
                $this->database->delete($this->tp.'users', array(
                    'ID = ?' => $id
                    
                ));
                
            } catch (Exception $ex) {
                
            }
             $this->_redirect("/admin/User");  
           

            
        }
        
     
       
        
    }