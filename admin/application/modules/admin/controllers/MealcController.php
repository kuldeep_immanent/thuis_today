<?php

    class Admin_mealcController extends Zend_Controller_Action
{
        
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        
        public function init(){
            //initializing resources
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->appConfig = $bootstrap->getResource('AppConfig');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate'); 
            $this->model      = new Admin_Model_Category();
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            $this->view->headTitle('Admin Panel | Meals');
        }
        
        public function preDispatch(){
            //checking session
            if(!$this->session->admin->loggedin)
            $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
            //adding few variables to view
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config  = $this->config;
            $this->view->appConfig = $this->appConfig;
            $this->view->sidebar_menu_item = "mealcategory";
        }
        
        public function postDispatch()
             {
            //adding few variables to view
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
             }
		
       public function translate($message)
        {
            return $this->translator->translate($message);
        }
        /****
         * function 
         */
        public function indexAction()
        {
            $response        = new stdClass;
            $response->error = false;
            $response->errormessage = null;
            
            try 
            {
                
                $response->data = $this->model->fetchdata();
                
            } 
            catch (Exception $ex) 
            {
                $response->error = true;
               $response->errormessage = $ex->getMessage();
            }
            
            
            $this->view->assign((array)$response);
            
        }
        
        /******
         * function to add edit the category 
         */
        public function addeditAction()
        {
            
            
             try 
             {
             if($this->getRequest()->isPost())
             {
 // 'crust_on'=>$this->getRequest()->crust_on,
 //                                    'souce_on'=>$this->getRequest()->souce_on

                    $data = array('category_name'=>$this->getRequest()->category_name,
					              'category_name_ar'=>$this->getRequest()->category_name_ar,
                                  'is_enabled'=>$this->getRequest()->is_enabled
                                   
                                    );

                    if(isset($this->getRequest()->id))
                    {
                        
                          $this->model->edit($data,'category_id='.$this->getRequest()->id);
                          $response->success = true;     
                    }
                    else
                    {
                     
                             $this->model->add($data);
                             $response->success = true;   
                    }


             }
  
             if(isset($_GET['edit']))
             { 

                $response->data = $this->model->fetchrowdata('category_id='.$this->getRequest()->edit);
                
                

             }
           }
           catch(Exception $e)
           {
               $response->error  = true;
               $response->success = false;   
               $response->errormessage = $e->getMessage();


           }
           $this->view->assign((array)$response);  
            
        }
        
         /***
       * function to delete the data 
       *****/
         public function deletedataAction()
         {
           
           if(isset($_GET['delete']))
           {

             try 
             {
              
              $this->database->delete(
                                $this->tp.'restaurant_meals_category',
                                'category_id = '.$this->getRequest()->delete
                            );
          
                
              } 
              catch (Exception $e) 
              {
                $error = $e->getMessage();
              } 
             
           }
              
               $this->_redirect('/admin/mealc');
         }
        
      
        
       
        
    }
    
    ?>

