<?php
include('CommonController.php');
    class Admin_BulkofferController extends Zend_Controller_Action{
        
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_C_Helper;
        public $_m_table ='for_coupons';
        public $_r_table ='fos_users';
        public function init(){
            //initializing resources
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig = $bootstrap->getResource('AppConfig');
            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test       =commonnotify::Notification();
            $this->count     =    commonnotify::total_count();
            $this->_C_Helper = $this->getHelper('Custom');
            $this->_m_table ='for_coupons';
             $this->_r_table ='fos_users';
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            
        	
            $this->view->headTitle($this->translate('Shop Admin Panel').' |');
        }
        
        public function preDispatch(){
			if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
           
        }
        
        public function postDispatch(){
            
        }
        
        private function translate($message){
            return $this->translator->translate($message);
        }
        
        private function checkLogin(){
            if(!$this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'restaurant-admin-login', true);
        }
        
       
        
        public function managedeliveryareasAction(){
            $this->checkLogin();
            
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
            
            if(isset($this->getRequest()->xaction)){
                try{
                    switch($this->getRequest()->xaction){
                        case 'delete': {
                            $this->database->delete(
                                $this->tp . "restaurant_delivery_areas",
                                "restaurant_id = ".$this->getRequest()->restaurant_id." and location_id = ".$this->getRequest()->location_id
                            );
                            break;
                        }
                    }
                }catch(Exception $e){
                    $response->error = true;
                    $response->errorMessage = $e->getMessage();
                }
            }
            
            try{
                $response->deliveryAreas = $this->database->fetchAll("select * from {$this->tp}restaurant_delivery_areas left join {$this->tp}locations on {$this->tp}restaurant_delivery_areas.location_id = {$this->tp}locations.location_id where restaurant_id = ?", $this->session->admin->resid); 
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $this->translate('Failed to load delivery areas. Please re-try again.');
            }
            
            try{
                $response->cities = array('' => $this->translate('-- Select A City / Town --'), 'Istanbul' => 'Istanbul');
                $response->locations = array('' => $this->translate('-- Select A Location / Region --'));
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $this->translate('Failed to load location details.');
            }
            $response->test=$this->test;
		    $response->count=$this->count;
            $this->view->sidebar_menu_item = 'settings';
            $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Delivery Areas').' | ', 'PREPEND');
        }
        
       
        
        
        public function ajaxsaveamountAction(){
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
            
            try{
                //validating fields
                    if(!Zend_Validate::is($this->getRequest()->delivery_amount, 'NotEmpty'))
                        throw new Exception($this->translate('All fields are mandatory.'));
                    if(!is_numeric($this->getRequest()->delivery_amount) || $this->getRequest()->delivery_amount < 0)
                        throw new Exception($this->translate('Delivery amount must be numeric and greater than or equal to 0.'));
                //updating delivery amount
                    try{
                        $this->database->update(
                            $this->tp."restaurant_delivery_areas",
                            array(
                                'minimum_delivery_amount' => $this->getRequest()->delivery_amount
                            ),
                            'restaurant_id = ' . $this->getRequest()->restaurant_id . ' and location_id = ' . $this->getRequest()->location_id
                        );
                        $response->success = true;
                    }catch(Exception $e){
                        throw new Exception($this->translate('Failed to add. Please re-try again.'));
                    }
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Json')->sendJson($response);
        }
        
       
        
      
        
        /*****
         * function the 
         */
        public function indexAction()
        {
            $response = new stdclass;
            $response->error = false;
            $response->errormessage = null;
            $response->success = false;
                   
            try {
                  $current_date  =strtotime(date('Y-m-d'));
                  $codes =  $this->_C_Helper->G_result("Select code,id from {$this->_m_table} where expiry_date>$current_date");
                
                 $response->coupon[0] ='Select Coupon';
                 $start =  1;
                 foreach($codes as $key=>$code)
                 {
                     $response->coupon[$code->id] = $code->code;
                     $start++;
                     
                 }
                    $cities = $this->database->fetchAll("select * from {$this->tp}locations   order by location_city asc");     
                    $response->cities[$entry->location_id] = array('' => $this->translate('-- Select A City / Town --'));
                    foreach($cities as $entry)
                        {
                    $response->cities[$entry->location_id] = stripslashes($entry->location_name);
                        }
                 if($this->getRequest()->isPost())
                        {
          
                           $result = $this->To_Find_The_Filter_People_To_Send_Offer();
              
                           if($result->failure)
                           {
                               throw new Exception($this->translate($result->results[0]->error)); 
                               
                               
                           }
                           else
                           {
                               
                               $response->success = true;
                                       
                               
                           }
                        }      
            } catch (Exception $ex) {
                
                 $response->error = true;
                 $response->success = false;
                 $response->errormessage = $ex->getMessage();
                
                
            }
             
            $this->view->sidebar_menu_item = 'bulkoffer';
            $this->view->assign((array)$response);
              
            
        }
        
        /********
         * Function to Find The People To Get The Offer 
         ********/
        private function To_Find_The_Filter_People_To_Send_Offer()
        {
          
            $coupon_id  = $this->getRequest()->coupon;
            $Area       = $this->getRequest()->Area;
            $ages       = $this->getRequest()->ages_range;
            $money      = $this->getRequest()->money_range;
            $error      = null;
            $result     = true;
        


            $Area      = "Where Cityid= $Area";   
                
                     
            
            if(isset($ages) && !empty($ages)):
               
                    $ages      =   explode(',',$ages);
                   // $ages =  'OR date_of_birth between '.$this->_C_Helper->G_Date_Convert($ages[0]).' and '.$this->_C_Helper->G_Date_Convert($ages[0]);
				   $ages =  'OR date_of_birth between '.($ages[0]).' and '.($ages[1]);
                   
                else:
                    $ages      =  '';
           endif;
            
            
            /*****
             * Finding money ranges
             *****/
            $moneyquery    = $this->To_Get_IDS($this->To_GET_People_Spent_Money(),$money); 
           
            try {
             
               $user_lists = $this->_C_Helper->G_result("Select * from fos_users  $Area  $ages $moneyquery  ");         
              
             
               $coupondetail  = $this->_C_Helper->G_result("Select * from for_coupons where id=$coupon_id");         
                
               foreach($user_lists as $money):
                            
                        if($coupondetail[0]->discount_type=='cash'):
                            $message ="Use this Coupon  to get   " .$coupondetail[0]->discount." SAR cash discount on total cart price <br/> ";
                            $message .="coupon Code:".$coupondetail[0]->code." <br/>";
                            $message .="Expirary Date:".date('Y-m-d',$coupondetail[0]->expiry_date)." <br/>";
                           
                            else:
                            $message  ="Use this Coupon  to get free ".$coupondetail[0]->discount."  on order<br/>";
                            $message .="coupon Code:".$coupondetail[0]->code."<br/>";
                            $message .="Expirary Date:".date('Y-m-d',$coupondetail[0]->expiry_date)." <br/>";
                        endif;
                        
                        
                        $this->To_Send_Emails($money->Email,$coupondetail);
                        if($money->device_type=='android'):
                            $registatoin_ids = array($money->device_id);
                             
                             $pushmess = $this->_C_Helper->Message_Json($coupondetail,$message);

                              $message = array("message" => $pushmess);
                            
							$google_api_key = "AIzaSyC7p7C6_B2z9-rC9wImaJ4SUaxWoMHuEI0";
                            $result = $this->_C_Helper->send_notification($registatoin_ids,$message,$google_api_key);
                            
                              else:
							 $pushmess        = $this->_C_Helper->Message_Json($coupondetail,$message);  
							  
                           $pushmess['tag']    ='order_status';
						   $pushmess['badge']  = 1;
						   $pushmess['sound'] = 'default';
			                $this->Iphone_Notification($pushmess,$money->device_id); 
                        endif;
                
                
            endforeach;
               
               
               
            } catch (Exception $ex) {
                $error = $ex->getMessage();
            }
               
            return $result;
            
        }
        
        /******************************
*Function to Notify User for Iphone alerts 
********************************/	
  public function Iphone_Notification($message,$deviceToken)
  {
	        $payload['aps'] = array('alert' => $message,);
            $payload = json_encode($payload);

            $apnsHost = APPLE_GATEWAY_URL;
            $apnsPort = PORT;
            $apnsCert = APNS_DEV_PEM;

            $streamContext = stream_context_create();
            stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);

            $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);


            //$deviceToken    = 'bb1107a95a74d63adf8d4c2a05393216c4e372612e4ee66f9448e5c08a2db648';
            //$deviceToken    = '194a4267af16688450be732df1b6c1484727ae7d6465abdcf065241aed520ef3';

            $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
            fwrite($apns, $apnsMessage);
            //echo $write;
           

            socket_close($apns);

            fclose($apns);
	  
	  
  }
        
        function To_Get_IDS($uservalues,$range)
        {
            
            if(count($uservalues)<1):
                return null;
                else:
                    $ids = null;
                
      
                   foreach($uservalues as $values):
                    if($range<=$values->sum):
                        $ids[] = $values->customer_id;
                    endif;
                   endforeach;
                   
                   if(!is_null($ids)):
                       $userid       = implode(',',$ids);
                       return $string ="OR ID in($userid)";
                       else:
                       return null;
                   endif;
            endif;
           
        }
        
        
        /****
         * function to find the 
         */
        public function ToAccesMoneySpentGuys()
        {
            $error = null;
            try
            {
            $GetUserID = $this->_C_Helper->G_result("Select  ID from fos_users ");
            }
            catch(Exception $e)
            {
             
                $error = $e->getMessage();   
            
            }
            
           
            if(!is_null($error))
            {
                
                return array();
            }
            else 
            {
                 $dummy = array();
                
                foreach($GetUserID  as $ids):
                    $dummy[] =$ids->ID;
                endforeach;
                
                return implode(',',$dummy);
                
            }
          
            
        }
        
        
        /*****
         * function to send the emails  to  usre
         */
        function To_Send_Emails($to,$data)
        {
            

            
            ob_start();
            include APPLICATION_PATH."/modules/admin/views/scripts/mails/coupon.phtml";
            $message = ob_get_contents();
            ob_end_clean();
            
           
              $header .= "Reply-To: Coupon Support <support@justdemo.com>\r\n"; 
              $header .= "Return-Path: Coupon Support <support@justdemo.com>\r\n"; 
              $header .= "From: Some One <support@justdemo.com>\r\n"; 
              $header .= "Organization:justdemo \r\n"; 
              $header .= "Content-Type: text/html\r\n";
          

              if(mail($to,'Coupon code Alert',$message,$header))
                  {
                             
                  }
                  else
                  {
                        throw new Exception($this->translate($result->results[0]->error));

                  }
            
            
            
        }
        
        function To_GET_People_Spent_Money()
        {
            
            $userid = $this->ToAccesMoneySpentGuys();
            $error  = null;
            if(count($userid)<1):
                
                return null;
                else:
                    try
                        {
                       
                        $GetUserID = $this->_C_Helper->G_result("Select sum(total_amount) as sum,customer_id  from fos_restaurant_customer_order  where customer_id in ($userid)");
                       
                        }
                        catch(Exception $e)
                        {

                            $error = $e->getMessage();   

                        } 
               
            endif;
            
            if(!is_null($error))
            {
                
                return array();
                
            }
            else
            {
                return $GetUserID;
                
            }
            
            
        }
        
        /*******
         * function  to save and edit the setttings
         */
        private function SaveEditSetting($type)
        {
            if(!Zend_Validate::is($this->getRequest()->res_delivery_time, 'NotEmpty')||
                    !Zend_Validate::is($this->getRequest()->res_extra_time, 'NotEmpty'))
                   
                        throw new Exception($this->translate('All fields are mandatory.'));
            
            
                 if($type=='insert'):
                         $this->database->insert(
                                $this->tp.'restaurant_setting',
                                array(
                                    'deliver_time' => $this->getRequest()->res_delivery_time,
                                    'extra_time' => $this->getRequest()->res_extra_time,
                                    'approval_check' => $this->getRequest()->autoapprove,
                                    'res_id' => $this->session->admin->resid
                                )
                     );
                     else:
                          $this->database->update(
                                $this->tp.'restaurant_setting',
                                array(
                                    'deliver_time' => $this->getRequest()->res_delivery_time,
                                    'extra_time' => $this->getRequest()->res_extra_time,
                                    'approval_check' => $this->getRequest()->autoapprove
                                    
                                ),
                                "res_id = ".$this->session->admin->resid
                            );
                         
                 endif;
            
            
        }
        
       
    }
    
    ?>