<?php

    class Admin_SettingsController extends Zend_Controller_Action{
        
        public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        
        public function init(){
            //initializing resources
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig = $bootstrap->getResource('AppConfig');
            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            $this->view->headTitle('Admin Panel');
        }
        
        public function preDispatch(){
            //checking session
            if(!$this->session->admin->loggedin)
                $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
            //adding few variables to view
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
        }
        
        public function postDispatch(){
            //adding few variables to view
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
        }
        
        public function translate($message){
            return $this->translator->translate($message);
        }
        
        public function locationsAction(){
            $response = new stdclass;
            $response->error = false;
            $response->addError = false;
            $response->addSuccess = false;
            
            if(isset($this->getRequest()->xaction) and $this->getRequest()->xaction == 'add'){
                try{
                    if(!Zend_Validate::is($this->getRequest()->city, 'NotEmpty')) throw new Exception($this->translate('You must select Country.'));
                    if(!Zend_Validate::is($this->getRequest()->location, 'NotEmpty')) throw new Exception($this->translate('You must enter location'));
                     if(!Zend_Validate::is($this->getRequest()->location_ar, 'NotEmpty')) throw new Exception($this->translate('You must enter location in Dutch'));
                    //checking whether location is already entered or not
                    $doesLocationExists = (bool)$this->database->fetchOne("select count(*) from {$this->tp}locations where location_city = ? and location_name = ?", array($this->getRequest()->city, $this->getRequest()->location));
                    if($doesLocationExists) throw new Exception($this->translate('Specified location already exists in database.'));
                    //adding location to database
                    $this->database->insert(
                        $this->tp.'locations',
                        array(
                            'location_city' => $this->getRequest()->city,
                            'location_name' => $this->getRequest()->location,
                            'location_name_ar' => $this->getRequest()->location_ar
                        )  
                    );
                    $response->addSuccess = true;
                }catch(Exception $e){
                    $response->addError = true;
                    $response->addErrorMessage = $e->getMessage();
                }
            }
            
            try{
                $response->selectedCity = (isset($this->getRequest()->city) ? $this->getRequest()->city : null);
                //loading all location 
                if($response->selectedCity == null)
                    $response->locations = $this->database->fetchAll("select *, (select count(*) from {$this->tp}restaurant_address where {$this->tp}restaurant_address.location_id = location_id) as restaurants_linked from {$this->tp}locations order by location_city asc, location_name asc");
                else
                    $response->locations = $this->database->fetchAll("select *, (select count(*) from {$this->tp}restaurant_address where {$this->tp}restaurant_address.location_id = location_id) as restaurants_linked from {$this->tp}locations where location_city = ? order by location_city asc, location_name asc", $response->selectedCity);
            

               $cities = $this->database->fetchAll("select * from {$this->tp}countries  order by country_name asc");
               $response->cities[$entry->location_id] = array('' => $this->translate('-- Select A Country / Town --'));
                
                foreach($cities as $entry)
                    {
                $response->cities[$entry->id] = stripslashes($entry->country_name);
                    }   
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage(); 
            }
            
            $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Manage Locations').' | ', 'PREPEND');
        }
        
  


        public function locationsareaAction(){
         
            $response = new stdclass;
            $response->error = false;
            $response->addError = false;
            $response->addSuccess = false;
            
            if(isset($this->getRequest()->xaction) and $this->getRequest()->xaction == 'add'){
                try{
                    if(!Zend_Validate::is($this->getRequest()->city, 'NotEmpty')) throw new Exception($this->translate('You must select city.'));
                    if(!Zend_Validate::is($this->getRequest()->area, 'NotEmpty')) throw new Exception($this->translate('You must enter area'));
                     if(!Zend_Validate::is($this->getRequest()->area_ar, 'NotEmpty')) throw new Exception($this->translate('You must enter area in Dutch'));
                    //checking whether location is already entered or not
                    $doesLocationExists = (bool)$this->database->fetchOne("select count(*) from {$this->tp}area where location_id = ? and area_name = ?", array($this->getRequest()->city, $this->getRequest()->area));
                    if($doesLocationExists) throw new Exception($this->translate('Specified area already exists in database.'));
                    //adding location to database
                    $this->database->insert(
                        $this->tp.'area',
                        array(
                            'location_id' => $this->getRequest()->city,
                            'area_name' => $this->getRequest()->area,
                            'area_name_ar' => $this->getRequest()->area_ar
                        )  
                    );
                    $response->addSuccess = true;
                }catch(Exception $e){
                    $response->addError = true;
                    $response->addErrorMessage = $e->getMessage();
                }
            }
            
            try{

                $response->selectedCity = (isset($this->getRequest()->city) ? $this->getRequest()->city : null);
                

                //loading all location 
                if($response->selectedCity == null)
                {
                    $query = "select a.*, (select count(*) from {$this->tp}restaurant_address as r where r.location_id = a.location_id and r.area_id= a.area_id) as conts from {$this->tp}area as a";
                    $response->area_city = $this->database->fetchAll($query);
                    }
                else            
                {
                      // $response->area_city = $this->database->fetchAll("select * from {$this->tp}area ");
                       $response->area_city = $this->database->fetchAll("select a.*, (select count(*) from {$this->tp}restaurant_address as r where r.location_id = a.location_id and r.area_id= a.area_id) as conts from {$this->tp}area as a where location_id = $response->selectedCity");
                    }   
                    $city = $this->database->fetchAll("select * from {$this->tp}locations  order by location_name asc");
                    $response->city[$entry->location_id] = array('' => $this->translate('-- Select A City --'));

                foreach($city as $entry)
                    {
                    $response->city[$entry->location_id] = stripslashes($entry->location_name);
                    }   
         
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage(); 
            }
              
            $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Manage Locations').' | ', 'PREPEND');
        }

         public function delareaAction()
           {
              $this->database->delete($this->tp.'area', 'area_id = '.$this->getRequest()->eid);
               $this->redirect('admin/settings/locationsarea'); 
            }


        public function ajaxdellocationAction(){
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
            
            try{
                $this->database->delete($this->tp.'locations', 'location_id = '.$this->getRequest()->location_id);
                $response->success = true;
            }catch(Exception $e){
                $response->error = true;
            }
            
            $this->getHelper('Json')->sendJson($response);
        }
        
    }