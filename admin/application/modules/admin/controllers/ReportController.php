<?php

include('CommonController.php');
include('mealhelper.php');


/*  Author:Gagandeep Singh

 *  Description : This class is specially for daily weekly or monthly report of particular restaurant

 *  class:daily,Weekly or Monthly Report

 *

 *  */



class Admin_ReportController extends Zend_Controller_Action

{





	public $database;

	public $tp; //Table prefix

	public $appConfig;

	public $config;

	public $session;

	public $translator;

	public $test;

	public $count;
	public $_C_Helper;



	/* initialising the  all parent  vallue and database */



	public  function  init()

	{

		 

		//initializing resources by singleton instance

		$bootstrap=Zend_Controller_Front::getInstance()->getParam('bootstrap');

		$this->database = $bootstrap->getPluginResource('db')->getDbAdapter();

		$this->database->setFetchMode(Zend_Db::FETCH_OBJ);

		$this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');

		$this->appConfig = $bootstrap->getResource('AppConfig');

		$this->config = Zend_Controller_Front::getInstance()->getParam('Config');

		$this->session = Zend_Controller_Front::getInstance()->getParam('Session');

		$this->translator = Zend_Registry::get('Zend_Translate');

		$this->test=commonnotify::Notification();

		$this->count=commonnotify::total_count();
        $this->_C_Helper  = $this->getHelper('Custom');


		Zend_Layout::startMvc(array(

                'layoutPath' => APPLICATION_PATH . '/modules/restaurant/views/layouts',

                'layout' => 'restaurantadmin'

                ));

                //adding default page title

                $this->view->headTitle($this->translate('Reports Panel').' | Reports');

                 

                 

	}





	// Setting the preprocess function before all action be performed

	public function preDispatch()

	{

		 

		//checking if session is created //

		if(!$this->session->admin->loggedin)

		$this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);

		//adding few variables to view

		$this->view->request = $this->getRequest();

		$this->view->session = $this->session;

		$this->view->config = $this->config;

		$this->view->appConfig = $this->appConfig;

		 

	}



	/* performing action in class */



	public function  indexAction()

	{

		 

		$report               = new stdclass;

		 

		$report->error        = false;

		 

		$report->errormessage = null;

		 

		$report->success      = false;

		 

		 

		$res_order_list       = $this->get_res_order_report(7);

			

		//$res_weekly_report=$this->get_res_order_report(30);



		if( $res_order_list->error==true)

		{

		  

		  

			$report->error=true;

			 

			$report->errormessage=$res_order_list->errormessage;



		}



		else



		{



			$report->order_report=$res_order_list->data;



			 

			$report->data->amount=array();

		  

			$dummy_date=array();

		  

			$start=0;

		  

			foreach($report->order_report as $rep):

		  

			$time = new DateTime($rep->order_date);

		  

			$date = $time->format('Y-m-d');



			// CHECK FOR REPEAT DATE //

			if(in_array($date,$dummy_date))

			{



				$report->data->amount[$date]=$report->data->amount[$date]+$rep->total_amount;



			}

		  

			else

			{



				$dummy_date[]=$date;

				 

				$report->data->amount[$date]=$rep->total_amount;

					

			}



			endforeach;

		  

			// getting start date and end date of report

		  

			$report->data->first_date = $res_order_list->today_date;

			$report->data->last_date =$res_order_list->last_week_date;

		  

		  

			unset($dummy_date);

		  

			 

		}

		 

		$week_total_list =  $this->get_week_date();

		 



		if($week_total_list->error == true)

		{

			 

			 

			$report->error=true;

			$report->errormessage=$week_total_list->errormessage;

			 

			 

			 

		}



		else



		{

			 

			$report->week=$week_total_list;

			 

			$report->first_week_date=$week_total_list->current_week_date;

			 

			$report->last_week_date=$week_total_list->last_week_date;

			 



			 

		}

		$result  = $this->get_every_month_data();



		if($result->error==true)

		{

			 

			$report->error=true;

			$report->errormessage=$result->errormessage;

			 

			 

		}

		else

		{

			 

			 

			$report->month = $result->monthdata;

			$report->start_month_date = $result->start_month_date;

			$report->last_month_date = $result->last_month_date;

			 

			 

			 



			 

			 

			 

		}







		//setting the another graph for weekly report //

		$response->test=$this->test;

		$response->count=$this->count;

		$this->view->sidebar_menu_item = "Reports";
        $this->view->sidebar_submenu_item ="Reportsmain";
		$this->view->headTitle($this->translate('Manage Reports').' | ', 'PREPEND');

		$this->view->assign((array)$report);

	}



     
        /****
         * function to   get the drill down report 
         */
        public function combinereportAction()
        {
            $response               = new stdclass;
            $response->error        = false;
            $response->errormessage = null;
            $dummuarray             = array();
            try
            {
            
                $allDeleliveries    = $this->_C_Helper->G_result("Select restaurant_id,restaurant_display_id from fos_restaurants  where is_active=1 ");
               
               
                if(count($allDeleliveries)>0):
                     $dummyobject = new stdClass;
                     foreach($allDeleliveries as $alldelive):
                      $data['restaurant_id']         = $alldelive->restaurant_id;
                      $data['restaurant_display_id'] = $alldelive->restaurant_display_id;
                      $data['rate']                  = $this->Restaurant_performace($alldelive->restaurant_id);
                      $data['cashorders']            = $this->Restaurant_performace($alldelive->restaurant_id,"and payment_status='cash' ");
                      $data['creditorders']          = $this->Restaurant_performace($alldelive->restaurant_id,"and payment_status='credit' ");
                      $data['approved']              = $this->Restaurant_performace($alldelive->restaurant_id,"and status=2 ");
                      $data['delivered']             = $this->Restaurant_performace($alldelive->restaurant_id,"and status=0 ");
                      $data['canceled']              = $this->Restaurant_performace($alldelive->restaurant_id,"and status=4 ");
                      $dummuarray[]                  = $data;
                     endforeach;
                    else:
                    
                endif;
               
                     
                    
                    
                    
                   
              
                $response->data     = $dummuarray;
                
                
            } 
            catch (Exception $ex) 
            {
          
                $response->error    = true;
                $response->errormessage = $ex->getMessage();
                        
                
                
            }
            
             $this->view->sidebar_menu_item = "Reports";
             $this->view->sidebar_submenu_item ="combine";
             $this->view->assign((array)$response);
            
                  
      
        }
        /******
		* Report based on Restaurant and date wise
		*********/
		
		public function reportperAction()
		{
			$response               = new stdclass;
            $response->error        = false;
            $response->errormessage = null;
            $dummuarray             = array();
            try
            {
            
			 
			
                $allDeleliveries    = $this->_C_Helper->G_result("Select restaurant_id,restaurant_display_id from fos_restaurants  where is_active=1 ");
               
			    if($this->getRequest()->isPost())
				{
					$start = $this->getRequest()->start;
					$end   = $this->getRequest()->end;
					$where = " and order_date between '$start' and '$end' ";
				}
				else
				{
					$where = '';
				}
               
                if(count($allDeleliveries)>0):
                     $dummyobject = new stdClass;
                     foreach($allDeleliveries as $alldelive):
                      $data['restaurant_id']         = $alldelive->restaurant_id;
                      $data['restaurant_display_id'] = $alldelive->restaurant_display_id;
                      $data['rate']                  = $this->Restaurant_profit($alldelive->restaurant_id,$where);
                      $data['cashorders']            = $this->Restaurant_performace($alldelive->restaurant_id,"and payment_status='cash' $where ");
                      $data['creditorders']          = $this->Restaurant_performace($alldelive->restaurant_id,"and payment_status='credit' $where ");
                      $data['approved']              = $this->Restaurant_performace($alldelive->restaurant_id,"and status=2  $where");
                      $data['delivered']             = $this->Restaurant_performace($alldelive->restaurant_id,"and status=0  $where");
                      $data['canceled']              = $this->Restaurant_performace($alldelive->restaurant_id,"and status=4  $where");
                      $dummuarray[]                  = $data;
                     endforeach;
                    else:
                    
                endif;
               
                     
                    
                    
                    
                   
              
                $response->data     = $dummuarray;
                
                
            } 
            catch (Exception $ex) 
            {
          
                $response->error    = true;
                $response->errormessage = $ex->getMessage();
                        
                
                
            }
            
             $this->view->sidebar_menu_item = "Reports";
             $this->view->sidebar_submenu_item ="reportper";
             $this->view->assign((array)$response);
			
			
			
		}
		
		    /*****
			* 
            * Action to Get Delivery boy report
            *			
			*********/
			public function deliverreportAction()
			{
		    $response               = new stdclass;
            $response->error        = false;
            $response->errormessage = null;
            $dummuarray             = array();
            try
            { 
               
			    if($this->getRequest()->isPost())
				{
					
					$resid   = $this->getRequest()->res_id;
					$allDeleliveries    = $this->_C_Helper->G_result("Select * from {$this->tp}delivery_boy_login   where res_id=$resid");
					
				
				
               
                if(count($allDeleliveries)>0):
                     $dummyobject = new stdClass;
                     foreach($allDeleliveries as $alldelive):
                      $data['delivery_id']          = $alldelive->id;
                      $data['name']                 = $alldelive->name;
                      $data['totalorders']          = $this->GettingDetail('order_id',$this->tp.'delivery_order_rel_boy'," where delivery_id=".$alldelive->id,'count');
                      $dummuarray[]                 = $data;
                     endforeach;
                    else:
                    
                endif;
				 $response->data     = $dummuarray;  
                }
   
                $deliverpoints = $this->database->fetchAll("select * from {$this->tp}restaurants");
                $response->resids[''] =  'Select A Delivery Point';  
                foreach($deliverpoints as $entry)
                    {
                      $response->resids[$entry->restaurant_id] = stripslashes($entry->restaurant_name);
                    } 
              if($this->getRequest()->isPost())
				{
					$response->restname = $response->resids[$resid];
				}
					
            } 
            catch (Exception $ex) 
            {
          
                $response->error    = true;
                $response->errormessage = $ex->getMessage();        
            }
		        
             $this->view->sidebar_menu_item = "Reports";
             $this->view->sidebar_submenu_item ="delivery";
             $this->view->assign((array)$response);
		
			}

		
		/*********
		* Function to Access the list of customer history
		**********/
		function customerhistoryAction()
		{
			$response       = new stdClass;
			$reponse->error = false;
			$response->errormessage =null;
			
			if($this->getRequest()->isPost())
			{
				$where ='where restuarant_service_id='.$this->getRequest()->res_id;
				
			}
			else
			{
				$where  ='';
			}
			
			try
			{
				
			  $deliverpoints = $this->database->fetchAll("select * from {$this->tp}restaurants");
              $response->resids[''] =  'Select A Delivery Point';  
              foreach($deliverpoints as $entry)
                    {
                      $response->resids[$entry->restaurant_id] = stripslashes($entry->restaurant_name);
                    } 	
			  $query  = "Select * from {$this->tp}restaurant_customer_order inner join {$this->tp}users on {$this->tp}restaurant_customer_order.customer_id={$this->tp}users.ID $where";
			  $result = $this->database->fetchAll($query);
              $response->data = $this->Filter_Customer_Report($result);
              		  
             		  
			} 
			catch(Exception $G_ex)
			{
				
				$reponse->error         = true;
			    $response->errormessage = $G_ex->getMessage();
				
			}
			$this->view->sidebar_menu_item = "Reports";
			$this->view->sidebar_submenu_item ="Customerhistory";
            $this->view->headTitle($this->translate('Customer  Order History ').' | ', 'PREPEND');
			$this->view->assign((array)$response);
			
			
		}

		/*******
		* funciton to filter the customer history  Result
		*********/

		private function Filter_Customer_Report($data)
		{
			 $customer_id  = array();
			 $count_Orders = 0;
			 $dummyresult  = array();
			 foreach($data as $dat):
			 if(in_array($dat->customer_id,$cutomer_id))
			 {
				
               	$dummyresult[$dat->customer_id]['totalcount']   = $dummyresult[$dat->customer_id]['totalcount']+1; 
                $dummyresult[$dat->customer_id]['totalAmount']  = $dummyresult[$dat->customer_id]['totalAmount']+$dat->total_amount; 
				 
			 }
			 else
			 {
               	$cutomer_id[] =	$dat->customer_id;	
                $dummyresult[$dat->customer_id]['name']         = $dat->firstname.' '.$dat->lastname;
               	$dummyresult[$dat->customer_id]['totalcount']   = 1; 
                $dummyresult[$dat->customer_id]['totalAmount']  = $dat->total_amount;				
			 }
		
			 endforeach;
			
			 return $dummyresult;
			
			
		}
        /******
         * function to ge the performance of restaurant 
         */
        private function Restaurant_performace($id,$where=null)
        {
            
            
            
            $count = $this->_C_Helper->G_result("Select count(*) as total from  {$this->tp}restaurant_customer_order  where restuarant_service_id=$id   $where");
            
            
            return $count[0]->total;
            
        }
     /******
         * function to ge the performance of restaurant 
         */
        private function Restaurant_profit($id,$where=null)
        {
            
            $result = $this->_C_Helper->G_result("Select sum(total_amount) as total from  {$this->tp}restaurant_customer_order  where restuarant_service_id=$id   $where");
           if(count($result)>0):
		     return $result[0]->total;
		   else:
		   return 0; 
		   endif;
          
            
        }

		/********
		*
        * Getting Delivery boy detail  by seperate		
		*
		********/
		private function  GettingDetail($select,$table,$where=null,$typeresult)
		{
			$query  = "select $select from $table $where";
			$result = $this->_C_Helper->G_result($query);
             if($typeresult=='count')
			 {
				 return count($result);
				 
			 }
			 else
			 {
				 return $result;
			 }
			
			
			
		}
  /* all meals Sales per Meal wise report */

        

        public function mealreportAction()

        

        {

        	

        	$meals = new stdclass;

        	$meals->error = false;

        	$meals->errormessage = null;

        	

        	

              if($this->getRequest()->isGET()):

              

			  $res_id->restaurant_info = $this->getRequest()->restaurant_id;

			 

			  

			  

			  endif;

             

              

             // daily wise report

        	    if($this->getRequest()->category == 'daily'):

        	   $data = mealhelper::particular_meal_orders(mealhelper::get_all_restaurant_meals($res_id->restaurant_info),$res_id->restaurant_info,$start_date=null);

        	   $meal->data=$data;

        	   $meal->porletname = 'Daily Per Order Report';

        	       

             // Weekly wise report

        	   elseif ($this->getRequest()->category == 'Weekly' ):

        	   

        	    $start_date = $this->fetching_date($date=null);

        	    $data = mealhelper::particular_meal_orders(mealhelper::get_all_restaurant_meals($res_id->restaurant_info),$res_id->restaurant_info,$start_date);

        	     

        	   $meal->data = $data;

        	   $meal->date =  $start_date;

        	   

        	   $meal->porletname = 'Weekly Per Order Report';



        	  

        	   // Monthly wise report

        	    elseif($this->getRequest()->category == 'Monthly'):

        	   

        	     $start_date = $this->last_month_date($date=null);

        	     

        	     $data = mealhelper::particular_meal_orders(mealhelper::get_all_restaurant_meals($res_id->restaurant_info),$res_id->restaurant_info,$start_date);



        	    $meal->data = $data;

        	    $meal->date =  $start_date;

        	    $meal->porletname =' Monthly Per Order Report';

        	    

        	   

        	    else:

        	    

        	    //Default by Daily

        	   $data = mealhelper::particular_meal_orders(mealhelper::get_all_restaurant_meals($res_id->restaurant_info),$res_id->restaurant_info,$start_date=null);

        	   $meal->data=$data;

        	   

        	    $meal->porletname = 'Daily Per Order Report';

        	   

        	   endif;

              

              

            $meals->test = $this->test; 	

		    $meals->count = $this->count;

		    

		   

		    $this->view->sidebar_menu_item = "Meals Report";

            $this->view->headTitle($this->translate('Meals Reports').' | ', 'PREPEND');

            $this->view->assign((array) $meal);

        	

        	

        }

        

        /* function to get invoice according to % commision of super admin 

         * 

         * 

         * 

         */

        

        public function invoiceperresAction()

        {

        	

          $res_commission=new stdclass;

          $res_commission->error=false;

          $res_commission->errormessage=null;

          

          

            // if($this->getRequest()->category == 'daily')

            // {

            // 	$start_date = $this->date_converter($this->fetching_date($date=null));

            	

            // 	$res_commission->porletname = 'Daily Per Order Report';

            	

            	

            // }

            // elseif($this->getRequest()->category == 'Weekly')

            // {

            // 	$start_date =  $this->date_converter($this->last_month_date($date=null));

            	

            // 	$res_commission->date =  $this->last_month_date($date=null);

            	

        	   //  $res_commission->porletname = 'Weekly Per Order Report';

            	

            // }

            // elseif($this->getRequest()->category == 'Monthly')

            // {

                 

            //     $start_date = $this->date_converter($this->get_last_year_date());

                

            //     $res_commission->date = $this->get_last_year_date();

               

        	   //  $res_commission->porletname =' Monthly Per Order Report';

            

            // }

            // else {

            	

            	

            // 	    $start_date = $this->date_converter($this->fetching_date($date=null));

            // 		$res_commission->porletname = 'current Month Report';

            	

            // }

            $data = mealhelper::get_commission_per_order();
            $res_commission->porletname = 'current Month Report';
            

          if($data->error == true)

          {

          	

          	 $res_commission->error=false;

             $res_commission->errormessage=$data->errormessage;

          	

          	

          }

          else 



          {

          	

          	

          	$res_commission->data=$data->data;


           

          

          	

          	

          	

          }

        	

        	

        	

        	$res_commission->test = $this->test; 	

		    $res_commission->count = $this->count;

		    

		   

		    $this->view->sidebar_menu_item = "Commission Center";

            $this->view->headTitle($this->translate('Commision Report').' | ', 'PREPEND');

            $this->view->assign((array) $res_commission);

        	

        	

        }


     /*************************************

      function use for edit resturant commition 
     *******************************************/


      public function editinvoiceperresAction()
         {

         	 	//quering database for selecting the particular id

		$response = new stdclass;

		$response->error=false;

		$response->errormessage=null;

		try {

				
			if($this->getRequest()->isGET()):

			$response->res_id = $this->getRequest()->id;

		     $data = mealhelper::get_commission_per_order_by_id($response->res_id);
              
			// $week2="SELECT {$this->tp}restaurants.restaurant_name, {$this->tp}restaurants.per_order_commision, {$this->tp}restaurant_invoice.* FROM {$this->tp}restaurants LEFT JOIN {$this->tp}restaurant_invoice ON {$this->tp}restaurants.restaurant_id  = {$this->tp}restaurant_invoice.restaurant_id WHERE {$this->tp}restaurants.restaurant_id = $response->res_id"; 
			 
			//  $data=$this->database->fetchrow($week2);
            $response->data = $data->data;
            


			endif;
			
             if(isset($this->getRequest()->restaurant_name))
             {  

             	$response->res_id = $this->getRequest()->id;
                $restaurant_name = $this->getRequest()->restaurant_name;
                $commision = $this->getRequest()->commision_per;
                $paid = $this->getRequest()->paid;
                $paiddate = date('Y-m-d H:i:s'); 
                 
                $this->database->update($this->tp.'restaurants' , array('per_order_commision' => $commision), "restaurant_id IN ($response->res_id)");

                if(!empty($paid))
                {
               $this->database->insert($this->tp.'restaurant_invoice' , array('restaurant_id' => $response->res_id, 'paid' => $paid, 'paid_date'=> $paiddate));
			    }

              $this->getHelper('Redirector')->gotoRoute(array(), 'admin-Report-invoiceperres', true);
             }
		

	   	}

		catch(Exception $e)

		{

			
			$response->error=true;

			$response->errormessage=$this->translate($e->getMessage());
	

		}
		
       $this->view->assign((array) $response);  
             

        }
 
     


     /*************************************

      function use for Detail of resturant commition 
     *******************************************/


     public function invoicedetailAction()
       {
        $response = new stdclass;

		$response->error=false;

		$response->errormessage=null;

		try {
            $response->res_id = $this->getRequest()->id;
            $query = "select * from {$this->tp}restaurants where restaurant_id = $response->res_id";   
            $response->detail = $this->database->fetchrow($query);
            if($this->getRequest()->year)
            {
             $y = $this->getRequest()->year;
            }
            else
            {
             $y=date('Y');  
            }
            $response->data =  mealhelper::get_monthly_detail($response->res_id, $y);
            $response->porletname = 'Details Of '. $y;
            $response->year  = $y;
          }


     catch(Exception $e)
		{

		$response->error=true;

		$response->errormessage=$this->translate($e->getMessage());
	
		}
		
       $this->view->assign((array) $response);  
             
       }

	/* function to translate the language */

    public function invoiceupdateAction()
    {
       
       $rest_id= $response->res_id = $this->getRequest()->rest_id;
       $month= $response->res_id = $this->getRequest()->month;
       $amount= $response->res_id = $this->getRequest()->amount;
       $paiddate = date("Y-m-d H:i:s", strtotime($month));

       $this->database->insert($this->tp.'restaurant_invoice' , array('restaurant_id' => $rest_id, 'paid' => $amount, 'paid_date'=> $paiddate));
       $lastid=  $this->database->lastInsertId(); 
       if($lastid) 
       {
       	echo true;
       	die;
       }
       else
       {
       	echo false;
       	die;
       }


      }

	public  function translate()

	{





		return $this->translator->translate($message);







	}

		

		

		

	//seperate function for getting  restaurant id//

	private function get_res_id()

	{

		//quering database for selecting the particular id

		$res_id = new stdclass;

		$res_id->error=false;

		$res_id->errormessage=null;

		try {

				
			if($this->getRequest()->isGET()):

			$res_id->restaurant_info = $this->getRequest()->restaurant_id;

			endif;

			 
			if(!isset($_GET['restaurant_id']))

			{


				$res_id->restaurant_info="no_id";


			}


		}

		catch(Exception $e)

		{

			
			$res_id->error=true;

			$res_id->errormessage=$this->translate($e->getMessage());
	

		}

			



		return $res_id;


	}

		

	//another function for getting order for particular restaurant on daily basis//

		

	private function get_res_order_report($time)

	{



		// for changing the amendmends //

		$result=new stdclass;

		 

		$result->error=false;

		 

		$result->errormessage=null;

		 

		$id   =  $this->get_res_id();

		if($id->error==true)

		{



			$result->error=true;



			$result->errormessage=$id->errormessage;





		}

		else

		{

			 



			$resid = $id->restaurant_info;

			//for all restaurant orders report //

			if($resid =="no_id")

			{



				$resaurant_detail="";



			}

			else

			{

				//for particular  restaurant orders report by id //



				$resaurant_detail=" AND restuarant_service_id ={$resid}" ;

			}



			 

			try

			{



				$today_date=date("Y-m-d H:i:s");

				

				$timestamp = time();

				

				for ($i = 0 ; $i < $time ; $i++) {

					// echo date('Y-m-d H:i:s', $timestamp) . '<br />';

					$timestamp -= 24 * 3600;

				}

				$last_week_date= date('Y-m-d H:i:s',$timestamp);

					

				$orders_report = "SELECT order_date,total_amount FROM {$this->tp}restaurant_customer_order"  . " WHERE status=0  $resaurant_detail  ". " AND order_date BETWEEN  '{$last_week_date}' " . "AND '{$today_date}' ORDER BY order_date ";

				

				$result->data  = $this->database->fetchAll($orders_report);


				$result->today_date = $this->time_stamp_converter($today_date);

				$result->last_week_date = $this->time_stamp_converter($last_week_date);

			}

			 

			catch(Exception $e)

			{

				$result->error=true;



				$result->errormessage=$this->translate($e->getMessage());



			}

			 

		}

		 
		return $result;



	}

		

		

		

		

	//getting data according to week

		

	private function  get_week_date()

	{





		$week= new stdclass;

		$week->error=false;

		$week->errormessage=null;



		$data=array();

		$id=$this->get_res_id();



			

		 

		 

		if($id->error==true)

		{



			$week->error=true;



			$week->errormessage=$id->errormessage;





		}

		else

		{

			 

			$resid=$id->restaurant_info;

			//for all restaurant orders report //

			if($resid =="no_id")

			{



				$resaurant_detail=" ";



			}

			else

			{

				//for particular retaurant report//



				$resaurant_detail=" AND restuarant_service_id ={$resid}" ;

				 

			}

			 

			// data is fetched every weekend wise

			try{

				$first_week[0]= $this->fetching_date($new_time=null);

				//Preserves old date for getting next last date

				$next=$first_week[0];



				$current=date('Y-m-d H:i:s');



				$week->current_week_date=$this->time_stamp_converter($current);



				$first_week[0]=$this->date_converter($first_week[0]);





				$sql="SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE status=0   $resaurant_detail ". " AND order_date BETWEEN  '{$first_week[0]}' " . "AND '{$current}' ORDER BY order_date ";



				$data[]=$this->database->fetchrow($sql);



				//next week data //

				$first_week[1]=$this->fetching_date($next);

				//Preserves old date for getting next last date

				$next=$first_week[1];

				$current=$first_week[0];



				$first_week[1]=$this->date_converter($first_week[1]);



				$week2="SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE status=0   $resaurant_detail ". " AND order_date BETWEEN  '{$first_week[1]}' " . "AND '{$current}' ORDER BY order_date ";





				$data[]=$this->database->fetchrow($week2);





				$first_week[2]=$this->fetching_date($next);

				//Preserves old date for getting next last date

				$next=$first_week[2];

					

				 

				$current=$first_week[1];



				$first_week[2]=$this->date_converter($first_week[2]);

				 

				$week2="SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE status=0   $resaurant_detail ". " AND order_date BETWEEN  '{$first_week[2]}' " . "AND '{$current}' ORDER BY order_date ";



				$data[]=$this->database->fetchrow($week2);



				$first_week[3]=$this->fetching_date($next);

				 

				//Preserves old date for getting next last date

				$next=$first_week[3];

				 

				$current=$first_week[2];



				$first_week[3]=$this->date_converter($first_week[3]);

				 

				$week2="SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE status=0   $resaurant_detail ". " AND order_date BETWEEN  '{$first_week[3]}' " . "AND '{$current}' ORDER BY order_date ";



				$data[]=$this->database->fetchrow($week2);



				$week->last_week_date=$this->time_stamp_converter($first_week[3]);



					

			}

			catch(Exception $e)

			{

				 

				$week->error=true;

				$week->errormessage=$e->getMessage();



			}

		}

		 

		//clearing unrequired memory variables

		unset($first_week);

		unset($current);

		unset($next);

		//sending data to



		$week->data=$data;

			

		return $week;









	}

		

	//Getting the last week date according to current date

		

	public function  fetching_date($new_time)

	{



		if($new_time !=null)

		{

			$timestamp =$new_time;

				

		}





		else {

				

				

			$timestamp = date('Y-m-d');

				

				

		}

		$previous_week = strtotime("-1 week $timestamp ");







		$start_week = date("Y-m-d",$previous_week);

		//$end_week = date("Y-m-d",$end_week);



		unset($previous_week);



		return $start_week;









	}

		

	// getting last every month data//

		

	public function get_every_month_data()

	{

		$result= new stdclass;

		$result->error=false;

		$result->errormessage=null;

			

		$last_date =date('Y-m-d H:i:s');

		$start_date=date('Y-m-d');

		$result->start_month_date=$start_date;

		for($start=0;$start<13;$start++):





		//getting the date last date of month year

		if($start==12)

		{

				

	        $result->last_month_date = $this->time_stamp_converter($next_date);

				

		}



		$date[]   = date('Y,m,d', strtotime( $last_date ));

			



		$start_date = $this->last_month_date($start_date);



		$next_date  =  $start_date;

		 

		$start_date =  $this->date_converter($start_date);









		$data[]     =  $this->quering_database($start_date,$last_date);





		$last_date=$this->date_converter($next_date);

		$start_date=$next_date;



		endfor;







		if($result->error==true)

		{

			$result->error=true;

			$result->errormessage=$data->errormessage;

				

				

		}

		else

		{

				

			$result->data=$data;

			$result->months=$date;



			foreach($result->data as $dat)

			{

				if($dat->data->sum=="")

				{

					$dat->data->sum=0;



				}

				$result->monthdata['data'][]=$dat->data->sum;

					

					

			}

			for($start=0; $start<count($result->months);$start++)

			{

					

				$result->monthdata['months'][]=$result->months[$start];

					

					

					

			}



				

				

				

		}







		return $result;

















	}

		

		

		

	public function quering_database($start_date,$end_date)

	{

			

		$month=new stdclass;

		$month->error=false;

		$month->errormessage=null;

		$id=$this->get_res_id();



		if($id->error==true)

		{

				

			$month->error=true;

			$month->errormessage=$id->errormessage;

				

		}

		else

		{

			$id=$id->restaurant_info;

				

			if($id =="no_id")

			{

				//for all restaurant orders report //

				$resaurant_detail="";



			}

			else

			{

				//for Particular restaurant orders report by id //

				$resaurant_detail=" AND restuarant_service_id ={$id}" ;

				 

			}	

			try

                        {



				$data="SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE status=0  $resaurant_detail ". " AND order_date BETWEEN  '{$start_date}' " . "AND '{$end_date}' ORDER BY order_date ";

				$month_data=$this->database->fetchrow($data);

			}

			catch(Exception $e)

			{

				 

				$month->error=true;

				$month->errormessage=$e->getMessage();



			}

				

			$month->data = $month_data;

				

				

				

				

				

				

		}





		return $month;







	}

		

	//fetching last month date from current//

		

	public function last_month_date($last_month_date=null)

	{

		if(!is_null($last_month_date))

		{

				

				

			$month_date = $last_month_date;

				

				

				

				

		}



		$previous_month = strtotime("-1 month $month_date ");







		$start_month = date("Y-m-d", $previous_month);



		unset($previous_month);

		unset($month_date);



		return $start_month;



	}

		

		

	//Converting the date with  in time stamp formate

	public function date_converter($date)

	{



		$time = new DateTime($date);



		$date = $time->format('Y-m-d H:i:s');



		return $date;





	}

		

	// converting the time stamp into date

		

	public function time_stamp_converter($timestamp)

	{





		$time = new DateTime($timestamp);



		$date = $time->format('Y-m-d');



		return $date;





	}

	// Get last year date  from todays date 

   public function get_last_year_date()

   {

  

        $month_date = date('Y-m-d');

        

		$previous_month = strtotime("-1 Year $month_date ");







		$start_month = date("Y-m-d", $previous_month);



		unset($previous_month);

		unset($month_date);



		return $start_month;

   	

   	

   }

		

		

		

		











}













?>