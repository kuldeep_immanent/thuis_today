<?php



/* Class: admin Orders controller 

 * Author:Gagandeep Singh

 * Company:Boom Infotech Mohali

 * */



     include('CommonController.php');

    class Admin_OrdersController extends Zend_Controller_Action{

        

        public $database;

        public $tp; //Table prefix

        public $appConfig;

        public $config;

        public $session;

        public $translator;

       

        

        public function init(){

           //initializing resources

            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');

            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();

            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);

            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');

            $this->appConfig = $bootstrap->getResource('AppConfig');

            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');

            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');

            $this->translator = Zend_Registry::get('Zend_Translate');

            $this->test=commonnotify::Notification();

            $this->count=commonnotify::total_count();

            //initializing layout

            Zend_Layout::startMvc(array(

                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',

                'layout' => 'main'

            ));

            //adding default page title

            $this->view->headTitle($this->translate('Admin Panel').' | Kebapiste');

        }

        

        public function preDispatch(){

            //checking session

            if(!$this->session->admin->loggedin)

                $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);

            //adding few variables to view

            $this->view->request = $this->getRequest();

            $this->view->session = $this->session;

            $this->view->config = $this->config;

            $this->view->appConfig = $this->appConfig;

        }

		

		public function translate($message){

            return $this->translator->translate($message);

        }

        

        

       

        

        public function editAction()

		{

        	

        	 

			 $detail=new stdclass;

			 $detail->error=false;

			 $detail->success=false;

			 

			

			  try{

                 $detail->restaurants->count = 0;

                

                $queryStr = "SELECT id, restaurant_name, firstname, lastname,address,postcode,city, Order_date, delivery_date, total_amount,cart,deleted"." ".

                   

                "FROM {$this->tp}restaurant_customer_order"." "."INNER JOIN "." "."{$this->tp}restaurants ON {$this->tp}restaurants.restaurant_id = {$this->tp}restaurant_customer_order.restuarant_service_id "." "." WHERE

                {$this->tp}restaurant_customer_order.id= {$_GET['id']}";

                

				

                $detail->restaurants->list = $this->database->fetchAll($queryStr);

                

                

                $detail->restaurants->count = count( $detail->restaurants->list); 

               

			  }

            catch(Exception $e){

                 $detail->error = true;

                 $detail->errorMessage = $this->translate('Failed to load restaurant list. Please re-try again.');

            }

            

			/* if getting the request to edit the value */

			

			 if($this->getRequest()->isPost()){

			 	

			   

			  

			  try{

                    //validating fields by exception handling 

                  

                     

                    if( !Zend_Validate::is($this->getRequest()->res_firstname, 'NotEmpty') ||!Zend_Validate::is($this->getRequest()->res_lastname , 'NotEmpty')

                       

                    ||!Zend_Validate::is($this->getRequest()->res_total_amount , 'NotEmpty')    ||!Zend_Validate::is($this->getRequest()->res_address , 'NotEmpty')

                     ||!Zend_Validate::is($this->getRequest()->res_city , 'NotEmpty')) 

                     

                        throw new Exception($this->translate('Mandatory fields are found empty.'));

                        

                        

                        



                   /* posted data */  

                        	   

                     

						 try{

						 	

                     $this->database->update(

                                $this->tp.'restaurant_customer_order',

                                

                                array(

							    'firstname'      => $this->getRequest()->res_firstname,

							    'lastname'      => $this->getRequest()->res_lastname,

	                            'address'      => $this->getRequest()->res_address,

	                            'postcode'     => $this->getRequest()->res_postcode,

	                            'city'         => $this->getRequest()->res_city,

	                            'total_amount' => $this->getRequest()->res_total_amount 

						    ),

                                'id = '.$this->getRequest()->res_id

                            );

			          }

			          

			          catch(Exception $e)

			          {

                        throw new Exception($this->translate('Failed to save. Please re-try again.'));

                       }

                      

                            

                            $detail->success=true;

                            

	

			     }

			     //caching exception

				 catch(Exception $e){

                 $detail->error = true;

                 $detail->errorMessage = $this->translate('Failed to update particular order restaurant name. Please re-try again.');

                }

				

				}

				 $detail->test=$this->test;

                 $detail->count=$this->count;

			

            $this->view->assign((array) $detail);

            

            $this->view->sidebar_menu_item = 'OrderEdit';

            $this->view->headTitle($this->translate('Dashboard').' | ', 'PREPEND');

        }

		

        	

            

        

        

        /* calling the list of order of */
public function orderlistAction(){
   

}
        

     public function ordersAction(){

     	       $id=$_GET['id'];

               $response = new stdclass;

               $response->error = false;

               $response->success = false;

               

                /* Filter Requests */

              

                $where_clause=null;

                $sort_by='asc';

                $order_date=null;

                $deliver_date=null;

                // post requests //

		  if($this->getRequest()->isPost()){

		  	 

             

                try{

                    switch($this->getRequest()->frmaction){

                        

                    	case 'deleted': {

                    		

                        	  if(!empty($this->getRequest()->selectedcheckbox)):

                            foreach($this->getRequest()->selectedcheckbox as $resid){

                            	

                            	

                                try{

                               $this->database->update(

                                        $this->tp.'restaurant_customer_order',

                                        array('status' => '1'),

                                        'id = '.$resid

                                        

                                    );

                                    $response->success=true;

                                    

                                }		

                                catch(Exception $e){

                                    //do nothing

                                }

                            }

                            endif; 

                            break;

                        }

                        

                        case 'publish': {

						

                        	

                        	 if(!empty($this->getRequest()->selectedcheckbox)):

                            foreach($this->getRequest()->selectedcheckbox as $resid){

                                try{

                                	

                                    $this->database->update(

                                        $this->tp.'restaurant_customer_order',

                                        array('status' => 0),

                                        'id = '.$resid

                                        

                                    );

                                    

                                   $response->success=true;  

                                }catch(Exception $e){

                                    //do nothing

                                }

                            }

                            endif;

                            break;

                           }

                      }

                     }

						catch(Exception $e){

						

							$response->error = true;

							$response->errorMessage = $e->getMessage();

						 }

						 

						 // sorting by different Status

						 try{

						 	  if(!empty($this->getRequest()->filterbystatus)) 

						 	  {

						 	

                               if($this->getRequest()->filterbystatus=="Deleted")

                               {

                               try{

	     

						 			 $where_clause='AND deleted = 1';	

						 				

						 			}

						 			catch (Exception $e)

						 			{

						 				$response->error = true;

							             $response->errorMessage = $e->getMessage();

						 				

						 			}

						 					 				

						 			}

						 			

						 			else 

						 			{

						 			 $where_clause='AND deleted = 0 ';	

						 				

						 			}

						 	

						 	}

						 

						 }

						 catch(Exception $e)

						 {

						 	

						 	$response->error = true;

							$response->errorMessage = $e->getMessage();

						 	

						 	

						 }

						 

						 //order of data

						 if(!empty($this->getRequest()->sortorder))

						 {

						 	

						 	switch ($this->getRequest()->sortorder)

						 	{

						 		

						 		case 'desc':

						 			{

						 				

						 			$sort_order='order by total_amount'.$this->getRequest()->sortorder;	

				

						 				

						 			}

						 			

						 		default :

						 			

						 			{

						 				

						 			$sort_order='order by total_amount asc';		

						 				

						 				

						 			}

						 			

						 		

						 		

						 		

						 		

						 	}

						 	

						 	

						 	

						 	

						 }

						 

				if(!empty($this->getRequest()->Order_Date)&&!empty($this->getRequest()->Order_from))

				{		 

						 

					try{

						 	

						  $Order_from = new DateTime($this->getRequest()->Order_from);

						  $order_date= "'".$Order_from->format("Y-m-d H:i:s")."'";

						  $Order_to = new DateTime($this->getRequest()->Order_to);

						  $orders_to= "'".$Order_to->format("Y-m-d H:i:s")."'";

						  $order_date=' AND  order_date BETWEEN '. $order_date.''.'AND'.''.$orders_to;

						 

						 }

						 catch (Exception $e)

						 {

						 	

						 $response->error = true;

						 $response->errorMessage = $e->getMessage();	

						 	

						 	

					 }

				}

				

				

		  }

						

				

    

     	  try{

     	  	

     	  	    

                

                $response->restaurants->count = 0;

                

                $queryStr = "SELECT id, restaurant_name, firstname, lastname,address,postcode,status,city, Order_date, delivery_date,payment,direction,order_domain, total_amount " .

                   

                "FROM {$this->tp}restaurant_customer_order"." "."INNER JOIN "." "."{$this->tp}restaurants ON {$this->tp}restaurants.restaurant_id = {$this->tp}restaurant_customer_order.restuarant_service_id 

                WHERE restuarant_service_id={$id}".

                 " ".(!empty($where_clause) ? $where_clause : '')."  ".(!empty( $order_date) ? $order_date : '')." ".(!empty($sort_order) ? $sort_order : '')." LIMIT 0 , 30" ;



                $response->restaurants->list = $this->database->fetchAll($queryStr);

 

                $response->restaurants->count = count($response->restaurants->list);

  

            }

            catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $this->translate('Failed to load restaurant list. Please re-try again.');

            }

             $response->test=$this->test;

             $response->count=$this->count;

             $this->view->assign((array)$response);

             $this->view->sidebar_menu_item = 'Orders';

             $this->view->headTitle($this->translate('Dashboard').' | ', 'PREPEND');

        }

        

    // order detail

	    public function detailAction(){

     	       $id=$_GET['id'];

               $response = new stdclass;

               $response->error = false;

               $response->success = false;

               

                /* Filter Requests */

              

                $where_clause=null;

                $sort_by='asc';

                $order_date=null;

                $deliver_date=null;

                // post requests //

		  if($this->getRequest()->isPost()){

		  	 

                try{

                    switch($this->getRequest()->frmaction){

                        

                    	case 'deleted': {

                    		

                        	  if(!empty($this->getRequest()->selectedcheckbox)):

                            foreach($this->getRequest()->selectedcheckbox as $resid){

                            	

                            	

                                try{

                               $this->database->update(

                                        $this->tp.'restaurant_customer_order',

                                        array('deleted' => '1'),

                                        'id = '.$resid

                                        

                                    );

                                    $response->success=true;

                                    

                                }		

                                catch(Exception $e){

                                    //do nothing

                                }

                            }

                            endif; 

                            break;

                        }

                        

                        case 'publish': {

                        	

                        	 if(!empty($this->getRequest()->selectedcheckbox)):

                            foreach($this->getRequest()->selectedcheckbox as $resid){

                                try{

                                	

                                    $this->database->update(

                                        $this->tp.'restaurant_customer_order',

                                        array('deleted' => 0),

                                        'id = '.$resid

                                        

                                    );

                                    

                                   $response->success=true;  

                                }catch(Exception $e){

                                    //do nothing

                                }

                            }

                            endif;

                            break;

                           }

                      }

                     }

						catch(Exception $e){

						

							$response->error = true;

							$response->errorMessage = $e->getMessage();

						 }

						 

						 // sorting by different Status

						 try{

						 	  if(!empty($this->getRequest()->filterbystatus)) 

						 	  {

						 	

                               if($this->getRequest()->filterbystatus=="Deleted")

                               {

                               try{

	     

						 			 $where_clause='AND deleted = 1';	

						 				

						 			}

						 			catch (Exception $e)

						 			{

						 				$response->error = true;

							             $response->errorMessage = $e->getMessage();

						 				

						 			}

						 					 				

						 			}

						 			

						 			else 

						 			{

						 			 $where_clause='AND deleted = 0 ';	

						 				

						 			}

						 	

						 	}

						 

						 }

						 catch(Exception $e)

						 {

						 	

						 	$response->error = true;

							$response->errorMessage = $e->getMessage();

						 	

						 	

						 }

						 

						 //order of data

						 if(!empty($this->getRequest()->sortorder))

						 {

						 	

						 	switch ($this->getRequest()->sortorder)

						 	{

						 		

						 		case 'desc':

						 			{

						 				

						 			$sort_order='order by total_amount'.$this->getRequest()->sortorder;	

				

						 				

						 			}

						 			

						 		default :

						 			

						 			{

						 				

						 			$sort_order='order by total_amount asc';		

						 				

						 				

						 			}

						 			

						 		

						 		

						 		

						 		

						 	}

						 	

						 	

						 	

						 	

						 }

						 

				if(!empty($this->getRequest()->Order_Date)&&!empty($this->getRequest()->Order_from))

				{		 

						 

					try{

						 	

						  $Order_from = new DateTime($this->getRequest()->Order_from);

						  $order_date= "'".$Order_from->format("Y-m-d H:i:s")."'";

						  $Order_to = new DateTime($this->getRequest()->Order_to);

						  $orders_to= "'".$Order_to->format("Y-m-d H:i:s")."'";

						  $order_date=' AND  order_date BETWEEN '. $order_date.''.'AND'.''.$orders_to;

						 

						 }

						 catch (Exception $e)

						 {

						 	

						 $response->error = true;

						 $response->errorMessage = $e->getMessage();	

						 	

						 	

					 }

				}

				

				

		  }

						

				

    

     	  try{

     	  	    $response->restaurants->count = 0;

                

                $queryStr = "SELECT * " .

                   

                "FROM {$this->tp}restaurant_customer_order"."  

                WHERE id={$id}".

                 " ".(!empty($where_clause) ? $where_clause : '')."  ".(!empty( $order_date) ? $order_date : '')." ".(!empty($sort_order) ? $sort_order : '')." LIMIT 0 , 30" ;



			

                $response->restaurants->list = $this->database->fetchAll($queryStr);

 

                $response->restaurants->count = count($response->restaurants->list);

  

            }

            catch(Exception $e){

                $response->error = true;

                $response->errorMessage = $this->translate('Failed to load restaurant list. Please re-try again.');

            }

             $response->test=$this->test;

             $response->count=$this->count;

             $this->view->assign((array)$response);

             $this->view->sidebar_menu_item = 'detail';

             $this->view->headTitle($this->translate('Dashboard').' | ', 'PREPEND');

        }

     

    } 

    

     



?>