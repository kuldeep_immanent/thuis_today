<?php

    
    include('CommonController.php');
    class Admin_MainController extends Zend_Controller_Action
	{
        
        public $database;
        public $tp; //Table prefix
        public $config;
        public $appConfig;
        public $session;
        public $translator;
        public $test;
        public $count;
        public $_C_helper;
        public $checkhelper;
        public function init(){
            //initializing resources
            $bootstrap        = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database   = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp         = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig  = $bootstrap->getResource('AppConfig');
            $this->config     = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session    = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
            $this->test       = commonnotify::Notification();
            $this->count      = commonnotify::total_count();
            $this->_C_helper  = $this->getHelper('Custom');
          
            //initializing layout
            Zend_Layout::startMvc(array(
                'layoutPath' => APPLICATION_PATH . '/modules/admin/views/layouts',
                'layout' => 'main'
            ));
            //adding default page title
            $this->view->headTitle('Admin Panel | Food Order System |');
        }
        
        public function preDispatch(){
            if(!$this->session->admin->loggedin and $this->getRequest()->getCookie('rememberme')){
                //auto login the user
                try{
                    $user = $this->database->fetchRow("select * from {$this->tp}admin_users where rememberme = ?", $this->getRequest()->getCookie('rememberme'));
                    if($user){
                        $this->session->admin = (object)array(
                            'loggedin' => true,
                            'id' => $user->user_id,
                            'name' => $user->user_name,
                            'privileges' => unserialize($user->user_privileges),
                            'is_super_admin' => (bool)$user->is_super_admin 
                        );
                        //updating last login date time
                        $this->database->update(
                            $this->tp."admin_users",
                            array("last_login_on" => new Zend_Db_Expr('now()')),
                            'user_id = '.$user->user_id
                        );
                        //forwarding to dashboard
                        $this->getHelper('Redirector')->gotoRoute(array(), 'admin-dashboard', true);
                    }
                }catch(Exception $e){
                    echo $e->getMessage(); exit;
                }
            }
        }
        
        public function translate($message){
            return $this->translator->translate($message);
        }
        
        public function postDispatch(){
            //adding few variables to view
            $this->view->request = $this->getRequest();
            $this->view->session = $this->session;
            $this->view->config = $this->config;
            $this->view->appConfig = $this->appConfig;
        }
        
        public function indexAction(){
            
        }
        
        public function loginAction(){
		
		    
            $this->getHelper('Layout')->disableLayout();
            
            $response = new stdclass;
            $response->error = false;
            $response->rememberme = false;
            
            if($this->session->admin->loggedin) $this->getHelper('Redirector')->gotoRoute(array(), 'admin-dashboard', true);
            
            if($this->getRequest()->isPost()){
                try{
                    if(!Zend_Validate::is($this->getRequest()->username, 'NotEmpty') || !Zend_Validate::is($this->getRequest()->password, 'NotEmpty'))
                        throw new Exception($this->translator->translate('All fields are mandatory.'));
                    //validating the login details  
                    $user = $this->database->fetchRow("select * from {$this->tp}admin_users where user_email = ? and user_password = md5(?)", array($this->getRequest()->username, $this->getRequest()->password));
                    if(!$user) throw new Exception($this->translator->translate('Invalid login details.'));
                    if((bool)$user->is_active == false) throw new Exception($this->translator->translate('Your account is deactivated.'));
                    if(isset($this->getRequest()->remember)){
                        $rememberme_code = md5($user->user_id.time());
                        setcookie('rememberme', $rememberme_code, time() + (30*24*60*60));
                        //updating remember me code
                        $this->database->update(
                            $this->tp.'admin_users',
                            array('rememberme' => $rememberme_code),
                            'user_id = '.$user->user_id
                        );
                    }
                    else{
                        if($this->getRequest()->getCookie('rememberme')){
                            setcookie('rememberme', '', (time() - 3600));
                        }
                    }
                    $this->session->admin = (object)array(
                        'loggedin' => true,
                        'id' => $user->user_id,
                        'name' => $user->user_name,
                        'privileges' => unserialize($user->user_privileges),
                        'is_super_admin' => (bool)$user->is_super_admin 
                    );
                    //updating last login date time
                    $this->database->update(
                        $this->tp."admin_users",
                        array("last_login_on" => new Zend_Db_Expr('now()')),
                        'user_id = '.$user->user_id
                    );
                    //forwarding to dashboard
                    $response->test=$this->test;
                    $response->count=$this->count;
                    $this->getHelper('Redirector')->gotoRoute(array(), 'admin-dashboard', true);
                }
                catch(Exception $e){
                    $response->error = true;
                    $response->errorMessage = $e->getMessage();
                }
            }
            
            if(isset($this->getRequest()->rememberme)){
                $response->rememberme = true;
            }
                 
            $this->view->assign((array)$response);
            $this->view->headTitle($this->translate('Login').' | ', 'PREPEND');
        }
        
        public function logoutAction(){
            //removing rememberme cookie, if set
            if($this->getRequest()->getCookie('rememberme')) setcookie('rememberme', '', (time() - 3600));
            unset($this->session->admin);
            $this->session->admin->loggedin = false;
            $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
        }
        
        public function dashboardAction(){
       
            
            $dasboard                = new stdClass;
            $dashboard->error        = false;
            $dashboard->errormessage = null;
            $weekly_report           = $this->_C_helper->Weekly_Report();
            try
            {
            $total_new_orders        = $this->_C_helper->G_Get_Data("SELECT count(id) as  id  FROM {$this->tp}restaurant_customer_order where status=3");
                        
            $dashboard->total_number_new_order =($total_new_orders[0]->id>0 ? $total_new_orders[0]->id : 0);
            
            $total_profit            = $this->_C_helper->G_Get_Data("SELECT sum(total_amount) as  amount  FROM {$this->tp}restaurant_customer_order where status=0");
            
            
            $dashboard->total_profit =(count($total_profit)>0 ? $total_profit[0]->amount : 0);
            
            $nodelivery              = $this->_C_helper->G_Get_Data("SELECT count(restaurant_id) as  id  FROM {$this->tp}restaurants ");
            $dashboard->Total_Delivery_Centers =($nodelivery[0]->id>0 ? $nodelivery[0]->id : 0);
            
            }
            catch(Exception $e)
            {
                
                   $dashboard->error        = true;
                   $dashboard->errormessage  = $e->getMessage();  
                
            }
            
            
            
            if($weekly_report->error):
                  $dashboard->error        = true;
                  $dashboard->errormessage = $weekly_report->errormessage;
            else:
                  $dashboard->data         = $weekly_report->data;
            endif;    
                
            
            $this->view->test   = $this->test;
            $this->view->count  = $this->count; 
            $this->view->sidebar_menu_item = 'dashboard';
            $this->view->assign((array)$dashboard);
            $this->view->headTitle($this->translate('Dashboard').' | ', 'PREPEND');
        }
        
        public function ajaxrecoverpasswordAction(){
            $response = new stdclass;
            $response->error = false;
            $response->success = false;
            
            try{
                if(!Zend_Validate::is($this->getRequest()->email, 'NotEmpty')) throw new Exception($this->translate('You must enter your account email address.'));
                //finding any account matching email 
                $account = $this->database->fetchRow("select * from {$this->tp}admin_users where user_email = ?", $this->getRequest()->email);
                if(!$account) throw new Exception($this->translate('No account found matching specified email address.'));
                $temppassword = strtoupper(substr(uniqid(), 0, 10));
                //updating the password and sending email
                $this->database->update(
                    $this->tp.'admin_users',
                    array('user_password' => md5($temppassword)),
                    'user_id = '.$account->user_id
                );
                $mail = new Zend_Mail('utf-8');
                $mail->setSubject('kebapiste.com : '.$this->translate('Your account new password'));
                $mail->addTo($account->user_email, $account->user_name);
                $this->getHelper('ViewRenderer')->setNoRender();
                $this->getHelper('Layout')->disableLayout();
                $this->view->mail_subject = $mail->getSubject();
                $this->view->user_name = stripslashes($account->user_name);
                $this->view->temppassword = $temppassword;
                $mail->setBodyHtml($this->view->render('mails/recover_password.phtml'));
                $this->getHelper('Layout')->enableLayout();
                $this->getHelper('ViewRenderer')->setNoRender(false);
                $this->getHelper('Mailer')->sendMail($mail);
                $response->success = true;
            }catch(Exception $e){
                $response->error = true;
                $response->errorMessage = $e->getMessage();
            }
            
            $this->getHelper('Json')->sendJson($response);
        }
        
        public function myprofileAction(){
            
            $response = new stdclass;
            $response->error = false;
             $response->test=$this->test;
              $response->count=$this->count;
            
            $this->view->headTitle($this->translate('My Profile').' | ', 'PREPEND');   
        }
        /* orders detail of every order */
        public function orderAction()
        {
           $response = new stdclass;
           $response->error = false;
           $response->test=$this->test;
           $response->count=$this->count;
            
            $this->view->headTitle($this->translate('Orders').' | ', 'PREPEND');
        
        
        
        
        }

         /*******
         * 
         * function to  take the backup of database
         */
        public function backupAction()
        {
           $config   =  new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini', 'staging');
         
            $config  =  $config->toArray();
            $params  =  $config['resources']['db']['params'];
			$baseurl =  $params['baseurl'];
            
            $file    = $this->_C_helper->backup_tables('localhost',$params['username'],$params['password'],$params['dbname']);
           
		   
		   
		    header('Content-Type: file/sql');
            
            header('Content-Disposition: attachment; filename='.$file);   	   
            
            readfile($baseurl.'/'.$file);  
 
            $this->view->layout()->disableLayout();

            $this->_helper->viewRenderer->setNoRender(true);
            
        }


        
    }