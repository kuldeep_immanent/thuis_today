<?php

class mealhelper extends Zend_Controller_Action{
	
	    public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
        public $count;
        
        
  /* initialising the  all parent  vallue and database */
        
        public  function  init()
        {
        	
        	 //initializing resources by singleton instance
        	 $bootstrap=Zend_Controller_Front::getInstance()->getParam('bootstrap');
        	 $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
        	 $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
        	 $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
             $this->appConfig = $bootstrap->getResource('AppConfig');
             $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
             $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
             $this->translator = Zend_Registry::get('Zend_Translate');
         
        	
        }
        
         // Setting the preprocess function before all action be performed
        public function preDispatch()
        {
        	
        	//checking if session is created //
        	  if(!$this->session->admin->loggedin)
               $this->getHelper('Redirector')->gotoRoute(array(), 'admin-login', true);
	            //adding few variables to view
	        	$this->view->request = $this->getRequest();
	            $this->view->session = $this->session;
	            $this->view->config = $this->config;
	            $this->view->appConfig = $this->appConfig;	
        	
        }
 
        // Getting all meals of particular restaurant
        public function get_all_restaurant_meals($res_id)
        {
        	
        	$r_meals= new stdclass();
        	
        	$r_meals->error=false;
        	$r_meals->errormessage=null;
        	  
        	
        	
         
        	
        	try
        	{
        		
        		$meals_data=$this->database->select()->from($this->tp.'restaurant_meals',array('meal_id','meal_name'))->where('restaurant_id = ?',$res_id);
        		
        		
        		$r_meals->data = $this->database->fetchAll($meals_data);
        		
        		
        		
        		
        	}
        	catch (Exception $e)
        	{
        		$r_meals->error=true;
        		$r_meals->errormessage=$e->getMessage();
        		
        		
        	}
        	
        		
        	
        	return $r_meals;
        	
        	
        	
        	
        }
        
        
        
        public function  particular_meal_orders($array ,$restaurant_id,$start_date=null)
        {
         $orders=new stdclass;
         $orders->error=false;
         $orders->errormessage=null;
         
         if($array->error == true)
         {
         	
         $orders->error = true;
         $orders->errormessage = $array->errormessage;	
         	
         	
         }
         else 
         {
         	$today_date = date("Y-m-d");
         	
             if(!is_null($start_date))
             {
             	$cluase = " order_date BETWEEN '{$start_date}' AND '{$today_date}'";
             	
             	
             	
             }
             else {
             	
             $cluase = "order_date  = '{$today_date }'";	
             	
             	
             	
             }
         	
	
         try
         {
 	
         	//print_r($orders->data[0]->order_date);
         
         	$product_report=array();
         	$initial=0;
         	
         	foreach($array->data as $arr):
         	
         	 $id=$arr->meal_id;
         	 
         	$order = "SELECT sum(total) as total, sum(quantity) as quantity FROM {$this->tp}restaurant_per_item_order_report" . "  WHERE res_id={$restaurant_id} " . " AND  $cluase  AND item_id ={$id} " ; 
         	
            
         		
         	$product_order = $this->database->fetchAll($order);
         
         	
         	 if($product_order[0]->total == "")
         	 {
         	 	
         	 	$product_order[0]->total = 0;
         	 	$product_order[0]->quantity = 0;
         	 	
         	 }
         
         	
         	$product_report['name'][$initial] = $arr->meal_name;
         	$product_report['total'][$initial] = $product_order[0]->total;
         	$product_report['quantity'][$initial] = $product_order[0]->quantity;
         	
         
         	
            $initial++;
             
         	endforeach;
         
         	$orders->data=$product_report;
         
         		

         	
         }
         catch(Exception $e)
         {
         	
         	
         $orders->error=true;
         $orders->errormessage=$e->getMessage();	
         	
         	
         }
         
         }
        
         return $orders;
         
         }
        	
        	
        	//  function for getting the commision per order
        	
         public function get_commission_per_order()
         {
         	
         	$commission = new stdclass;
         	$commission->error = false;
         	$commission->errormessage = null; 
         	
         	try {
         		
         		$get_value = $this->database->select()->from($this->tp.'restaurants',array('per_order_commision','restaurant_id','restaurant_name'));
         		
         		$get_value = $this->database->fetchAll($get_value);
         		
         			
         		
         		$today_date = date("Y-m-d H:i:s");
				
				$timestamp = time();
				
				for ($i = 0 ; $i < $size ; $i++) {
					// echo date('Y-m-d H:i:s', $timestamp) . '<br />';
					$timestamp -= 24 * 3600;
				}
				 
				 	$last_week_date = date("Y-m-01 H:i:s");
				 	
				 	
				 $commission->data=array();
				 $initial=0;
				 foreach($get_value as $getvar):
			       
			
         		
         		$get_data = " SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE status = 0  AND restuarant_service_id ={$getvar->restaurant_id} ". " AND order_date BETWEEN  '{$last_week_date}' " . " AND '{$today_date}' ORDER BY order_date ";
         		
         		$get_data = $this->database->fetchrow($get_data);
         		
         		$percentage =($getvar->per_order_commision / 100) * $get_data->sum;
         		
         		if($get_data->sum == ""):
         		
         		$get_data->sum = 0;
         		
         		$percentage=0;
         		
         		endif;

              $paid_data = " SELECT sum(paid) as sumpaid FROM {$this->tp}restaurant_invoice WHERE  restaurant_id ={$getvar->restaurant_id} ". " AND paid_date BETWEEN  '{$last_week_date}' " . " AND '{$today_date}' ORDER BY paid_date ";
        

                $paid_data = $this->database->fetchrow($paid_data);
                if(count($paid_data) >0)
                {
                if($paid_data->sumpaid == ""):
                $paid = 0;
                else:
                     $paid = $paid_data->sumpaid;
                endif;
               }
                
                $commission->data['restaurant_id'][$initial] = $getvar->restaurant_id; 
         		$commission->data['name'][$initial] = $getvar->restaurant_name; 
         		$commission->data['percentage'][$initial]= $getvar->per_order_commision;
         		$commission->data['total'][$initial] = $get_data->sum; 
         		$commission->data['commission'][$initial] = $percentage;
                $commission->data['paid'][$initial] = $paid;
         		
         		
         		$initial++;
         		endforeach;		
         	}
           	catch(Exception $e)
         	{
         		
         	$commion->error = true;
         	$commion->errormessage = $e->getMessage(); 	 		       		
         	}   
            	
         	return $commission;     	
         }
       
	 // get paid amount for single restaurant 


          //  function for getting the commision per order
            
         public function get_commission_per_order_by_id($res_id)
         {  
          
            
            $commission = new stdclass;
            $commission->error = false;
            $commission->errormessage = null; 
            
            try {
                 
                 $query = "SELECT * FROM {$this->tp}restaurants WHERE restaurant_id = $res_id";
                 
                 $getvar = $this->database->fetchrow($query);
                 

                $today_date = date("Y-m-d H:i:s");
                $last_week_date = date("Y-m-01 H:i:s");
                    
                    
                 $commission->data=array();
                $get_data = " SELECT sum(total_amount) as sum FROM {$this->tp}restaurant_customer_order" . " WHERE status = 0  AND restuarant_service_id ={$getvar->restaurant_id} ". " AND order_date BETWEEN  '{$last_week_date}' " . " AND '{$today_date}' ORDER BY order_date ";
                
                $get_data = $this->database->fetchrow($get_data);
                
                $percentage =($getvar->per_order_commision / 100) * $get_data->sum;
                
                if($get_data->sum == ""):
                
                $get_data->sum = 0;
                
                $percentage=0;
                
                endif;      

               

             $paid_data = " SELECT sum(paid) as sumpaid FROM {$this->tp}restaurant_invoice WHERE  restaurant_id ={$getvar->restaurant_id} ". " AND paid_date BETWEEN  '{$last_week_date}' " . " AND '{$today_date}' ORDER BY paid_date ";
        

                $paid_data = $this->database->fetchrow($paid_data);
                if(count($paid_data) >0)
                {
                if($paid_data->sumpaid == ""):
                $paid = 0;
                else:
                     $paid = $paid_data->sumpaid;
                endif;
               }
                $commission->data['restaurant_id'] = $getvar->restaurant_id; 
                $commission->data['restaurant_name'] = $getvar->restaurant_name; 
                $commission->data['per_order_commision']= $getvar->per_order_commision;
                $commission->data['total']= $get_data->sum; 
                $commission->data['commission']= $percentage;
                $commission->data['paid']= $paid;

                }
            catch(Exception $e)
            {
                
            $commion->error = true;
            $commion->errormessage = $e->getMessage();                      
            }   
                
            return $commission;         
         }
        

   /****************************
   monthly detail of invoice 
    ***********************************/
   public function get_monthly_detail($id,$yy)
   {
        $commition = new stdclass;
        $commision->error = false;
        $commision->errormessage = null; 
        try 
        {
        $query = "SELECT per_order_commision FROM {$this->tp}restaurants WHERE restaurant_id = $id";      
        $getvar = $this->database->fetchrow($query);
        $percommision =  $getvar->per_order_commision;
        $querysale = "SELECT sum(total_amount) as sum , MONTHNAME(order_date) as ordermonth FROM {$this->tp}restaurant_customer_order" ." where restuarant_service_id = $id and YEAR(order_date) =$yy and status = 0 GROUP  BY (Month(order_date)) order by Month(order_date)";

        $order = $this->database->fetchAll($querysale);
     
         $result = array();
          foreach($order as $key=>$val){

           $data['total']  =  $val->sum;
          
           $data['commision'] = (($percommision / 100) * $val->sum);  
           $data['month'] = $val->ordermonth;

           $querypaid = "SELECT  sum(paid) as paidtotal FROM {$this->tp}restaurant_invoice" ." where restaurant_id = $id  and MONTHNAME(paid_date) = '$val->ordermonth' and YEAR(paid_date) =$yy";
          
          $paid = $this->database->fetchrow($querypaid);
         if(!empty($paid))
         {
            $data['paid'] = $paid->paidtotal;

         }
         else{
            $data['paid']  = 0;
         }

         // stor all data  
         $result[] = $data;
             }
        
         $commision->data = $result;
        }
        catch(Exception $e)
        {
          $commision->error = true;
          $commision->errormessage = $e->getMessage();

        }

        return $commision->data;

   }  


   /* function to translate the language */
			
			public  function translate()
			{
				
				
				  return $this->translator->translate($message);
				
				
				
			}
			
	// Get Order Report  of every particular  Meal of particular Restaurant

	
	
	
	
	
	
	
	
}







?>