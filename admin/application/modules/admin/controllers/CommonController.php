<?php

class commonnotify extends Zend_Controller_Action 
{
 
	    public $database;
        public $tp; //Table prefix
        public $appConfig;
        public $config;
        public $session;
        public $translator;
        public $test;
	
	
	  public function  init()
	 {
	 	
	 	    $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->database = $bootstrap->getPluginResource('db')->getDbAdapter();
            $this->database->setFetchMode(Zend_Db::FETCH_OBJ);
            $this->tp = Zend_Controller_Front::getInstance()->getParam('TablePrefix');
            $this->appConfig = $bootstrap->getResource('AppConfig');
            $this->config = Zend_Controller_Front::getInstance()->getParam('Config');
            $this->session = Zend_Controller_Front::getInstance()->getParam('Session');
            $this->translator = Zend_Registry::get('Zend_Translate');
	 	
	 }
	 

	public function Notification()
	
	{
		
	            $notify=new stdclass;
				$notify->error=false;
				$notify->success=false;
				
				$notify->errormessage=false;
				/* check for adding the main concept */
				try {
					
				// quering the database  for fething extract data //
				  $id= $this->session->admin->id;
				  
				 $query=$this->database->select()->from( $this->tp.'restaurant_messages')->where('super_admin_id ='.$id.' '.'AND status = 1 AND send_by = "admin" ');	
                 
				
				 
					$result=$this->database->fetchall($query);
				
					
				  
					 $start=0;
					 
					 $sendby=null;
					 
					foreach($result as $res)
					{
						$notify->user->data['message'][$start]=$res->message;
						$notify->user->data['time'][$start]=$res->time;
						$notify->user->data['status'][$start]=$res->status;
						$notify->user->data['user_id'][$start]=$res->res_admin_id;
						
						$sendby=$res->send_by;
		
						if($sendby == 'admin')
						{

							try
							{
								
                             $id=$res->res_admin_id;
						     $info=$this->database->select()->from( $this->tp.'restaurant_admin_users')->where('restaurant_id = ?', $id);	
					         $result=$this->database->fetchrow($info);
					         $notify->user->data['username'][$start]=$result->user_name;
                            
                           
							}
							
								catch(Exception $e)
								{
								$notify->error=true;
								
								$notify->errormessage=$this->translate('error found'.$e);
								
								
							}
	
						}
						
						
						$start++;
						
					}
		
		
		
	}
	catch (Exception $e)
	{
		
		
		$notify->error=true;
								
	    $notify->errormessage=$this->translate('error found'.$e);
		
		
	}
	
	
	return $notify;
	
	}
	
	
	 function  total_count()
	 {
	 	
	 	       $count=new stdclass;
				$count->error=false;
				$count->success=false;
				
				$count->errormessage=null;
				/* check for adding the main concept */
				try {
					
				// quering the database  for fething extract data //
				  $id= $this->session->admin->id;
				  
				 $query=$this->database->select()->from( $this->tp.'restaurant_messages')->where('super_admin_id ='.$id.' '.'AND status = 1 AND send_by = "admin" ');	
               
				 
					$result=$this->database->fetchall($query);
					
					$count->count=count($result);
					 
				
					
				
					
	 	
	 	
	 	
	 	
	 }
	 
	 catch(Exception $e)
	 {
	 	
	 	$count->error=true;
	 	$count->errormessage=$this->translate('error found'.$e);
	 	
	 	
	 	
	 }
	 
	 return $count;
	 
	 
	 }
	 
	 
	 
	 //ajax response//
	 
	 

	 //seperate action response//
	 
public  function response()
	 {
	 	
	 	try {
						
					
				// quering the database  for fething extract data //
				
				  $id= $this->session->admin->id;
				  $user_id=$this->getRequest()->to;
				  
				 $query=$this->database->select()->from( $this->tp.'restaurant_messages')->where('super_admin_id ='.$id.' AND res_admin_id ='.$user_id);	
                    
					$result=$this->database->fetchall($query);
				
					
					 $start=0;
					 
					 $sendby=null;
					 
					foreach($result as $res)
					{
						$response->user->data['message'][$start]=$res->message;
						$response->user->data['time'][$start]=$res->time;
						$response->user->data['status'][$start]=$res->status;
						
						$sendby=$res->send_by;
		
						if($sendby == 'super_admin')
						{

							try
							{
								
                             $id=$res->super_admin_id;
								
							$info=$this->database->select()->from($this->tp.'admin_users')->where('user_id = ?',$id);
								
						    $super_admin_array=$this->database->fetchrow($info);
						    
                            $response->user->data['username'][$start]=$super_admin_array->user_name;
                            
                           
							}
							
							catch(Exception $e)
							{
								$response->error=true;
								
								$response->errormessage=$this->translate('error found'.$e);
								
								
							}
	
						}
						else 
						{
						 $id=$res->res_admin_id;
						 
						 try{
						 $info=$this->database->select()->from( $this->tp.'restaurant_admin_users')->where('restaurant_id = ?', $id);	
					     $result=$this->database->fetchrow($info);
					     
					      $response->user->data['username'][$start]=$result->user_name;	
							
						 }
						 catch(Exception $e)
						 {
						 $response->error=true;	
						 	
						 	
						 	
						 }
							
						}
						
						
						$start++;
						
					}
							
					
				}
				
				catch (Exception $e)
				
				{
					$response->error=false;
					
					$response->errormessage=$this->translate('query exception occured');
	
					
				}
				if($response->error==false)
				{
				?>
				<ul class="chats" id="myMsg">
								     
							       <?php 
							       
							     
                                     
							       foreach($response->user as $user ){
							      

							       	for($initial=0; $initial<count($user['message']);$initial++)
							       	{

							       		
							       		if($user['username'][$initial]=="Superadmin"){
							       	?>
								  
								  
							        <li class='out'>
										<img class="avatar img-responsive" alt="" src="Resturant/assets/img/defaultpic.png"  />
										<div class="message">
											<span class="arrow"></span>
											<a href="#" class="name"><?php echo "Me"; ?></a>
											<span class="datetime"> <span  class="space" >at</span><?php echo $user['time'][$initial]; ?></span>
											<span class="body">
											<?php echo $user['message'][$initial];  ?>;
											</span>
										</div>
									</li>
									
									 <?php  
							       		}
							       		
									 	else
									 {?>
									 	
									 	<li class="in">
										<img class="avatar img-responsive" alt="" src="Resturant/assets/img/defaultpic.png" />
										<div class="message">
											<span class="arrow"></span>
											<a href="#" class="name"><?php echo $user['username'][$initial]; ?></a>
											<span class="datetime"> <span  class="space">at</span><?php echo $user['time'][$initial]; ?></span>
											<span class="body">
											<?php echo $user['message'][$initial];  ?>;
											</span>
										</div>
									</li>
									 	
									 	
									 	
									<?php  }
									 
							       	 } 
							       }
									//print_r($user);
									?>
									
								</ul>
				
				  <?php
				}
				else 
				{
					
					
					echo $response->errormessage ;   
				}
	 	
	 	
	 	
	 	
	 	
	 }
	 
	 
	 
	 
	
	
}

	
	







?>