<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    
    protected function _initLocale(){
        $locale = new Zend_Locale('en_US');
        Zend_Registry::set('Zend_Locale', $locale);
    }
    
    protected function _initTranslate(){
       
        Zend_Registry::set('Zend_Translate', $translate);
        $translate = new Zend_Translate(array(  
            'adapter' => 'gettext',
            'content' => APPLICATION_PATH . '/languages/en.mo',
            'locale' => 'ar'
        ));
        $translate->addTranslation(array('content' => APPLICATION_PATH . '/languages/en.mo', 'locale' => 'us'));
        Zend_Registry::set('Zend_Translate', $translate);    
    }
    
    protected function _initAppConfig(){
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/config.ini', 'production', array('allowModifications' => true));
        return $config;
    }
    
    protected function _initHelpers(){
        Zend_Controller_Action_HelperBroker::addPath( APPLICATION_PATH . '/helpers', 'FOS_Action_Helpers_');
        Zend_Controller_Action_HelperBroker::addPath( APPLICATION_PATH . '/modules/admin/helpers', 'FOS_Admin_Helpers_' );
    }

}

