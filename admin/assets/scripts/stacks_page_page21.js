
// 'stacks' is the Stacks global object.
// All of the other Stacks related Javascript will 
// be attatched to it.
var stacks = {};


// this call to jQuery gives us access to the globaal
// jQuery object. 
// 'noConflict' removes the '$' variable.
// 'true' removes the 'jQuery' variable.
// removing these globals reduces conflicts with other 
// jQuery versions that might be running on this page.
stacks.jQuery = jQuery.noConflict(true);

// Javascript for stacks_in_228_page21
// ---------------------------------------------------------------------

// Each stack has its own object with its own namespace.  The name of
// that object is the same as the stack's id.
stacks.stacks_in_228_page21 = {};

// A closure is defined and assigned to the stack's object.  The object
// is also passed in as 'stack' which gives you a shorthand for referring
// to this object from elsewhere.
stacks.stacks_in_228_page21 = (function(stack) {

	// When jQuery is used it will be available as $ and jQuery but only
	// inside the closure.
	var jQuery = stacks.jQuery;
	var $ = jQuery;
	
var $stackslider = jQuery.noConflict();
$(window).load(function(){
	
	var first = 0;
	var speed = 600;
	var pause = 2000;
	intervalstacks_in_228_page21 = setInterval(removeFirst, pause); 
	
		function removeFirst(){
			first = $stackslider('#stackSliderstacks_in_228_page21 div:first').html();
			$stackslider('#stackSliderstacks_in_228_page21 div:first')
			.animate({opacity: 0, height: 'hide'}, speed)
			.hide('slow', function() {$stackslider(this).remove();
			});
			addLast(first);
		}
		
		function addLast(first){
			last = '<div style="display:block">'+first+'</div>';
			$stackslider('#stackSliderstacks_in_228_page21').append(last)
			$stackslider('#stackSliderstacks_in_228_page21 div:last')
			.animate({opacity: 1, height: 'show'}, speed)
			.show('slow')
		}
		
		// Pauses stack slider on hover
		$stackslider('#stackSliderstacks_in_228_page21').hover(function() {
		clearInterval(intervalstacks_in_228_page21);
		},
		function() {
		intervalstacks_in_228_page21 = setInterval(removeFirst, pause);
		});
		
});
	return stack;
})(stacks.stacks_in_228_page21);


// Javascript for stacks_in_677_page21
// ---------------------------------------------------------------------

// Each stack has its own object with its own namespace.  The name of
// that object is the same as the stack's id.
stacks.stacks_in_677_page21 = {};

// A closure is defined and assigned to the stack's object.  The object
// is also passed in as 'stack' which gives you a shorthand for referring
// to this object from elsewhere.
stacks.stacks_in_677_page21 = (function(stack) {

	// When jQuery is used it will be available as $ and jQuery but only
	// inside the closure.
	var jQuery = stacks.jQuery;
	var $ = jQuery;
	
var $stackslider = jQuery.noConflict();
$(window).load(function(){
	
	var first = 0;
	var speed = 600;
	var pause = 10000;
	intervalstacks_in_677_page21 = setInterval(removeFirst, pause); 
	
		function removeFirst(){
			first = $stackslider('#stackSliderstacks_in_677_page21 div:first').html();
			$stackslider('#stackSliderstacks_in_677_page21 div:first')
			.animate({opacity: 0, height: 'hide'}, speed)
			.hide('slow', function() {$stackslider(this).remove();
			});
			addLast(first);
		}
		
		function addLast(first){
			last = '<div style="display:block">'+first+'</div>';
			$stackslider('#stackSliderstacks_in_677_page21').append(last)
			$stackslider('#stackSliderstacks_in_677_page21 div:last')
			.animate({opacity: 1, height: 'show'}, speed)
			.show('slow')
		}
		
		// Pauses stack slider on hover
		$stackslider('#stackSliderstacks_in_677_page21').hover(function() {
		clearInterval(intervalstacks_in_677_page21);
		},
		function() {
		intervalstacks_in_677_page21 = setInterval(removeFirst, pause);
		});
		
});
	return stack;
})(stacks.stacks_in_677_page21);


