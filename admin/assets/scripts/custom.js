/**
Custom module for you to write your own javascript functions
**/
var Custom = function () {
      //alert('hello');
    // private functions & variables

    var myFunc = function(text) {
        alert(text);
    }
    
    
    /*****
     * 
     *  function to switch  the content
     * 
     */
      var swichme =function()
      {
          
          
          $('#mySwitch').on('switch-change', function (e, data) {
               var $el = $(data.el)
                , value = data.value;
              console.log(e, $el, value);
          });  
          
          
          
      }
       var datetimesetup =function()
        {
            
            $('#Order_to').datetimepicker();
            $('#Order_from').datetimepicker();
             $('#time').datetimepicker();
           
        }
	// custom function to be loaded
	var loadfloatchart=function()
	{
	var placeholder = $("#placeholder"),
         data        = datavalues(); 
	
	             if(data !=null)  
				 {
	           $('#placeholder').show(); 
			   
			   }
         
                var previousPoint2 = null;
                $('#site_activities_loading').hide();
                    
	 
	     
	            var plot_activities = $.plot(
						placeholder, [{
						data: data,
						color: "rgba(107,207,123, 0.9)",
						shadowSize: 0,
						
						}
						],  
						{series: {
                        lines: {
                            show: true,
                            lineWidth: 4,
                            fill: true,
                            fillColor: {
                                colors: [{
                                        opacity: 0.05
                                    }, {
                                        opacity: 0.01
                                    }
                                ]
                            }
                        },
                        points: {
                            show: true
                        },
                        shadowSize:3
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderWidth: 0
                    },
                    colors: ["#d12610", "#37b7f3", "#52e136"],
                    xaxis: {
                        
                      
						mode:"time",  
						tickLength: 0,
						axisLabel: "Date",
						 timeformat: "%m/%d"
					
						
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0,
						mode:Date	
						
                    }
					
                });
		 $("#placeholder").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
							
                            var x = item.datapoint[0],
							
                                y = item.datapoint[1].toFixed(2);
								
								x = (new Date(x).getMonth() + 1)+'/'+(new Date(x).getDate());
								
                            
							showTooltip('Detail', item.pageX, item.pageY, x,y,'');
                        }
                    }
                });
				
				$('#placeholder').bind("mouseleave", function () {
                    $("#tooltip").remove();
                });
				

        //console.log(data);      						
	}
    
	var loadfloatchartweekly=function()
	{
	var placeholder = $("#placeholder_weekly"),
         data        = weekly_data(); 
	
	             if(data !=null)  
				 {
	           $('#placeholder_weekly').show(); 
			   
			   }
         
                var previousPoint2 = null;
                $('#site_activities_loading_weekly').hide();
                    
	 
	     
	            var plot_activities = $.plot(
						placeholder, [{
						data: data,
						color: "rgba(107,207,123, 0.9)",
						shadowSize: 0,
						
						}
						],  
						{series: {
                        lines: {
                            show: true,
                            lineWidth: 4,
                            fill: true,
                            fillColor: {
                                colors: [{
                                        opacity: 0.05
                                    }, {
                                        opacity: 0.01
                                    }
                                ]
                            }
                        },
                        points: {
                            show: true
                        },
                        shadowSize:3
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderWidth: 0
                    },
                    colors: ["#d12610", "#37b7f3", "#52e136"],
                    xaxis: {
                         ticks:[[1, "First Week"], [2, "Second Week"], [3, "Third Week"], [4, "last Week"]],
                        tickDecimals: false,
						 axisLabel: 'Weekly'


						
						
                       
                        
						
						
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0,
						mode:Date	
						
                    }
                });
					$("#placeholder_weekly").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
							
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                              if(x==1)
							  {
							  x ='First';
							  
							  
							  }else if(x==2)
							  {
							  x ='Second';
							  
							  }
							  else if(x==3)
							  {
							  x='Third';
							  }
							  else
							  {
							  
							  x='last';
							  
							  
							  }
							showTooltip('Detail', item.pageX, item.pageY, x,y,'Week');
                        }
                    }
                });
				
				$('#placeholder_weekly').bind("mouseleave", function () {
                    $("#tooltip").remove();
                });
				

              						
	}
	
	  // yearly load chart
	  
	  var loadfloatchartyealy=function()
	 {
	var placeholder = $("#placeholder_yearly"),
         
		 data        = yearly_report(); 
		 
		  
	    
	             if(data !=null)  
				 {
				 
	           $('#placeholder_yearly').show(); 
			   
			   
			   }
         
                var previousPoint2 = null;
                $('#site_activities_loading_yearly').hide();
                    
	 
	     
	            var plot_activities = $.plot(
						placeholder, [{
						data: data,
						color: "rgba(107,207,123, 0.9)",
						shadowSize: 0,
						
						}
						],  
						{series: {
                        lines: {
                            show: true,
                            lineWidth: 4,
                            fill: true,
                            fillColor: {
                                colors: [{
                                        opacity: 0.05
                                    }, {
                                        opacity: 0.01
                                    }
                                ]
                            }
                        },
                        points: {
                            show: true
                        },
                        shadowSize:3
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderWidth: 0
                    },
                    colors: ["#d12610", "#37b7f3", "#52e136"],
                    xaxis: {
                      
						 
                        mode: "time",
                        tickSize: [ 1,"month"],
                        monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                        tickLength: 0,
                        axisLabel: 'Monthly',
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels:12,
                        axisLabelPadding: 1
						
						
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0,
						mode:Date	
						
                    }
                });
					$("#placeholder_yearly").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
							   
                            var x = convertToDate(item.datapoint[0]),
                                y = item.datapoint[1].toFixed(2);
								
							
                             
							showTooltip('Detail', item.pageX, item.pageY, x,y,'');
                        }
                    }
                });
				
				$('#placeholder_yearly').bind("mouseleave", function () {
                    $("#tooltip").remove();
                });
				

              						
	}
        
        
       
        
      
	
    // public functions  
    return {

        //main function
        init: function () {
		
		    loadfloatchart();
			loadfloatchartweekly();
			loadfloatchartyealy();
			datetimesetup();
                        swichme();
            //initialize here something.            
        },

        //some helper function
        doSomeStuff: function () {
            myFunc();
        }

    }; 
 
}();
           //changing date into month
        function convertToDate(timestamp) {
        var newDate = new Date(timestamp);
        var dateString = newDate.getMonth();
        var monthName = getMonthName(dateString);
 
        return monthName;
         }
 


		   
		function getMonthName(numericMonth) {
          var monthArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
          var alphaMonth = monthArray[numericMonth];
 
         return alphaMonth;
    }
			function showTooltip(title, x, y, contentA,contentb,label) {
			$('<div id="tooltip"><div class="label label-success">'+ contentA +' '+label+' <\/div><div class="label label-danger">Totalsale: $' + contentb  + '<\/div></div>').css({
			position: 'absolute',
			display: 'none',
			top: y + 5,
			left: x + 15,
			border: '1px solid #333',
			padding: '4px',
			color: '#fff',
			'border-radius': '3px',
			'background-color': '#333',
			opacity: 0.80
			}).appendTo("body").fadeIn(200);
			}

		
			
			


/***
Usage
***/
//Custom.init();
//Custom.doSomeStuff();