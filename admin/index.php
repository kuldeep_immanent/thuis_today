<?php
ob_start();
ini_set('default_charset', 'UTF-8');
error_reporting(1);

date_default_timezone_set('Europe/Istanbul');

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));
	// Define path to upload directory
defined('UPLOAD_IMAGE')
      ||define('UPLOAD_IMAGE', '/uploads/');  
defined('APPLICATION_ASSETS')
      ||define('APPLICATION_ASSETS', '/assets/'); 
defined('GOOGLE_API_KEY')
      ||define('GOOGLE_API_KEY', 'AIzaSyA8fd6kJcYVZkuAouMnXymADN9yRX2W3c8'); 	
defined('UPLOAD_IMAGE')
    ||define('UPLOAD_IMAGE', '/uploads/');  
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
// define('APPLICATION_ENV', 'development');
define('BASEURLAPP', 'http://deliveryapp.demodemo.ga/restaurant/');
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();
/* error_reporting(E_ALL);
ini_set("display_errors", 1); */			

			?>