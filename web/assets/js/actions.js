// JavaScript Document
$(document).ready(function(){
	
		// Added 16.01.2017
	var today = new Date();
	$( "#datepickeropen" ).datepicker({minDate: today});
	$( "#datepicker2" ).datepicker({minDate: today});
	$( "#datepickerpick" ).datepicker({minDate: today});
	
	$(".select-shop").click(function(){
		$(".shop-options").toggle();
		$(".select-shop").toggleClass("orange");
	});
	
	
	$(".more-cats").click(function(){
		
		$(".tabs").toggleClass("more");
		$(".rest").toggle();
		
	});

	$(".filter-shops").click(function(){
		
		$(".filters-listing").toggle();
		
		
	});

	
	$(".opt1").click(function(){


			$("#datepickeropen").prop('disabled', false);
			$("#duringopeninghourlist").prop('disabled', false);
			$("#datepicker2").prop('disabled', true);
			$('#datepicker2').val('');
			$("#duringweekdayslist").prop('disabled', true);
			$('#duringweekdayslist').val('');
			$("#datepickerpick").prop('disabled', true);
			$('#datepickerpick').val('');
			$("#pickatlocationlist").prop('disabled', true);
			$('#pickatlocationlist').val('');

			$(".during-opening-hours").slideDown();
			$(".during-weekdays").slideUp();
			$(".ok-pickup").slideUp();
			$(".address-contact").slideDown();
	});

	$(".opt2").click(function(){


			$("#datepickeropen").prop('disabled', true);
			$('#datepickeropen').val('');
			$("#duringopeninghourlist").prop('disabled', true);
			$('#duringopeninghourlist').val('');
			$("#datepicker2").prop('disabled', false);
			$("#duringweekdayslist").prop('disabled', false);
			$("#datepickerpick").prop('disabled', true);
			$('#datepickerpick').val('');
			$("#pickatlocationlist").prop('disabled', true);
			$('#pickatlocationlist').val('');

			$(".during-opening-hours").slideUp();
			$(".during-weekdays").slideDown();
			$(".ok-pickup").slideUp();
			$(".address-contact").slideDown();
	});

		$(".opt3").click(function(){


			$("#datepickeropen").prop('disabled', true);
			$('#datepickeropen').val('');
			$("#duringopeninghourlist").prop('disabled', true);
			$('#duringopeninghourlist').val('');
			$("#datepicker2").prop('disabled', true);
			$('#datepicker2').val('');
			$("#duringweekdayslist").prop('disabled', true);
			$('#duringweekdayslist').val('');
			$("#datepickerpick").prop('disabled', false);
			$("#pickatlocationlist").prop('disabled', false);

			$(".during-opening-hours").slideUp();
			$(".during-weekdays").slideUp();
			$(".ok-pickup").slideDown();
			$(".address-contact").slideUp();
			
	});

	/* $(".zoom").click(function(){
		$(".modal-background3").show();
		$("#prductNameId").show();
		$("body").addClass("modal-body");
    }); */
   
		$(".zoom").hover(

				  function() {
				    $( this ).addClass( "big" );
				  }, function() {
				    $( this ).removeClass( "big" );
				  }
			);
	
		$(".order-details").click(
			
			function(){
				$(".modal-background").show();
				$("body").addClass("modal-body");
			}
			
		);
	
			$(".help").click(
			
			function(){
				$(".modal-background1").show();
				$("body").addClass("modal-body");
			}
			
		);
	
		$(".forgot").click(
			
			function(){
				$(".modal-background2").show();
				$("body").addClass("modal-body");
			}
			
		);
	
		$(".close").click(
			
			function(){
				$(".modal-background").hide();
				$(".modal-background1").hide();
				$(".modal-background2").hide();
				$("body").removeClass("modal-body");
			}
			
		);

		$(".ideal").click(
			function(){
				
				$(".ideal-price").show();
				$(".p-price").hide();
			}
		);
	
		$(".pp").click(
			function(){
				
				$(".ideal-price").hide();
				$(".p-price").show();
			}
		);
		$(".no").click(
			function(){
				
				$(".ideal-price").hide();
				$(".p-price").hide();
			}
		);
		$(".close").click(function(){
			$(".modal-background3").hide();
			$("body").removeClass("modal-body");
		});
		
	$('#addProductQuantity').on('click',function(){
        var qty = $('#QtyValueOfProduct').val();
			qty++;
        $('#QtyValueOfProduct').val(qty);
    });
	
    $('#minusProductQuantity').on('click',function(){
        var qty = $('#QtyValueOfProduct').val();
			if(qty == 0){				
				$('#QtyValueOfProduct').val(1);
				alert("Zero quantity will not be acceptable as quantity.");
			}else{
				$('#QtyValueOfProduct').val(qty);
			}
			qty--;
    });
});

/* function productPopUp(id){	
	var pname = $('#pInputName'+id).val();
	var pprice = $('#pInputPrice'+id).val();
	var pimage = $('#pInputImg'+id).val();
	$(".modal-background3").show();	
    $("body").addClass("modal-body");
	
	$("#prductPriceId").html(pprice);
	$("#prductNameId").html(pname);
	$('#p_image_popup').attr('src', pimage);
	$('#p_image_popup').css('height', '70px');
	$('#p_image_popup').css('width', '70px');
} */


