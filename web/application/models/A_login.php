<?php
class A_login extends CI_Model{
/***********************************
function using for admin login
***********************************/
   public function login($data)
   {
          $query = $this->db->get_where('login', array('username'=> $data['username'], 'password'=>$data['password']));
        
          return ($query->num_rows()>0) ? $query->row() : false;  
    }
/***********************************
function using for subadmin login
***********************************/
	function sublogin($data)
	{
		
		$query = $this->db->get_where('login', array('username'=> $data['username'], 'password'=>$data['password'], 'type'=>'sub_admin'));
        
          return ($query->num_rows()>0) ? $query->row() : false;
	}
/***********************************
function using for facility list
***********************************/
	function facility_data()
	{
		$this->db->select('*');
		$this->db->from('facility');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $result = $query->result();
		}
		else
		{
			return false;
		}
	}
}
