<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopowner_model extends CI_Model{
 
     public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }
// //******************************************************************//
	////****** function is used for Insert Email in database *******//
// //******************************************************************//	
	

	public function usertokeninsertforshopownerregistration($id, $user_token)
	{
		$data=array('email'=>$em);
		$this->db->insert('fos_restaurant_admin_users', $data);
		$id = $this->db->insert_id();
		return $id;
		 $data3 = array('restaurant_id'=>$last_id, 'user_email'=>$email);
		 $this->db->insert('fos_restaurant_admin_users',$data3);
	}	
	/////////////////////////////////////////////////////////////////
	////////////////////  request for shop owner ////////////////////////

 function add_newshop($email, $user_token)
	 {
		 $data1 = array('contact_email'=>$email);
		 $this->db->insert('fos_restaurants',$data1);
		 $last_id = $this->db->insert_id();
			////////////////////////////////////
		$data5 = array('res_id'=>$last_id) ;
		 $this->db->insert('fos_restaurant_setting',$data5);		 
			//////////////////////////////////////////
		 $data2 = array('restaurant_id'=>$last_id);
		 $this->db->insert('fos_restaurant_address',$data2);
		 /////////////////////////////////////
		 $data3 = array('restaurant_id'=>$last_id, 'user_email'=>$email, 'token'=>$user_token);
		 $this->db->insert('fos_restaurant_admin_users',$data3);
		 return $user_token;
		
	 }

	 ////////////////////////////////////////////////////////////////////////
	 //          shop owner registration
	 ///////////////////////////////////////////////////////////////////////
	public function shopownerregistration($data)
	 {
	 	 $data1 = array('restaurant_name_ar'=>$data['restaurant_name_ar'], 'contact_email'=>$data['contact_email']); 

		 	 $this->db->where('restaurant_id', $data['id']);
			 $this->db->update('fos_restaurants',$data1);		 			 
			//////////////////////////////////////////
		 $data2 = array('address_ar'=>$data['address_ar'], 'category'=>$data['category'], 'postal_code'=>$data['postal_code']);

			 $this->db->where('restaurant_id', $data['id']);
			 $this->db->update('fos_restaurant_address',$data2);
			 /////////////////////////////////////
		 $data3 = array('user_name'=>$data['user_name'], 'account_password'=>$data['account_password'], 'contact_number'=>$data['contact_number']);

			 $this->db->where('restaurant_id', $data['id']);
			 $this->db->update('fos_restaurant_admin_users',$data3);	
			 return 2;	 
	 }
	// //******************************************************************//
	////****** function is used for Insert Data for Registration *******//
// //******************************************************************//
	public function freelancernewregistration($data, $id)
	{			
	    $this->db->where('id', $id);
	    $this->db->update('fos_freelancer', $data);
	    return 1;	
	}
	
	
 //******************************************************************//
 	 //****** function is used for checktoken link and userid *******//
     //******************************************************************//	
	 function checkemail($email)
	  {
	  	$status = 1;
		$this->db->select("*");
        $this->db->from('fos_restaurant_admin_users');
        $this->db->where('status', $status);
		$this->db->where('user_email', $email);
        $query = $this->db->get();
            if($query->num_rows()>0)
                { 
                   return $result = $query->result();				  
				}
              else
                {
                  return false;
                }
	  }
	
	 //******************************************************************//
 	 //****** Select all shop categories*******//
     //******************************************************************//	
	 function fosshopcategories()
	  {	  	
		$this->db->select("*");
        $this->db->from('fos_shop_categories');       
        $query = $this->db->get();
            if($query->num_rows()>0)
                { 
                   return $result = $query->result();				  
				}
              else
                {
                  return false;
                }
	  }
	 //******************************************************************//
 	 //****** function is used for checktoken link and userid *******//
     //******************************************************************//	
	 function checkemailnotactive($email)
	  {
	  	$status = 0;
		$this->db->select("*");
        $this->db->from('fos_restaurant_admin_users');
        $this->db->where('status', $status);
		$this->db->where('user_email', $email);
        $query = $this->db->get();
            if($query->num_rows()>0)
                { 
                   return $result = $query->row();				  
				}
              else
                {
                  return false;
                }
	  }
	
//******************************************************************//
 	 //****** function is used for checktoken link and userid *******//
     //******************************************************************//	
	 function checktoken($token)
	  {
	  	//$status = 0;
		$this->db->select("*");
        $this->db->from('fos_restaurant_admin_users');
        $this->db->where('token', $token);		
        $query = $this->db->get();
            if($query->num_rows()>0)
                { 
                   return $result = $query->row();				  
				}
              else
                {
                  return false;
                }
	  }

	  //******************************************************************//
 	 //****** function is used for checktoken link and userid *******//
     //******************************************************************//	
	 function getshopemailwithid($id)
	  {
	  	$query = $this->db->query("Select * from fos_restaurants as a LEFT JOIN fos_restaurant_admin_users as b on a.restaurant_id = b.restaurant_id LEFT JOIN fos_restaurant_address as c on a.restaurant_id = c.restaurant_id where a.restaurant_id = $id"); 	   		
	  		return ($query->num_rows()>0)?$query->row():false;
	  }

 //********************** for login *********************//
	  // ********************************************//
	 public function getuserdetail($id)
	  {
	  	$query = $this->db->get_where('fos_users',array('id'=>$id));
	 	return ($query->num_rows() > 0)?$query->row():FALSE;
	  }
	public function getShopCategoryName($shopCatId){
		$this->db->select("*");
        $this->db->from('fos_shop_categories');
        $this->db->where('category_id', $shopCatId);		
        $query = $this->db->get()->row_array();
		if(!empty($query)){
			return $query;
		}else{
			return false;
		}

	}	
}
