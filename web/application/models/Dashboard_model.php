<?php
class Dashboard_model extends CI_Model
{
  
	/************************************************************************
    ---- function location search function ----
 **********************************************************************/ 
	public function location_search($street=false , $city=false)
	{
		$this->db->select("*");
		$this->db->from("pcdata");
		$this->db->like('straatnaam_utf8_nen',$street);
		if($city != false && $city != "")
		{
			$this->db->like('plaatsnaam_utf8',$city);
		}
		$this->db->limit(5);
		$this->db->group_by('plaatsnaam_utf8');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return FALSE;
		} 
	}
	// public function get_shoplist($zip)
	// {
	// 	if($zip != "")
	// 	{
	// 		$this->db->select('*');
	// 		$this->db->from('fos_area');
	// 		$this->db->where('post_code', $zip);
	// 		$query = $this->db->get();
	// 		if ($query->num_rows() > 0) {
	// 			return $query->result();
	// 		} else {
	// 			return FALSE;
	// 		} 
	// 	}else {
	// 			return FALSE;
	// 		} 
	// }
	
	public function get_shoplist($zip=false, $sendzip=false, $params = array())
		{
			if($zip != "")
			{
				$this->db->select('*');				
				$this->db->from('fos_area a'); 
	            $this->db->join('fos_restaurants b', 'b.restaurant_id=a.shop_id', 'left');
	            $this->db->join('fos_restaurant_address c', 'c.restaurant_id=a.shop_id', 'left');
	            $this->db->join('fos_restaurant_admin_users d', 'd.restaurant_id=a.shop_id', 'left');
	            $this->db->join('fos_shop_categories e', 'e.category_id=c.category', 'left');
	            $this->db->join('fos_category_image f', 'e.image_id=f.id', 'left');
				$this->db->where('a.post_code', $zip);
				///////
					 if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
				            $this->db->limit($params['limit'],$params['start']);
				        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
				            $this->db->limit($params['limit']);
				        }
				/////
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					return $query->result();
				} else {
					return FALSE;
				} 
			}
			else if ($sendzip != "")
			{
				$this->db->select('*');				
				$this->db->from('fos_area a'); 
	            $this->db->join('fos_restaurants b', 'b.restaurant_id=a.shop_id', 'left');
	            $this->db->join('fos_restaurant_address c', 'c.restaurant_id=a.shop_id', 'left');
	            $this->db->join('fos_restaurant_admin_users d', 'd.restaurant_id=a.shop_id', 'left');
	            $this->db->join('fos_shop_categories e', 'e.category_id=c.category', 'left');
	            $this->db->join('fos_category_image f', 'e.image_id=f.id', 'left');
				$this->db->where('a.post_code', $sendzip);
				///////
					 if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
				            $this->db->limit($params['limit'],$params['start']);
				        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
				            $this->db->limit($params['limit']);
				        }
				/////
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					return $query->result();
				} else {
					return FALSE;
				} 
			}
			else{
					return FALSE;
				} 
		}

		////////////////////////////////////////////////////////////////////////////////////////////
						/////////////// Get category list/////////////////////////////////////

		public function getcategories()
		{
		   $query = $this->db->get_where('fos_shop_categories', array('status'=>'1'));
		   return ($query->num_rows()>0)?$query->result():false;			
		}
		//*****************************************************************//
		// 				Getting shops according to categories
		 /////////////*************************************////////////////
		public function get_shoplistCateid($zip=false, $sendzip=false, $cateids, $params = array())
		{
		$cateid = implode(",",$cateids); 		
			if($zip != "")
			{				
					 if(array_key_exists("start",$params) && array_key_exists("limit",$params))
					    {	
					    	$limit = $params['limit'];
					    	$start = $params['start'];
				            //$this->db->limit($params['limit'],$params['start']);
					 	$query = $this->db->query("SELECT * FROM `fos_area` `a` LEFT JOIN `fos_restaurants` `b` ON `b`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_address` `c` ON `c`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_admin_users` `d` ON `d`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_shop_categories` `f` ON `f`.`category_id`=`c`.`category` LEFT JOIN `fos_category_image` `g` ON `f`.`image_id`=`g`.`id` WHERE `a`.`post_code` = '$zip' AND `c`.`category` IN($cateid) LIMIT $limit OFFSET $start");
				        }
			        elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
				   		{		
				   			$limit = $params['limit'];		        
							$query = $this->db->query("SELECT * FROM `fos_area` `a` LEFT JOIN `fos_restaurants` `b` ON `b`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_address` `c` ON `c`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_admin_users` `d` ON `d`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_shop_categories` `f` ON `f`.`category_id`=`c`.`category` LEFT JOIN `fos_category_image` `g` ON `f`.`image_id`=`g`.`id` WHERE `a`.`post_code` = '$zip' AND `c`.`category` IN($cateid) LIMIT $limit");
					 	}
					   elseif(!array_key_exists("start",$params) && !array_key_exists("limit",$params))
				   		{					   				        
							$query = $this->db->query("SELECT * FROM `fos_area` `a` LEFT JOIN `fos_restaurants` `b` ON `b`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_address` `c` ON `c`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_admin_users` `d` ON `d`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_shop_categories` `f` ON `f`.`category_id`=`c`.`category` LEFT JOIN `fos_category_image` `g` ON `f`.`image_id`=`g`.`id` WHERE `a`.`post_code` = '$zip' AND `c`.`category` IN($cateid)");
					 	}

				if ($query->num_rows() > 0) {
					return $query->result();
				} else {
					return FALSE;
				} 
			}
			else if ($sendzip != "")
			{
				// $this->db->select('*');				
				// $this->db->from('fos_area a'); 
	   //          $this->db->join('fos_restaurants b', 'b.restaurant_id=a.shop_id', 'left');
	   //          $this->db->join('fos_restaurant_address c', 'c.restaurant_id=a.shop_id', 'left');
	   //          $this->db->join('fos_restaurant_admin_users d', 'd.restaurant_id=a.shop_id', 'left');
				// $this->db->where('a.post_code', $sendzip);
				// $this->db->where_in('c.category', $cateid);
				// $query = $this->db->get();
				//echo "helo";die;
				
				if(array_key_exists("start",$params) && array_key_exists("limit",$params))
					    {	
					    	$limit = $params['limit'];
					    	$start = $params['start'];
							$query = $this->db->query("SELECT * FROM `fos_area` `a` LEFT JOIN `fos_restaurants` `b` ON `b`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_address` `c` ON `c`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_admin_users` `d` ON `d`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_shop_categories` `f` ON `f`.`category_id`=`c`.`category` LEFT JOIN `fos_category_image` `g` ON `f`.`image_id`=`g`.`id` WHERE `a`.`post_code` = '$sendzip' AND `c`.`category` IN($cateid) LIMIT $limit OFFSET $start");
						}
				elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
				   		{		
				   			$limit = $params['limit'];	
				   			$query = $this->db->query("SELECT * FROM `fos_area` `a` LEFT JOIN `fos_restaurants` `b` ON `b`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_address` `c` ON `c`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_admin_users` `d` ON `d`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_shop_categories` `f` ON `f`.`category_id`=`c`.`category` LEFT JOIN `fos_category_image` `g` ON `f`.`image_id`=`g`.`id` WHERE `a`.`post_code` = '$sendzip' AND `c`.`category` IN($cateid) LIMIT $limit");
				   		}
				 elseif(!array_key_exists("start",$params) && !array_key_exists("limit",$params))
				   		{	
				   			$query = $this->db->query("SELECT * FROM `fos_area` `a` LEFT JOIN `fos_restaurants` `b` ON `b`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_address` `c` ON `c`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_restaurant_admin_users` `d` ON `d`.`restaurant_id`=`a`.`shop_id` LEFT JOIN `fos_shop_categories` `f` ON `f`.`category_id`=`c`.`category` LEFT JOIN `fos_category_image` `g` ON `f`.`image_id`=`g`.`id` WHERE `a`.`post_code` = '$sendzip' AND `c`.`category` IN($cateid)");
				   		}

				if ($query->num_rows() > 0) {
					return $query->result();
				} else {
					return FALSE;
				} 
			}
			else{
					return FALSE;
				} 
		}

//********************** Register data *******************//
		public function userregistration($data)
		 {
		 	$this->db->insert('fos_users', $data);
		 	$id = $this->db->insert_id();
		 	if(!empty($id))
		 	{
		 		$query = $this->db->get_where('fos_users',array('id'=>$id));
	 			return ($query->num_rows() > 0)?$query->row():FALSE;
	        }
	        else 
	         {
	         	return FALSE;
	         }		 		
		 }

//**************** check email ************************//
	public function checkemail($email)
	 {
	 	$query = $this->db->get_where('fos_users',array('email'=>$email));
	 	return ($query->num_rows() > 0)?$query->result():FALSE;
	 }	

//********************** for login *********************//
	 public function login($data)
	  {
	  	$query = $this->db->get_where('fos_users',array('email'=>$data['username'], 'password'=>$data['password']));
	 	return ($query->num_rows() > 0)?$query->row():FALSE;
	  }

//*************** function is used to get all orders according to userid ************//

 public function togetordersofuser($userid, $params = array())
  {
  		$this->db->select('*');				
		$this->db->from('customer_order a'); 
  		//$query = $this->db->get_where('customer_order',array('user_id'=>$userid));
  		$this->db->join('fos_restaurants b', 'b.restaurant_id = a.shopid', 'left');
  		$this->db->where('a.user_id', $userid);
  		 if(array_key_exists("start",$params) && array_key_exists("limit",$params))
  		 {
            $this->db->limit($params['limit'],$params['start']);
         }
        elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
         {
            $this->db->limit($params['limit']);
         }	
  		$query = $this->db->get();
	 	return ($query->num_rows() > 0)?$query->result():FALSE;
  }

  //**************** check password ************************//
	public function checkpass($pass, $user_id)
	 {
	 	$query = $this->db->get_where('fos_users',array('password'=>$pass, 'id'=>$user_id));
	 	return ($query->num_rows() > 0)?$query->row():FALSE;
	 }	

	 //**********************update the user profile *******************//
		public function updateuserprofile($data, $user_id)
		 {	
		    $this->db->where('id', $user_id);	 	
		 	$this->db->update('fos_users', $data);		 	
	        return TRUE;	        	 		
		 }
	// ********************** to get single order details *********************//
		 public function togetsingleorder($order_id)
		  {
		 	$this->db->select('*');				
			$this->db->from('customer_order a'); 
	  		$this->db->join('fos_restaurants b', 'b.restaurant_id = a.shopid', 'left');
	  		$this->db->where('a.id', $order_id);
	  		$query = $this->db->get();
	 		return ($query->num_rows() > 0)?$query->row():FALSE;
		  }
	//*************************************************//
	 //********function use for register data***********//
	 //*************************************************//
	 function mail_exists($email)
	 {
		$this->db->select('*');
		$this->db->from('fos_users');
		$this->db->where('email', $email);
		$result = $this->db->get();
		if($result->num_rows() > 0)
		{
			return $data = $result->row();  //row
        }
		else
		{
			return false;
		}
	 }
}