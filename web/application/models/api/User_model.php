<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model{
 
     public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }
	 //*************************************************//
	 //********function use for register data***********//
	 //*************************************************//
	 function register_data($data)
	 {
		$this->db->insert('fos_users',$data);  
		$last_id=$this->db->insert_id();
		$this->db->select('*');
		$this->db->from('fos_users');
		$this->db->where('id', $last_id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
		 
	 }
	 //*************************************************//
	 //********function use for register data***********//
	 //*************************************************//
	 function mail_exists($email)
	 {
		$this->db->select('*');
		$this->db->from('fos_users');
		$this->db->where('email', $email);
		$result = $this->db->get();
		if($result->num_rows() > 0)
		{
			return $data = $result->row();  //row
        }
		else
		{
			return false;
		}
	 }
	 //*************************************************//
	 //********function use for verify_user***********//
	 //*************************************************//
	 public function verify_user($id)
	 {
		$data=array('status'=>1);
	    $this->db->where('id', $id);
	    $this->db->update('fos_users', $data);
	     return true;
	 }
	 //*************************************************//
	 //********function use for cheak_user_detail***********//
	 //*************************************************//
	 function cheak_user_detail($email, $password)
	 {
		$this->db->select('*');
		$this->db->from('fos_users');
		$this->db->where('email', $email);
		$this->db->where('password', $password);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		    return $data = $query->row();
  		}
		else
		{
			return false;
		}
    }
	 //*************************************************//
	 //********function use for check_verify***********//
	 //*************************************************//
	public function check_verify($email,$password)
	 {
		$this->db->select('*');
		$this->db->from('fos_users');
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$this->db->where('status',1);
		$query = $this->db->get();
        if($query->num_rows()>0)
          { 
              return $result = $query->row();
		  }
        else
          {
              return false;
          }
	 }
	 //*************************************//
	//*** function used for Shop Type ***//
	//*************************************//
	function shopType()
	{
		$this->db->select('fs.*,im.image');
		$this->db->from('fos_shop_categories AS fs');
		$this->db->join('fos_category_image AS im', 'fs.image_id = im.id', 'INNER');
		$this->db->where('fs.status', 1);
		$query = $this->db->get();
        if($query->num_rows()>0)
          { 
              return $result = $query->result();
		  }
        else
          {
              return false;
          }
	}
	//*************************************//
	//*** function used for Fetch product ***//
	//*************************************//
	function fetchProduct($shop_id,$category_id)
	{
		$this->db->select('rm.*,v.vat as vat_name,v.type');
		$this->db->from('fos_restaurant_meals as rm');
		$this->db->join('fos_vat AS v', 'rm.vat = v.id', 'INNER');
		if(!empty($category_id))
		{
		$this->db->where('rm.category_id',$category_id);
		}
		$this->db->where('rm.restaurant_id',$shop_id);
		$query = $this->db->get();
        if($query->num_rows()>0)
          { 
              return $result = $query->result();
		  }
        else
          {
              return false;
          }
	}
	//****************************************//
	//function used for Fetch product category//
	//****************************************//
	function fetchProductCategory($shop_id)
	{
		$this->db->select('*');
		$this->db->from('fos_restaurant_meals_category');
		$this->db->where('restaurant_id',$shop_id);
		$query = $this->db->get();
        if($query->num_rows()>0)
          { 
              return $result = $query->result();
		  }
        else
          {
              return false;
          }
	}
	//****************************************//
	//function used for Fetch findShopZip//
	//****************************************//
	function findShopZip($zipcode)
	{
		$this->db->distinct();
		$this->db->select('res.*,add.*,cat.category_name,png.image,sett.*');
		$this->db->from('fos_area as ar');
		$this->db->join('pcdata AS pd', 'pd.wijkcode = ar.post_code', 'RIGHT');
		$this->db->join('fos_restaurants AS res', 'ar.shop_id = res.restaurant_id', 'LEFT');
		$this->db->join('fos_restaurant_address AS add', 'res.restaurant_id = add.restaurant_id', 'LEFT');
		$this->db->join('fos_shop_categories AS cat', 'add.category = cat.category_id', 'LEFT');
		$this->db->join('fos_category_image AS png', 'cat.image_id = png.id', 'LEFT');
		/////////////////////////////////////////////////
		$this->db->join('fos_restaurant_setting AS sett', 'ar.shop_id = sett.res_id', 'LEFT');
		/////////////////////////////////////////////////
		$this->db->where('pd.wijkcode',$zipcode);
		$this->db->where('ar.post_code',$zipcode);
		$query = $this->db->get();
        if($query->num_rows()>0)
          { 
              return $result = $query->result();
		  }
        else
          {
              return false;
          }
		
	}
	//****************************************//
	//function used for Fetch findShopCity//
	//****************************************//
	function findShopCity($cityname)
	{
		$city = explode(',',$cityname);
		$count = count($city);
		$this->db->distinct();
		$this->db->select('res.*,add.*,cat.category_name,png.image,sett.*');
		$this->db->from('fos_area as ar');
		if($count == 1)
		{
			$street_name = $city[0];
			$this->db->like('pd.straatnaam_utf8_nen', $street_name);
		}
		else if($count == 2)
		{
			$street_name = $city[0];
			$city_name = $city[1];
			$this->db->like('pd.straatnaam_utf8_nen', $street_name);
			$this->db->like('pd.plaatsnaam_utf8_nen', $city_name);
		}
		$this->db->join('pcdata AS pd', 'pd.wijkcode = ar.post_code', 'LEFT');
		$this->db->join('fos_restaurants AS res', 'ar.shop_id = res.restaurant_id', 'LEFT');
		$this->db->join('fos_restaurant_address AS add', 'res.restaurant_id = add.restaurant_id', 'LEFT');
		$this->db->join('fos_shop_categories AS cat', 'add.category = cat.category_id', 'LEFT');
		$this->db->join('fos_category_image AS png', 'cat.image_id = png.id', 'LEFT');
		/////////////////////////////////////////////////
		$this->db->join('fos_restaurant_setting AS sett', 'ar.shop_id = sett.res_id', 'LEFT');
		/////////////////////////////////////////////////
		$this->db->limit(25,0);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
        if($query->num_rows()>0)
          { 
              return $result = $query->result();
		  }
        else
          {
              return false;
          }
	}
	//****************************************//
	//function used for Fetch findShopZipCatg//
	//****************************************//
	function findShopZipCatg($zipcode,$category)
	{
		$this->db->distinct();
		$this->db->select('res.*,add.*,cat.category_name,png.image,sett.*');
		$this->db->from('fos_area as ar');
		
		$this->db->join('pcdata AS pd', 'pd.wijkcode = ar.post_code', 'RIGHT');
		$this->db->join('fos_restaurants AS res', 'ar.shop_id = res.restaurant_id', 'LEFT');
		
		$this->db->join('fos_restaurant_address AS add', 'res.restaurant_id = add.restaurant_id', 'LEFT');
		$this->db->join('fos_shop_categories AS cat', 'add.category = cat.category_id', 'LEFT');
		$this->db->join('fos_category_image AS png', 'cat.image_id = png.id', 'LEFT');
		/////////////////////////////////////////////////
		$this->db->join('fos_restaurant_setting AS sett', 'ar.shop_id = sett.res_id', 'LEFT');
		/////////////////////////////////////////////////
		$this->db->where('pd.wijkcode',$zipcode);
		$this->db->where_in('cat.category_id',$category);
		$query = $this->db->get();
        if($query->num_rows()>0)
          { 
              return $result = $query->result();
		  }
        else
          {
              return false;
          }
	}
	//****************************************//
	//function used for Fetch findShopCityCatg//
	//****************************************//
	function findShopCityCatg($cityname,$category)
	{
		$city = explode(',',$cityname);
		$count = count($city);
		$this->db->distinct();
		$this->db->select('res.*,add.*,cat.category_name,png.image,sett.*');
		$this->db->from('fos_area as ar');
		if($count == 1)
		{
			$street_name = $city[0];
			$this->db->like('pd.straatnaam_utf8_nen', $street_name);
		}
		else if($count == 2)
		{
			$street_name = $city[0];
			$city_name = $city[1];
			$this->db->like('pd.straatnaam_utf8_nen', $street_name);
			$this->db->like('pd.plaatsnaam_utf8_nen', $city_name);
		}
		$this->db->join('pcdata AS pd', 'pd.wijkcode = ar.post_code', 'LEFT');
		$this->db->join('fos_restaurants AS res', 'ar.shop_id = res.restaurant_id', 'LEFT');
		$this->db->join('fos_restaurant_address AS add', 'res.restaurant_id = add.restaurant_id', 'LEFT');
		$this->db->join('fos_shop_categories AS cat', 'add.category = cat.category_id', 'LEFT');
		$this->db->join('fos_category_image AS png', 'cat.image_id = png.id', 'LEFT');
		/////////////////////////////////////////////////
		$this->db->join('fos_restaurant_setting AS sett', 'ar.shop_id = sett.res_id', 'LEFT');
		/////////////////////////////////////////////////
		$this->db->where_in('cat.category_id',$category);
		//$this->db->limit(25,0);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
        if($query->num_rows()>0)
          { 
              return $result = $query->result();
		  }
        else
          {
              return false;
          }
	}
	//****************************************//
	//function used for Fetch find street City//
	//****************************************//
	function findStreetCity($cityname='')
	{
		if($cityname != '')
		{
		$city = explode(',',$cityname);
		$count = count($city);
		}
		$this->db->distinct();
		$this->db->select('straatnaam_utf8_nen,plaatsnaam_utf8_nen');
		$this->db->from('pcdata');
		if($cityname != '')
		{
			if($count == 1)
			{
				$street_name = $city[0];
				$this->db->like('straatnaam_utf8_nen', $street_name);
			}
			else if($count == 2)
			{
				$street_name = $city[0];
				$city_name = $city[1];
				$this->db->like('straatnaam_utf8_nen', $street_name);
				$this->db->like('plaatsnaam_utf8_nen', $city_name);
			}
		}
		$query = $this->db->get();
        if($query->num_rows()>0)
          { 
              return $result = $query->result();
		  }
        else
          {
              return false;
          }
	}
	//****************************************//
		//function used for place an order//
	//****************************************//
	function placeAnOrder($data)
	{
		$this->db->insert('customer_order',$data);  
		$last_id=$this->db->insert_id();
		$this->db->select('*');
		$this->db->from('customer_order');
		$this->db->where('id', $last_id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}
	//****************************************//
	  //function used for get order history//
	//****************************************//
	function orderHistory($user_id,$offset)
	{
		$this->db->select('order.*,res.restaurant_name_ar');
		$this->db->from('customer_order As order');
		$this->db->join('fos_restaurants AS res', 'order.shopid = res.restaurant_id', 'LEFT');
		$this->db->where('user_id', $user_id);
		$this->db->limit(25,$offset);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		    return $data = $query->result();
  		}
		else
		{
			return false;
		}
	}
	//****************************************//
	  //function used for get order detail//
	//****************************************//
	function orderDetail($id)
	{
		$this->db->select('*');
		$this->db->from('customer_order');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
		    return $data = $query->row();
  		}
		else
		{
			return false;
		}
	}
	//***********************************************************//
	 //****** function is used for update_profile *******//
   //*************************************************************//	
	function edit_profile($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('fos_users', $data);
		$this->db->select('*');
		$this->db->from('fos_users');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			return $data = $query->row();
		}
	}
	//********************** for shop setting *********************//
	 public function getshopdetailsetting($id)
	  {
	  	$query = $this->db->get_where('fos_restaurant_setting',array('res_id'=>$id));
	 	return ($query->num_rows() > 0)?$query->row():FALSE;
	  }
	  //*************************************************//
		//function used for check old password//
	//************************************************//
	function old_password($old_password,$user_id,$new_password)
	{
	    $this->db->select('*');
		$this->db->from('fos_users');
		$this->db->where('id', $user_id);
		$this->db->where('password', $old_password);
	    $query = $this->db->get();
		if($query->num_rows() == 1)
		{
		    return $query->row();
		}
		else
		{
			return false;
		}
	}
	//*************************************************//
		//function used for update new password//
	//************************************************//
	function update_password($user_id, $new_password)
	{
		$data=array('password'=>$new_password);
		$this->db->where('id',$user_id);
		$this->db->update('fos_users',$data);
		return TRUE;
	}
	//*************************************************//
		//function used for get facebook user detail//
	//************************************************//
	function fb_userdetail($email,$device_id,$device_type)
	{
		$this->db->select('*');
		$this->db->from('fos_users');
		$this->db->where('email', $email);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$data=array('device_id'=>$device_id,'device_type'=>$device_type);
			$this->db->where('email',$email);
			$this->db->update('fos_users',$data);
			
			$this->db->select('*');
			$this->db->from('fos_users');
			$this->db->where('email', $email);
			$query1 = $this->db->get();
			if($query1->num_rows() > 0)
			{
				$result = $query1->row();
				return $result;
			}
		}
		else
		{
			return false;
		}
	}
		
}