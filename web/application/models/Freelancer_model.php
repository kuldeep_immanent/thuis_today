<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Freelancer_model extends CI_Model{
 
     public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }
// //******************************************************************//
	////****** function is used for Insert Email in database *******//
// //******************************************************************//	
	

	public function insert($em, $token)
	{
		$data=array('email'=>$em, 'token'=>$token);
		$this->db->insert('fos_freelancer', $data);
		$id = $this->db->insert_id();
		return $token;
	}	
	
	// //******************************************************************//
	////****** function is used for Insert Data for Registration *******//
// //******************************************************************//
	public function freelancernewregistration($data, $id)
	{			
	    $this->db->where('id', $id);
	    $this->db->update('fos_freelancer', $data);
	    return 1;	
	}
	
	
 //******************************************************************//
 	 //****** function is used for checktoken link and userid *******//
     //******************************************************************//	
	 function checkemail($email)
	  {
	  	$status = 1;
		$this->db->select("*");
        $this->db->from('fos_freelancer');
        $this->db->where('status', $status);
		$this->db->where('email', $email);
        $query = $this->db->get();
            if($query->num_rows()>0)
                { 
                   return $result = $query->result();				  
				}
              else
                {
                  return false;
                }
	  }
	
	 //******************************************************************//
 	 //****** function is used for checktoken link and userid *******//
     //******************************************************************//	
	 function checkemailnotactive($email)
	  {
	  	$status = 0;
		$this->db->select("*");
        $this->db->from('fos_freelancer');
        $this->db->where('status', $status);
		$this->db->where('email', $email);
        $query = $this->db->get();
            if($query->num_rows()>0)
                { 
                   return $result = $query->row();				  
				}
              else
                {
                  return false;
                }
	  }
	
	 
	  //******************************************************************//
 	 //****** function is used for checktoken link and userid *******//
     //******************************************************************//	
	  function tochecktoken($token)
	  {
		$this->db->select("*");
        $this->db->from('fos_freelancer');
		$this->db->where('token', $token);
        $query = $this->db->get();
            if($query->num_rows()>0)
                { 
                   return $result = $query->row();
				  
				}
              else
                {
                  return false;
                }
	  }
	 
	///***************************************************************///
	  		// Get Product listing according to shop //
	//************************************************************// 
	 public function getshopname($restaurant_id)
	  {
		  $query = $this->db->get_where('fos_restaurants', array('restaurant_id'=>$restaurant_id));
		  return ($query->num_rows()>0)?$query->row():false;
	  }

  ///***************************************************************///
	  		// Category listing according to shop //
	//************************************************************// 
	 public function getCategoriesList($restaurant_id)
	  {
	  	$query = $this->db->get_where('fos_restaurant_meals_category', array('restaurant_id'=>$restaurant_id));
	  	return ($query->num_rows()>0)?$query->result():false;
 	  }

  ///***************************************************************///
	  		// Category listing according to shop //
	//************************************************************// 
	 public function getVatpercentage($restaurant_id, $category_id=false)
	  {
		if($category_id != false)
		 {

			$cateid = implode(",",$category_id); 
			// $this->db->where('fos_restaurant_meals.category_id', $category_id);
			$query = $this->db->query("SELECT `fos_restaurant_meals`.*, `fos_vat`.`vat` as `bat`, `fos_vat`.`type` FROM `fos_restaurant_meals` LEFT JOIN `fos_vat` ON `fos_restaurant_meals`.`vat` = `fos_vat`.`id` WHERE `fos_restaurant_meals`.`restaurant_id` = '$restaurant_id' AND `fos_restaurant_meals`.`is_active` = '1' AND `fos_restaurant_meals`.`category_id` IN($cateid)");
			if ($query->num_rows() > 0) {
					return $query->result();
				} else {
					return FALSE;
				}
		 }
		else
		{
			$this->db->select('fos_restaurant_meals.*, fos_vat.vat as bat, fos_vat.type');
			$this->db->from('fos_restaurant_meals');
			$this->db->join('fos_vat','fos_restaurant_meals.vat = fos_vat.id','left');
			$this->db->where('fos_restaurant_meals.restaurant_id', $restaurant_id);	
			$this->db->where('fos_restaurant_meals.is_active', '1');		
			$query = $this->db->get();		
			return ($query->num_rows()>0)?$query->result():false;
		}
 	  }

  //********************** for login *********************//
	 public function getuserdetail($id)
	  {
	  	$query = $this->db->get_where('fos_users',array('id'=>$id));
	 	return ($query->num_rows() > 0)?$query->row():FALSE;
	  }
  
  //********************** for shop setting *********************//
	 public function getshopdetailsetting($id)
	  {
	  	$query = $this->db->get_where('fos_restaurant_setting',array('res_id'=>$id));
	 	return ($query->num_rows() > 0)?$query->row():FALSE;
	  }
  
  // //******************************************************************//
	////****** function is used for Insert Email in database *******//
// //******************************************************************//	
	

	public function inserOrders($data)
	{		
		$this->db->insert('customer_order', $data);
		$id = $this->db->insert_id();
		$query = $this->db->get_where('customer_order',array('id'=>$id));
	 	return ($query->num_rows() > 0)?$query->row():FALSE;
		//return $id;
	}	
  // //******************************************************************//
	////****** function is used for Insert invoice in database *******//
// //******************************************************************//	
	

	public function insertnewinvoiceindatabase($data)
	{		
		$this->db->insert('fos_invoice', $data);
		$id = $this->db->insert_id();
		return $id;
	}	


  //********************** for order detail *********************//
	 public function getdatawithtoken($token)
	  {
	  	$query = $this->db->get_where('customer_order',array('token'=>$token));
	 	return ($query->num_rows() > 0)?$query->row():FALSE;
	  }

	  ///***************************************************************///
	  		// Category listing according to shop for ajax//
	//************************************************************// 
	 public function getVatpercentageajax($restaurant_id, $params = array(), $category_id=false)
	  {
		if($category_id != false)
		 {

			$cateid = implode(",",$category_id); 
			// $this->db->where('fos_restaurant_meals.category_id', $category_id);
			$query = $this->db->query("SELECT `fos_restaurant_meals`.*, `fos_vat`.`vat` as `bat`, `fos_vat`.`type` FROM `fos_restaurant_meals` LEFT JOIN `fos_vat` ON `fos_restaurant_meals`.`vat` = `fos_vat`.`id` WHERE `fos_restaurant_meals`.`restaurant_id` = '$restaurant_id' AND `fos_restaurant_meals`.`is_active` = '1' AND `fos_restaurant_meals`.`category_id` IN($cateid)");
			if ($query->num_rows() > 0) {
					return $query->result();
				} else {
					return FALSE;
				}
		 }
		else
		{
			$this->db->select('fos_restaurant_meals.*, fos_vat.vat as bat, fos_vat.type');
			$this->db->from('fos_restaurant_meals');
			$this->db->join('fos_vat','fos_restaurant_meals.vat = fos_vat.id','left');
			$this->db->where('fos_restaurant_meals.restaurant_id', $restaurant_id);	
			$this->db->where('fos_restaurant_meals.is_active', '1');
			
			 if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit']);
        }		
			$query = $this->db->get();		
			return ($query->num_rows()>0)?$query->result():false;
		}
 	  }

	//************************************************************************//
 	  		//    function is used to get all orders of customer //
 	  //********************************************************************//

 	  public function getallOrders()
 	   {
 	   		 $this->db->group_by('shopid'); 
 	   		$query = $this->db->get('customer_order');
	  		return ($query->num_rows()>0)?$query->result():false;
 	   }

 	   	//************************************************************************//
 	  		//    function is used to get all orders of customer //
 	  //********************************************************************//

 	  public function getallshopdetailschangestatus($shopid)
 	   {
 	   		$data = array('admin_status'=>'1');
	 	   	$this->db->where('shopid', $shopid);
		    $this->db->update('customer_order', $data);  	   		 
	  		return 1;
 	   }

 		//************************************************************************//
 	  		//    function is used to get single shop orders //
 	  //********************************************************************//

 	  public function getallshopdetails($id)
 	   {
 	   		$query = $this->db->query("Select customer_order.*, b.*, c.name as name from customer_order LEFT JOIN fos_restaurants as b on customer_order.shopid = b.restaurant_id LEFT JOIN fos_users as c on customer_order.user_id = c.id where customer_order.shopid = $id and customer_order.admin_status=0");
 	   		// usleep(5000000);
	  		return ($query->num_rows()>0)?$query->result():false;
 	   }

 	   	//************************************************************************//
 	  		//    function is used to get single shop details //
 	  //********************************************************************//

 	  public function getsingleshopdetail($id)
 	   {
 	   		$query = $this->db->query("Select * from fos_restaurants as a LEFT JOIN fos_restaurant_admin_users as b on a.restaurant_id = b.restaurant_id LEFT JOIN fos_restaurant_address as c on a.restaurant_id = c.restaurant_id where a.restaurant_id = $id");
 	   		// usleep(5000000);
	  		return ($query->num_rows()>0)?$query->row():false;
 	   }
 	   
}
