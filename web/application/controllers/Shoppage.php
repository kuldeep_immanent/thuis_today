<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shoppage extends CI_Controller {

 public function __construct() 
     {
        parent::__construct();
          
	      $this->load->model('freelancer_model');
	      $this->load->library('cart');
	      $this->load->library('Ajax_pagination');
          $this->perPage = 10;          
     }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		 $data = array();	
		$this->session->unset_userdata('shop_name');	

		$category_id = '';
		$resturant_id = $this->input->get('id');
		$data['postcode'] = $this->input->get('postcode');
		$data['cart_total'] = $this->cart->total();
		$data['name'] = $this->freelancer_model->getshopname($resturant_id);

		$session_shop = array('shop_name'=>$data['name']->restaurant_name_ar, 'resturant_id'=>$resturant_id);
		$this->session->set_userdata('shop_name', $session_shop);

		$data['categories'] = $this->freelancer_model->getCategoriesList($resturant_id);
		if(!empty($data['categories'])){$category_id = $data['categories'][0]->category_id;}
	////////////////////////////////////////////////////////
       
        
        //total rows count
        $totalRec = count($this->freelancer_model->getVatpercentageajax($resturant_id));
        
        //pagination configuration
        $config['target']      = '#postList';
        $config['base_url']    = base_url().'Shoppage/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $data['products'] = $this->freelancer_model->getVatpercentageajax($resturant_id, array('limit'=>$this->perPage));
        
        //load the view
        //$this->load->view('posts/index', $data);
	/////////////////////////////////////////////////////////////	
		//$data['products'] = $this->freelancer_model->getVatpercentage($resturant_id);	
		$data['categorycheck'] = '1';
		if(empty($data['products']))
		 {
			$this->session->set_flashdata('error_msg', 'No Products!');
	     }
			$this->load->view('page', $data);		
	}
/////////////////////////////// for ajax pagination //////////////////////////////////
	function ajaxPaginationData()
	  {
	  	
	  	$resturant_id = $this->session->userdata['shop_name']['resturant_id'];
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //total rows count
        $totalRec = count($this->freelancer_model->getVatpercentageajax($resturant_id));
        
        //pagination configuration
        $config['target']      = '#postList';
        $config['base_url']    = base_url().'Shoppage/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $data['products'] = $this->freelancer_model->getVatpercentageajax($resturant_id, array('start'=>$offset,'limit'=>$this->perPage));
        //$data['posts'] = $this->post->getRows(array('start'=>$offset,'limit'=>$this->perPage));
        
        //load the view
        $this->load->view('ajax-pagination-data', $data, false);
      }
/////////////////////////////////////////////////
	//  Listing Product according to category //
	///////////////////////////////////////////////////
	
	public function categoryproductlist(){
		$resturant_id = $this->input->get('resturantid');
		$category_id = $this->input->get('category_id');
		$category_id_all = $this->input->get('all');
		if(!empty($category_id_all))
		 {
		 	$data['categorycheck'] = '1';
		 //	$data['products'] = $this->freelancer_model->getVatpercentage($resturant_id);	
		 		////////////////////////////////////////////////////////
       
        
			        //total rows count
			        $totalRec = count($this->freelancer_model->getVatpercentageajax($resturant_id));
			        
			        //pagination configuration
			        $config['target']      = '#postList';
			        $config['base_url']    = base_url().'Shoppage/ajaxPaginationData';
			        $config['total_rows']  = $totalRec;
			        $config['per_page']    = $this->perPage;
			        $this->ajax_pagination->initialize($config);
			        
			        //get the posts data
			        $data['products'] = $this->freelancer_model->getVatpercentageajax($resturant_id, array('limit'=>$this->perPage));
			        
			        //load the view
			        //$this->load->view('posts/index', $data);


				/////////////////////////////////////////////////////////////	
	     }
	    else
	     {	
	     	$data['categoryidcheck'] = $category_id;     	
	     //	$data['products'] = $this->freelancer_model->getVatpercentage($resturant_id, $category_id);	
	     		///////////////////////////////////////////////////////////////
       
        
			        //total rows count
			        $totalRec = count($this->freelancer_model->getVatpercentageajax($resturant_id));
			        
			        //pagination configuration
			        $config['target']      = '#postList';
			        $config['base_url']    = base_url().'Shoppage/ajaxPaginationData';
			        $config['total_rows']  = $totalRec;
			        $config['per_page']    = $this->perPage;
			        $this->ajax_pagination->initialize($config);
			        
			        //get the posts data
			        $data['products'] = $this->freelancer_model->getVatpercentageajax($resturant_id, array('limit'=>$this->perPage), $category_id);
			        
			        //load the view
			        //$this->load->view('posts/index', $data);


				//////////////////////////////////////////////////////////////////////
	     }
		$data['postcode'] = $this->input->get('postid');
		$data['cart_total'] = $this->cart->total();
		$data['name'] = $this->freelancer_model->getshopname($resturant_id);
		$data['categories'] = $this->freelancer_model->getCategoriesList($resturant_id);		
		$this->load->view('page', $data);
	}
}
