<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Dashboard extends CI_Controller 
{

   function __construct()
    {
      parent::__construct();
      $this->load->model('user_model');
      $this->load->library('form_validation');
      $this->load->library('pagination');
	  $this->load->model('Dashboard_model');
    }
 /*******************************************
   -------------load admin dashboard -------
 *******************************************/   
   
     function index()
       { 
       	$this->load->template('index');
       }

/***********************************************
   ------------- Invoice Template ----------
 *********************************************/

    function invoices()
      {
          $check['invoice'] = $this->user_model->checkData();
          $this->load->template('invoices', $check);
      }
   

/***********************************************
   ------------- Services Template ----------
 *********************************************/

  function services()
      {  
          $check['service'] = $this->user_model->checkDataService();
          $this->load->template('services', $check);
      }
 


/***********************************************
   ------------- Payments Template ----------
 *********************************************/

    function payments()
      {
          $check['payment'] = $this->user_model->checkDataPayment();
          $this->load->template('payments', $check);
      }


/***********************************************
   ------- save invoices----------
 *********************************************/

  public function saveInvoice()
    {
         $data['subject'] = $this->input->post('subject');
         $data['above_content'] = $this->input->post('above');
         $data['below_content'] = $this->input->post('below');
        // $data['status'] = $this->input->post('status');
                    
         $insert = $this->user_model->insertinvoice($data);
         redirect('admin/dashboard/invoices/');   
    }


/***********************************************
   ------- save invoices----------
 *********************************************/

  public function saveGoods()
    {
         $data['subject'] = $this->input->post('subject');
         $data['content'] = $this->input->post('content');
       //  $data['status'] = $this->input->post('status');
                    
         $insert = $this->user_model->insertgoodService($data);
         redirect('admin/dashboard/services/');   
    }


/***********************************************
   ------- save invoices----------
 *********************************************/

  public function saveRequestPayment()
    {
         $data['subject'] = $this->input->post('subject');
         $data['content'] = $this->input->post('content');
        // $data['status'] = $this->input->post('status');
                    
         $insert = $this->user_model->insertpaymentRequest($data);
         redirect('admin/dashboard/payments/');   
    }
/*********************************************************************
   ------- insert serviceman in register table by admin----------
 ********************************************************************/
	function add_serviceman()
	{
	//$res = $this->input->post();
	//print_r($res);
	//die;
		$data['name'] = $this->input->post('name');
		$data['email'] = $this->input->post('email_address');
		$data['address'] = $this->input->post('address');
		$data['login_type'] = $this->input->post('login_type');
		$data['subadmin_id'] = $this->input->post('subadmin');
		$data['facility_name'] = $this->input->post('facility_name');
		$password=str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
		$message=$data['name']."\n".$data['email']."\n".$data['address']."\nYour login password is: ".$password." \nUse this password to login your account.\n \nThanks";
		$data['password'] = md5($password);
		$datas = $this->Dashboard_model->add_servicemans($data);
		if(!empty($datas))
		{
					    $this->load->library('email');
						$this->email->from('youexample@gmail.com', 'Testing');
					    $this->email->to($data['email']);
					    $this->email->subject('Login Detail');
					    $this->email->message($message);
					 if($this->email->send())
					  {	
						redirect('admin/Issue/serviceman_list/');
					  }
		}
					
    }
/*********************************************************************
   ------- update serviceman in register table by admin----------
 ********************************************************************/
	function update_serviceman()
	{
		$id = $this->input->post('id');
	    $data['name'] = $this->input->post('name');
		$data['email'] = $this->input->post('email_address');
		$data['address'] = $this->input->post('address');
		$message=$data['name']."\n".$data['email']."\n".$data['address']."\Your new detail.\n \nThanks";
		$pic_name = $_FILES["image"]["name"];
		if(!empty($pic_name))
		 {
					$config['upload_path']          = './uploads/';
					$config['allowed_types']        = 'gif|jpg|png|jpeg';
					$config['max_size']             = 2097152;
					 $this->load->library('upload', $config);                 	
					if ( ! $this->upload->do_upload('image')):                
							$error = array('error' => $this->upload->display_errors());
						 echo json_encode(array('jwt'=>$this->jwt->encode(array(
				     	 'status'=>0,
					 	 'message'=>'Large File Size.'
				   		 ), CONSUMER_SECRET)));
							die;                   
					else: //$upload = $this->upload->data('file_name');
						//$data['image'] = $this->upload->data('file_name');endif;
						$profile = $this->upload->data('file_name');
						//echo $profile;die;
						//$pic_name = $_FILES["image"]["name"];
						$data['image'] = base_url().'uploads/'.$profile;endif;
						//echo $data['image'];die;
		}
		$datas = $this->Dashboard_model->update_serviceman($data, $id);
		if(!empty($datas))
		{
					    $this->load->library('email');
						$this->email->from('youexample@gmail.com', 'Testing');
					    $this->email->to($data['email']);
					    $this->email->subject('Login Detail');
					    $this->email->message($message);
					 if($this->email->send())
					  {	
						redirect('admin/Issue/serviceman_list/');
					  }
		}
	}
	function facility_name()
	{
		$id=$this->input->post('id');
		$facility_name = $this->Dashboard_model->facility_list($id);
		echo $facility_name->facility_name;die;
		
	}
	/************************************************************************
    ---- function for create_anausement ----
 **********************************************************************/ 
   function create_anausement()
   {
	   $id = array();
	   $id1 = array();
	   $message = $this->input->post('create_anausement');
	   $andorid_device_id = $this->Dashboard_model->andorid_device_id();
	   $ios_device_id = $this->Dashboard_model->ios_device_id();
	 foreach($andorid_device_id as $value)
	 {
		 $data = $value['device_id'];
		 $id[] = $data;
	 }
	 foreach($ios_device_id as $value1)
	 {
		 $data1 = $value1['device_id'];
		 $id1[] = $data1;
	 }
		///////////////////////////////////////////////////////////////
		$this->andorid_brodcact_notif($message, $id);
		$this->Iphone_brodcactNoti($message, $id1);
		////////////////////////////////////////////////////////////
		redirect('admin/Dashboard');
   }
   function andorid_brodcact_notif($message, $id) 
   {
	   define( 'API_ACCESS_KEY', 'AIzaSyAcR1cjnRqHzv2jl1ay3L-P56oemjaUGRg' );
		$registrationIds = $id;
		// prep the bundle
	$msg = array
	(
		'message' 	=> $message
	);
	$fields = array
	(
		'registration_ids' 	=> $registrationIds,
		'notification'              => array( "title" => "Android Learning", "body" => $msg),
		'data'			=> $msg
	);
 
	$headers = array
	(
		'Authorization: key=' . API_ACCESS_KEY,
		'Content-Type: application/json'
	);
 
	$ch = curl_init();
	curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	curl_setopt( $ch,CURLOPT_POST, true );
	curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	$result = curl_exec($ch );
	curl_close( $ch );
	//echo $result;
	return $result;
	}
 /*********************************************************
  Function to Notify User for Iphone alerts 
**************************************************/ 
 function Iphone_brodcactNoti($message,$deviceToken)
  {
       $count = count($deviceToken);
		 $payload['aps'] = array('alert' => $message);
             $payload = json_encode($payload);
             $apnsCert = './CertificatesPush.pem';
             $streamContext = stream_context_create();
             stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
      // stream_context_set_option($streamContext, 'ssl', 'passphrase', self::$passphrase);
       $apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $streamContext);
	        
             //$apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken[$i])) . chr(0) . chr(strlen($payload)) . $payload;
			 for($i=0; $i<$count; $i++)
			 {
             $apnsMessage = chr(0) . pack('n', 32) . pack('H*', $deviceToken[$i]) . pack('n', strlen($payload)) . $payload;
			 fwrite($apns, $apnsMessage, strlen($apnsMessage));
			 }
          
	      socket_close($apns);
             fclose($apns); 
  }
	
}
?>