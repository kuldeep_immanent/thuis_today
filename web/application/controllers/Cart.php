<?php
class Cart extends CI_Controller
{
		public function __Construct()
		 {
			 parent::__Construct();
			 $this->load->helper('url');
			 $this->load->library('cart');
			 $this->load->library('paypal_lib');
			 $this->load->model('freelancer_model');
			 $this->load->database();
		 }
		 
 //**********This Function is used to Load the View Page 'CART' **********//
 //	*********************************************************************//
 
		public function index()
		 {  
		 	//print_r($this->cart->contents());die;
		 	$data['id'] = $this->input->get('id');
		 	$data['name'] = $this->freelancer_model->getshopname($data['id']);
		 	$data['cart_total'] = $this->cart->total();
		 	$data['cartdetail'] = $this->cart->contents();
			$this->load->view('cart', $data);	
		 }	
		 
	
//**********  This Function is used to cancel a order **********//		 
//****************************************************************//
		 
	   public function cancel()
		 {	
		 	echo "<script>
					alert('We are sorry! Your last transaction was cancelled.');
					window.location.href='checkout';
				  </script>";
		 	// $sess_id = $this->session->userdata['user_data']['user_id'];
			 // 	if(!empty($sess_id))
			 // 	 {
			 // 	 	$data['cart_total'] = $this->cart->total();
			 // 	 	$data['shop_name'] = $this->session->userdata['shop_name']['shop_name'];
			 // 	 	$data['resturant_id'] = $this->session->userdata['shop_name']['resturant_id'];
			 	 				 	 	
			 // 	 	$data['settingDetails'] = $this->freelancer_model->getshopdetailsetting($data['resturant_id']);

			 // 	 	$data['user_detail'] = $this->freelancer_model->getuserdetail($sess_id);	
			 // 	 	$this->load->view('checkout', $data);
			 // 	 }
			 // 	else
			 // 	 {
			 // 	 	 redirect("Login");
			 // 	 }			  	
		 }



//**********  This Function is used to Checkout on CART **********//		 
//****************************************************************//
		 
	   public function checkout()
		 {	
		 	$sess_id = $this->session->userdata['user_data']['user_id'];
			 	if(!empty($sess_id))
			 	 {
			 	 	$data['cart_total'] = $this->cart->total();
			 	 	$data['shop_name'] = $this->session->userdata['shop_name']['shop_name'];
			 	 	$data['resturant_id'] = $this->session->userdata['shop_name']['resturant_id'];
			 	 				 	 	
			 	 	$data['settingDetails'] = $this->freelancer_model->getshopdetailsetting($data['resturant_id']);

			 	 	$data['user_detail'] = $this->freelancer_model->getuserdetail($sess_id);	
			 	 	$this->load->view('checkout', $data);
			 	 }
			 	else
			 	 {
			 	 	  $session_data = array('set'=>'order');
		              $this->session->set_userdata('checkout', $session_data);
			 	 	 redirect("Login");
			 	 }			  	
		 }
		 
 
 //*************** add products to cart ********************//
		 //**************************************//
		 	
	public function cartsave()
	 {
		 $this->input->post('resid');
		 $this->input->post('postcode');
		 $insert_data = array( 'id' => $this->input->post('id'),
		 'name' => $this->input->post('name'),
		 'price' => $this->input->post('price'),
		 'qty' => $this->input->post('qty'),
		 'coupon' => $this->input->post('imgname'),
		 'vat' => $this->input->post('vat'),
		 'type' =>$this->input->post('type'),
		 'originalprice'=>$this->input->post('originalprice'));
		 // This function add items into cart.
		 $this->cart->insert($insert_data);	
		 $result = $this->cart->total();
		 echo $result;		
	 }


//************************* remove a item form cart  **************************//
	      //***************************************************//
	public function remove()
	 {
			$data['id'] = $this->input->get('id');
			$rowid = $this->input->get('rowid');
			$this->cart->remove($rowid);

			$data['name'] = $this->freelancer_model->getshopname($data['id']);
		 	$data['cart_total'] = $this->cart->total();
		 	$data['cartdetail'] = $this->cart->contents();
			$this->load->view('cart', $data);
	 }

	//************************* Destroy a cart **************************//
	      //***************************************************//
	public function destroy()
     {
			$data['id'] = $this->input->get('id');
			$this->cart->destroy();
		
		 	$data['name'] = $this->freelancer_model->getshopname($data['id']);
		 	$data['cart_total'] = $this->cart->total();
		 	$data['cartdetail'] = $this->cart->contents();
			$this->load->view('cart', $data);	
	 }

	 //**********************  update the cart  ***************************//
	    ////////////////////////////////////////////////////////////
	 public function update()
	  {
	 	 $data = array('rowid' => $this->input->post('rowid'),
		 'price' => $this->input->post('price'),
		 'qty' => $this->input->post('qty'),
		 'amount' => $this->input->post('amount'));	 

		 // This function update item into cart.
		 $this->cart->update($data);
		 $result = $this->cart->total();
		 echo $result;
	  }

	  //**********  This Function is used to Checkout on CART **********//		 
//****************************************************************//
		// function minutes_round ($hour, $minutes = '30', $format = "H:i")
		// 	{
		// 	    // by Femi Hasani [www.vision.to]
		// 	    $seconds = strtotime($hour);
		// 	    $rounded = round($seconds / ($minutes * 60)) * ($minutes * 60);
		// 	    return date($format, $rounded);
		// 	} 
	   public function checkouttiming()
		 {	
		 	$date = $this->input->post('opendate');
		 	$shopid = $this->input->post('shopid');
		 	$strtotime = strtotime($date);
		 	$day = strtolower(date("D", $strtotime));

		 	$settingDetails = $this->freelancer_model->getshopdetailsetting($shopid);

		 	switch ($day) {
						    case 'sun':
						      // echo $settingDetails->su;						       
						       	if(isset($settingDetails->su) AND !empty($settingDetails->su))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->su);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
									
						        break;
						    case 'mon':
						    	if(isset($settingDetails->mo) AND !empty($settingDetails->mo))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->mo);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
								else{echo '0';}								
						        break;
						    case 'tue':
						        //echo $settingDetails->tu;
						        if(isset($settingDetails->tu) AND !empty($settingDetails->tu))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->tu);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
							
						        break;
						    case 'wed':
						        //echo $settingDetails->we;
						        if(isset($settingDetails->we) AND !empty($settingDetails->we))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->we);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
							
						        break;
						    case 'thu':
						       // echo $settingDetails->th;
						        if(isset($settingDetails->th) AND !empty($settingDetails->th))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->th);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
									
						        break;
						    case 'fri':
						      //  echo $settingDetails->fr;
						        if(isset($settingDetails->fr) AND !empty($settingDetails->fr))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->fr);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
								
						        break;		
						    case 'sat':
						       // echo $settingDetails->sa;
						        if(isset($settingDetails->sa) AND !empty($settingDetails->sa))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->sa);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
									
						        break;				   
						    default:
						        0;
						} 

		 				 	 	// $data['cart_total'] = $this->cart->total();
			 	 	// $data['shop_name'] = $this->session->userdata['shop_name']['shop_name'];
			 	 	// $resturant_id = $this->session->userdata['shop_name']['resturant_id'];
			 	 				 	 	
			 	 	// $settingDetails = $this->freelancer_model->getshopdetailsetting($resturant_id);

			 	 	// $data['user_detail'] = $this->freelancer_model->getuserdetail($sess_id);	
			 	 	// $this->load->view('checkout', $data);
			 				  	
		 }

//***************************** function is used for during week days************************************//

	 public function checkouttimingweekdays()
		 {	
		 	$date = $this->input->post('opendate');
		 	$shopid = $this->input->post('shopid');
		 	$strtotime = strtotime($date);
		 	$day = strtolower(date("D", $strtotime));

		 	$settingDetails = $this->freelancer_model->getshopdetailsetting($shopid);

		 	switch ($day) {
						    case 'sun':
						      // echo $settingDetails->su;						       
						       	if(isset($settingDetails->sun) AND !empty($settingDetails->sun))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->sun);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
									
						        break;
						    case 'mon':
						    	if(isset($settingDetails->mon) AND !empty($settingDetails->mon))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->mon);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
								else{echo '0';}								
						        break;
						    case 'tue':
						        //echo $settingDetails->tu;
						        if(isset($settingDetails->tue) AND !empty($settingDetails->tue))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->tue);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
							
						        break;
						    case 'wed':
						        //echo $settingDetails->we;
						        if(isset($settingDetails->wed) AND !empty($settingDetails->wed))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->wed);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
							
						        break;
						    case 'thu':
						       // echo $settingDetails->th;
						        if(isset($settingDetails->thr) AND !empty($settingDetails->thr))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->thr);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
									
						        break;
						    case 'fri':
						      //  echo $settingDetails->fr;
						        if(isset($settingDetails->fri) AND !empty($settingDetails->fri))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->fri);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
								
						        break;		
						    case 'sat':
						       // echo $settingDetails->sa;
						        if(isset($settingDetails->sat) AND !empty($settingDetails->sat))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->sat);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												echo "<option value=".$opentime.">".$opentime.":00</option>";
										 }
									}
								 }
									
						        break;				   
						    default:
						        0;
						} 

		 				 	 	// $data['cart_total'] = $this->cart->total();
			 	 	// $data['shop_name'] = $this->session->userdata['shop_name']['shop_name'];
			 	 	// $resturant_id = $this->session->userdata['shop_name']['resturant_id'];
			 	 				 	 	
			 	 	// $settingDetails = $this->freelancer_model->getshopdetailsetting($resturant_id);

			 	 	// $data['user_detail'] = $this->freelancer_model->getuserdetail($sess_id);	
			 	 	// $this->load->view('checkout', $data);
			 				  	
		 }
function checkpayurl(){
	 redirect('thuistoday://');
}
		 /*******************************************
	Paypal pay ment form and detail
	********************************************/
    function pay(){
		// $this->db->select('album_name');
		// $this->db->where('id', $album_id);
		// $query = $this->db->get('sc_album_category');
		// $album = $query->row();
		$token = $this->input->get('token');
       // if(!empty($token))
       //    {

       //    }
    	$amount = '0';
    	$user_id = '0';	
    	if(!empty($token))
          {
          		$dataResult1 = $this->freelancer_model->getdatawithtoken($token);
                      if(!empty($dataResult1))
                       {
                         $amount = $dataResult1->total_payment;
                         $user_id = $dataResult1->user_id;
                         $returnURL = base_url().'index.php/Cart/checkpayurl'; //payment success url
					     $cancelURL = base_url().'index.php/Cart/cancel'; //payment cancel url
					     $notifyURL = base_url().'index.php/Cart/checkpayurl'; //ipn url
                       }
          }
        else
        {        	
			$amount = $this->session->userdata['payment_detail'] ['amount'];
			//print_r($this->session->userdata['payment_detail'] ['amount']);die;
			$user_id = $this->session->userdata['payment_detail'] ['user_id'];
			//Set variables for paypal form 
	        $returnURL = base_url().'index.php/Paypal/ipn'; //payment success url
	        $cancelURL = base_url().'index.php/Cart/cancel'; //payment cancel url
	        $notifyURL = base_url().'index.php/Paypal/ipn'; //ipn url
		}
        
        //get particular product data
		// $login = $this->session->userdata('login');
  //       $userID = $login->id; //current user id
        $logo = base_url().'images/logo.png';
        
        $this->paypal_lib->add_field('business', 'aparna.immanentsolutions109@gmail.com');
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('item_name', 'pay as you want!');
        $this->paypal_lib->add_field('custom', $user_id);
        $this->paypal_lib->add_field('cents', 0.30);
        $this->paypal_lib->add_field('item_number', 1);
        $this->paypal_lib->add_field('amount', $amount); 
      //  $this->paypal_lib->add_field('mc_currency', 'EUR'); 
            
        $this->paypal_lib->image($logo);
        
        $this->paypal_lib->paypal_auto_form();
    }

    // ********************** function is used for save payment values in session ************************//
    public function savevaluesinsession()
     {
     	$delivery_time = $this->input->post('time');
     	$delivery_date = $this->input->post('date');
     	$delivery_charge = $this->input->post('delivery_charge');
     	$shopid = $this->input->post('shopid');

     	$session_data = array('delivery_time'=>$delivery_time, 'delivery_date'=>$delivery_date, 'delivery_charge'=>$delivery_charge, 'shopid'=>$shopid);
		   $this->session->set_userdata('session_payment', $session_data);
		   print_r($this->session->userdata('session_payment'));
     }
//************************************* function to send the payment thought session *****************************//
     public function sendpaymentthoughsession()
     {
     	$amount = $this->input->post('amount');
		$user_id = $this->input->post('user_id');
		$url = $this->input->post('url');
		if(!empty($amount) and !empty($user_id))
		{
		   $session_data = array('amount'=>$amount, 'user_id'=>$user_id);
		   $this->session->set_userdata('payment_detail', $session_data);
		   print_r($url);
		}
     }
  //********************* Function is used for Ideal payment gateway ***********************//

     public function idealpay()
      {
      	$this->load->view('payideal');
      }
}

?>
