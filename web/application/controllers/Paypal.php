<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Paypal extends CI_Controller 
{
     function  __construct(){
        parent::__construct();
        $this->load->library('paypal_lib');
        $this->load->library('cart');
        $this->load->model('freelancer_model');
        $this->load->model('Shopowner_model');
       // $this->load->model('Web_album_model');
     }
     function ipnss(){
       echo "<script>
          alert('Your transaction is successfully completed. Thanks!!');
          window.location.href='/web';
          </script>";}
     
     function ipn(){              
        $deliveryOption = $this->session->userdata('session_payment');
        if(!empty($deliveryOption)){
			$data['delivery_time'] = $deliveryOption['delivery_time'];
			$data['delivery_date'] = $deliveryOption['delivery_date'];
			$data['delivery_charge'] = $deliveryOption['delivery_charge'];
			$data['shopid'] = $deliveryOption['shopid'];               
            $cart = $this->cart->contents();
            $data['cart'] = json_encode($cart);
            $paypalInfo    = $this->input->post();
            $data['trans_id'] = $paypalInfo["txn_id"];
      		$data['user_id'] = $paypalInfo['custom'];
            $data['service_charge'] = '0.30';//$paypalInfo['cents'];
			// $data['album_id'] = $paypalInfo["item_number"];
            $data['total_payment'] = $paypalInfo["mc_gross"];
			// $data['currency_code'] = $paypalInfo["mc_currency"];
			// $data['payer_email'] = $paypalInfo["payer_email"];
            $data['status'] = $paypalInfo["payment_status"];
            $data['date'] = $paypalInfo["payment_date"];
            $data['payment_method'] = 'Paypal';
            $data['quantity'] = count($cart);	
			$paypalURL = $this->paypal_lib->paypal_url;        
			$result    = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);
			//$token = $this->Web_album_model->check_token($data['album_id']);	
			//check whether the payment is verified
			if(preg_match("/VERIFIED/i",$result)){
				//insert the transaction data into the database
			   // $this->Web_album_model->insertTransaction($data);
				$dataResult = $this->freelancer_model->inserOrders($data);
				if(!empty($dataResult)){ 
			 /////////////////////////////////////////// email send ////////////////////////////////////////////////
					$email = $this->Shopowner_model->getshopemailwithid($data['shopid']);
					$EMAILADD = $email->user_email; 
					$userdetails = $this->Shopowner_model->getuserdetail($data['user_id']);
					//////				
					$tempdata['order_detail'] = $dataResult;
					$tempdata['shop_detail'] = $email;		
					$tempdata['user_detail'] = $userdetails;
					$tempdata['orderd_products'] = $cart;	
					$body = $this->load->view('order',$tempdata,TRUE);
					$usrname = $userdetails->name;
					$usremail = $userdetails->email;
					$usrphone = $userdetails->phone;
					$usraddress = $userdetails->address;

					 /* $body = 'New order
						  Customer Name:'.$usrname.'
						  Email:'.$usremail.'
						  Phone:'.$usrphone.'
						  Adress:'.$usraddress; */
					include APPPATH.'phpmailer/PHPMailerAutoload.php';
					$mail = new PHPMailer();
					$mail->SMTPDebug = true; 
					$mail->SMTPAuth = true;
					$mail->Host = 'mail.justdemo.org';
					$mail->Port = 25;
					$mail->Username = 'immanentinfo@justdemo.org';
					$mail->Password = 'netone@108'; 
					$mail->setFrom('immanentinfo@justdemo.org');
					$mail->IsHTML(true); 
					$mail->addAddress($EMAILADD); 
					//$mail->AddCC($sub_email, $sub_name);
					$mail->Subject = 'Home Today';
					$mail->msgHTML($body);
					$mail->send();
					  ///////         ///////////////////////////////////////////////////////////////////////////////////////////////////////     
				  
					$this->load->view('payment-succesfull',$tempdata);
				}
        }
      }
    }
    public function welcome()
    { 
      redirect('/Welcome/index');
    }

 //********************* function  is used for save the pick at locations orders ********************************//
    public function pickordersave(){
        $deliveryOption = $this->session->userdata('session_payment'); 
        if(!empty($deliveryOption)){
			$data['delivery_time'] = $deliveryOption['delivery_time'];
			$data['delivery_date'] = $deliveryOption['delivery_date'];
			$data['delivery_charge'] = $deliveryOption['delivery_charge'];                 
            $data['shopid'] = $deliveryOption['shopid'];
        }
        $amount = $this->session->userdata['payment_detail'] ['amount'];
        $user_id = $this->session->userdata['payment_detail'] ['user_id'];
        $cart = $this->cart->contents();
        $data['cart'] = json_encode($cart);
        $data['date'] = date("h:i:s M d, Y");
                
        $data['user_id'] = $user_id;
        $data['total_payment'] = $amount;
             
        $data['payment_method'] = 'Free';
        $data['quantity'] = count($cart);
        $dataResult = $this->freelancer_model->inserOrders($data);
        if(!empty($dataResult)){
         /////////////////////////////////////////// email send ////////////////////////////////////////////////
            $email = $this->Shopowner_model->getshopemailwithid($data['shopid']);
			$EMAILADD = $email->user_email;
			$userdetails = $this->Shopowner_model->getuserdetail($data['user_id']);
			//////  				
			$tempdata['order_detail'] = $dataResult;
			$tempdata['shop_detail'] = $email;		
			$tempdata['user_detail'] = $userdetails;
			$tempdata['orderd_products'] = $cart;	
			$body = $this->load->view('order',$tempdata,TRUE);
		
			$usrname = $userdetails->name;
			$usremail = $userdetails->email;
			$usrphone = $userdetails->phone;
			$usraddress = $userdetails->address;
			
			include APPPATH.'phpmailer/PHPMailerAutoload.php';
			$mail = new PHPMailer();
			$mail->SMTPDebug = true; 
			$mail->SMTPAuth = true;
			$mail->Host = 'mail.justdemo.org';
			$mail->Port = 25;
			$mail->Username = 'immanentinfo@justdemo.org';
			$mail->Password = 'netone@108'; 
			$mail->setFrom('immanentinfo@justdemo.org');
			$mail->IsHTML(true); 
			$mail->addAddress($EMAILADD);
			//$mail->AddCC($sub_email, $sub_name);
			$mail->Subject = 'Home Today';
			$mail->msgHTML($body);
			$mail->send();
			///////         
			$this->load->view('payment-succesfull',$tempdata);
		}       
    }
}