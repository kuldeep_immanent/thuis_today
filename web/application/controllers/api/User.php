<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class User extends CI_Controller
{
        public function __construct()
        {  
	        parent::__construct();  
	        $this->load->model('api/User_model');
	        $this->load->model('Shopowner_model');
			$this->load->library("JWT");
	    }

//************************************//
//*** function used for registration ***//
//*******************************//

public function register()
 {
	 $data['name'] = $this->input->post('name');
	 $data['email'] = $this->input->post('email');
	 if(isset($_POST['device_id']))
		{
		$data['device_id'] = $this->input->post('device_id');
		}
		if(isset($_POST['device_type']))
		{
		$data['device_type'] = $this->input->post('device_type');
		}
	if(isset($_POST['password']) && !empty($this->input->post('password')))
	 {
	 $data['password'] = $this->input->post('password');
	 }
	 $data['phone'] = $this->input->post('phone');
	 $data['zipcode'] = $this->input->post('zipcode');
	 $data['address'] = $this->input->post('address');
	 $data['city'] = $this->input->post('city');
	 $email = $this->input->post('email');
	 if(!empty($data['email']))
	 {
		$mail_exists = $this->User_model->mail_exists($data['email']);
		if($mail_exists == false)
		{
			$register = $this->User_model->register_data($data);
			if(!empty($register))
			{
				$data['user_data'] = $register;
				$body = $this->load->view('email_formate',$data,TRUE);
				include APPPATH.'phpmailer/PHPMailerAutoload.php';
				$mail = new PHPMailer();
				$mail->SMTPDebug = true; 
				$mail->SMTPAuth = true;
				$mail->Host = 'mail.justdemo.org';
				$mail->Port = 25;
				$mail->Username = 'immanentinfo@justdemo.org';
				$mail->Password = 'netone@108'; 
				$mail->setFrom('immanentinfo@justdemo.org');
				$mail->IsHTML(true); 
				$mail->addAddress($email);
				//$mail->AddCC($sub_email, $sub_name);
				$mail->Subject = 'Home Today';
				$mail->msgHTML($body);
				if (!$mail->send()) {
					echo 'Mailer Error: '.$mail->ErrorInfo;die;
				}
				else
				{
					echo json_encode(array('jwt'=>$this->jwt->encode(array(
			    'status'=>1,
				'data'=>$register,
				'message'=>'You have registered successfully. A verification link has been sent to your email account. Please use the link to activate your user account.'
			    ), CONSUMER_SECRET)));
				}
			}
			else
			{
				echo json_encode(array('jwt'=>$this->jwt->encode(array(
			      'status'=>0,
				  'message'=>'Error'
			    ), CONSUMER_SECRET)));
			}
		}
		else
		{
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
			      'status'=>0,
				  'message'=>'This email id is already registered. Please register with a different email id.'
			    ), CONSUMER_SECRET)));
		}
	 }
	 else
	 {
		 echo json_encode(array('jwt'=>$this->jwt->encode(array(
			      'status'=>0,
				  'message'=>'Email id is required.'
			    ), CONSUMER_SECRET)));
	 }
 }
	//*************************************//
	//*** function used for verify user ***//
	//*************************************//
 function verify() 
	{
        $id = $this->input->get('id');		
        $data = $this->User_model->verify_user($id); //update the status of the user as verified 
        echo $messages = "<h1>Your account has been successfully activated.</h1>";
		die;		
    }
	//*************************************//
	//*** function used for check mail in facebook condition ***//
	//*************************************//
	function is_login()
	{
		$email = $this->input->post('email');
		$device_id = $this->input->post('device_id');
		$device_type = $this->input->post('device_type');
		$mail_exists = $this->User_model->mail_exists($email);
		if($mail_exists == false)
		{
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>2,
				  'message'=>'You are not register.'
				   ), CONSUMER_SECRET)));
		}
		else
		{
			$data = $this->User_model->fb_userdetail($email,$device_id,$device_type);
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
			'status'=>1,
			'data'=>$data,
			'message'=>'You are successfully login.'
			 ), CONSUMER_SECRET)));
		}
		
	}
	//*************************************//
	//*** function used for login ***//
	//*************************************//
	public function login()
    {
      $email = $this->input->post('email');
	  $password = $this->input->post('password');
	  	   if(isset($_POST['device_id']))
		{
		$data['device_id'] = $this->input->post('device_id');
		}
		if(isset($_POST['device_type']))
		{
		$data['device_type'] = $this->input->post('device_type');
		}
	  $data = $this->User_model->cheak_user_detail($email, $password);

	  if(!empty($data))
	   {
		   $login = $this->User_model->check_verify($email,$password);
	       if(empty($login))
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>0,
				  'message'=>'Your account is not activated. Please activate your account first using the verification link.'
				   ), CONSUMER_SECRET)));
			  }
			  else
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>1,
				  'data'=>$data,
				  'message'=>'You are successfully login.'
				   ), CONSUMER_SECRET)));   
			  }
			  
			  
	   }
	  else
	   {
		  echo json_encode(array('jwt'=>$this->jwt->encode(array(
	      'status'=>0,
		  'message'=>'Your email or password is not matched!'
	  	  ), CONSUMER_SECRET)));
	   }	  
	}

		 /////////////////////////////////////////////////////////////////////////
	// ******************* To Generate Random Numbers ****************//
	public function random_string($length) 
	  {
			$key = '';
			$keys = array_merge(range(0, 9), range('a', 'z'));
	  		for ($i = 0; $i < $length; $i++) 
				{
					$key .= $keys[array_rand($keys)];
	            }	
	        	 return $key;
      }
	//*************************************//
	//*** function used for Shop Type ***//
	//*************************************//
	function shopType()
	{
		$data = $this->User_model->shopType();
		if(!empty($data))
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>1,
				  'data'=>$data
				   ), CONSUMER_SECRET)));
			  }
			  else
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>0,
				  'message'=>'There have no shop.'
				   ), CONSUMER_SECRET)));   
			  }
	}
	//*************************************//
	//*** function used for Fetch product ***//
	//*************************************//
	function fetchProduct()
	{
		$shop_id = $this->input->post('shop_id');
		$category_id = $this->input->post('category_id');
		$data = $this->User_model->fetchProduct($shop_id,$category_id);
		if(!empty($data))
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>1,
				  'data'=>$data
				   ), CONSUMER_SECRET)));
			  }
			  else
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>0,
				  'message'=>'There have no product.'
				   ), CONSUMER_SECRET)));   
			  }
	}
	//****************************************//
	//function used for Fetch product category//
	//****************************************//
	function fetchProductCategory()
	{
		$shop_id = $this->input->post('shop_id');
		$data = $this->User_model->fetchProductCategory($shop_id);
		if(!empty($data))
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>1,
				  'data'=>$data
				   ), CONSUMER_SECRET)));
			  }
			  else
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>0,
				  'message'=>'There have no product category.'
				   ), CONSUMER_SECRET)));   
			  }
	}
	//****************************************//
		//function used for Find shop//
	//****************************************//
	function findShop()
	{
		$cityname = $this->input->post('cityname');
		$zipcode = $this->input->post('zipcode');
		$category = $this->input->post('category');
		if(empty($cityname) && empty($category))
		{
			$data = $this->User_model->findShopZip($zipcode);
			if(!empty($data))
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>1,
				  'data'=>$data
				   ), CONSUMER_SECRET)));
			  }
			  else
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>0,
				  'message'=>'No shop found with zipcode.'
				   ), CONSUMER_SECRET)));   
			  }
		}
		else if(empty($zipcode) && empty($category))
		{
			$data = $this->User_model->findShopCity($cityname);
			if(!empty($data))
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>1,
				  'data'=>$data
				   ), CONSUMER_SECRET)));
			  }
			  else
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>0,
				  'message'=>'No shop found with street or city name.'
				   ), CONSUMER_SECRET)));   
			  }
		}
		else if(empty($cityname) && !empty($zipcode) && !empty($category))
		{
			$data = $this->User_model->findShopZipCatg($zipcode,$category);
			if(!empty($data))
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>1,
				  'data'=>$data
				   ), CONSUMER_SECRET)));
			  }
			  else
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>0,
				  'message'=>'No shop found with zipcode or category id.'
				   ), CONSUMER_SECRET)));   
			  }
		}
		else if(!empty($cityname) && empty($zipcode) && !empty($category))
		{
			$data = $this->User_model->findShopCityCatg($cityname,$category);
			if(!empty($data))
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>1,
				  'data'=>$data
				   ), CONSUMER_SECRET)));
			  }
			  else
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>0,
				  'message'=>'No shop found with zipcode or category id.'
				   ), CONSUMER_SECRET)));   
			  }
		}
			
	}
	//****************************************//
		//function used for Find street/city//
	//****************************************//
	function findStreetCity()
	{
		$cityname = $this->input->post('cityname');
		//if(!empty($cityname))
		//{
			$data = $this->User_model->findStreetCity($cityname);
			if(!empty($data))
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>1,
				  'data'=>$data
				   ), CONSUMER_SECRET)));
			  }
			  else
			  {
				  echo json_encode(array('jwt'=>$this->jwt->encode(array(
				  'status'=>0,
				  'message'=>'No result found!'
				   ), CONSUMER_SECRET)));   
			  }
		//}
	}
	//****************************************//
		//function used for place an order//
	//****************************************//
	function placeAnOrder()
	{	
		$data['user_id'] = $this->input->post('user_id');
		$data['total_payment'] = $this->input->post('total_payment');
		$data['date'] = $this->input->post('date');
		$data['delivery_date'] = $this->input->post('delivery_date');
		$data['delivery_time'] = $this->input->post('delivery_time');
		$data['quantity'] = $this->input->post('quantity');
		$data['delivery_charge'] = $this->input->post('delivery_charge');
		
		$payment_method = $this->input->post('payment_method');
		$service_charge = $this->input->post('service_charge');
		$cart = $this->input->post('cart');
		//$status = $this->input->post('status');
		$shopid = $this->input->post('shopid');
		$delivery_option = $this->input->post('delivery_option');
		$trans_id = $this->input->post('trans_id');
		if(!empty($payment_method)){$data['payment_method'] = $payment_method;}
		if(!empty($service_charge)){$data['service_charge'] = $service_charge;}
		if(!empty($cart)){$data['cart'] = $cart;}
		if(!empty($shopid)){$data['shopid'] = $shopid;}
		if(!empty($delivery_option)){$data['delivery_option'] = $delivery_option;}
		if(!empty($trans_id)){$data['trans_id'] = $trans_id;}
		$data['token'] = $this->random_string(8);
    	$token = $data['token'];
		$data1 = $this->User_model->placeAnOrder($data);
		if(!empty($data1))
		{	
			$email = $this->Shopowner_model->getshopemailwithid($shopid);
			$EMAILADD = $email->user_email;
			$userdetails = $this->Shopowner_model->getuserdetail($data['user_id']);
			
			$tempdata['order_detail'] = $data1;
			$tempdata['shop_detail'] = $email;		
			$tempdata['user_detail'] = $userdetails;
			$tempdata['orderd_products'] = json_decode($cart);
			/* foreach($tempdata['orderd_products'] as $val){
				echo $val->coupon;
				echo $val->name;
				echo $val->qty;
				echo $val->price;
				echo '<br>';
			} 
		exit; */
			$body = $this->load->view('API/order',$tempdata,TRUE);
			
			//////	
			$usrname = $userdetails->name;
			$usremail = $userdetails->email;
			$usrphone = $userdetails->phone;
			$usraddress = $userdetails->address;

			/* $body = 'New order
						Customer Name:'.$usrname.'
						Email:'.$usremail.'
						Phone:'.$usrphone.'
						Adress:'.$usraddress; */
				include APPPATH.'phpmailer/PHPMailerAutoload.php';
				$mail = new PHPMailer();
				$mail->SMTPDebug = true; 
				$mail->SMTPAuth = true;
				$mail->Host = 'mail.justdemo.org';
				$mail->Port = 25;
				$mail->Username = 'immanentinfo@justdemo.org';
				$mail->Password = 'netone@108'; 
				$mail->setFrom('immanentinfo@justdemo.org');
				$mail->IsHTML(true); 
				$mail->addAddress($EMAILADD);
				//$mail->AddCC($sub_email, $sub_name);
				$mail->Subject = 'Home Today';
				$mail->msgHTML($body);
				$mail->send();
			///////
			$data['paypal_url'] = base_url().'Cart/pay?token='.$token;
			$data['ideal_url'] = base_url().'Ideal?token='.$token;
			
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
			'status'=>1,
			'data'=>$data,
			'message'=>'Your order successfully placed.'
			), CONSUMER_SECRET)));
		}
		else
		{
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
			'status'=>0,
			'message'=>'Your order not placed.Plese try again!'
			), CONSUMER_SECRET)));   
		}
	}
	//****************************************//
	  //function used for get order history//
	//****************************************//
	function orderHistory()
	{
		$user_id = $this->input->post('user_id');
		$offset = $this->input->post('offset');
		$data = $this->User_model->orderHistory($user_id,$offset);
		if(!empty($data))
		{
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
			'status'=>1,
			'data'=>$data
			), CONSUMER_SECRET)));
		}
		else
		{
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
			'status'=>0,
			'message'=>'No result found'
			), CONSUMER_SECRET)));   
		}
	}
	//****************************************//
	  //function used for get order detail//
	//****************************************//
	function orderDetail()
	{
		$id = $this->input->post('order_id');
		$data = $this->User_model->orderDetail($id);
		if(!empty($data))
		{
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
			'status'=>1,
			'data'=>$data
			), CONSUMER_SECRET)));
		}
		else
		{
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
			'status'=>0,
			'message'=>'No result found'
			), CONSUMER_SECRET)));   
		}
	}
	//****************************************//
	  //function used for edit profile//
	//****************************************//
	function edit_profile()
	{
		$id = $this->input->post('user_id');
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
		$zipcode = $this->input->post('zipcode');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		if(!empty($name)){$data['name'] = $name;}
		if(!empty($phone)){$data['phone'] = $phone;}
		if(!empty($zipcode)){$data['zipcode'] = $zipcode;}
		if(!empty($address)){$data['address'] = $address;}
		if(!empty($city)){$data['city'] = $city;}
		$new_data = $this->User_model->edit_profile($data, $id);
            if($new_data)
			{
				echo json_encode(array('jwt'=>$this->jwt->encode(array(
			     	 'status'=>1,
					 'data'=>$new_data,
				 	 'message'=>'Your profile updated successfully.'
			   		 ), CONSUMER_SECRET)));
			}
			else
			{
				echo json_encode(array('jwt'=>$this->jwt->encode(array(
			    'status'=>0,
				'message'=>'Profile not updated.'
			   	), CONSUMER_SECRET)));
			}	
	}
	//****************************************//
	  //function used for edit user image//
	//****************************************//
	function editUser_image()
	{
		$id = $this->input->post('user_id');
		if(!empty($_FILES["profile_pic"]["name"]))
		{
			$config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = '52428800';
            $this->load->library('upload', $config);  
			if ( ! $this->upload->do_upload('profile_pic')):                
                        $error = array('error' => $this->upload->display_errors());
                        echo json_encode(array('status'=>0, 'message'=>'Large File Size'));die;                   
                else: //$upload = $this->upload->data('file_name');
				      $profile = $this->upload->data('file_name');
					   //print_r($profile);die;
					   $data['profile_pic'] = base_url().'uploads/'.$profile;endif; 
        $new_data = $this->User_model->edit_profile($data, $id);
            if($new_data)
			{
				echo json_encode(array('jwt'=>$this->jwt->encode(array(
			     	 'status'=>1,
					 'data'=>$new_data,
				 	 'message'=>'Your profile updated successfully.'
			   		 ), CONSUMER_SECRET)));
			}
			else
			{
				echo json_encode(array('jwt'=>$this->jwt->encode(array(
			    'status'=>0,
				'message'=>'Profile not updated.'
			   	), CONSUMER_SECRET)));
			}
	    }
		else
		{
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
			    'status'=>0,
				'message'=>'Profile not updated.'
			   	), CONSUMER_SECRET)));
		}	
	}
	//****************************************//
	  //function used for checkouttimingweekdays//
	//****************************************//
	public function checkouttimingweekdays()
		 {
			$date = $this->input->post('opendate');
		 	$shopid = $this->input->post('shopid');
		 	$strtotime = strtotime($date);
		 	$day = strtolower(date("D", $strtotime));
			//echo $day;die;
			$settingDetails = $this->User_model->getshopdetailsetting($shopid);

		 	switch ($day) {
						    case 'sun':
						      // echo $settingDetails->su;						       
						       	$tm = '';
								if(isset($settingDetails->sun) AND !empty($settingDetails->sun))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->sun);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												$tm.= $opentime.":00,";
												
										 }
										 $new_data = rtrim($tm,',');
										 echo json_encode(array('jwt'=>$this->jwt->encode(array(
										'status'=>1,
										'data'=>$new_data
										), CONSUMER_SECRET)));
									}
								 }
								 else{
								echo json_encode(array('jwt'=>$this->jwt->encode(array(
								'status'=>0,
								'message'=>'Shop closed on this day.'
								 ), CONSUMER_SECRET)));}	
						        break;
						    case 'mon':
							$tm = '';
						    	if(isset($settingDetails->mon) AND !empty($settingDetails->mon))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->mon);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												$tm.= $opentime.":00,";
												
										 }
										 $new_data = rtrim($tm,',');
										 echo json_encode(array('jwt'=>$this->jwt->encode(array(
										'status'=>1,
										'data'=>$new_data
										), CONSUMER_SECRET)));
									}
								 }
								echo json_encode(array('jwt'=>$this->jwt->encode(array(
								'status'=>0,
								'message'=>'Shop closed on this day.'
								), CONSUMER_SECRET)));								
						        break;
						    case 'tue':
							$tm = '';
						        //echo $settingDetails->tu;
						        if(isset($settingDetails->tue) AND !empty($settingDetails->tue))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->tue);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												$tm.= $opentime.":00,";
												
										 }
										 $new_data = rtrim($tm,',');
										 echo json_encode(array('jwt'=>$this->jwt->encode(array(
										'status'=>1,
										'data'=>$new_data
										), CONSUMER_SECRET)));
									}
								 }
								 else{
							echo json_encode(array('jwt'=>$this->jwt->encode(array(
								'status'=>0,
								'message'=>'Shop closed on this day.'
								 ), CONSUMER_SECRET)));}
						        break;
						    case 'wed':
							$tm = '';
						        //echo $settingDetails->we;
						        if(isset($settingDetails->wed) AND !empty($settingDetails->wed))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->wed);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												$tm.= $opentime.":00,";
												
										 }
										 $new_data = rtrim($tm,',');
										 echo json_encode(array('jwt'=>$this->jwt->encode(array(
										'status'=>1,
										'data'=>$new_data
										), CONSUMER_SECRET)));
									}
								 }
								 else{
							echo json_encode(array('jwt'=>$this->jwt->encode(array(
								'status'=>0,
								'message'=>'Shop closed on this day.'
								 ), CONSUMER_SECRET)));}
						        break;
						    case 'thu':
							$tm = '';
						        //echo $settingDetails->th;
								$tm = '';
						        if(isset($settingDetails->thr) AND !empty($settingDetails->thr))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->thr);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												$tm.= $opentime.":00,";
												
										 }
										 $new_data = rtrim($tm,',');
										 echo json_encode(array('jwt'=>$this->jwt->encode(array(
										'status'=>1,
										'data'=>$new_data
										), CONSUMER_SECRET)));
										 
									}
								 }else{
								echo json_encode(array('jwt'=>$this->jwt->encode(array(
								'status'=>0,
								'message'=>'Shop closed on this day.'
								 ), CONSUMER_SECRET)));}	
						        break;
						    case 'fri':
							$tm = '';
						      //  echo $settingDetails->fr;
						        if(isset($settingDetails->fri) AND !empty($settingDetails->fri))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->fri);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												$tm.= $opentime.":00,";
												
										 }
										 $new_data = rtrim($tm,',');
										 echo json_encode(array('jwt'=>$this->jwt->encode(array(
										'status'=>1,
										'data'=>$new_data
										), CONSUMER_SECRET)));
									}
								 }
								 else{
								echo json_encode(array('jwt'=>$this->jwt->encode(array(
								'status'=>0,
								'message'=>'Shop closed on this day.'
								 ), CONSUMER_SECRET)));}
						        break;		
						    case 'sat':
							$tm = '';
						       // echo $settingDetails->sa;
						        if(isset($settingDetails->sat) AND !empty($settingDetails->sat))
						    	 {
							        list($open, $close) = preg_split('/[-]/', $settingDetails->sat);
								    $opneti = substr($open, 0, 2); $closetime = substr($close, 0, 2);
								    $opentime = $opneti+1;
							      if($opentime < $closetime)
							      	{ 
								      	for ($opentime; $opentime < $closetime; $opentime=$opentime+1)
								         { 
												$tm.= $opentime.":00,";
												
										 }
										 $new_data = rtrim($tm,',');
										 echo json_encode(array('jwt'=>$this->jwt->encode(array(
										'status'=>1,
										'data'=>$new_data
										), CONSUMER_SECRET)));
									}
								 }
								 else{
								echo json_encode(array('jwt'=>$this->jwt->encode(array(
								'status'=>0,
								'message'=>'Shop closed on this day.'
								 ), CONSUMER_SECRET)));	}
						        break;				   
						    default:
						        0;
						} 
		}
	//*****************************************//
	//*** function used for forget password ***//
	//*****************************************//
	public function forget_password()
	{
		$email = $this->input->post('email');
		$data = $this->User_model->mail_exists($email);
		if(!empty($data))
		{			
			$user_name = $data->name;
			$user_id = $data->id;
			$otp = str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
			$body="User Id:- ".$user_id."<br/>".$user_name."<br/>".$email."<br/>Your OTP number is: ".$otp." Use this number to verify the password change process.<br/><br/> Thanks";
				/* $this->load->library('email');
				$this->email->from('youexample@gmail.com', 'Testing');
			    $this->email->to($email);
			    $this->email->subject('OTP');
				$this->email->set_mailtype("html");
			    $this->email->message($message);
				if($this->email->send()) */
				include APPPATH.'phpmailer/PHPMailerAutoload.php';
				$mail = new PHPMailer();
				$mail->SMTPDebug = true; 
				$mail->SMTPAuth = true;
				$mail->Host = 'mail.justdemo.org';
				$mail->Port = 25;
				$mail->Username = 'immanentinfo@justdemo.org';
				$mail->Password = 'netone@108'; 
				$mail->setFrom('immanentinfo@justdemo.org');
				$mail->IsHTML(true); 
				$mail->addAddress($email);
				//$mail->AddCC($sub_email, $sub_name);
				$mail->Subject = 'Home Today';
				$mail->msgHTML($body);
				if($mail->send())
				{	
					//$this->User_model->insert_otp($user_id,$otp);
					echo json_encode(array('jwt'=>$this->jwt->encode(array(
					'status'=>1,
					'user_id'=>$user_id,
					'otp'=>$otp,
					'message'=>'OTP is sent on your email.'
					), CONSUMER_SECRET)));
				}
				else 
				{
			        echo json_encode(array('jwt'=>$this->jwt->encode(array(
			      	'status'=>0,
				 	'message'=>'Error.'
			  		 ), CONSUMER_SECRET)));
				}
		}
		else
		{
			echo json_encode(array('jwt'=>$this->jwt->encode(array(
		    'status'=>0,
			'message'=>'Username is not matched!'
		    ), CONSUMER_SECRET)));
		}	
	}
	//******************************************//
	//*** function used for new passwrd ********//
	//******************************************//
	function new_password()
	{
		if(isset($_POST['old_password']) && !empty($_POST['old_password']))
		{
			$old_password = $this->input->post('old_password');
			$user_id = $this->input->post('user_id');
			$new_password = $this->input->post('new_password');
			$data = $this->User_model->old_password($old_password,$user_id,$new_password);
			if($data)
			{
				 $success_pass=$this->User_model->update_password($user_id, $new_password);
				 if($success_pass)
				  {  
					  //$this->User_model->delete_otp($user_id); 
					 echo json_encode(array('jwt'=>$this->jwt->encode(array(
					 'status'=>1,
					 'message'=>'Password changed successfully.'
					 ), CONSUMER_SECRET)));
				  }
			}
			else
			{
				//$this->User_model->delete_otp($user_id); 
				 echo json_encode(array('jwt'=>$this->jwt->encode(array(
				 'status'=>0,
				 'message'=>'Password not matched.'
				 ), CONSUMER_SECRET)));
		    }
		}
		else
		{
		$user_id = $this->input->post('user_id');
		$new_password = $this->input->post('new_password');

		$success_pass=$this->User_model->update_password($user_id, $new_password);
	     if($success_pass)
		  {  
		  	  //$this->User_model->delete_otp($user_id); 
		 	 echo json_encode(array('jwt'=>$this->jwt->encode(array(
	     	 'status'=>1,
		 	 'message'=>'Password updated successfully.'
	   		 ), CONSUMER_SECRET)));
		  }
		}
	}
}
?>