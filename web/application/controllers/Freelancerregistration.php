<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Freelancerregistration extends CI_Controller {

	   function __construct()
    {
      parent::__construct();
      $this->load->model('freelancer_model');
      $this->load->library('form_validation');
      $this->load->library('pagination'); 
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
  public function index()
	{			
		
		$data['token'] = $this->input->get('token');
		// $data['id'] = $this->input->get('id');
		 if(!empty($data['token']))
			{	
		// 		$this->load->model('freelancer_model');
				$data['user_detail'] = $this->freelancer_model->tochecktoken($data['token']);
				if(!empty($data['user_detail']))
				 {
				 	$this->load->view('freelancerregister', $data);
				 }
				else
				 {
				 	echo "<h1>Page Not found</h1>";
				 	die;
				 }				
			}
		 else
			{
				echo "<h1> Page Not found </h1>";
				die;
			}
	}

	//////////////////////////** For Freelancer Email Request***/////////////////////////////////
	public function request(){
		$user_token = $this->random_string(15);	
		$email=$this->input->post('email');
		if(!empty($email)){	

			$checkemail = $this->freelancer_model->checkemail($email);
			if(empty($checkemail)){

				$checkemailnotactive = $this->freelancer_model->checkemailnotactive($email);
				if(!empty($checkemailnotactive))
					{ $id = $checkemailnotactive->id;
					  $user_tokens = $checkemailnotactive->token;
					 }
			  if(empty($checkemailnotactive))
				{	
				 //////////////////////////
						usleep(200000);
					////////////////////////////////////////////						
					$user_tokens = $this->freelancer_model->insert($email, $user_token);
				}
			////////////////////////////////////////////
					$message = '<p>Here is a your registration link. Please click to registered as a freelancer. </p><p>Thanks.</p>'.base_url().'index.php/Freelancerregistration?token='.$user_tokens;		
			
			      //////////////////////////
			      // Configure email library
					
					$config['smtp_host'] = 'mail.justdemo.org';
					$config['smtp_port'] = 25;
					$config['smtp_user'] = 'immanentinfo@justdemo.org';
					$config['smtp_pass'] = 'netone@108';

					// Load email library and passing configured values to email library
					$this->load->library('email', $config);
					$this->load->helper('path');
					// Sender email address
					$this->email->from('immanentinfo@demodemo.ga', 'Home Today');
					// Receiver email address
					$this->email->to($email);
					// Subject of email
					$this->email->subject('Registration Link');
					$this->email->set_mailtype("html");
					// Message in email
					$this->email->message($message);
					$path = set_realpath('uploads/file/');		     	
			    	$this->email->attach($path . 'Freelance verkoper.pdf');
					
			      ////////////////////////////
			      // $this->email->send();
				      if($this->email->send())
				         {
				         	$this->session->set_flashdata('error_msg', 'Your request has been sent successfully and Registration link has been sent to your email account.');				         	
					     }
				         else
				        {
				         show_error($this->email->print_debugger());
				        }
				  }
				  else
				  {
				  	$this->session->set_flashdata('error_msg1', 'This email is already registered and active. Please try with different email.');
				  }
				  

			///////////////////////////////////////////
				
		}
		$this->load->view('request');
	}
	
	 /////////////////////////////////////////////////////////////////////////
	// ******************* To Generate Random Numbers ****************//
	public function random_string($length) 
	  {
			$key = '';
			$keys = array_merge(range(0, 9), range('a', 'z'));
	  		for ($i = 0; $i < $length; $i++) 
				{
					$key .= $keys[array_rand($keys)];
	            }	
	        	 return $key;
      }
// //******************************************************************//
 ////****** function is used for Save Data of New Freelancer for Registration *******//
// //******************************************************************//

	public function freelancer_registration()
	{		 
		$data['name']=$this->input->post('f_name').' '.$this->input->post('l_name');
		$data1['id'] = $this->input->post('id');
		$data1['email'] = $this->input->post('eml');		
		$data['contact']=$this->input->post('Con_number');
		$data['password']=$this->input->post('password');
		$data['bank_account']=$this->input->post('bank_acc');
		$data['address']=$this->input->post('address');
		$data['status']='2';

		// $image_name = isset($_FILES["file_attach"]["name"]);
		
		$this->form_validation->set_message('matches', 'Password and Confirm password does not match.');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[confirmpass]');
		$this->form_validation->set_rules('confirmpass', 'Password Confirmation', 'required');
		$this->form_validation->set_rules('password','password','trim|required|min_length[6]|max_length[20]');
		
		if ($this->form_validation->run() == FALSE)
		{
			
			$this->load->view('freelancerregister', $data1);
		}
		else
		{
			 
			$message = $this->load->view('email_temp_freelancer',$data,TRUE);
			      // Configure email library  
				  //echo'<pre>';print_r($message);exit;
			$EMAIL = $data1['email'];		
			$config['smtp_host'] = 'mail.justdemo.org';
			$config['smtp_port'] = 25;
			$config['smtp_user'] = 'immanentinfo@justdemo.org';
			$config['smtp_pass'] = 'netone@108';
			// Load email library and passing configured values to email library
			$this->load->library('email', $config);
			$this->load->helper('path');
			// Sender email address
			$this->email->from('immanentinfo@demodemo.ga', 'Home Today');
			// Receiver email address
			$this->email->to($EMAIL);
			// Subject of email
			$this->email->subject('Registration Link');
			$this->email->set_mailtype("html");
			// Message in email
			$this->email->message($message);
		// if(!empty($image_name))
		// {
		// 	$config['upload_path'] = './uploads/';
		// 	$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|docx';
		// 	$config['max_size'] = 15728640;
		// 	$this->load->library('upload', $config);
		// 	if ( ! $this->upload->do_upload('file_attach')):$error = array('error' => $this->upload->display_errors());
		// 	// echo json_encode(array('jwt'=>$this->jwt->encode(array('status'=>0,'message'=>'Large File Size.'), CONSUMER_SECRET)));
		// 	// die;
		// 	$message = "File Issue!";
		// 	echo "<script type='text/javascript'>alert('$message');</script>";		

		// 	else: //$upload = $this->upload->data('file_name');//$data['image'] = $this->upload->data('orig_name');endif;
		// 	$profile = $this->upload->data('file_name');
		// 	$data['attachment'] = base_url().'uploads/'.$profile;endif;
		// }				
		if($this->email->send()){
			$this->session->set_flashdata('error_msg', 'Your request has been sent successfully and Registration link has been sent to your email account.');				         	
		} else {
			show_error($this->email->print_debugger());
		}
		$insertdata = $this->freelancer_model->freelancernewregistration($data, $data1['id']);
		// if($insertdata == '1'){
		// 		$message = "You have been registered successfully. Please wait untill your account is active. We will inform you through email when your account has been successfully activated.";
		// 		echo "<script type='text/javascript'>alert('$message');</script>";
		// 	}
	    $this->session->set_flashdata('error_msg', 'You have been registered successfully. Please wait untill your account is active. We will inform you through email when your account has been successfully activated.');
		redirect('Welcome');
	}
	}
}
	?>
