<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	 public function __construct() 
     {
        parent::__construct();
          $this->load->library('cart');
	      $this->load->model('Dashboard_model');
          
     }
	public function index()
	{
		$data['categories'] = $this->Dashboard_model->getcategories();
		$this->cart->destroy();
		$this->load->view('index.php', $data);
	}
	public function location_search()
	{
		 $location = $this->input->post('location');
		   $pos = strpos($location, ',');
		  if ($pos === false) {
			  
			$result	= $this->Dashboard_model->location_search($location);
				
		  }else
		  {
			 $city = substr(strrchr($location, ","), 1);
			  $street = strtok($location,  ',');
			$result	= $this->Dashboard_model->location_search($street,$city);
		  }
		if($result != false)
		{
			echo "<ul class='s_list'>";
			foreach($result as $data)
			{
				echo "<li onclick='set_location(this)' id=".$data->wijkcode .">".$data->straatnaam_utf8_nen.' , '.$data->plaatsnaam_utf8_nen ."</li>";
			}
			echo "</ul>";
		}
		die;
	}

   public function terms_contitions()
	{
		$this->load->view('terms_contitions');
	}

   public function privacy_statement()
	{
		$this->load->view('privacy_statement');
	}

   public function cookie_policy()
	{
		$this->load->view('cookie_policy');
	}
	
}
