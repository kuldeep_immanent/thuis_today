<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	 public function __construct() 
     {
        parent::__construct();
	      $this->load->model('dashboard_model');
	      $this->load->library('form_validation');  
	      $this->load->model('freelancer_model'); 
	      $this->load->library('Ajax_pagination');
          $this->perPage = 10;           
     }
//****************** function is used for to load user login page ****************//
   public function index()
	{
	    $sess_id = isset($this->session->userdata['user_data']['user_id']);
	    if(!empty($sess_id))
		 {
		 	//$checkemail = $this->dashboard_model->checkemail($email);
		 	 $data['user_id'] = $this->session->userdata['user_data']['user_id'];
		 	////////////////////////////////////////////////////////////    
	        //total rows count
	        $totalRec = count($this->dashboard_model->togetordersofuser($data['user_id']));
	        //pagination configuration
	        $config['target']      = '#postList';
	        $config['base_url']    = base_url().'Login/ajaxPaginationData';
	        $config['total_rows']  = $totalRec;
	        $config['per_page']    = $this->perPage;
	        $this->ajax_pagination->initialize($config);
	        //get the posts data
	        $data['orders'] = $this->dashboard_model->togetordersofuser($data['user_id'], array('limit'=>$this->perPage));
		/////////////////////////////////////////////////////////////////	
		 	//$data['orders'] = $this->dashboard_model->togetordersofuser($data['user_id']);
		 	$data['user_detail'] = $this->freelancer_model->getuserdetail($data['user_id']);
		 	 $data['user_name'] = $this->session->userdata['user_data']['user_name'];
		 	$this->load->view('profilepage', $data);		   
		 }
		else
		 {  		 	
		 	$setcheck = isset($this->session->userdata['checkout']);
		 	if(!empty($setcheck))
		 	{
		 	  $data['set'] = $this->session->userdata['checkout']['set'];
		 	 // print_r($data['set']);die;
		 	  $this->load->view('login', $data);
		 	}
		 	else{ 	$this->load->view('login'); }
		 }
	}
//*************  function is used for user registration ***********//
	public function register()
	 {
	 	$data = array('name' => $this->input->post('name'),
	 				  'phone' => $this->input->post('phone'),
	 				  'email' => $this->input->post('email'),
	 				  'zipcode' => $this->input->post('zipcode'),
	 				  'city' => $this->input->post('city'),
	 				  'address' => $this->input->post('address'),
	 				  'password' => $this->input->post('password'));
	 	// $this->form_validation->set_message('valid_email', 'Please enter valid email only.');
	 	$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
	 	$this->form_validation->set_message('matches', 'Password and Confirm password does not match.');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[confirmpass]');
		$this->form_validation->set_rules('confirmpass', 'Password Confirmation', 'required');
		$this->form_validation->set_rules('password','password','trim|required|min_length[6]|max_length[20]');
		
		if ($this->form_validation->run() == FALSE)
		 {
			
			$this->load->view('login', $data);
		 }
		else
		 {
		 	$email = $this->input->post('email');
		 	$checkemail = $this->dashboard_model->checkemail($email);	
		 	if(!empty($checkemail))
		 		{   
		 			$this->session->set_flashdata('error_msg1', 'This email is already registered. Please try with different email.');
		 			redirect('Login'); 			
		 		}
		 	 else
		 	  {
			 	$insertdata = $this->dashboard_model->userregistration($data);	
			 	//////////////////////////////////////////////////////////////////////////////////
			 	$name = $this->input->post('name');
			 		 $email = $this->input->post('email');
			 		 $password = $this->input->post('password');

			 		 			$message = '<p>Hi '.$name.'</p><p>You are registered successfully. </p><p>Here is a your username and password:</p><p>Username: '.$email.'</p><p>Password: '.$password.'</p><p>Thanks.</p>';		
			
			      //////////////////////////
			      // Configure email library
					
					$config['smtp_host'] = 'mail.justdemo.org';
					$config['smtp_port'] = 25;
					$config['smtp_user'] = 'immanentinfo@justdemo.org';
					$config['smtp_pass'] = 'netone@108';

					// Load email library and passing configured values to email library
					$this->load->library('email', $config);
					$this->load->helper('path');
					// Sender email address
					$this->email->from('immanentinfo@demodemo.ga', 'Home Today');
					// Receiver email address
					$this->email->to($email);
					// Subject of email
					$this->email->subject('User Detail');
					$this->email->set_mailtype("html");
					// Message in email
					$this->email->message($message);
					// $path = set_realpath('uploads/file/');		     	
			  //   	$this->email->attach($path . 'Freelance verkoper.pdf');
					
			      ////////////////////////////
			      // $this->email->send();
				      if($this->email->send())
				         {
				         	$this->session->set_flashdata('error_msg', 'You have been registered successfully.');				         	
					     }
				         else
				        {
				         show_error($this->email->print_debugger());
				        }
			 	////////////////////////////////////////////////////////////////////////////////////	
		  			
		  			  $session_data = array('username'=>$insertdata->email, 'user_id'=>$insertdata->id, 'user_name'=>$insertdata->name);
		              $this->session->set_userdata('user_data', $session_data);
		              //redirect('Cart/checkout'); 			
		              $this->load->view('login'); 	
			  }
		 }
	 }

//********************* function is used for user login***************************//

	public function do_login()
		{ 
		  $this->form_validation->set_rules('username', 'username', 'trim|required|valid_email');
		  $this->form_validation->set_rules('password', 'Password', 'trim|required');
		    if($this->form_validation->run() == FALSE)
		     { 
		        $this->load->view('login');
		     }
		    else
		    { 
			    $data['username'] = $this->input->post('username');
			    $data['password'] = $this->input->post('password');		     
		   	    $result = $this->dashboard_model->login($data);		     
		          if(!empty($result))
		             {             
		              $session_data = array('username'=>$result->email, 'user_id'=>$result->id, 'user_name'=>$result->name);
		              $this->session->set_userdata('user_data', $session_data);		              
		             $check = $this->input->post('checkoutcheck');
		             	if($check == 'order'){ redirect('Cart/checkout'); 
						}else{
			              	    //$data['user_name'] = $this->session->userdata['user_data']['user_name'];
			 					 redirect('Login/profile');
			              }
		            }
			    else
			    { 	
			   			$this->session->set_flashdata('error_msg1', 'Invalid username or password!');
			            redirect('Login');
			    }
		    }		 
		}

//********************* function is used for profile page***************************//

	public function profile()
	 {
	 	$sess_id = isset($this->session->userdata['user_data']['user_id']);
	    if(!empty($sess_id))
		 {	
		 	 $data['user_id'] = $this->session->userdata['user_data']['user_id'];

		////////////////////////////////////////////////////////////    
	        //total rows count
	        $totalRec = count($this->dashboard_model->togetordersofuser($data['user_id']));
	        //pagination configuration
	        $config['target']      = '#postList';
	        $config['base_url']    = base_url().'Login/ajaxPaginationData';
	        $config['total_rows']  = $totalRec;
	        $config['per_page']    = $this->perPage;
	        $this->ajax_pagination->initialize($config);
	        //get the posts data
	        $data['orders'] = $this->dashboard_model->togetordersofuser($data['user_id'], array('limit'=>$this->perPage));
		/////////////////////////////////////////////////////////////////	
		 //	$data['orders'] = $this->dashboard_model->togetordersofuser($data['user_id']);
		 	$data['user_detail'] = $this->freelancer_model->getuserdetail($data['user_id']);
		 	 $data['user_name'] = $this->session->userdata['user_data']['user_name'];
		 	 
		 	$this->load->view('profilepage', $data);		   
		 }
		 else
		 {
		 	redirect('Welcome');
		 }
	 }
/////////////////////////////// for ajax pagination //////////////////////////////////
	function ajaxPaginationData()
	  {
	  	
	  	$data['user_id'] = $this->session->userdata['user_data']['user_id'];
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //total rows count
        $totalRec = count($this->dashboard_model->togetordersofuser($data['user_id']));
        
        //pagination configuration
        $config['target']      = '#postList';
        $config['base_url']    = base_url().'Login/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
      //  $data['products'] = $this->freelancer_model->getVatpercentageajax($resturant_id, array('start'=>$offset,'limit'=>$this->perPage));
        $data['orders'] = $this->dashboard_model->togetordersofuser($data['user_id'], array('start'=>$offset,'limit'=>$this->perPage));
        //load the view
        $this->load->view('ajax-pagination-profile_page', $data, false);
      }
/////////////////////////////////////////////////

	//********************* function is used for logout a user***************************//

	public function logout()
	 {	 	
	 		$this->session->sess_destroy();		 	 
		 	redirect('Welcome');		
	 } 

	 //************* update profile ***********//
	public function updateprofile()
	 {	 
		 	$pass = $this->input->post('password');
		 	$user_id = $this->input->post('useridd');
		 	if(!empty($pass))
		 		{
				 	$passwordcheck = $this->dashboard_model->checkpass($pass, $user_id);	
				 	if(empty($passwordcheck))
				 		{   
				 			$this->session->set_flashdata('error_msg5', 'The old password does not matched. Please try again.');
				 			redirect('Login/profile'); 			
				 		}
				 	 else
				 	  {
				 	  	$data = array('name' => $this->input->post('name'),
			 				  'phone' => $this->input->post('phone'),
			 				  'zipcode' => $this->input->post('zipcode'),
			 				  'city' => $this->input->post('city'),
			 				  'address' => $this->input->post('address'),
			 				  'password' => $this->input->post('newpassword')
			 				  );
					 	$insertdata = $this->dashboard_model->updateuserprofile($data, $user_id);		
				  		$this->session->set_flashdata('error_msg', 'You have been updated your profile successfully.');		      
				        redirect('Login/profile'); 				
					  }
				}
			else
			   {
			   				$data = array('name' => $this->input->post('name'),
			 				  'phone' => $this->input->post('phone'),
			 				  'zipcode' => $this->input->post('zipcode'),
			 				  'city' => $this->input->post('city'),
			 				  'address' => $this->input->post('address')			 				  
			 				  );
					 	$insertdata = $this->dashboard_model->updateuserprofile($data, $user_id);		
				  		$this->session->set_flashdata('error_msg', 'You have been updated your profile successfully.');		      
				        redirect('Login/profile'); 
			   }		 
	 } 

 //******************** function is used for get single order detail **********************//
	 public function getsingleorderdetail()
	  {
	  	$order_id = $this->input->post('id');
	  	$address = $this->input->post('address');
	  	$data = $this->dashboard_model->togetsingleorder($order_id);
	  	$cart = json_decode($data->cart);
	  		// $diagnosa = '';
     //            foreach ($nama_diagnosa as $cellDiagnosa){
     //                $diagnosaTemp=$cellDiagnosa['nama_diagnosa'];
     //                $diagnosa=$diagnosa.",".$diagnosaTemp;
     //            } echo $diagnosa;
	  	      			$orderstatus='';
	  	      			if(($data->status)== '2')
	  	      			 {
                       		$orderstatus = '<div class="pending left-floatedff">In Process</div>';
                         }
                        if(($data->status)== '3')
                         {
                       		$orderstatus = '<div class="approveed left-floatedff">Approved</div>';
                       	 } 
                        if(($data->status)== '0')
                         {
                        	$orderstatus = '<div class="delivered left-floatedff">Delivered</div>';
                         } 
                        if(($data->status)== '4')
                         {
                        	$orderstatus = '<div class="pendingapprovalgg left-floatedff">Pending for Approval</div>';
                         }
                        if(($data->status)== '5')
                         {
                        	$orderstatus = '<div class="canceled left-floatedff">Canceled</div>';
                         }
                        if(($data->status)== '7')
                         {
                       		$orderstatus = '<div class="pendingapproval left-floatedff">Out of Delivery</div>';
                         }

	  	$foreach = '';
			foreach ($cart as $value) 
			{
                $html = '<tr>                   
	                        <td><img src="'.IMAGE_URL.$value->coupon.'" height="65px" width="65px"></td>
	                        <td> <p><strong>'.$value->name.'</strong></p></td>
	                        <td>'.$value->qty.'</td>
	                        <td>&euro;'.$value->price.'</td>                        
                   		 </tr>';
                   		$foreach = $foreach.$html; 
            }
		$ddate = trim(str_replace("PST", '', $data->date));
		 $date = date("d.m.Y", strtotime($ddate));	  	
	  	$dataresult = '
		<h2>Details for Order number '.$data->id.'</h2>
		<p><strong>Order placed on:</strong>'.$date.'</p>
		<p><strong>Order status: </strong>&nbsp;'.$orderstatus.'</p>
		<p><strong>Delivery Address:</strong> '.$address.'</p>
		<p><strong>Payment method:</strong> '.$data->payment_method.'</p>
		<p><strong>Products ordered:</strong></p>
	  	<table cellpadding="5" cellspacing="0" width="100%" align="center" class="listing-checkout">
		  			<tr>
                        <td>Product photo</td>
                        <td> Product name</td>
                        <td> Quantity</td>
                        <td>
                            Price
                        </td>
                       
                    </tr>
                    '.$foreach.'
                      <tr>
						<td colspan="2"><strong>Payment Fee:&euro; '.$data->service_charge.'</strong</td>
							<td colspan="2"><strong>Delivery Cost:&euro; '.$data->delivery_charge.'</strong</td>
					</tr>
                    <tr>
						<td colspan="2"><strong>Total Amount:&euro; '.$data->total_payment.'</strong</td>
							<td colspan="2"><a href="'.base_url().'Login/downloadpdforder?id='.$data->id.'" class="invoice"><img src="'.base_url().'assets/images/pdf.png" width="20">Download invoice</a></td>
					</tr>                    
		</table>';
		print_r($dataresult);
	  }

	  //******************** function is used for get single order detail **********************//
	 public function downloadpdforder()
	  {
	  	$sess_id = isset($this->session->userdata['user_data']['user_id']);
	  	 if(!empty($sess_id))
		 {	
		 	 $data['user_id'] = $this->session->userdata['user_data']['user_id'];

		  	$data['user_detail'] = $this->freelancer_model->getuserdetail($data['user_id']);
		  	include (FCPATH."mpdf/mpdf.php");
		  	$order_id = $this->input->get('id');
		  	$data['orders'] = $this->dashboard_model->togetsingleorder($order_id);	  	
		  	$template = $this->load->view('downloadorderpdf',$data,TRUE);
		  	 //usleep(2000000);
		  	$pdf_file_name = "invoice".time().".pdf";
		  	  $mpdf = new mPDF('c',    // mode - default ''
	                           array(300,250),    // format - A4, for example, default ''
	                           0,     // font size - default 0
	                           '',    // default font family
	                           17,    // margin_left
	                           17,    // margin right
	                           10,     // margin top
	                           0,    // margin bottom
	                           10,     // margin header
	                           9,     // margin footer
	                           'L');  // L - landscape, P - portrait 
		  	 $mpdf->list_indent_first_level = 0;
	         $mpdf->SetHTMLFooter('Thuis.today');
	         $mpdf->WriteHTML($template);  
	         $mpdf->Output($pdf_file_name, 'D');
        }
	  }
	  //**function is used for forget password *****//
	  function forget_password()
	  {
		$this->load->view('forget_password');  
	  }
	  function forgot_password()
	  {
		$email = $this->input->post('username');
		$data = $this->dashboard_model->mail_exists($email);
		if(!empty($data))
		{			
			$user_name = $data->name;
			$user_id = $data->id;
			$token = $this->random_string(8);
			//$otp = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			//$body="User Id:- ".$user_id."<br/>".$user_name."<br/>".$email."<br/>Your OTP number is: ".$otp." Use this number to verify the password change process.<br/><br/> Thanks";
			$body = "Use following link to change your password:<br/>
			<a href='".site_url()."/Login/verify?token=".$token."'>change password</a>";
				include APPPATH.'phpmailer/PHPMailerAutoload.php';
				$mail = new PHPMailer();
				$mail->SMTPDebug = true; 
				$mail->SMTPAuth = true;
				$mail->Host = 'mail.justdemo.org';
				$mail->Port = 25;
				$mail->Username = 'immanentinfo@justdemo.org';
				$mail->Password = 'netone@108'; 
				$mail->setFrom('immanentinfo@justdemo.org');
				$mail->IsHTML(true); 
				$mail->addAddress($email);
				//$mail->AddCC($sub_email, $sub_name);
				$mail->Subject = 'Home Today';
				$mail->msgHTML($body);
				if($mail->send())
				{	
					$data = $this->dashboard_model->token_save($email, $token);
					$this->session->set_flashdata('error_msg', 'otp is send your email.');
			        redirect('Login/forget_password');
				}
				else 
				{
					$this->session->set_flashdata('error_msg1', 'Mail not send.');
					redirect('Login/forget_password');
				}
		}
		else
		{
			$this->session->set_flashdata('error_msg1', 'Invalid username.');
			redirect('Login/forget_password');
		}	
		  
	  }
	  /************************************************/
	  // ***** To Generate Random Numbers ***********//
	  /************************************************/
	public function random_string($length) 
	  {
			$key = '';
			$keys = array_merge(range(0, 9), range('a', 'z'));
	  		for ($i = 0; $i < $length; $i++) 
				{
					$key .= $keys[array_rand($keys)];
	            }	
	        	 return $key;
      }
	  /************************************************/
	  // ***** To verify token ***********//
	  /************************************************/
	  function verify()
	  {
		 $token = $this->input->get('token');
		 $data['token'] = $token;
		 $check_token = $this->dashboard_model->check_token($token);
		 if(!empty($check_token))
		 {
			 $this->load->view('change_password', $data);
		 }
		 else
		 {
			 echo "page not found.";
		 }
		 		 
	  }
	  /************************************************/
	  // ***** To change password ***********//
	  /************************************************/
	  function change_password()
	  {
		 $password = $this->input->post('password');
		 $token = $this->input->post('token');
		 $change_pass = $this->dashboard_model->change_password($password, $token);
		 $this->session->set_flashdata('error_msg', 'password has been changed successfully.');
	    redirect('Login');
	  }
	function additionalInfo(){ 
		$this->load->view('fb_registerpage');
	}
	function FbUserData(){ 
		$fbID = $this->input->post('fb_id');
		$getUser = $this->dashboard_model->checkUserExist($fbID);		
		if(!empty($getUser)){
			$session_data = array('username'=>$getUser->email, 'user_id'=>$getUser->id, 'user_name'=>$getUser->name);
			$this->session->set_userdata('user_data', $session_data);
			$sese = $this->session->userdata('checkout');
			$check = $this->input->post('checkoutcheck');			
			if($sese['set'] == 'order'){ 
				$return = array(
						"CODE"=>100,
						"MSG"=>$getUser
						);
			}else{
				$return = array(
						"CODE"=>103,
						"MSG"=>$getUser
						);
			}
			echo json_encode($return);
		}else{			
			$data = array('fb_id' => $this->input->post('fb_id'),
						'first_name' => $this->input->post('first_name'),
						'last_name' => $this->input->post('last_name'),
						'name' => $this->input->post('first_name').$this->input->post('last_name'),
						'email' => $this->input->post('email'),
						'phone' => $this->input->post('phone'),
						'zipcode' => $this->input->post('zipcode'),
						'address' => $this->input->post('address'),
						'city' => $this->input->post('city')
					);
			//$result = $this->session->set_userdata('fb_user_data', $data);
			$result = $this->session->set_userdata('fb_user_data', $data);
			$return = array(
						"CODE"=>101,
						"MSG"=>$result
						);
			echo json_encode($return);			
		}
				
	}
	function FbUserRegister(){	
		$userData = $this->session->userdata('fb_user_data');
		$fb_id = $userData['fb_id'];
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$name = $first_name.' '.$first_name;
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$zipcode = $this->input->post('zipcode');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$data = array('fb_id' => $fb_id,
						'name' => $name,
						'email' => $email,
						'phone' => $phone,
						'zipcode' => $zipcode,
						'address' => $address,
						'city' => $city
					);
		$result = $this->dashboard_model->facebookUserRegister($data);		
		//echo'<pre>';print_r($insertdata);exit;
		$this->session->unset_userdata('fb_user_data');
		$session_data = array('username'=>$result->email, 'user_id'=>$result->id, 'user_name'=>$result->name);
		$this->session->set_userdata('user_data', $session_data);
		$sese = $this->session->userdata('checkout');
		//echo'<pre>';print_r($sese);exit;
		$check = $this->input->post('checkoutcheck');
		if($check == 'order'){ 
			redirect('Cart/checkout'); 
		}else{
			//$data['user_name'] = $this->session->userdata['user_data']['user_name'];
			redirect('Login/profile');
		}
		
		//$session_data = array('username'=>$result->email, 'user_id'=>$result->id, 'user_name'=>$result->name);
		//redirect('Login/profile');
	}	
}