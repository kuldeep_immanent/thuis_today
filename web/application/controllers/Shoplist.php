<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shoplist extends CI_Controller {

	 public function __construct() 
     {
        parent::__construct();
          
	      $this->load->model('Dashboard_model');
	      $this->load->library('Ajax_pagination');
          $this->perPage = 10;   
          
     }
	public function index()
	{
		$accordingToCategory = $this->input->post('categoryids');
		$codezip = '';
		$zipcode = $this->input->post('zipcode');
		// $send = $this->input->post('sendzip');
		// $sendz = $this->input->get('id');
		if(!empty($this->input->post('sendzip')) AND empty($this->input->get('id')))
		 {
			$sendzip = $this->input->post('sendzip');
		 }
		 else
		 {
		 	$sendzip = $this->input->get('id');
		 }
		  if(!empty($zipcode))
			{
				$data['postcode'] = $zipcode;
			}
		  else
		    {
		    	$data['postcode'] = $sendzip;
		    }
		if(is_numeric($sendzip))
			{
				$codezip = $sendzip;
			}
		if(!empty($accordingToCategory))
		{						 	
			//	$data['list'] = $this->Dashboard_model->get_shoplistCateid($zipcode, $codezip, $accordingToCategory);	
				////////////////////////////////////////////////////////
       
			      $session_shopdata = array('szipcode'=>$zipcode, 'scodezip'=>$codezip, 'saccordingToCategory'=>$accordingToCategory, 'spostcode'=>$data['postcode']);
					$this->session->set_userdata('shopzipcodedetails', $session_shopdata);  
			        //total rows count
			        $totalRec = count($this->Dashboard_model->get_shoplistCateid($zipcode, $codezip, $accordingToCategory));
			        
			        //pagination configuration
			        $config['target']      = '#postList';
			        $config['base_url']    = base_url().'Shoplist/ajaxPaginationData';
			        $config['total_rows']  = $totalRec;
			        $config['per_page']    = $this->perPage;
			        $this->ajax_pagination->initialize($config);
			        
			        //get the posts data
			    $data['list'] = $this->Dashboard_model->get_shoplistCateid($zipcode, $codezip, $accordingToCategory, array('limit'=>$this->perPage));
			        
			        //load the view
			        //$this->load->view('posts/index', $data);
				/////////////////////////////////////////////////////////////			 
		}
		else
		{
				//$data['list'] = $this->Dashboard_model->get_shoplist($zipcode, $codezip);
				////////////////////////////////////////////////////////
       
              $session_shopdata = array('szipcode'=>$zipcode, 'scodezip'=>$codezip, 'spostcode'=>$data['postcode']);
					$this->session->set_userdata('shopzipcodedetailsid', $session_shopdata);  
		        //total rows count
		        $totalRec = count($this->Dashboard_model->get_shoplist($zipcode, $codezip));
		        
		        //pagination configuration
		        $config['target']      = '#postList';
		        $config['base_url']    = base_url().'Shoplist/ajaxPaginationDatawithid';
		        $config['total_rows']  = $totalRec;
		        $config['per_page']    = $this->perPage;
		        $this->ajax_pagination->initialize($config);
		        
		        //get the posts data
		        $data['list'] = $this->Dashboard_model->get_shoplist($zipcode, $codezip, array('limit'=>$this->perPage));
		        
		        //load the view
		        //$this->load->view('posts/index', $data);
			/////////////////////////////////////////////////////////////	
		}
		$data['categories'] = $this->Dashboard_model->getcategories();
		if(empty($data['list']))
		 {
			$this->session->set_flashdata('error_msg', 'Geen winkels hier,Vraag uw speciaalzaak om een ​​winkel te openen hier,dus u kunt online bestellen starten.');
		 }
		$this->load->view('listing',$data);
	}

	/////////////////////////////// for ajax pagination //////////////////////////////////
	function ajaxPaginationData()
	  {
	  	
	  	$zipcode = $this->session->userdata['shopzipcodedetails']['szipcode'];
	  	$codezip = $this->session->userdata['shopzipcodedetails']['scodezip'];
	  	$data['postcode'] = $this->session->userdata['shopzipcodedetails']['spostcode'];
	  	$accordingToCategory = $this->session->userdata['shopzipcodedetails']['saccordingToCategory'];
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //total rows count
        $totalRec = count($this->Dashboard_model->get_shoplistCateid($zipcode, $codezip, $accordingToCategory));
        
        //pagination configuration
        $config['target']      = '#postList';
        $config['base_url']    = base_url().'Shoplist/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $data['list'] = $this->Dashboard_model->get_shoplistCateid($zipcode, $codezip, $accordingToCategory, array('start'=>$offset,'limit'=>$this->perPage));
        //$data['posts'] = $this->post->getRows(array('start'=>$offset,'limit'=>$this->perPage));
        
        //load the view
        $this->load->view('ajax-pagination-data-shoplist', $data, false);
      }
	////////////////////////////////////////////////////////////////////////////////////////////////


	/////////////////////////////// for ajax pagination //////////////////////////////////
	function ajaxPaginationDatawithid()
	  {
	  	
	  	$zipcode = $this->session->userdata['shopzipcodedetailsid']['szipcode'];
	  	$codezip = $this->session->userdata['shopzipcodedetailsid']['scodezip'];
	  	$data['postcode'] = $this->session->userdata['shopzipcodedetailsid']['spostcode'];
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //total rows count
        $totalRec = count($this->Dashboard_model->get_shoplist($zipcode, $codezip));
        
        //pagination configuration
        $config['target']      = '#postList';
        $config['base_url']    = base_url().'Shoplist/ajaxPaginationDatawithid';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $data['list'] = $this->Dashboard_model->get_shoplist($zipcode, $codezip, array('start'=>$offset,'limit'=>$this->perPage));
        //$data['posts'] = $this->post->getRows(array('start'=>$offset,'limit'=>$this->perPage));
        
        //load the view
        $this->load->view('ajax-pagination-data-shoplist', $data, false);
      }
	////////////////////////////////////////////////////////////////////////////////////////////////
}
