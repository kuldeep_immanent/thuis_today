<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopowner extends CI_Controller {

	   function __construct()
    {
      parent::__construct();
      $this->load->model('shopowner_model');
      $this->load->library('form_validation');
      $this->load->library('pagination'); 
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
  public function index()
	{	
				$this->load->view('shoprequest');
		
	}


	//////////////////////////** For ShopOwner Email Request***/////////////////////////////////
	public function request()
	{	
		$user_token = $this->random_string(15);	
		$email = $this->input->post('email');
		if(!empty($email)){	

			$checkemail = $this->shopowner_model->checkemail($email);
			if(empty($checkemail)){

				$checkemailnotactive = $this->shopowner_model->checkemailnotactive($email);
				if(!empty($checkemailnotactive))
					{ $id = $checkemailnotactive->user_id;
					$user_tokens = $checkemailnotactive->token; }
			  if(empty($checkemailnotactive))
				{		
				 //////////////////////////
						usleep(200000);
					////////////////////////////////////////////				
					$user_tokens = $this->shopowner_model->add_newshop($email, $user_token);
				}

			

				//////////////////////////////////
				//if(!empty($user_tokens)){
				$message = '<p>Here is a your registration link. Please click to registered as a shop owner. </p><p>Thanks.</p>'.base_url().'index.php/shopowner/shopownerregistration?token='.$user_tokens;		
			
			      //////////////////////////
			      // Configure email library
				//usleep(200000);
					
					$config['smtp_host'] = 'mail.justdemo.org';
					$config['smtp_port'] = 25;
					$config['smtp_user'] = 'immanentinfo@justdemo.org';
					$config['smtp_pass'] = 'netone@108';

					// Load email library and passing configured values to email library
					$this->load->library('email', $config);
					$this->load->helper('path');
					// Sender email address
					$this->email->from('immanentinfo@demodemo.ga', 'Home Today');
					// Receiver email address
					$this->email->to($email);
					// Subject of email
					$this->email->subject('Registration Link');
					$this->email->set_mailtype("html");
					// Message in email
					$this->email->message($message);
				//	$path = set_realpath('uploads/file/');		     	
			    //	$this->email->attach($path . 'Freelance verkoper.pdf');
					
			      ////////////////////////////
			      // $this->email->send();
				      if($this->email->send())
				         {
				   //       	$message = "Your request has been sent successfully and Registration link has been sent to your email account.";
							// echo "<script type='text/javascript'>alert('$message');</script>";
							$this->session->set_flashdata('error_msg', 'Your request has been sent successfully and Registration link has been sent to your email account.');
					     }
				         else
				        {
				         show_error($this->email->print_debugger());
				        }
				 //   }
				  }
				  else
				  {
				  	$this->session->set_flashdata('error_msg1', 'This email is already registered and active. Please try with different email.');
				  	//$message = "This email is already registered and active. Please try with different email.";
					//		echo "<script type='text/javascript'>alert('$message');</script>";
				  }
				  

			///////////////////////////////////////////
				
		}
		$this->load->view('shoprequest');
	}
 /////////////////////////////////////////////////////////////////////////
	// ******************* To Generate Random Numbers ****************//
	public function random_string($length) 
	  {
			$key = '';
			$keys = array_merge(range(0, 9), range('a', 'z'));
	  		for ($i = 0; $i < $length; $i++) 
				{
					$key .= $keys[array_rand($keys)];
	            }	
	        	 return $key;
      }
  //******************************************************************//
 //****** function is used for Save Data of New Freelancer for Registration *******//
//******************************************************************//

	public function shopowner_registration()
	{ 
		$data['restaurant_name_ar']=$this->input->post('company_name');		
		$data['address_ar'] = $this->input->post('address');
		$data1['email'] = $this->input->post('email');	
		$data['email'] = $this->input->post('email');	
		$data['postal_code'] = $this->input->post('postcode');		
		$data['id'] = $this->input->post('id');
		$data['account_password']=$this->input->post('password');
		$data['category']=$this->input->post('shopcategory');
		$data['contact_number']=$this->input->post('phone');
		$data['user_name']=$this->input->post('contact_name');
		$data['contact_email']=$this->input->post('contact_email');
		// $image_name = isset($_FILES["file_attach"]["name"]);
		
		// get category name on the basis of id
		$shopCatName = $this->getShopCategoryName($data['category']);
		if(!empty($shopCatName)){
			$data['categoryName'] = $shopCatName['category_name'];
		}	
		$this->form_validation->set_message('matches', 'Password and Confirm password does not match.');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[confirmpass]');
		$this->form_validation->set_rules('confirmpass', 'Password Confirmation', 'required');
		$this->form_validation->set_rules('password','password','trim|required|min_length[6]|max_length[20]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data1['shop_category'] = $this->shopowner_model->fosshopcategories();
			$this->load->view('shopowner', $data1);
		}
		else
		{							
		$insertdata = $this->shopowner_model->shopownerregistration($data);
			if(!empty($insertdata)){
				$message = $this->load->view('email_temp_shop_owner',$data,TRUE);
				$config['smtp_host'] = 'mail.justdemo.org';
				$config['smtp_port'] = 25;
				$config['smtp_user'] = 'immanentinfo@justdemo.org';
				$config['smtp_pass'] = 'netone@108';
				$EMAIL = $data1['email'];
				// Load email library and passing configured values to email library
				$this->load->library('email', $config);
				$this->load->helper('path');
				// Sender email address
				$this->email->from('immanentinfo@demodemo.ga', 'Home Today');
				// Receiver email address
				$this->email->to($EMAIL);
				// Subject of email
				$this->email->subject('Registration Link');
				$this->email->set_mailtype("html");
				// Message in email
				$this->email->message($message);
							
				if($this->email->send()){
					$this->session->set_flashdata('error_msg', 'Your request has been sent successfully and Registration link has been sent to your email account.');				         	
				} else {
					show_error($this->email->print_debugger());
				}
					$this->session->set_flashdata('error_msg', 'You have been registered successfully. Please wait untill your account is active. We will inform you through email when your account has been successfully activated.');
					redirect('Welcome');
			}
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////
public function shopownerregistration()
	{			
		
		$data['token'] = $this->input->get('token');		
		 if(!empty($data['token']))
			{	
				
				$data['shop_category'] = $this->shopowner_model->fosshopcategories();
				$data['user_detail'] = $this->shopowner_model->checktoken($data['token']);
				if(!empty($data['user_detail']))
				 {
				 	$this->load->view('shopowner', $data);
				 }
				 else
				 {
				 	echo "<h1> Page Not found </h1>";
					die;
				 }			
			}
		 else
			{
				echo "<h1> Page Not found </h1>";
				die;
			}
	}
	public function getShopCategoryName($shopCatId){
		$data = $this->shopowner_model->getShopCategoryName($shopCatId);
		return $data;
	}
///////////////////////////////////////////////////
}
	?>
