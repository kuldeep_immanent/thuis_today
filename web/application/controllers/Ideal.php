<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ideal extends CI_Controller 
{
     function  __construct(){
        parent::__construct();
        $this->load->library('ing_lib');
        $this->load->library('cart');
        $this->load->model('Shopowner_model');
        $this->load->model('freelancer_model');
     }
     public function index()
     {
       $token = $this->input->get('token');
       if(!empty($token))
          {
            $session_data = array('token'=>$token);
            $this->session->set_userdata('token_data', $session_data);
          }
       $data['issuers'] =  $this->ing_lib->ingGetIssuers();
       $this->load->view('issuers_list', $data);     
     }
      public function create_order()
       {

            $token_data = $this->session->userdata('token_data'); 
              if(!empty($token_data))
               {
                   $token = $this->session->userdata['token_data']['token'];
                      $issuer_id = $this->input->post('select');                     
                        $return_url = base_url().'index.php/Ideal/thankyouverymuch';
                    // $return_url = 'ThuisToday://';
                      $dataResult1 = $this->freelancer_model->getdatawithtoken($token);
                      if(!empty($dataResult1))
                       {
                         $total = $dataResult1->total_payment;
                        //print_r($this->session->userdata['payment_detail'] ['amount']);die;
                        // $user_id = $this->session->userdata['payment_detail'] ['user_id'];
                        // $total = '200';
                        //$order_id = '12';
                        $description = 'Pay with Ideal';
                        $ing_order =  $this->ing_lib->ingCreateIdealOrder($order_id=false, $total, $issuer_id, $return_url, $description);
                        $paymenturl = $ing_order['transactions'][0]['payment_url'];
                        // print_r($ing_order);die;
                        redirect($paymenturl);
                       }
               }

          $issuer_id = $this->input->post('select');
          $return_url = base_url().'index.php/Ideal/thankyou';
         $total = $this->session->userdata['payment_detail'] ['amount'];
          //print_r($this->session->userdata['payment_detail'] ['amount']);die;
         // $user_id = $this->session->userdata['payment_detail'] ['user_id'];
         // $total = '200';
          //$order_id = '12';
          $description = 'Pay with Ideal';
          $ing_order =  $this->ing_lib->ingCreateIdealOrder($order_id=false, $total, $issuer_id, $return_url, $description);
          $paymenturl = $ing_order['transactions'][0]['payment_url'];
          // print_r($ing_order);die;
          redirect($paymenturl);
       }

       public function thankyouverymuch()
        {
          echo "Your transaction is successfully completed. Thanks!!";
         
          redirect('thuistoday://');
        }
        public function thankyou()
        {
            $deliveryOption = $this->session->userdata('session_payment'); 
            if(!empty($deliveryOption))
            {
				$data['delivery_time'] = $deliveryOption['delivery_time'];
				$data['delivery_date'] = $deliveryOption['delivery_date'];
				$data['delivery_charge'] = $deliveryOption['delivery_charge'];                 
				$data['shopid'] = $deliveryOption['shopid'];
            }
            $amount = $this->session->userdata['payment_detail'] ['amount'];
            $user_id = $this->session->userdata['payment_detail'] ['user_id'];
            $cart = $this->cart->contents();
            $data['cart'] = json_encode($cart);
            $data['date'] = date("h:i:s M d, Y");  
            $data['user_id'] = $user_id;
            $data['total_payment'] = $amount;
             
		    $data['payment_method'] = 'Ideal';
		    $data['service_charge'] = '0.25';
		    $data['quantity'] = count($cart);
            $dataResult = $this->freelancer_model->inserOrders($data);
			if(!empty($dataResult)){ 
         /////////////////////////////////////////// email send ////////////////////////////////////////////////
                $result = $this->Shopowner_model->getshopemailwithid($data['shopid']);
                $email = $result->user_email;  
                $userdetails = $this->Shopowner_model->getuserdetail($data['user_id']);
                $tempdata['order_detail'] = $dataResult;
                $tempdata['shop_detail'] = $result;
                $tempdata['user_detail'] = $userdetails;
                $tempdata['orderd_products'] = $cart;
				
				//echo'<pre>';print_r($tempdata);exit; 
				
                $body = $this->load->view('order',$tempdata,TRUE);
                 //////////////////
                $usrname = $userdetails->name;
                $usremail = $userdetails->email;
                $usrphone = $userdetails->phone;
                $usraddress = $userdetails->address;

                 // $body = 'New order
                 //      Customer Name:'.$usrname.'
                 //      Email:'.$usremail.'
                 //      Phone:'.$usrphone.'
                 //      Adress:'.$usraddress;
                  include APPPATH.'phpmailer/PHPMailerAutoload.php';
                  $mail = new PHPMailer();
                  $mail->SMTPDebug = true; 
                  $mail->SMTPAuth = true;
                  $mail->Host = 'mail.justdemo.org';
                  $mail->Port = 25;
                  $mail->Username = 'immanentinfo@justdemo.org';
                  $mail->Password = 'netone@108'; 
                  $mail->setFrom('immanentinfo@justdemo.org');
                  $mail->IsHTML(true); 
                  $mail->addAddress($email);
                  //$mail->AddCC($sub_email, $sub_name);
                  $mail->Subject = 'Home Today';
                  $mail->msgHTML($body);
                  $mail->send();
                  ///////
         ///////////////////////////////////////////////////////////////////////////////////////////////////////      
				$this->cart->destroy(); 
				$this->load->view('payment-succesfull',$tempdata);
        }
       }
 //     function ipn(){
              
 //              $deliveryOption = $this->session->userdata('session_payment'); 
 //              if(!empty($deliveryOption))
 //               {
 //                 $data['delivery_time'] = $deliveryOption['delivery_time'];
 //                 $data['delivery_date'] = $deliveryOption['delivery_date'];
 //                 $data['delivery_charge'] = $deliveryOption['delivery_charge'];
 //                 $data['shopid'] = $deliveryOption['shopid'];
 //               }
 //              $cart = $this->cart->contents();
 //              $data['cart'] = json_encode($cart);
 //              $paypalInfo    = $this->input->post();
 //            //  print_r($paypalInfo);
 //              $data['trans_id'] = $paypalInfo["txn_id"];
 //      		    $data['user_id'] = $paypalInfo['custom'];
 //             $data['service_charge'] = '0.30';//$paypalInfo['cents'];
 //       // $data['album_id'] = $paypalInfo["item_number"];
 //               $data['total_payment'] = $paypalInfo["mc_gross"];
 //       // $data['currency_code'] = $paypalInfo["mc_currency"];
 //       // $data['payer_email'] = $paypalInfo["payer_email"];
 //               $data['status'] = $paypalInfo["payment_status"];
 //               $data['date'] = $paypalInfo["payment_date"];
 //               $data['payment_method'] = 'Paypal';
 //               $data['quantity'] = count($cart);
 //            //   print_r($data); die;
 //        $paypalURL = $this->paypal_lib->paypal_url;        
 //        $result    = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);
	// 	//$token = $this->Web_album_model->check_token($data['album_id']);

 //        //check whether the payment is verified
 //        if(preg_match("/VERIFIED/i",$result)){
 //            //insert the transaction data into the database
 //           // $this->Web_album_model->insertTransaction($data);
 //            $dataResult = $this->freelancer_model->inserOrders($data);
 //            if(!empty($dataResult)){ echo "<script>
 //          alert('Your transaction is successfully completed. Thanks!!');
 //          window.location.href='welcome';
 //          </script>";}
 //        }
 //    }
 //    public function welcome()
 //    { 
 //      redirect('/Welcome/index');
 //    }

 // //********************* function  is used for save the pick at locations orders ********************************//
 //     public function pickordersave()
 //      {
 //            $deliveryOption = $this->session->userdata('session_payment'); 
 //              if(!empty($deliveryOption))
 //               {
 //                 $data['delivery_time'] = $deliveryOption['delivery_time'];
 //                 $data['delivery_date'] = $deliveryOption['delivery_date'];
 //                 $data['delivery_charge'] = $deliveryOption['delivery_charge'];                 
 //                 $data['shopid'] = $deliveryOption['shopid'];
 //               }
 //              $amount = $this->session->userdata['payment_detail'] ['amount'];
 //              $user_id = $this->session->userdata['payment_detail'] ['user_id'];
 //             $cart = $this->cart->contents();
 //              $data['cart'] = json_encode($cart);
 //            $data['date'] = date("h:i:s M d, Y");
                
 //               $data['user_id'] = $user_id;
 //              $data['total_payment'] = $amount;
             
 //               $data['payment_method'] = 'Free';
 //               $data['quantity'] = count($cart);
 //            $dataResult = $this->freelancer_model->inserOrders($data);
 //            if(!empty($dataResult)){ echo "<script>
 //          alert('Thanks!!');
 //          window.location.href='welcome';
 //          </script>";}       
 //      }
}