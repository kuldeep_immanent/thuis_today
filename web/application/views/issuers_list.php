<head>
<style>
.block {
    border-radius: 5px;
    border: 1px solid #c4bdb4;
    margin-bottom: 20px;
    width: 35%;
	float: left;
	margin-left: 33%;
	padding: 10px
}
* {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
}
.select{
	float: left;
    border: 1px solid #ccc !important;
    border-radius: 3px;
    overflow: hidden;
    padding: 5px 8px;
    width: 40%;
    border: none;
    box-shadow: none;
    background: transparent;
    background-image: none;
    -webkit-appearance: none;
}
.btn-b{
	    background: -webkit-linear-gradient(top,#e65c00 0,#f60 100%);
    background: -o-linear-gradient(top,#e65c00 0,#f60 100%);
    background: linear-gradient(to bottom,#e65c00 0,#f60 100%);
    -webkit-box-shadow: none;
    box-shadow: none;
    width: 35%;
}
.buttons-set * [class^="btn-"] {
    width: 80%;
}
* [class^="btn-"] {
    display: inline-block;
    margin: 0;
    padding: 10px 30px;
    border-radius: 5px;
    font-size: 14px;
    font-weight: 400;
    line-height: normal;
    text-align: center;
    vertical-align: middle;
    cursor: pointer;
    white-space: nowrap;
    border-width: 1px;
    border-style: solid;
    width: 35% !important;
    float: right;
    color: white;
}
* {
    outline: 0;
    box-sizing: border-box;
}
h2 {
    margin: 25px 0 15px;
    color: grey;
    font-size: 36px;
    font-family: Rockwell-Light,Arial,sans-serif;
}
h1, h2, h3 {
    padding: 0;
    font-weight: 400;
    line-height: normal;
}
.footer {
    position: relative;
    margin-top: 50px;
    padding-bottom: 8px;
    border-bottom: 3px solid #f60;
    width: 40%;
    float: left;
    margin-left: 31%;
}
.logo-payment-ing {
    display: inline-block;
    background-position: -5px -155px;
    width: 125px;
    height: 31px;
}
img {
    width: 200px;
    float: left;
    margin-bottom: 19%;
    margin-left: -150px !important;
    margin-top: -68px;
}
.powered {
    float: right;
    margin-right: 50px;
    margin-top: 11px;
}

</style>
</head>
</br></br></br>
<center>
<div class="divborder">
<h2>Please select your payment method</h2>
</br></br>
<form enctype="multipart/form-data" action="<?php echo site_url();?>/Ideal/create_order" method="post">
<div class="block block-highlight block-paymentmethod">
<select name="select" class="select">
		 <?php 
		 	 foreach ( $issuers as $issuer ) 
		     	{
					echo '<option value="' . $issuer['id'] . '">' . htmlspecialchars( $issuer['name'] ) . '</option>';
				} 
		?>
</select>
 <input type="submit" class="btn-b" value="Next">
 </div>
</form>
<div class="footer">
        <span class="logo-payment-ing"> <img src="<?php echo base_url();?>assets/images/ING_logo-880x660.png"></span>
        
        <span class="powered">Powered by ING</span>
</div>
</div>
</center>