<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Thuis.today - Welkom</title>				
	</head>
	<body>
		<?php include('include/help.php');?>
		 	<!--facebook-like-script - please developers configure this properly-->
       
       <div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1298368983524392";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
      
      <?php  
                $lerror = $this->session->flashdata('error_msg');
                  if(isset($lerror))
                  {
                      echo '<div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.'</div>'; 
                  }
             ?>
       
		<!-- End facebook like script      -->
		<!--Header zone for the main page-->
		<header>
			
			<div class="header-container">
				
				<h1 class="logo">Lorem ipsum dolor sit amet consectuer</h1>
				<div class="menu-top">
					<div class="links-menu-top">
						<a href="<?php echo base_url();?>freelancerregistration/request/" class="store">Freelance verkoper</a>
						<a href="<?php echo base_url();?>Shopowner" class="store">Start uw winkel</a>
						<a href="<?php echo base_url(); ?>Login" class="my-account">mijn account</a>
						<a href="#" class="help">Hulp nodig?</a>
						<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="100" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
						<!--<a href="#" class="language">NL</a>-->
					</div>
				</div>
				<br clear="all">
				
				<!--Search form container-->
				<div class="search-container">
					
					<h1 class="title">Waar wilt u bestellen bij?</h1>
					<div class="searchform hh">
						<form action="<?php echo site_url(); ?>/Shoplist" method="POST">
						<input type="text" required Placeholder="Geef uw postcode or straatnaam" name= "sendzip" onkeyup="search_location(this)" id="location">
					<div id="list_location"></div> 
						<input type="hidden" name="zipcode" value="" id="zipcode">
					<!-- 	<input type="text" value="Enter zip code or city name">
						<input type="submit" value="find shop" class="search-location"> -->
						<input type="submit" value="Vind winkel" class="search-location">
						<a href="#" class="select-shop">Selecteer winkel type  &nbsp;&nbsp; &#x25BC;</a>
						
						<div class="shop-options">
							<label><input type="checkbox" class="chk_boxes">Alle</label><br>
							<?php if(!empty($categories)){ foreach ($categories as $value) {?>
							<label><input type="checkbox" name="categoryids[]" value="<?php echo $value->category_id; ?>" class="chk_boxes1"><?php echo $value->category_name; ?></label><br>
							<?php } }?>												
						</div>
					
						
					<form>
						
					</div>
					
				</div>
				
				
				
			</div>
			
		</header>
		
		<!--End Header-->
		<!-- Main content Home -->
		
		<div class="main-container">
			
			<ul class="steps">
				<li>Kies je locatie en <br>jouw producten!</li>
				<li>Betaal online,<br>veilig en beveiligen</li>
				<li>Krijg je producten <br>geleverd vandaag</li>
			</ul>
			<br><br clear="all"><br>
			<p align="center"><img src="<?php echo base_url(); ?>/assets/images/payment.gif"></p>
			<br><br>
			<!-- Why thuis today -->
			<h1 class="stripes"> WAAROM THUIS.TODAY</h1>
			<!-- End -->
			
			<p align="center"><img src="<?php echo base_url(); ?>/assets/images/why.jpg"></p>
			<p align="center">In een snel veranderende markt waarbij grote bedrijven steeds groter worden en de kwaliteit veelal de dupe is, wil Thuis.today met een focus om de middenstander in uw eigen regio een antwoord bieden op de behoefte naar eerlijke, goede en gezonde producten. Met thuis.today bieden we u het gemak en eenvoud van de grote waren huizen. U wordt weer een echt klant met de service die u mag verwachten. Bovendien besteld u vandaag en krijgt u uw producten thuis.today!</p>
			<br><br>
			<h1 class="stripes"> BESTEL VERSE PRODUCTEN altijd en overal!</h1>
			<p align="center">
				<a href="#"><img src="<?php echo base_url(); ?>/assets/images/googleplay.gif"></a>&nbsp;&nbsp;&nbsp;<a href="#"><img src="<?php echo base_url(); ?>/assets/images/appstore.gif"></a>
			</p>
			<p align="center">Wat kan er verser zijn dan de producten die direct van de veiling en/of boer naar uw lokale specialist gaan? Niets toch? Dat is de meerwaarde van thuis.today. Verse en gezonde producten direct geleverd vanuit uw lokale winkelier.  Thuis.today verbind de lokale winkelier met internet en maakt de gezonde producten direct beschikbaar. </p>
			
		</div>
<?php include('include/footor.php'); ?>	
	</body>
</html>
<script>
	function search_location(e)
	{
		var val = $(e).val() ;
		if(val != "")
		{
			$.ajax({
				   type: "post",
				   url: '<?php echo site_url();?>/Welcome/location_search',
				   data: { location : val },
				  // dataType: 'json',
				   success: function(data) {
					  
						$("#list_location").html(data);	
				   },
				});
		}else
		{
			$("#list_location ul").remove();
			$("#zipcode").val("");
		}
		
	}
	function set_location(e)
	{
		var data = $(e).text();
		$("#location").val(data);
		$("#list_location ul").remove();
		var postcode = $(e).attr("id");		
		$("#zipcode").val(postcode);
	}
</script>
<script>
$(function() {
    $('.chk_boxes').click(function() {
        $('.chk_boxes1').prop('checked', this.checked);
    });
});
</script>