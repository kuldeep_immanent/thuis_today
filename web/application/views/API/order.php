<!doctype html>
<?php 
?>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
		
		<table cellpadding="0" cellspacing="0" width="700" align="center">
			<tr>
				<td width="35%"><img src="logo.png"><br><br></td>
				<td align="center"><h1 style="font-size:25px;font-family: Arial;font-weight: normal"><?php echo $shop_detail->restaurant_name_ar; ?></h1></td>
			</tr>
		</table>
		
		<table cellpadding="20" cellspacing="0" width="700" align="center" style="border:1px solid #ddd;font-family:arial;font-size:14px;">
			<tr>
				<td>
					
					<h1 style="font-family: Arial;font-size:25px;color:#62990a">You have a new order! <span style="background:#ddd;color:#000;font-size:14px;padding:5px;">Order-status: Pending Delivery</span>
					<br>
					<span style="font-size:14px;color:#000">Order number: <?php echo $order_detail->id; ?></span> <span style="font-size:14px;color:#000">&raquo;</span> <span style="font-size:14px;color:#000">Order date: <?php echo $order_detail->date; ?></span> <span style="font-size:14px;color:#000">&raquo;</span> <span style="font-size:14px;color:#000">Delivery date: <?php echo $order_detail->delivery_date; ?></span></h1>
					<br>
					<p style="font-size:16px;"><strong><em>Customer information:</em></strong></p>
					<table cellpadding="10" cellspacing="0" width="100%" style="background:#eee;">
						<tr>
							<td>
								<p style="margin:10px 0px 10px 0px;padding:0px;"><strong>Name:</strong><?php echo $user_detail->name; ?></p>
								<p style="margin:10px 0px 10px 0px;padding:0px;"><strong>Delivery address:</strong><br><?php echo $user_detail->address; ?></p>
								<p style="margin:10px 0px 10px 0px;padding:0px;"><strong>Contact number:</strong> <?php echo $user_detail->phone; ?></p>
							</td>
						</tr>
					</table>
					<br>
					
					<p style="font-size:16px;border-bottom:1px solid #ddd"><strong><em>Order details</em></strong><br><br></p>
					
				<table cellpadding="5" cellspacing="0" width="100%" align="center" >
		  			<tr>
                        <td>Product photo</td>
                        <td> Product name</td>
                        <td> Quantity</td>
                        <td>
                            Price
                        </td> 
                       
                    </tr> 
			<?php   foreach($orderd_products as $detail){   ?>
					<tr>
                        <td><img src="<?php echo IMAGE_URL.$detail->coupon;?>" width="48"></td>
                        <td> <p><strong><?php echo $detail->name;?></strong></p></td>
                        <td><?php echo $detail->qty;?></td>
                        <td>&euro;<?php echo $detail->price;?></td>
                        
                    </tr>
			<?php   }       ?>		
                   
                    <tr>
						<td colspan="4">&nbsp;</td>
					</tr>
                   	<tr>
						<td colspan="4" align="center" style="border-top:1px solid #ddd;">
						<br>
					<!--- <strong>Delivery method:</strong> <span style="background:#62990a;color:#fff;font-size:14px;padding:5px;">Pick up at store</span> --->&nbsp;&nbsp;&nbsp;&nbsp;<strong>Delivery cost</strong> <span style="background:#62990a;color:#fff;font-size:14px;padding:5px;">&euro;<?php echo $order_detail->delivery_charge; ?></span>
							<br><br>
						</td>
                   	</tr>
                   
                    <tr>
						<td colspan="4" style="background:#62990a;color:#fff;" align="center">
							<h2 style="margin:0px;padding:0px;">Order value: &euro; <?php echo $order_detail->total_payment; ?></h2>
						</td>
					</tr>
					<tr>
						<td colspan="4" align="center">
						<br><br>
							<a href="<?php echo SHOP_OWNER_BASE_URL;?>" style="font-size:20px;color:#fd6602;text-decoration:none;">Check order details &raquo;</a>
						</td>
					</tr>
					</table>
					
					
					
				</td>
			</tr>
		</table>
	
</body>
</html>
