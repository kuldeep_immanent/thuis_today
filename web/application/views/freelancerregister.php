<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Thuis.Today - Freelancer Join</title>
<link href="<?php echo base_url(); ?>/assets/css/default_freelancer.css" rel="stylesheet">
</head>
       
       <div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1298368983524392";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
				
<body>
		<?php include('include/help.php');?>
		<div class="main-container">
		
		<h1 class="logo">Lorem ipsum dolor sit amet consectuer</h1>
			<div class="menu-top">
					<a href="#" class="store">Add your store</a>
					<a href="<?php echo base_url(); ?>Login" class="my-account">My account</a>
					<a href="#" class="help">Need help?</a>
					<a href="#" class="language">NL</a>
					<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="100" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
			</div>
			
			<div class="enter-email">
			<form enctype="multipart/form-data" action="<?php echo site_url();?>/freelancerregistration/freelancer_registration" method="post">
				<input type="hidden" name="id" value="<?php if(!empty($user_detail)){echo $user_detail->id;} ?>"> 
				<input type="hidden" name="eml" value="<?php if(!empty($user_detail)){echo $user_detail->email;} if(!empty($email)){echo $email;}?>">
				 
				<h1>Registreer alsjeblieft</h1>

				<p>
					Voornaam<br>
					<input type="text" name="f_name" class="register" value="<?php echo set_value('f_name');?>" required>
				</p>
				<p>
					Achternaam<br>
					<input type="text" class="register" name="l_name" value="<?php echo set_value('l_name');?>" required>
				</p>
				<p>
					Email<br>
					<input type="text" class="register" name="email" disabled value="<?php if(!empty($user_detail)){echo $user_detail->email;} if(!empty($email)){echo $email;} ?>" required>
					
				</p>
				
				<p>
					Contact nummer<br>
					<input type="text" class="register" name="Con_number" value="<?php echo set_value('Con_number');?>"required>
					
				</p>
				<p> 
					Password<br>
					<input type="password" name="password" class="register" required>
					
				</p>
				<span style="color:red;"><?php echo validation_errors(); ?> </span>
				<p>
					Confirm password<br>
					<input type="password" name="confirmpass" class="register" required>
					
				</p>				
				<p>
				<p>
					Bank account number<br>
					<input type="text" class="register" name="bank_acc" value="<?php echo set_value('bank_acc');?>" required>
					
				</p>
				<p>
					Address<br>
					<input type="text" class="register" name="address" value="<?php echo set_value('address');?>" required>
					
				</p>
				<!-- <p>
					Gehechtheid<br>
					<input type="file" name="file_attach" value ="<?php echo set_value('file_attach'); ?>" required>
					
				</p> -->
				<p>
					<input type="submit" value="Registreren" class="submit">
				</p>									
				
			</form>				
									
			</div>			
			<!-- <br clear="all">								 -->
				
		</div>
				<!-- End Main Content Home -->
		
		<div class="small-footer">
			
			<table cellpadding="0" cellspacing="0" width="815" align="center">
				<tr>
					<td width="10%" height="57" valign="center"><img src="<?php echo base_url(); ?>/assets/images/small-logo.gif"></td>
					<td width="90%" align="center" valign="middle"><a href="#">Hulp nodig?</a>   <a href="#">winkel toe te voegen</a> <a href="<?php echo base_url(); ?>Welcome/terms_contitions">Gebruiksvoorwaarden</a>  <a href="<?php echo base_url(); ?>Welcome/privacy_statement">Privacy Verklaring</a>  <a href="#">Colofon</a> <a href="<?php echo base_url(); ?>Welcome/cookie_policy">koekje Verklaring</a></td>
				</tr>
			</table>
			
		</div>
		
		<footer>
			<span class="footer-container">
				<div>
					<h2>Verse producten</h2>
					<a href="#">Groenten</a><br>
					<a href="#">Vlees</a><br>
					<a href="#">Kaas</a><br>
					<a href="#">Brood</a><br>
					<a href="#">Vis</a><br>
					
					<h2>Steden</h2>
					<a href="#">  Amsterdam</a><br>
					<a href="#">Rotterdam</a><br>
					<a href="#">Den Haag</a><br>
					<a href="#">Utrecht</a><br>
					<a href="#">Eindhoven</a><br>
					<a href="#">Tilburg</a><br>
					<a href="#">Almere</a><br>
					<a href="#">Groningen</a><br>
					<a href="#">Breda</a><Br>
					<a href="#">Nijmegen</a><br>
					
				</div>
				<div>
					<h2>Commercieel</h2>
					<a href="#">Wordt verkoper</a><br>
					
					
				</div>
				<div>
					
					<h2>Nieuwe winkels</h2>
					<a href="#"> Steffie's, Roosendaal</a><br>
					<a href="#">Pizzeria Victoria, Alsemberg</a><br>
					<a href="#">Pizza & Pasta Veloce, Ebikon</a><br>
					<a href="#">Pizzeria Gul, Anderlecht</a><br>
					<a href="#">Panorama, Hilversum</a><br>
					<a href="#">Five Flavors, Voorburg</a><br>
					<a href="#">Kebab Bueno, Oostende</a><br>
					<a href="#">Da Sebastiano 2, Den Haag</a><br>
					<a href="#">Fast & delicious, Liedekerke</a><br>
					<a href="#">Break Time, Rotterdam</a><br>
				</div>
				<div>
					
					<h2>Partners</h2>
					<a href="#"> Steffie's, Roosendaal</a><br>
					<a href="#">Pizzeria Victoria, Alsemberg</a>
					<a href="#">Pizza & Pasta Veloce, Ebikon</a><br>
					<a href="#">Pizzeria Gul, Anderlecht</a><br>
					<a href="#">Panorama, Hilversum</a><br>
					<a href="#">Five Flavors, Voorburg</a><br>
					<a href="#">Kebab Bueno, Oostende</a><br>
					<a href="#">Da Sebastiano 2, Den Haag</a><br>
					<a href="#">Fast & delicious, Liedekerke</a><br>
					<a href="#">Break Time, Rotterdam</a><br>
				</div>
				<br clear="all">
			</span>
		</footer>
			
		<!-- <div class="small-footer">
			
			<table cellpadding="0" cellspacing="0" width="800" align="center">
				<tr>
					<td width="10%" height="57" valign="center"><img src="<?php echo base_url(); ?>/assets/images/small-logo.gif"></td>
					<td width="90%" align="center" valign="middle"><a href="#">Need help?</a>   <a href="#">Add store</a> <a href="#">Terms of use</a>  <a href="#">Privacy statement</a>  <a href="#">Colophon</a> <a href="#">Cookie statement</a></td>
				</tr>
			</table>
			
		</div>
		
    <footer>
   <span class="footer-container">
    <div>
     <h2>Fresh products</h2>
     <a href="#">Vegetables</a><br>
     <a href="#">Meat</a><br>
     <a href="#">Cheese</a><br>
     <a href="#">Bread</a><br>
     <a href="#">Fish</a><br>
     
     <h2>Cities</h2>
     <a href="#">  Amsterdam</a><br>
     <a href="#">Rotterdam</a><br>
     <a href="#">Den Haag</a><br>
     <a href="#">Utrecht</a><br>
     <a href="#">Eindhoven</a><br>
     <a href="#">Tilburg</a><br>
     <a href="#">Almere</a><br>
     <a href="#">Groningen</a><br>
     <a href="#">Breda</a><Br>
     <a href="#">Nijmegen</a><br>
     
    </div>
    <div>
      <h2>Commerciel</h2>
      <a href="#">Wordt verkoper</a><br>
      
     
    </div>
    <div>
     
     <h2>New stores</h2>
     <a href="#"> Steffie's, Roosendaal</a><br>
     <a href="#">Pizzeria Victoria, Alsemberg</a><br>
     <a href="#">Pizza & Pasta Veloce, Ebikon</a><br>
     <a href="#">Pizzeria Gul, Anderlecht</a><br>
     <a href="#">Panorama, Hilversum</a><br>
     <a href="#">Five Flavors, Voorburg</a><br>
     <a href="#">Kebab Bueno, Oostende</a><br>
     <a href="#">Da Sebastiano 2, Den Haag</a><br>
     <a href="#">Fast & delicious, Liedekerke</a><br>
     <a href="#">Break Time, Rotterdam</a><br>
    </div>
    <div>
     
     <h2>Partners</h2>
     <a href="#"> Steffie's, Roosendaal</a><br>
     <a href="#">Pizzeria Victoria, Alsemberg</a>
     <a href="#">Pizza & Pasta Veloce, Ebikon</a><br>
     <a href="#">Pizzeria Gul, Anderlecht</a><br>
     <a href="#">Panorama, Hilversum</a><br>
     <a href="#">Five Flavors, Voorburg</a><br>
     <a href="#">Kebab Bueno, Oostende</a><br>
     <a href="#">Da Sebastiano 2, Den Haag</a><br>
     <a href="#">Fast & delicious, Liedekerke</a><br>
     <a href="#">Break Time, Rotterdam</a><br>
    </div>
    <br clear="all">
   </span>
  </footer> -->
		
		
		
	
</body>	
</html>
