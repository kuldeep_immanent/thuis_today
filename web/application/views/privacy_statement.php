<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Thuis.today - Welcome</title>	
</head>
<body>
	<?php include('include/help.php');?>
 	<!--facebook-like-script - please developers configure this properly-->
       
       <div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1298368983524392";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
      
      
       
		<!-- End facebook like script      -->
	<!--Header zone for the main page-->
	<div class="header-pages">
		<div class="header-pages-container">

		<a href="<?php echo base_url();?>"> 	<h1 class="logo">Lorem ipsum dolor sit amet consectuer</h1></a>

			<div class="menu-top">
				
				<div class="links-menu-top">
					<a href="<?php echo base_url(); ?>" class="change-location">Change Location</a>
					<a href="<?php echo base_url(); ?>Login" class="clear"><img src="<?php echo base_url(); ?>/assets/images/user-pages.png"></a>
					<a href="#" class="clear help"><img src="<?php echo base_url(); ?>/assets/images/question-mark.png"></a>
					<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="100" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
					<!--						<a href="#" class="clear"><img src="images/language-nl.png"></a>-->
				</div>
			</div>
			<div class="filters">
				<div class="filters-listing">
					<label><input type="checkbox">Vegetable shop</label><br>
					<label><input type="checkbox">Butcher</label><br>
					<label><input type="checkbox">Cheese</label><br>
					<label><input type="checkbox">Bakery</label><br>
					<label><input type="checkbox">Other</label><br>
					<input type="submit" value="ok" class="small-input">
				</div>
				<div class="spacer">&nbsp;</div>

			</div>


		</div>
	</div>

	<!--End Header-->
	<!-- Main content Home -->

	<div class="main-container">
	  <h1 class="pages">Privacy statement</h1>
	  <div class="text-content">
		<p><strong>Inleiding:</strong></p>
		<p>Thuis.today is een eenmanszaak van A.H. Aarts en handelend onder de naam thuis.today en/of thuistoday.nl. Het KvK nummer is 16056243 ingeschreven bij de KvK den Bosch.</p>
		<p>Thuis.today faciliteert winkels in hun online aanwezigheid en bied diensten aan deze winkels gebaseerd op locatie en e-commerce. De winkels die gebruik maken van thuis.today kunnen u diverse mogelijkheden bieden om uw boodschappen te bezorgen of af te laten halen in de winkel. Thuis.today verwerkt persoonsgegevens en andere data in overeenstemming met geldende wet- en regelgeving. In dit privacy statement (verder te noemen: “Privacy Statement”) zijn de overwegingen hierover vastgelegd.</p>

		<p><strong>Toepasselijkheid</strong></p>
		<p>Toepasselijkheid Dit Privacy Statement is van toepassing op de verwerking van persoonlijke informatie door gebruikers van thuis.today en de door hun verstrekte persoonlijke informatie (hierna: Persoonsgegevens), alsmede data verkregen vanuit bezoek aan en gebruik van de websites en/of apps van thuis.today en het gebruik van aanvullende diensten.</p>
		
		<p><strong>Hoe verzamelt thuis.today persoonsgegevens?</strong></p>
		<p>Thuis.today behandelt Persoonsgegevens vertrouwelijk en zorgvuldig. Thuis.today verzamelt Persoonsgegevens bij het registreren als gebruiker, wanneer je je abonneert op onze nieuwsbrief en/of folder, wanneer je een bestelling plaatst, wanneer je gebruik maakt van onze WhatsApp-service, informatie opvraagt, meedoet aan een enquête of test, als je informeert naar onze activiteiten, deelneemt aan een actie, een vraag stelt, reageert op onze website of op een andere manier met ons in contact treedt.</p>
		
		<p><strong>Welke persoonsgegevens verzamelt thuis.today</strong></p>
		<p>Thuis.today verzamelt contactgegevens zoals je naam, adres, telefoonnummer, e-mailadres, geboortedatum en bankgegevens, gegevens met betrekking tot uw nieuwe en/of eerdere bestellingen.. We slaan ook op wanneer en waarover we contact met je hebben gehad, bijvoorbeeld wanneer je een vraag stelt aan onze klantenservice. Als je gebruik maakt van onze WhatsApp-service zien we je gebruikersnaam en telefoonnummer en kunnen we de inhoud van de gesprekken bewaren om je in de toekomst beter van dienst te kunnen zijn en onze eigen informatievoorziening te verbeteren. In aanvulling op de persoonsgegevens die je zelf verstrekt aan thuis.today, kan thuis.today aanvullende persoonsgegevens verzamelen, vastleggen en verwerken over jouw gebruik van onze website(s). Het gaat hierbij om gegevens over de door jou gebruikte apparatuur, zoals een unieke device ID, de versie van het besturingssystemen instellingen van het apparaat dat je gebruikt om onze website te bezoeken, alsmede gegevens over het gebruik van onze website(s) en/of thuis.today app’s, zoals het tijdstip waarop je ons bezoekt en de onderwerpen die je bekijkt. De respons (open en kliks) op onze e-mails worden ook op individueel niveau vastgelegd.</p>
		<p><strong>Thuis.today Privacy Statement</strong></p>
		<p>In sommige gevallen kunnen Persoonsgegevens door thuis.today worden gecombineerd met informatie afkomstig uit externe bronnen (zoals demografische en geografische segmenten). Op basis van al die gegevens kunnen wij je beter van dienst zijn en de inhoud van onze diensten beter op je voorkeuren afstemmen. Indien je dit niet kun je hier bezwaar tegen aantekenen door het versturen van een mail aan info@thuis.today.</p>
		<p><strong>Waarom verzamelt thuis.today Persoonsgegevens?</strong></p>
		
		<p>Thuis.today verwerkt Persoonsgegevens om je bestelling volgens overeenkomst uit te voeren en om een bestelaccount aan te maken waardoor je in het vervolg sneller en gemakkelijker bestellingen kunt plaatsen en om aanvullende diensten te leveren zoals onze messenger em/of WhatsApp-service en/of sms service. We slaan gegevens op om klachten en terugvorderingen te verwerken en voor onze klantenservice, bijvoorbeeld om vragen te kunnen beantwoorden. Aan de hand van de inzichten die we uit het gebruik en bezoek van onze websites krijgen en met behulp van vragen en meldingen bij de klantenservice, analyseren en verbeteren wij onze dienstverlening. Persoonsgegevens worden ook verwerkt om je op de hoogte te kunnen houden van onze dienstverlening en activiteiten, bijvoorbeeld omdat je geabonneerd bent op onze e-mailnieuwsbrief, digitale folder en/of ander type diensten. Thuis.today verstrekt je gegevens nooit aan derden muv. Bij die winkels waar je je bestellingen plaatst.
		</p>
		
		<p><strong>Delen met anderen</strong></p>
		<p>Thuis.tody verkoopt jouw gegevens niet aan derden en zal deze uitsluitend verstrekken indien dit nodig is voor de uitvoering van onze overeenkomst met jou of om te voldoen aan een wettelijke verplichting. Met bedrijven die jouw gegevens verwerken in onze opdracht, sluiten wij een bewerkersovereenkomst om te zorgen voor eenzelfde niveau van beveiliging en vertrouwelijkheid van jouw gegevens. Thuis.today blijft verantwoordelijk voor deze verwerkingen.</p>
		
		<p><strong>Jouw rechten:</strong></p>
		<p>Correctie, inzage en verzet Je kunt te allen tijde en zonder kosten inzicht krijgen in jouw gegevens die door thuis.today worden verwerkt en je gegevens desgewenst aanpassen. Ook kan je bezwaar maken tegen het ontvangen van gerichte informatie over onze dienstverlening die we je per e-mail, telefoon, post en/ of SMS of via WhatsApp toesturen. Indien je van één van deze mogelijkheden gebruik wilt maken, stuur dan een e-mail naar info@thuis.today</p>
		
		<p><strong>Hoe beveiligt thuis.today jouw gegevens?</strong></p>
		<p>		Thuis.today schakelt bij de uitvoering van haar dienstverlening derden in, zoals een e-mailverzendhuis,  De winkels, een callcenter en bezorgdiensten. Voor zover deze derden bij het uitvoeren van de betreffende diensten je gegevens verwerken, doen zij dit in hoedanigheid van bewerker voor thuis.today. thuis.today heeft technische en organisatorische maatregelen getroffen om te verzekeren dat je gegevens uitsluitend voor bovenstaande doeleinden worden verwerkt. De betreffende dienstverlener ontvangt van thuis.today alleen de noodzakelijke gegevens en de dienstverleners worden verplicht om jouw Persoonsgegevens te beschermen. Met al onze bewerkers sluiten wij overeenkomsten waarin wij dit expliciet opnemen. Voor onze WhatsApp-service maakt thuis.today gebruik van een partner die hiervoor software aanbiedt. Deze dienstverlener heeft geen inzage in de gesprekken en slaat deze versleuteld maximaal 24 uur op, zodat wij in staat zijn je van antwoord te voorzien. Thuis.todayis niet verantwoordelijk voor gegevens die door WhatsApp zelf worden verwerkt. Voor het gebruik van WhatsApp ben je akkoord gegaan met de voorwaarden van WhatsApp: <a href="https://www.whatsapp.com/legal/?l=nl#terms-of-service">https://www.whatsapp.com/legal/?l=nl#terms-of-service</a>
		</p>		
		<p><strong>Cookies</strong></p>
		<p>Voor een deel van onze diensten moeten wij zogenaamde cookies inzetten. Cookies zijn kleine databestanden die een internetbrowser op jouw computer opslaat. In cookies kan informatie over jouw bezoek aan onze site worden opgeslagen. De meeste browsers zijn standaard zo ingesteld dat ze cookies accepteren. Je kunt je browser zo configureren dat hij cookies weigert of vooraf om bevestiging vraagt. Als je cookies weigert, kan het echter tot gevolg hebben dat niet al onze functies storingsvrij werken.</p>
		<p><strong>Thuis.today Privacy Statement</strong></p>
		<p>De websites van thuis.today maken gebruik van Google Analytics, een reclame-analysedienst van Google Inc. (‘Google’). Google Analytics maakt gebruik van zgn. cookies, tekstbestanden die op een computer opgeslagen worden en het mogelijk maken om gebruik van onze website te analyseren. De door de cookies geproduceerde informatie over het gebruik (inclusief uw IP-adres) worden aan een server van Google in de VS doorgegeven en daar opgeslagen. Google gebruikt deze informatie om gebruik van de website te evalueren, om rapporten over de websiteactiviteiten voor de websitebeheerders samen te stellen en voor verdere diensten die met het website- en internetgebruik samenhangen. Ook zal Google deze informatie in bepaalde gevallen aan derden doorgeven, zolang het wettelijk voorgeschreven is of zover de derde partij deze gegevens in opdracht van Google verwerkt. Google zal in geen geval een IPadres met andere data van Google linken. Door gebruik te maken van onze website stem je ermee in dat Google informatie op de hierboven genoemde wijze verwerkt voor het hiervoor genoemde doel.
		</p>

		<p>De gegevens die door e-tracker-technologieën verzameld worden, worden zonder uitdrukkelijk gegeven toestemming van jou niet gebruikt om je persoonlijk te identificeren en niet met persoonsgebonden gegevens over de drager van het pseudoniem samengevoegd. De websites van thuis.today maken gebruik van plug-ins van Facebook en Twitter, bijv. de Facebook ‘Vind ik leuk’-knop en de Twitter ‘Volg’-knop. Bij die knop, resp. het erop klikken, wordt een directe verbinding tussen een computer en de server van de aanbieder van de plug-in (bijv. dus Facebook, Twitter) tot stand gebracht. Daarbij kunnen Facebook en Twitter persoonsgebonden en niet-persoonsgebonden gegevens verzamelen. Voor meer informatie over de wijze waarop deze diensten gebruik maken van persoonsgegevens verwijzen wij door naar hun privacybeleid, vindbaar op <a href="http://www.facebook.com/privacy/explanation.php">http://www.facebook.com/privacy/explanation.php</a> en <a href="http://twitter.com/privacy">http://twitter.com/privacy</a> .
		</p>
	  </div>
	</div>
	<!-- End Main Content Home -->

<?php include('include/footor.php');?>


</body>
</html>