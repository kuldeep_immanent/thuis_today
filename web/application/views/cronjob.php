<?php $this->load->helper('custom_helper'); ?>

    <!-- Restaurants List -->
    <p><b>Shop Owner Name: </b> <?php print_r($shopdetail->user_name); ?> </p>
     <p><b>Address: </b> <?php print_r($shopdetail->address_ar); ?> </p>
      <p><b>Post Code: </b> <?php print_r($shopdetail->postal_code); ?> </p>
       <p><b>Commission rate: </b> 6 </p>
    <div class="row">
        <div class="col-sm-12">

            <table border="1" cellspacing="0" cellpadding="1">       
                <thead>
                   
                    <tr>
                        <th class="text-center">Order ID</th>
                        <th class="text-center">Base Amount</th>

                        <th> Fee </th>

                        <th> Vat low </th>

                        <th> Vat high </th>

                        <th> Delivery Cost </th>

                        <th> Payment Cost </th>

                        <th>€ Total Amount </th>

                        <th> Payment Method </th>
                       
                    </tr>
                </thead>
                <tbody>
                       <?php foreach($orders as $entry){ ?>
                        <tr class="<?php if(!$entry->is_active): ?>danger <?php endif; ?><?php if(!$entry->is_verified): ?>warning<?php endif; ?>">  

                        <?php $vatlow = (gethighlowvat($entry->cart)[6])*(6/100);
                              $vathigh = (gethighlowvat($entry->cart)[21])*(21/100);
                              $totalamout = ((gethighlowvat($entry->cart)[6]) + (gethighlowvat($entry->cart)[21]));
                              $commsion = round(($totalamout)*(6/100), 2);
                              $array['comm'] +=$commsion;
                              $array[$entry->payment_method] +=$entry->total_payment;
                              $array['total'] +=$entry->total_payment;                             
                         ?>     
                            <td><?php echo $entry->id ?></td>        
                            <td><?php echo str_replace('.', ',', round($totalamout, 2)) ?></td>
                            <td><?php echo str_replace('.', ',', $commsion) ?></td>
                            <td><?php echo str_replace('.', ',', round($vatlow, 2))  ?></td>
                <td class="text-center" style="vertical-align: middle;">
                                <?php echo str_replace('.', ',', round($vathigh, 2)) ?>
                                
                            </td>
                           <td class="text-right"> <?php echo str_replace('.', ',', $entry->delivery_charge)  ?></td>
                           <td class="text-right"> <?php echo str_replace('.', ',', $entry->service_charge)  ?></td>
                            <td class="text-right"> <?php echo str_replace('.', ',', $entry->total_payment) ?></td>
                            <td class="text-right"> <?php echo $entry->payment_method  ?></td>
                            
                        </tr>
                        <?php } ?>
                    
                </tbody>
            </table>
            <?php 
             $paymentsystem = (($array['Paypal']) + ($array['Ideal']));
             $commsionvat = round(($array['comm'] * (21/100)), 2);
             $totalcommsrecive = ($commsionvat + $array['comm']);
             if($paymentsystem > $totalcommsrecive){
             $lastpayment =  ($paymentsystem - $totalcommsrecive);
             $topayrec = "pay";
         }
             else{$lastpayment =  ($totalcommsrecive - $paymentsystem); $topayrec = "receive";}
             ?>
                    
                                    <p><b>Total Commission: </b> <?php echo '€'.str_replace('.', ',', $array['comm']);?>  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>Total of Payment systems: </b> <?php echo '€'.str_replace('.', ',', $paymentsystem);?></p>
                        <p><b>Vat commission 21%: </b> <?php echo '€'.str_replace('.', ',', $commsionvat); ?>   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>Total of Cash: </b> <?php echo '€'.str_replace('.', ',', $array['Free']); ?> </p>
                        <p><b>Total commission to receive: </b> <?php echo '€'.str_replace('.', ',', $totalcommsrecive);?>   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>Total Sold: </b> <?php echo '€'.str_replace('.', ',', $array['total']);?> </p>
                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                         <p><b>To <?php echo str_replace('.', ',', $topayrec);?>: </b> <?php echo '€'.str_replace('.', ',', $lastpayment); ?> </p>
                    
        </div>
    </div>
