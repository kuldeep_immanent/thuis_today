<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Thuis.today - Welcome</title>
</head>
<body>
	<?php include('include/help.php');?>
 	<!--facebook-like-script - please developers configure this properly-->
       
       <div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1298368983524392";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
      
      
       
		<!-- End facebook like script      -->
	<!--Header zone for the main page-->
	<div class="header-pages">
		<div class="header-pages-container">

			<a href="<?php echo base_url();?>"> <h1 class="logo">Lorem ipsum dolor sit amet consectuer</h1></a>

			<div class="menu-top">
				
				<div class="links-menu-top">
					<a href="<?php echo base_url(); ?>" class="change-location">Change Location</a>
					<a href="<?php echo base_url(); ?>Login" class="clear"><img src="<?php echo base_url(); ?>/assets/images/user-pages.png"></a>
					<a href="#" class="clear help"><img src="<?php echo base_url(); ?>/assets/images/question-mark.png"></a>
					<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="100" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
					<!--						<a href="#" class="clear"><img src="images/language-nl.png"></a>-->
				</div>
			</div>
			<div class="filters">
				<div class="filters-listing">
					<label><input type="checkbox">Vegetable shop</label><br>
					<label><input type="checkbox">Butcher</label><br>
					<label><input type="checkbox">Cheese</label><br>
					<label><input type="checkbox">Bakery</label><br>
					<label><input type="checkbox">Other</label><br>
					<input type="submit" value="ok" class="small-input">
				</div>
				<div class="spacer">&nbsp;</div>

			</div>


		</div>
	</div>

	<!--End Header-->
	<!-- Main content Home -->

	<div class="main-container">
		<h1 class="pages">Cookie verklaring</h1>
		<div class="text-content">       
		<p>Op onze  website&nbsp;thuis.today wordt gebruik  gemaakt van cookies. Ook via derden die door thuis.today zijn ingeschakeld  worden cookies geplaatst. In het onderstaande document informeren wij u over  het gebruik van cookies op onze website.</p>
        <p>Wat is een cookie?​</p>
        <p>Een cookie  is een eenvoudig klein bestandje dat met pagina&rsquo;s van deze website wordt  meegestuurd en door uw browser op uw harde schrijf van uw computer wordt  opgeslagen. De daarin opgeslagen informatie kan bij een volgend bezoek weer naar  onze servers of die van de betreffende derde partijen teruggestuurd worden.</p>
        <p>Wat is een script?​</p>
        <p>Een script  is een stukje programmacode dat wordt gebruikt om onze website goed te laten  functioneren en interactief te maken. Deze code wordt uitgevoerd op onze  server, of op uw apparatuur.</p>
        <p>Advertentie Cookie<br>
          Wij  maken op onze website gebruik van verschillende technieken om advertenties te  tonen en de effectiviteit van deze advertenties te meten. Deze informatie  hebben wij nodig voor ons affiliate-programma en om u gepersonaliseerde  advertenties te kunnen tonen.<br>
          Wij  gebruiken advertentiecookies en technieken van, maar niet beperkt tot,  Criteo, AppNexus, Improve Digital, OpenX,  Right Media (onderdeel van Yahoo), Rubicon, Pubmatic, Yieldlab, Smart AdServer,  Tribal Fusion (onderdeel van Exponential), Casalemedia (onderdeel van Index  Exchange), AdSpirit, AdTiger, AdScale, Adition, Adtech, Vertical Network,  Populis, Xaxis, ValueClick Media, Advertising.com, Glam Media, Lijit, Ve  Interactive en BidSwitch (onderdeel van Iponweb)<br>
          Wij  hebben geen invloed op het gebruik van data door bovengenoemde bedrijven en/of  derden. Lees de privacyverklaring van deze bedrijven om meer informatie te  verkrijgen hoe zij met gegevens omgaan (let op; deze verklaring kan regelmatig  wijzigen).<br>
          De  informatie die Criteo, AppNexus, OpenX, Yahoo, Pubmatic, Exponential, Xaxis en  Rubicon verzamelen wordt opgeslagen op servers in de Verenigde Staten en wordt  zoveel mogelijk geanonimiseerd. Deze dienst stelt zich te houden aan de Safe  Harbor principes en is aangesloten bij het Safe Harbor-programma van het  Amerikaanse Ministerie van Handel. Dit houdt in dat er sprake is van een  passend beschermingsniveau voor de verwerking van eventuele persoonsgegevens.</p>
        <p>In- en uitschakelen van cookies en verwijdering daarvan</p>
        <p>Meer  informatie omtrent het in- en uitschakelen en het verwijderen van cookies kunt  u vinden in de instructies en/of met behulp van de Help-functie van uw browser.</p>
        <p>Vragen<br>
          Voor vragen of  opmerkingen kunt u terecht bij <a href="mailto:info@thuis.today">info@thuis.today</a></p>
         </div>
    </div>
	<!-- End Main Content Home -->
<?php include('include/footor.php');?>


</body>
</html>