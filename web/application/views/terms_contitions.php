<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Thuis.today - Welcome</title>
</head>
<body>
  <?php include('include/help.php');?>
  <!--facebook-like-script - please developers configure this properly-->
       
       <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1298368983524392";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
      
      
       
    <!-- End facebook like script      -->
	<!--Header zone for the main page-->
	<div class="header-pages">
		<div class="header-pages-container">

		<a href="<?php echo base_url();?>"> 	<h1 class="logo">Lorem ipsum dolor sit amet consectuer</h1> </a>

			<div class="menu-top">
				
				<div class="links-menu-top">
					<a href="<?php echo base_url(); ?>" class="change-location">Change Location</a>
					<a href="<?php echo base_url(); ?>Login" class="clear"><img src="<?php echo base_url(); ?>/assets/images/user-pages.png"></a>
					<a href="#" class="clear help"><img src="<?php echo base_url(); ?>/assets/images/question-mark.png"></a>
          <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="100" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
					<!--						<a href="#" class="clear"><img src="images/language-nl.png"></a>-->
				</div>
			</div>
			<div class="filters">
				<div class="filters-listing">
					<label><input type="checkbox">Vegetable shop</label><br>
					<label><input type="checkbox">Butcher</label><br>
					<label><input type="checkbox">Cheese</label><br>
					<label><input type="checkbox">Bakery</label><br>
					<label><input type="checkbox">Other</label><br>
					<input type="submit" value="ok" class="small-input">
				</div>
				<div class="spacer">&nbsp;</div>

			</div>


		</div>
	</div>

	<!--End Header-->
	<!-- Main content Home -->

	<div class="main-container">
		<h1 class="pages">Algemene Voorwaarden Consumenten</h1>
		<div class="text-content">
          <p>
            Deze 'Algemene Voorwaarden Consumenten' zijn  van toepassing op de relatie tussen A.H. Aarts hodn. Thuis.today (&quot;Thuis.today&quot;)  en de Consumenten. Winkels verwijzen wij door naar de 'Algemene Voorwaarden winkels'. <br><br>
            <strong>1.Definities</strong><br>
            <strong>Aanbod :</strong> het  product –en dienstenaanbod van de winkel dat door de Consument via het Platform  bij het Restaurant kan worden besteld.<br>
            <strong>Bestelling : </strong>een  bestelling van de Consument bij de winkel van het door de Consument  geselecteerde Aanbod.<br>
            <strong>Consument:</strong> een  natuurlijk persoon die niet handelt in de uitoefening van een beroep of  bedrijf.<br>
            <strong>Overeenkomst :</strong> een  overeenkomst tussen de Consument en de winkel met betrekking tot een Bestelling  en de bezorging of het afhalen van de Bestelling.<br>
            <strong>Platform :</strong> de website(s),  apps, tools en andere apparaten van thuis.today en aan haar gelieerde bedrijven  en zakelijke partners waarop de Service beschikbaar wordt gemaakt.<br>
            <strong>Winkel:</strong> Het  bedrijf en/of bedrijven dat producten en diensten aanbied en verkoopt en voor  de totstandkoming en betaling van Overeenkomsten het Platform gebruikt.<br>
            <strong>Winkeltinformatie:</strong> de informatie van de winkel met betrekking tot, onder andere, bedrijfs -en  contactgegevens, algemene informatie, productaanbod (de producten en diensten),  prijzen voor ieder afzonderlijk product (inclusief BTW), bedrijfslogo, grafisch  materiaal, bezorggebied (inclusief postcodes), bezorgkosten en minimale  bestelbedragen.<br>
            <strong>Service :</strong> de  commerciële diensten en/of werkzaamheden die door thuis.today worden aangeboden  aan de Consument, onder meer bestaande uit het publiceren van het Aanbod, het  faciliteren van het tot stand komen van Overeenkomsten en het doorsturen van  Bestellingen naar de relavante winkel.<br><br>
            
            <strong>2.Identiteit van thuis.today</strong><br>
            A.H. Aarts handelend onder de naam &lsquo;thuis.today&rsquo;. <br>
            Adres hoofdvestiging:<br>
            Schout Lieshoutstraat 2<br>
            5251-TP Vlijmen<br>
            KvK: xx<br>
            BTW-nr: xx<br>
            Correspondentatieadres:<br>
            SchoutLieshoutstr. 2B<br>
            5251-TP Vlijmen<br>
            <strong>Email:</strong> <a href="mailto:info@thuis.today">info@thuis.today</a></p>
          <p style="color:#ff0000"><br>
            <strong>Tel:</strong> 053-4805860 (bereikbaar van maandag tot en met  woensdag van 09:00 tot 23:00, donderdag en vrijdag van 09:00 tot 00:00, en  zaterdag en zondag van 10:00 tot 00:00)<br>
			  <strong>Fax:</strong> 053-4805861<br><br></p>
            <p><strong>3.Toepasselijkheid</strong></p>
          <ol start="1" type="1">
            <li>Deze Algemene Voorwaarden Consumenten zijn slechts van toepassing       op de Service. Thuis.today is niet verantwoordelijk voor het Aanbod. Op       het Aanbod zijn, indien van toepassing, de algemene voorwaarden van de       winkel van toepassing.</li>
            <li>De Consument gaat door middel van het plaatsen van een Bestelling       direct een Overeenkomst met de winkel aan voor de levering van het door de       Consument geselecteerde Aanbod.</li>
            <li>Het aanbieden van betalingsfaciliteiten zijn namens de winkel. </li>
          </ol>
          <p><strong>4.Het Aanbod</strong></p>
          <ol start="1" type="1">
            <li>Thuis. today publiceert het Aanbod namens de Restaurants op het       Platform, overeenkomstig de door de Restaurants aangeleverde       Restaurantinformatie. Thuis.today aanvaardt geen verantwoordelijkheid of       aansprakelijkheid voor de inhoud van het Aanbod en van de Winkel informatie       op het Platform.</li>
            <li>Thuis.today geeft alle winkelinformatie zodanig weer, dat voor de       Consument duidelijk is wat zijn rechten en verplichtingen zijn die aan de       aanvaarding van het Aanbod zijn verbonden.</li>
            <li>Thuis.today aanvaardt geen aansprakelijkheid voor de       bereikbaarheid van het Platform.</li>
          </ol>
          <p><strong>5. De Overeenkomst</strong></p>
          <ol start="1" type="1">
            <li>De Overeenkomst komt tot stand op het moment dat de Consument de       Bestelling definitief maakt door het klikken op de knop 'Nu kopen' tijdens       het proces van het plaatsen van een Bestelling via het Platform.</li>
            <li>De Overeenkomst kan slechts door de winkel worden uitgevoerd       indien de Consument juiste en volledige contactinformatie invult bij de       afronding van de Bestelling. De Consument heeft de plicht om onjuistheden       in verstrekte of vermelde betaalgegevens onverwijld aan Thuis.today en de       winkel te melden.</li>
            <li>In verband met informatie over de status van zijn Bestelling,       dient de Consument na het plaatsen van de Bestelling telefonisch en per       e-mail bereikbaar te zijn voor zowel de winkel als Thuis.today via de door       hem aangegeven contactgegevens.</li>
            <li>Indien de Consument kiest voor bezorging van de Bestelling, dan       dient hij aanwezig te zijn op het door de Consument aangegeven bezorgadres       en tijdstip om de Bestelling in ontvangst te kunnen nemen.</li>
            <li>Indien de Consument kiest voor het afhalen van de Bestelling, dan       dient hij op de gekozen tijd aanwezig te zijn op de afhaallocatie van de       winkel, die in de bevestigingsmail of op de website van thuis.today is       weergegeven om de Bestelling in ontvangst te kunnen nemen.</li>
            <li>Thuis.today aanvaardt geen aansprakelijkheid in verband met de       uitvoering van de Overeenkomst.</li>
          </ol>
          <p><strong>6.Ontbinding van de Overeenkomst en  annulering van de Bestelling</strong></p>
          <ol start="1" type="1">
            <li>In verband met de bederfelijkheid van het Aanbod geldt dat de       Consument geen recht heeft om de Overeenkomst te ontbinden. Bestellingen       kunnen niet door de Consument bij Thuis.today worden geannuleerd.       Annulering van de Bestelling door de Consument bij de winkel is slechts       mogelijk indien het de winkel expliciet aangeeft dat Annulering van de       Bestelling door de Consument mogelijk is.</li>
            <li>Thuis.today wijst de Consument erop dat de winkel het recht heeft       de Bestelling te annuleren indien er bijvoorbeeld sprake is van overmacht,       indien het Aanbod niet meer beschikbaar is of indien de Consument een       incorrect of onbereikbaar telefoonnummer of bezorgadres heeft opgegeven.</li>
            <li>Indien de Consument een valse Bestelling plaatst (bijvoorbeeld       door niet te betalen of door niet aanwezig te zijn op de bezorglocatie of       afhaallocatie om de Bestelling in ontvangst te kunnen nemen) of anderszins       zijn plichten die voortvloeien uit de Overeenkomst niet nakomt, dan kan Thuis.today       besluiten om toekomstige Bestellingen van de betreffende Consument te       weigeren.</li>
            <li>Thuis.today heeft het recht om namens de winkel Bestellingen te       weigeren en Overeenkomsten te annuleren, indien er gerede twijfel bestaat       over de juistheid of authenticiteit van de Bestelling. Indien Thuis.today       een Bestelling annuleert die al betaald is, dan zal Thuis.today dat bedrag       overboeken op de dezelfde rekening als waarvan de betaling heeft       plaatsgevonden. Indien de Consument aantoonbaar valse of frauduleuze       Bestellingen plaatst, dan kan Thuis.today hiervan aangifte doen bij de       politie.</li>
          </ol>
          <p><strong>7.Betaling</strong></p>
          <ol start="1" type="1">
            <li>Op het moment dat de Overeenkomst tot stand is gekomen       overeenkomstig in het artikel 5.1 van deze Algemene Voorwaarden       Consumenten bepaalde, ontstaat er een betalingsverplichting van de       Consument jegens de winkel. Door de Consument kan aan deze       betalingsverplichting worden voldaan door te betalen met een online       betaalmiddel via het Platform of door betaling aan de winkel aan de deur       of op de afhaallocatie.</li>
            <li>Behoudens het bepaalde in artikel 6, vierde lid van deze Algemene       Voorwaarden Consumenten, is het (gedeeltelijk) terugboeken van een online       betaling alleen mogelijk indien de Bestelling niet (geheel) geleverd kan       worden. De terugboeking vindt altijd plaats op dezelfde rekening als       waarvan de betaling heeft plaatsgevonden.</li>
            <li>De winkel heeft Thuis.today geautoriseerd om namens het Restaurant       de online betaling van de Consument in ontvangst te nemen.</li>
          </ol>
          <p>&nbsp;</p>
          <p><strong>8. Klachtenregeling</strong></p>
          <ol start="1" type="1">
            <li>Klachten van de Consument over het Aanbod, de Bestelling of de       uitvoering van de Overeenkomst, dienen bij de winkel te worden neergelegd       via de in artikel 2 van deze Algemene Voorwaarden Consumenten onder       'Correspondentieadres' vermelde contactgegevens. Thuis.today kan bij de       klachtbehandeling slechts een bemiddelende rol vervullen.</li>
            <li>Als de Consument een klacht heeft over de Service, dan dient de       klacht via het contactformulier, per e-mail of schriftelijk via de post te       worden gecommuniceerd aan de klantenservice van Thuis.today op het in       artikel 2 van deze Algemene Voorwaarden Consumenten aangegeven       contactadres.</li>
            <li>Nadat de klacht door Thuis.today is ontvangen zal Thuis.today zo       snel mogelijk, doch uiterlijk binnen een week, reageren met een ontvangstbevestiging.       Thuis.today streeft ernaar de klacht zo snel mogelijk, doch uiterlijk       binnen 2 weken te behandelen.</li>
            <li>Klachten zoals omschreven in de leden 1 en 2 van dit artikel,       moeten binnen bekwame tijd nadat de Consument de gebreken heeft       geconstateerd, volledig en duidelijk omschreven worden ingediend bij de       winkel of thuis.today.</li>
          </ol>
          <p><strong>9.Toepasselijk recht en bevoegde rechter</strong></p>
          <ol start="1" type="1">
            <li>Op overeenkomsten tussen Thuis.today en de Consument waarop deze       Algemene Voorwaarden Consumenten betrekking hebben, is Nederlands recht       van toepassing.</li>
            <li>Geschillen tussen Thuis.today en de Consument over de       totstandkoming of uitvoering van overeenkomsten met betrekking tot de Service,       kunnen zowel door Thuis.today als de Consument uitsluitend worden       voorgelegd aan de bevoegde rechter te &lsquo;s-Hertogenbosch.</li>
            <li>De Europese Commissie beheert een ODR platform. Dit platform is te       vinden op http://ec.europa.eu/odr. Thuis.today sluit het gebruik van       Alternative Dispute Resolution als bedoeld in de richtlijn 2013/11/EU       expliciet uit. Het e-mailadres van Thuis.today is info@thuis.today</li>
          </ol>
          <p><strong>10.Nieuwsbrief</strong></p>
          <ol start="1" type="1">
            <li>Bij het afronden van de Bestelling kan de Consument kiezen voor       het ontvangen van de nieuwsbrief. De Consument kan zich afmelden voor deze       nieuwsbrief via www.thuisbezorgd.nl/nieuwsbrief of door contact op te       nemen met de klantenservice via de in artikel 2 van deze Algemene       Voorwaarden Consumenten onder 'Correspondentieadres' vermelde       contactgegevens.</li>
          </ol>
          <p><strong>11.Inzicht en correctie van opgeslagen  persoonsgegevens</strong></p>
          <ol start="1" type="1">
            <li>Thuis.today slaat persoonlijke gegevens op van de Consument. Op de       opslag van persoonsgegevens is het Privacy Statement van toepassing.</li>
          </ol>
        </div>
	</div>
	<!-- End Main Content Home -->

<?php include('include/footor.php');?>


</body>
</html>