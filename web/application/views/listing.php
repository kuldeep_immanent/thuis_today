<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Thuis.today - Welkom</title>		
	</head>
	<body>
		<?php include('include/help.php');?>
		 	<!--facebook-like-script - please developers configure this properly-->
       
       <div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1298368983524392";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
      
      
       
		<!-- End facebook like script      -->
		<!--Header zone for the main page-->
		<div class="header-pages">
			<div class="header-pages-container">
				
			<a href="<?php echo base_url();?>">	<h1 class="logo">Lorem ipsum dolor sit amet consectuer</h1> <a href="<?php echo base_url();?>"></a>
				
				<div class="menu-top">
					<p class="current-location-top">Bestel verse producten vanuit, Niejmegen</p>
					<div class="links-menu-top">
						<a href="<?php echo base_url();?>" class="change-location">Verander uw locatie</a>
						<a href="<?php echo base_url(); ?>Login" class="clear"><img src="<?php echo base_url(); ?>assets/images/user-pages.png"></a>
						<a href="#" class="clear help"><img src="<?php echo base_url(); ?>assets/images/question-mark.png"></a>
						<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="100" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
<!--						<a href="#" class="clear"><img src="images/language-nl.png"></a>-->
					</div>
				</div>
				<div class="filters">
					<div class="filters-listing">
						<form action="<?php echo site_url(); ?>/Shoplist" method="POST">
							<input type="hidden" name="sendzip" value="<?php echo $postcode;?>" id="zipcode">
						<label><input type="checkbox" class="chk_boxes">All</label><br>
						<?php if(!empty($categories)){ foreach ($categories as $value) {?>
							<label><input type="checkbox" name="categoryids[]" value="<?php echo $value->category_id; ?>" class="chk_boxes1"><?php echo $value->category_name; ?></label><br>
							<?php } }?>	
							<input type="submit" value="ok" class="small-input">
						</form>
					</div>
					<div class="spacer">&nbsp;</div>
					<a href="#">Kaart Uitzicht</a>
					<a href="#" class="grid filter-shops"><img src="<?php echo base_url(); ?>assets/images/filter.gif"> Filter by type</a>
				</div>
			</div>
		</div>
		
		<!--End Header-->
		<!-- Main content Home -->
		
		<div class="main-container">
				<?php  
	                $lerror = $this->session->flashdata('error_msg');
	                  if(isset($lerror))
	                  {
	                      echo '<div class="alert alert-info">
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.'</div>'; 
	                  }
	             ?> 
			<div id="postList">	
					<?php 
							if($list != FALSE)
							{
								foreach($list as $list1)
								{
					?>	
			<div class="shop-item">
				
				<table cellpadding="0" cellspacing="0" width="100%" align="center">
					<tr>
						<td width="20%" valign="middle" align="center"><img src="<?php echo IMAGE_URL.$list1->logo; ?>"  width="150px" height="100px"></td>
						<td width="60%" valign="top">
							
							<a href="<?php echo site_url(); ?>/Shoppage?id=<?php echo $list1->restaurant_id;?>&postcode=<?php echo $postcode;?>" class="shoplistd">
						<h1><?php echo $list1->restaurant_name_ar; ?></h1>
						<P><?php echo $list1->address_ar; ?></P>
					</a>
							<div class="shop-details-bar">
								<!-- <div class="main-info">FROM 15:30 | FREE DELIVERY FROM € 10,00 | MIN € 7,50 </div> -->
								<!-- 					<div class="discounts"><em>18 discounts</em></div>
								<div class="new">NEW</div>
								<div class="tips">TIPS</div> -->
								
							</div>
						</td>
						<td width="20%" valign="top" align="center">
							<img src="<?php echo IMAGE_URL.$list1->image; ?>" width="80"><br>
							<strong class="orange-bold"><?php echo $list1->category_name; ?></strong>
						</td>
					</tr>
				</table>
			</div>
			<?php
			
				} }
			?>
<div class="tabs-pagination">
					<?php echo $this->ajax_pagination->create_links(); ?>
					<br clear="all">
				</div>
				 
			</div>
			
		</div>
<?php include('include/footor.php');?>	
	</body>
</html>
<script>
$(function() {
    $('.chk_boxes').click(function() {
        $('.chk_boxes1').prop('checked', this.checked);
    });
});
</script>