<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Thuis.today - Welcome</title>	
</head>
<body>
	<?php include('include/help.php');?>
	 	<!--facebook-like-script - please developers configure this properly-->
       
       <div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1298368983524392";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
       
       
		<!-- End facebook like script      -->

	<!--Header zone for the main page-->
	<div class="header-pages">
		<div class="header-pages-container">

		<a href="<?php echo base_url(); ?>"> <h1 class="logo">Lorem ipsum dolor sit amet consectuer</h1> </a>

			<div class="menu-top">
				
				<div class="links-menu-top">
					<a href="<?php echo base_url(); ?>" class="change-location">Verander uw locatie</a>
					<a href="<?php echo base_url(); ?>Login" class="clear"><img src="<?php echo base_url(); ?>assets/images/user-pages.png"></a>
					<a href="#" class="clear help"><img src="<?php echo base_url(); ?>assets/images/question-mark.png"></a>
					<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="100" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
					<!--						<a href="#" class="clear"><img src="images/language-nl.png"></a>-->
				</div>
			</div>
			<div class="filters">
				<div class="filters-listing">
					<label><input type="checkbox">Vegetable shop</label><br>
					<label><input type="checkbox">Butcher</label><br>
					<label><input type="checkbox">Cheese</label><br>
					<label><input type="checkbox">Bakery</label><br>
					<label><input type="checkbox">Other</label><br>
					<input type="submit" value="ok" class="small-input">
				</div>
				<div class="spacer">&nbsp;</div>

			</div>


		</div>
	</div>

	<!--End Header-->
	<!-- Main content Home -->

	<div class="main-container">
		<?php  
                $lerror = $this->session->flashdata('error_msg');
                $lerror1 = $this->session->flashdata('error_msg1');
                  if(isset($lerror))
                  {
                      echo '<div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.'</div>'; 
                  }
                  if(isset($lerror1))
                  {
                      echo '<div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror1.'</div>'; 
                  }

             ?>
	  <h1 class="pages">login of registreer u als gebruiker</h1>
	  <div class="text-content">
			<table cellpadding="0" cellspacing="0" width="95%" align="center">
	  			<tr>
	  			 <form enctype="multipart/form-data" action="<?php echo site_url();?>/Login/forgot_password" method="post">
					<td width="50%" valign="top">
					 	<p><strong>Als u al een geregistreerd gebruiker bent, kunt u hier inloggen</strong></p>
					 	 <span style="color:red;"><?php echo form_error('username'); ?> </span>
						 <p>
							gebruikersnaam<sup>*</sup>:<br>
                            <input type="text" name="username" required class="form-input">
                         </p>
						<p> <input type="submit" class="orange-input-checkout" value="submit"></p>
					</td>
				</form>
				 
				</tr>
		  	</table>
	  </div>
	</div>
	<?php include('include/footor.php');?>	

</body>
</html>