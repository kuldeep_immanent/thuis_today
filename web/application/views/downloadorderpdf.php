	<div class="modal-background">
        <div class="modal-white">			
            <div id="foreach">
		<center><h2>Details for Order number <?php echo $orders->id;?></h2></center>
        <?php
             $ddate = trim(str_replace("PST", '', $orders->date));
             $date = date("d.m.Y", strtotime($ddate));
         ?>
         <table cellpadding="5" cellspacing="0" width="100%" align="center" class="listing-checkout">
            <tr>
               <td colspan="2"> <strong>Customer Name:</strong> <?php if(isset($user_detail->name)){echo $user_detail->name;}?></td>
               <td colspan="2"> <strong>Shop Name:</strong> <i><?php if(isset($orders->restaurant_name_ar)){echo $orders->restaurant_name_ar;}?></i></td>
            </tr>
            <tr>
                <td colspan="2"> <strong>Email:</strong> <?php if(isset($user_detail->email)){echo $user_detail->email;}?></td>
               <td colspan="2"> <strong>Phone:</strong> <?php if(isset($user_detail->phone)){echo $user_detail->phone;}?></td>
            </tr>
        	<tr>	
               <td colspan="2"> <strong>Order placed on:</strong><?php echo $date;?></td>

		       <td colspan="2"> <strong>Order status: </strong>&nbsp;<?php if(($orders->status)== '2')
                         {
                             echo 'In Process';
                         }
                        if(($orders->status)== '3')
                         {
                             echo 'Approved';
                         } 
                        if(($orders->status)== '0')
                         {
                            echo 'Delivered';
                         } 
                        if(($orders->status)== '4')
                         {
                             echo 'Pending for Approval';
                         }
                        if(($orders->status)== '5')
                         {
                             echo 'Canceled';
                         }
                        if(($orders->status)== '7')
                         {
                            echo 'Out of Delivery';
                         } ?></td></tr>
            <tr>
        		<td colspan="2"><strong>Delivery Address:</strong> <?php if(isset($user_detail->address)){echo $user_detail->address;}?></td>
        		<td colspan="2"><strong>Payment method:</strong> <?php echo $orders->payment_method;?></td>
            </tr>
        </table>
        		<strong>Products ordered:</strong>
	  	<table cellpadding="5" cellspacing="0" width="100%" align="center" class="listing-checkout" border="1">
		  			<tr>
                        <td>Product photo</td>
                        <td> Product name</td>
                        <td> Quantity</td>
                        <td>
                            Price
                        </td>
                       
                    </tr>
                       <?php 
                       $cart = json_decode($orders->cart);
                           foreach ($cart as $value) 
                            { ?>
                              <tr>                   
                                            <td><img src="<?php echo IMAGE_URL.$value->coupon; ?>" height="65px" width="65px"></td> 
                                            <td> <p><strong><?php echo $value->name; ?></strong></p></td>
                                            <td><?php echo $value->qty; ?></td>
                                            <td>€<?php echo $value->price; ?></td>                        
                               </tr>                                       
                           <?php } ?>
                            <tr>
                                 <td colspan="2"><strong>Payment Fee:€ <?php echo $orders->service_charge; ?></strong></td>
                                 <td colspan="2"><strong>Delivery Cost:€ <?php echo $orders->delivery_charge; ?></strong></td>
                            </tr>
                            
                                   
                    <tr>
						<td colspan="4"><strong>Total price:€ <?php echo $orders->total_payment;?></strong></td>
							<!-- <td colspan="2"><a href="#" class="invoice"><img src="<?php //echo base_url(); ?>/assets/images/pdf.png" width="20">Download invoice</a></td> -->
					</tr>                    
		</table>
    </div>
		</div>
    </div>