<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Thuis.today - Welcome</title>	
</head>
<body>
	<?php include('include/help.php');?>
	 	<!--facebook-like-script - please developers configure this properly-->
       
       <div id="fb-root"></div>
<script src="<?php echo base_url();?>assets/js/facebook_sdk.js"></script>
       
       
		<!-- End facebook like script      -->

	<!--Header zone for the main page-->

<script>
window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
    FB.init({
      appId      : '1277531565615540', // FB App ID
      cookie     : true,  // enable cookies to allow the server to access the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });
    
    // Check whether the user already logged in
	
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			//display user data
			/* var getUser = '<?php echo $this->session->set_userdata('fb_user_data');?>';
			if(getUser == ''){
				getFbUserData();				
			}else{
				RedirectUrl = "<?php echo base_url();?>index.php/Cart/checkout";
				window.location.href = RedirectUrl;
			} */
		}
	});
};

// Facebook login with JavaScript SDK
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, {scope: 'email,user_birthday,user_location,user_hometown,user_about_me'});
}

// Fetch the user profile data from facebook
function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,locale,link,gender,picture,age_range,location,albums,hometown,birthday,about'},
    function (response) {
		console.log(response);
		var fb_id = response.id;
		var first_name = '';
		var last_name = '';
		var email = '';  
		var phone = '';  
		var zipcode = '';  
		var address = '';  
		var city = '';  
		if($.inArray('first_name',response)){
			first_name = response.first_name;
		}
		if($.inArray('last_name',response)){
			last_name = response.last_name;
		}
		if($.inArray('email',response)){
			email = response.email; 
		}
		/* var userData = [];
		$.each(response,function(index,value){
			userData.push(value);
		}); */
		var url = "<?php echo base_url();?>index.php/Login/FbUserData";
			$.ajax({
				url:url,
				data:{'fb_id':fb_id,'first_name':first_name,'last_name':last_name,'email':email,'phone':phone,'zipcode':zipcode,'address':address,'city':city},
				type:'post',
				dataType:'json',
				success:function(res){
					console.log(res);
					var RedirectUrl = '';				
					if(res.CODE == 100){
						//alert('Cart');
						RedirectUrl = "<?php echo base_url();?>index.php/Cart/checkout";
						window.location.href = RedirectUrl;					
					}else if(res.CODE == 103){
						//alert('profile');
						RedirectUrl = "<?php echo base_url();?>index.php/Login/profile";
						window.location.href = RedirectUrl;
					}else{
						//alert('additionalInfo');
						RedirectUrl = "<?php echo base_url();?>index.php/Login/additionalInfo";
						window.location.href = RedirectUrl;
					}					
				}
			}); 
		
    });
}

// Logout from facebook
function fbLogout() {
	var srcLogOut = "<?php echo base_url();?>uploads/fblogin.jpg";
    FB.logout(function() {
        document.getElementById('fbLink').setAttribute("onclick","fbLogin()");
        document.getElementById('fbLink').innerHTML = '<img src="'+srcLogOut+'"/>';
		$('#name').val('');
		$('#email').val('');		
		$('#phone').val('');		
		$('#address').val('');		
		$('#city').val('');		
		$('#postcode').val('');		
		$('#passwrd').val('');		
		$('#cpass').val('');		
    });
	
} 
</script>	
	
	<div class="header-pages">
		<div class="header-pages-container">

		<a href="<?php echo base_url(); ?>"> <h1 class="logo">Lorem ipsum dolor sit amet consectuer</h1> </a>

			<div class="menu-top">
				
				<div class="links-menu-top">
					<a href="<?php echo base_url(); ?>" class="change-location">Verander uw locatie</a>
					<a href="<?php echo base_url(); ?>Login" class="clear"><img src="<?php echo base_url(); ?>assets/images/user-pages.png"></a>
					<a href="#" class="clear help"><img src="<?php echo base_url(); ?>assets/images/question-mark.png"></a>
					<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="100" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
					<!--						<a href="#" class="clear"><img src="images/language-nl.png"></a>-->
				</div>
			</div>
			<div class="filters">
				<div class="filters-listing">
					<label><input type="checkbox">Vegetable shop</label><br>
					<label><input type="checkbox">Butcher</label><br>
					<label><input type="checkbox">Cheese</label><br>
					<label><input type="checkbox">Bakery</label><br>
					<label><input type="checkbox">Other</label><br>
					<input type="submit" value="ok" class="small-input">
				</div>
				<div class="spacer">&nbsp;</div>

			</div>


		</div>
	</div>

	<!--End Header-->
	<!-- Main content Home -->

	<div class="main-container">
		<?php  
                $lerror = $this->session->flashdata('error_msg');
                $lerror1 = $this->session->flashdata('error_msg1');
                  if(isset($lerror))
                  {
                      echo '<div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.'</div>'; 
                  }
                  if(isset($lerror1))
                  {
                      echo '<div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror1.'</div>'; 
                  }

             ?>
	  <h1 class="pages">login of registreer u als gebruiker</h1>
	  <div class="text-content">
			<table cellpadding="0" cellspacing="0" width="95%" align="center">
	  			<tr>
	  			 <form enctype="multipart/form-data" action="<?php echo site_url();?>/Login/do_login" method="post">
					<td width="50%" valign="top">
					 	<p><strong>Als u al een geregistreerd gebruiker bent, kunt u hier inloggen</strong></p>
					 	 <span style="color:red;"><?php echo form_error('username'); ?> </span>
						 <p>
							gebruikersnaam<sup>*</sup>:<br>
                            <input type="text" name="username" required class="form-input">
                         </p>
						 <p>
							Wachtwoord<sup>*</sup>:<br>
                            <input type="password" name="password" required class="form-input">
                         </p> 
                         <input type="hidden" name="checkoutcheck" value="<?php if(!empty($set)){ echo $set;}?>">
						<p> <input type="submit" class="orange-input-checkout" value="login">&nbsp;&nbsp;&nbsp; <small><a href="<?php echo site_url();?>/Login/forget_password"> Ik ben mijn wachtwoord vergeten</a></small></p>
						<p>U kunt ook inloggen met uw facebook account.</p>
						
						<a href="javascript:void(0);" onclick="fbLogin()" id="fbLink"><img style="with:100px;heigth:20px" id="fbButton" src="<?php echo base_url();?>uploads/fblogin.jpg"/></a>
					</td>
				</form>
				 <form enctype="multipart/form-data" action="<?php echo site_url();?>/Login/register" method="post">
					<td width="50%" valign="top">
						
							<p><strong>Nog geen geregistreerd gebruiker, vul het formulier in om u te registreren </strong></p>
							<p>
                              naam<sup>*</sup><br>
                              <input id="name" type="text" name="name" required class="form-input">
                            </p>
                             <span style="color:red;"><?php echo form_error('email'); ?> </span>
							<p>
                              email<sup>*</sup><br>
                              <input id="email" type="text" name="email" required class="form-input">
                            </p>  
							<p>
                              Telefoon<sup>*</sup><br>
                              <input id="phone" type="text" name="phone" required class="form-input">
                            </p>     
							<p>
                              adres<sup>*</sup><br>
                              <input id="address" type="text" name="address" required class="form-input">
                            </p>   
							<p>
                              Woonplaats<sup>*</sup><br>
                              <input id="city" type="text" name="city" required class="form-input">
                            </p>  
							<p>
                              postcode<sup>*</sup><br>
                              <input id="postcode" type="text" name="zipcode" required class="form-input">
                            </p> 
                            <p>
                              wachtwoord<sup>*</sup><br>
                              <input id="passwrd" type="password" name="password" required class="form-input">
                            </p>  
                            <span style="color:red;"><?php echo form_error('password'); ?>  </span>  
							<p>
                             Bevesting uw wachtwaard<sup>*</sup><br>
                              <input id="cpass" type="password" name="confirmpass" required class="form-input">
                            </p>         
						<p> <input type="submit" class="orange-input-checkout" value="Registreer"></p>                                                                                        
					</td>
					</form>
				</tr>
		  	</table>
	  </div>
	</div>
	<?php include('include/footor.php');?>	

</body>
</html>