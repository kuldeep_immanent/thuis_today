<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Thuis.today - Welkom</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=latin-ext" rel="stylesheet">        
        <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
       
    </head>
    <body>
      <?php include('include/help.php');?>
            <!--facebook-like-script - please developers configure this properly-->
       
       <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1298368983524392";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
      
      
       
        <!-- End facebook like script      -->
        <!--Header zone for the main page-->
        <div class="header-pages">
            <div class="header-pages-container">
                <div class="basket">
                   <span class="bsket"><?php echo str_replace('.', ',', $cart_total);?></span>&euro;
                </div>
             <a href="<?php echo base_url();?>">   <h1 class="logo">Lorem ipsum dolor sit amet consectuer</h1></a>
                <div class="menu-top">
                    <p class="current-location-top">Bestel uw producten bij<strong><?php if(isset($name->restaurant_name_ar)){echo $name->restaurant_name_ar;}?></strong></p>
                    <div class="links-menu-top">
                        <a href="<?php echo base_url();?>" class="change-location">Zoek andere locatie</a>
                        <a href="<?php echo base_url(); ?>Login" class="clear"><img src="<?php echo base_url();?>assets/images/user-pages.png"></a>
                        <a href="#" class="clear help"><img src="<?php echo base_url();?>assets/images/question-mark.png"> </a>
                        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="100" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
<!--                        <a href="#" class="clear"><img src="images/language-nl.png"></a>-->
                    </div>
                </div>
            </div>
        </div>
        <!--End Header-->
        <!-- Main content Home -->
        <div class="main-container-shop-page">
            <h1 class="pages">Overzicht van uw winkelwagen</h1>
            
            <div class="basket-checkout">
                <table cellpadding="10" cellspacing="0" width="95%" align="center" class="listing-checkout">
                    <tr>
                        <td>Product foto</td>
                        <td> Omschrijving</td>
                        <td> Aantal</td>
                        <td>Prijs</td>
                        <td>&nbsp;</td>
                    </tr>
                    <?php if(!empty($cartdetail)){ foreach ($cartdetail as $value) { ?>
                    <tr>
                        <td><img src="<?php echo IMAGE_URL.'/'.$value['coupon']; ?>" width="65px" height="65px"></td>
                        <td> <p><strong><?php echo $value['name']; ?></strong></p></td>

                        <input type="hidden" value="<?php echo $value['rowid'];?>" id="getrowid<?php echo $value['id'];?>">

                        <td><input type="text" onkeyup="myFunction(<?php echo $value['id']; ?>)" data="<?php echo $value['price']; ?>" class="myQuantaty<?php echo $value['id'];?>" value="<?php echo $value['qty']; ?>">&nbsp;&nbsp;<?php if(($value['type']) != ('weight')){?>St.<?php }else{?>kg<?php } ?></td>
                        <td>&euro;<span id="total<?php echo $value['id'];?>"><?php echo $value['subtotal']; ?></span></td>
                        <td><a href="<?php echo base_url();?>Cart/remove?rowid=<?php echo $value['rowid']; ?>&id=<?php echo $id;?>" class="clear-item">x</a></td>
                    </tr>
                    <?php }  }?>
                    <tr>
                        <td colspan="5" align="Center">
                            <strong class="orange-text ">Totaal prijs: &euro;<span class="bsket"><?php echo str_replace('.', ',', $cart_total);?></span></strong>
                        </td>
                    </tr>

                </table>
                <div class="tabs tabs-inputs">
                    <a href="<?php echo base_url();?>Cart/checkout" class="active">Naar afrekenen</a>
                    <a href="<?php echo base_url();?>Cart/destroy?id=<?php echo $id;?>">Maak leeg</a>
                </div>
            </div>
        </div>
        <div class="cleaner"></div>
    
   <?php include('include/footor.php'); ?>
</body>
</html>
<script>
  function myFunction(id) {     
    $(document).ready(function(){
     var prices = $('.myQuantaty'+id).attr('data');        
     var qunty = $('.myQuantaty'+id).val();  
     if(qunty == 0)
        {
           var qunty = $('.myQuantaty'+id).val(1);  
           alert("Zero quantity will not be acceptable as quantity.");           
           var qunty = $('.myQuantaty'+id).val(); 
        }
     var rowid = $('#getrowid'+id).val();
     var price = parseFloat(prices);    
     var result = parseFloat(qunty.replace(",",".")) * price;  
     var results = result.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
     var res = results.replace(".",",");       
    $("#total"+id).html(res);
   //  var res = result.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

            $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>Cart/update",
            data: "id="+id+"&price="+prices+"&qty="+qunty+"&rowid="+rowid+"&amount="+result,

            success: function(result){
               // alert(result);
               var res = result.replace(".",",");  
                $(".bsket").html(res);
            }
            });

         });
}
</script>