<table cellpadding="10" cellspacing="0" width="100%" align="center" class="listing-checkout">
                <thead><tr>
                    <td><strong>Date</strong></td>
                    <td><strong>Order number</strong></td>
                    <td><strong> Products you've ordere order</strong></td>
                    <td><strong> Total price</strong></td>
                    <td><strong> Store</strong></td>
                    <td><strong>Order status</strong></td>
                    <td></td>
                </tr>  </thead>                  
                <tbody>
                   
                <?php if(!empty($orders)){ foreach ($orders as $key => $value) { ?>          
                  <tr>                    
                    <td><?php $ddate = trim(str_replace("PST", '', $value->date)); echo date("d.m.Y", strtotime($ddate)); ?></td>
                    <td><?php echo $value->id; $cart = json_decode($value->cart); ?></td>
                   <td> <?php $i = 0; foreach ($cart as $key => $values) {
                     if($i==2) break;
                    ?>
                   
                    <strong class="orange-text">{<?php echo $values->qty; if(($values->type) == 'weight'){echo 'kg';}else{echo 'St.';}?>}</strong> <?php echo $values->name; if($i==0){echo ',';} if($i==1){echo ' ...';}?>
                    <?php $i++; }?></td>
                    <td>&euro; <?php echo $value->total_payment; ?></td>
                    <td><?php echo $value->restaurant_name_ar;?></td>
                    <td>
                        <?php if(($value->status)== '2'){?>
                        <div class="pending">In Process</div>
                        <?php }?>
                         <?php if(($value->status)== '3'){?>
                        <div class="approveed">Approved</div>
                        <?php }?>
                         <?php if(($value->status)== '0'){?>
                        <div class="delivered">Delivered</div>
                        <?php }?>
                         <?php if(($value->status)== '4'){?>
                        <div class="pendingapprovalgg">Pending for Approval</div>
                        <?php }?>
                         <?php if(($value->status)== '5'){?>
                        <div class="canceled">Canceled</div>
                        <?php }?>
                          <?php if(($value->status)== '7'){?>
                        <div class="pendingapproval">Out of Delivery</div>
                        <?php }?>
                    </td>
                    <td><a href="#" onclick="myorderdetail(<?php echo $value->id;?>)" class="order-details" id="order-details<?php echo $value->id;?>">Order details&raquo;</a></td>
                  </tr> 
                <?php } }?> 
            </tbody>            
              </table>
              <div class="tabs-pagination">
                    <?php echo $this->ajax_pagination->create_links(); ?>
                    <br clear="all">
                </div>