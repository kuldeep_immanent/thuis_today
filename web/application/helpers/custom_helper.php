<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('gethighlowvat')){
/*******************************
        *
        * Function is used for to show the cart vat 
        *
        **********/
         function gethighlowvat($vat)
          {            
             $array = array();  
             $original = json_decode($vat);            
              foreach ($original as  $value) 
              {     
                $data = $value->originalprice*$value->qty;                   
                $array[$value->vat]+=$data;// array('price'=>$value->originalprice, 'vat'=> $value->vat);             
              }            
               return $array;
          }
}
